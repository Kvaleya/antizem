﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Glob;

namespace RenderdocBugTest
{
	public enum OutputType
	{
		LogOnly,
		Debug,
		Notify,
		Warning,
		MinorWarning,
		PerformanceWarning,
		Error,
	}

	public delegate void PrintEventHandler(PrintEventArgs e);

	/// <summary>
	/// Simple thread-safe text output. Auto-logs any printed text and invokes an event when a text is printed
	/// </summary>
	public class TextOutput : ITextOutputGlob
	{
		int _oldLogBackupCount = 8;
		const string LogFileExt = ".txt";

		public event PrintEventHandler PrintEvent;

		Thread _outputThread;
		ConcurrentQueue<PrintEventArgs> _messages;
		StreamWriter _logStreamWriter;

		public TextOutput(string logFile, int oldLogBackupCount)
		{
			_oldLogBackupCount = oldLogBackupCount;
			_messages = new ConcurrentQueue<PrintEventArgs>();

			try
			{
				// Keep N backups of the previous log files
				int doesntExist = 0;
				for(; doesntExist < _oldLogBackupCount; doesntExist++)
				{
					string file = logFile + doesntExist.ToString() + LogFileExt;
					if(!File.Exists(file))
						break;
				}

				// If all log files exist, delete the last one and start the renaming with the second to last one
				if(doesntExist == _oldLogBackupCount)
				{
					doesntExist--;
					File.Delete(logFile + doesntExist.ToString() + LogFileExt);
				}

				for(int j = doesntExist; j > 0; j--)
				{
					File.Move(logFile + (j - 1).ToString() + LogFileExt, logFile + j.ToString() + LogFileExt);
				}

				_logStreamWriter = new StreamWriter(logFile + "0" + LogFileExt);
			}
			catch(IOException e)
			{
				_logStreamWriter = null;
			}

			_outputThread = new Thread((() =>
			{
				Loop(this);
			}));
			_outputThread.Start();
		}

		static string FormatMessage(PrintEventArgs message)
		{
			string type = "LOG";

			switch(message.Type)
			{
				case OutputType.Debug:
				{
					type = "DEBUG";
					break;
				}
				case OutputType.Notify:
				{
					type = "NOTIFY";
					break;
				}
				case OutputType.Warning:
				{
					type = "WARNING";
					break;
				}
				case OutputType.MinorWarning:
					{
						type = "MINOR_WARNING";
						break;
					}
				case OutputType.PerformanceWarning:
				{
					type = "PERFORMANCE_WARNING";
					break;
				}
				case OutputType.Error:
				{
					type = "ERROR";
					break;
				}
			}

			return type + ": " + message.Message;
		}

		static void Loop(TextOutput text)
		{
			while(true)
			{
				PrintEventArgs message;
				while(text._messages.TryDequeue(out message))
				{
					if(text._logStreamWriter != null)
						text._logStreamWriter.WriteLine(FormatMessage(message));
					if(text.PrintEvent != null)
						text.PrintEvent(message);
				}
				if(text._logStreamWriter != null)
					text._logStreamWriter.Flush();
				Thread.Sleep(200);
			}
		}

		public static void OnPrintConsole(PrintEventArgs e)
		{
			ConsoleColor color = Console.ForegroundColor;

			if(e.Type == OutputType.LogOnly)
				return;
			if(e.Type == OutputType.Debug)
				Console.ForegroundColor = ConsoleColor.DarkGray;
			if(e.Type == OutputType.PerformanceWarning)
				Console.ForegroundColor = ConsoleColor.DarkYellow;
			if(e.Type == OutputType.Warning)
				Console.ForegroundColor = ConsoleColor.DarkRed;
			if(e.Type == OutputType.Warning)
				Console.ForegroundColor = ConsoleColor.Yellow;
			if(e.Type == OutputType.Error)
				Console.ForegroundColor = ConsoleColor.Red;

			Console.WriteLine(e.Message);

			Console.ForegroundColor = color;
		}

		public static TextOutput CreateCommon(string logFileName = "log", int logFileBackupCount = 8)
		{
			TextOutput output = new TextOutput(logFileName, logFileBackupCount);
			output.PrintEvent += OnPrintConsole;

			output.Print(OutputType.Debug, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

			return output;
		}

		public void Print(OutputType type, string message)
		{
			_messages.Enqueue(new PrintEventArgs(type, message));
		}

		public void PrintException(OutputType type, string message, Exception e)
		{
			Print(type, message + " (" + e.GetType().ToString() + ")");
			Print(OutputType.Debug, e.ToString());
		}

		public void Print(OutputTypeGlob type, string message)
		{
			OutputType ot = OutputType.Notify;

			if(type == OutputTypeGlob.LogOnly)
				ot = OutputType.LogOnly;
			if(type == OutputTypeGlob.Debug)
				ot = OutputType.Debug;
			if(type == OutputTypeGlob.Notify)
				ot = OutputType.Notify;
			if(type == OutputTypeGlob.Warning)
				ot = OutputType.Warning;
			if(type == OutputTypeGlob.PerformanceWarning)
				ot = OutputType.PerformanceWarning;
			if(type == OutputTypeGlob.Error)
				ot = OutputType.Error;

			Print(ot, message);
		}
	}

	public class PrintEventArgs : EventArgs
	{
		readonly OutputType _type;
		readonly string _message;

		public OutputType Type { get { return _type; } }
		public string Message { get { return _message; } }

		internal PrintEventArgs(OutputType type, string message)
		{
			_type = type;
			_message = message;
		}
	}
}
