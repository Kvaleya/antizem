﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace RenderdocBugTest
{
	class Program
	{
		static void Main(string[] args)
		{
			Window w = new Window(1280, 720);
			w.Run();
		}

		class Window : GameWindow
		{
			Device _device;
			TextOutput _textOutput;
			FileManager _fileManager;

			Texture2D _texOut;
			Texture2D _texIn;
			GraphicsPipeline _psoDisplayTexture;
			ComputePipeline _psoSimpleComp;

			Stopwatch _sw;
			int _w, _h;

			int _shaderCompute;

			public Window(int width, int height) : base(width, height)
			{
				_w = width;
				_h = height;
				_textOutput = TextOutput.CreateCommon();
				_fileManager = new FileManager(_textOutput, "", "src/Shaders", "src/Shaders/Resolved", "src/Shaders/Compiled");
				_device = new Device(_textOutput, _fileManager);

				_texIn = new Texture2D(_device, "Test texture IN", SizedInternalFormatGlob.R8, 4, 4, 1);
				_texIn.TexSubImage2D(_device, 0, 0, 0, _texIn.Width, _texIn.Height, PixelFormat.Red, PixelType.UnsignedByte, new byte[]
				{
					45, 17, 254, 155,
					178, 164, 46, 47,
					45, 185, 42, 178,
					87, 166, 221, 24,
				});
				_texOut = new Texture2D(_device, "Test texture OUT", SizedInternalFormatGlob.RGBA8, width, height, 1);

				_psoDisplayTexture = new GraphicsPipeline(_device, _device.GetShader("fullscreenSimple.vert"), _device.GetShader("simpleTexture.frag"), null, new RasterizerState(), new DepthState());
				_psoSimpleComp = new ComputePipeline(_device, _device.GetShader("simpleComp.comp"));

				_shaderCompute = MakeShader();

				_sw = new Stopwatch();
				_sw.Start();
			}

			protected override void OnRenderFrame(FrameEventArgs e)
			{
				base.OnRenderFrame(e);

				float ellapsed = _sw.ElapsedMilliseconds / 1000f;
				float val = (float)Math.Sin(ellapsed) * 0.5f + 0.5f;

				//RunComputeExperimental(val);
				RunComputeGlob(val);

				GL.MemoryBarrier(MemoryBarrierFlags.TextureFetchBarrierBit);

				_device.BindPipeline(_psoDisplayTexture);
				GL.ActiveTexture((TextureUnit)((int)TextureUnit.Texture0 + 0));
				GL.BindTexture(_texOut.Target, _texOut.Handle);
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

				_device.Invalidate();
				_device.Update();

				SwapBuffers();
			}

			void RunComputeExperimental(float val)
			{
				GL.BindProgramPipeline(0);
				GL.UseProgram(_shaderCompute);
				GL.Uniform1(0, val);
				GL.Uniform2(1, new Vector2(1f / _texOut.Width, 1f / _texOut.Height));

				GL.BindImageTexture(0, _texOut.Handle, 0, false, 0, TextureAccess.WriteOnly, (SizedInternalFormat)_texOut.Format);
				_device.BindImage2D(0, _texOut, TextureAccess.WriteOnly, 0, 0);

				GL.ActiveTexture((TextureUnit)((int)TextureUnit.Texture0 + 1));
				GL.BindTexture(_texIn.Target, _texIn.Handle);
				_device.BindTexture(_texIn, 1);

				GL.DispatchCompute((_texOut.Width + 7) / 8, (_texOut.Height + 7) / 8, 1);

				GL.UseProgram(0);
			}

			void RunComputeGlob(float val)
			{
				_device.BindPipeline(_psoSimpleComp);
				_device.ShaderCompute.SetUniformF("screensizeRcp", new Vector2(1f / _texOut.Width, 1f / _texOut.Height));
				_device.ShaderCompute.SetUniformF("val", val);
				GL.BindImageTexture(0, _texOut.Handle, 0, false, 0, TextureAccess.WriteOnly, (SizedInternalFormat)_texOut.Format);
				_device.BindImage2D(0, _texOut, TextureAccess.WriteOnly, 0, 0);

				GL.ActiveTexture((TextureUnit)((int)TextureUnit.Texture0 + 1));
				GL.BindTexture(_texIn.Target, _texIn.Handle);
				_device.BindTexture(_texIn, 1);

				GL.DispatchCompute((_texOut.Width + 7) / 8, (_texOut.Height + 7) / 8, 1);
			}

			int MakeShader()
			{
				string src;

				using(StreamReader sr = new StreamReader("src/Shaders/simpleComp.comp"))
				{
					src = sr.ReadToEnd();
				}

				int shader = GL.CreateShader(ShaderType.ComputeShader);
				GL.ShaderSource(shader, 1, new string[] {src}, new int[] {src.Length});
				GL.CompileShader(shader);

				int program = GL.CreateProgram();
				GL.AttachShader(program, shader);
				GL.LinkProgram(program);

				GL.DetachShader(program, shader);
				GL.DeleteShader(shader);

				return program;
			}
		}
	}
}
