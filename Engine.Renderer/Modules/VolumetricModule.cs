﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class VolumetricModule : RenderModule
	{
		[InjectProperty]
		public TestSceneData SceneData { get; set; }

		public Texture2D TexVolumetrics { get { return _texVolFiltered; } }
		public Texture2D TexCloudShadows { get { return _texShadow; } }

		// RGB: in-scattering, A: darken (multiply old color by A). RGB is pre-exposed
		Texture2D _texVolRenderRaw;
		Texture2D _texVolFiltered;
		Texture2D _texVolFilteredHistory;

		Texture2D _texRandom;
		const int TexRandomSize = 1024;

		//public Texture3D TexNoise3DView;
		Texture3D _texNoise3D;
		Texture2D _texNoise2D;
		Texture2D _texCoverage;
		//public Texture2D TexNoiseHighClouds;
		Texture1D _texHeightLut;

		Texture2D _texShadow;
		Texture2D _texShadowFilterBuffer;
		Texture2D _texShadowRaw;

		public Matrix4 ShadowMatrix { get; private set; }

		ComputePipeline _psoRender;
		ComputePipeline _psoFilterTemporal;

		ComputePipeline _psoRenderShadow;
		ComputePipeline _psoDownsampleShadow;
		ComputePipeline _psoFilterShadowInit;
		ComputePipeline _psoFilterShadow;

		ComputePipeline _psoApply;

		ComputePipeline _psoGenNoise3D;
		ComputePipeline _psoGenNoise3DMips;
		ComputePipeline _psoGenNoise2D;
		ComputePipeline _psoGenCoverage;
		ComputePipeline _psoGenHeightlut;
		//ComputePipeline _psoGenNoiseHighClouds;

		public float ShadowMaxDist = 1 << 16;
		public float ShadowRange = 4096 * 2;

		public float ShadowEsmConstant = 80;

		public float EsmConstPublic { get { return ShadowEsmConstant * ShadowMaxDist; } }

		const int ScaleCoverage = 65536;
		const int ScaleNoise3D = 2048;

		const float CloudMaxHeight = 4096f;

		bool _updateNoise = true;

		public VolumetricModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			var cloudsDefines = new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("e_scale_coverage", ScaleCoverage.ToString()),
				new Tuple<string, string>("e_scale_noise3d", ScaleNoise3D.ToString()),
			};

			_psoRender = new ComputePipeline(Device, Device.GetShader("clouds.comp", cloudsDefines));
			_psoFilterTemporal = new ComputePipeline(Device, Device.GetShader("clouds_temporal.comp"));
			_psoApply = new ComputePipeline(Device, Device.GetShader("clouds_apply.comp"));
			_psoDownsampleShadow = new ComputePipeline(Device, Device.GetShader("clouds_shadow_downsample.comp"));
			_psoRenderShadow = new ComputePipeline(Device, Device.GetShader("clouds_shadow.comp", cloudsDefines));
			_psoFilterShadowInit = new ComputePipeline(Device, Device.GetShader("clouds_shadow_esm_filter.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("INIT", "")
			}));
			_psoFilterShadow = new ComputePipeline(Device, Device.GetShader("clouds_shadow_esm_filter.comp"));

			_texShadowRaw = new Texture2D(Device, "CloudShadowRaw", SizedInternalFormatGlob.R32F, 512, 512);
			_texShadowFilterBuffer = new Texture2D(Device, "CloudShadowFilterBuffer", _texShadowRaw.Format, _texShadowRaw.Width / 2, _texShadowRaw.Height / 2);
			_texShadow = new Texture2D(Device, "CloudShadow", _texShadowRaw.Format, _texShadowRaw.Width / 2, _texShadowRaw.Height / 2);

			// Noise textures
			_texNoise3D = new Texture3D(Device, "CloudNoise3D", SizedInternalFormatGlob.RG8, 128, 128, 128, 7);
			Glob.Utils.SetTextureParameters(Device, _texNoise3D, TextureWrapMode.Repeat, TextureMagFilter.Linear, TextureMinFilter.LinearMipmapLinear);
			_texNoise2D = new Texture2D(Device, "CloudNoise2D", SizedInternalFormatGlob.RGBA8, 256, 256, 1);
			_texCoverage = new Texture2D(Device, "CloudCoverage", SizedInternalFormatGlob.RGBA8, 512, 512, 1);
			//TexNoise3DView = new Texture3D(Device, "CloudNoise3DView", SizedInternalFormatGlob.R8, TexNoise3D, 1, TexNoise3D.Levels - 1, 0, 1);
			_texHeightLut = new Texture1D(Device, "CloudHeightLut", SizedInternalFormatGlob.RG8, 128, 1);

			_psoGenNoise3DMips = new ComputePipeline(Device, Device.GetShader("cloud_noise_3d_mips.comp"));
			_psoGenNoise3D = new ComputePipeline(Device, Device.GetShader("cloud_noise_3d.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("TEX3DSIZE", _texNoise3D.Width.ToString())
			}));
			_psoGenNoise2D = new ComputePipeline(Device, Device.GetShader("cloud_noise_2d.comp"));
			_psoGenCoverage = new ComputePipeline(Device, Device.GetShader("cloud_gen_coverage.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("TEX2DSIZE", _texCoverage.Width.ToString())
			}));
			_psoGenHeightlut = new ComputePipeline(Device, Device.GetShader("cloud_gen_height.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("TEX1DSIZE", _texHeightLut.Width.ToString())
			}));

			_psoGenNoise3D.ShaderCompute.AddOnUpdated(() =>
			{
				_updateNoise = true;
			});
			_psoGenNoise2D.ShaderCompute.AddOnUpdated(() =>
			{
				_updateNoise = true;
			});
			_psoGenCoverage.ShaderCompute.AddOnUpdated(() =>
			{
				_updateNoise = true;
			});
			_psoGenHeightlut.ShaderCompute.AddOnUpdated(() =>
			{
				_updateNoise = true;
			});

			//TexNoiseHighClouds = new Texture2D(Device, "CloudNoise2DHighClouds", SizedInternalFormatGlob.RG8, 512, 512, 1);

			// Generate completely random texture for use in noise generation
			Random random = new Random(454);

			byte[] texrndBytes = new byte[TexRandomSize * TexRandomSize * 4];
			random.NextBytes(texrndBytes);

			unsafe
			{
				fixed (void* bytes = texrndBytes)
				{
					GL.BindBuffer(BufferTarget.PixelUnpackBuffer, 0);
					GL.BindBuffer(BufferTarget.PixelPackBuffer, 0);
					_texRandom = new Texture2D(Device, "RandomnessFromCpu", SizedInternalFormatGlob.RGBA8, TexRandomSize, TexRandomSize, 1);
					_texRandom.TexSubImage2D(Device, 0, 0, 0, TexRandomSize, TexRandomSize, PixelFormat.Bgra, PixelType.UnsignedByte, new IntPtr(bytes));
				}
			}
		}

		Matrix4 GetCloudShadowMatrix(Vector3d cameraDelta)
		{
			Vector3 forward = -SceneData.SunDir;
			Vector3 right = Vector3.Cross(forward, Vector3.UnitY).Normalized();
			Vector3 up = Vector3.Cross(forward, -right).Normalized();
			Matrix4 m = Matrix4.Identity;
			m.Column0 = new Vector4(right * ShadowRange, 0);
			m.Column1 = new Vector4(up * ShadowRange, 0);
			m.Column2 = new Vector4(forward * ShadowMaxDist * 0.5f, 0);
			m.Transpose();

			double align = (double)ShadowRange / _texShadow.Width * 8;

			// Compute texel-aligned position offset to make the shadowmap more stable
			Vector3d pos = RenderContext.CameraPosition - cameraDelta;
			pos = Engine.Common.Utils.Math.GridAlign(pos, (Vector3d)forward, (double)ShadowMaxDist / 256);
			pos = Engine.Common.Utils.Math.GridAlign(pos, (Vector3d)right, align);
			pos = Engine.Common.Utils.Math.GridAlign(pos, (Vector3d)up, align);
			Vector3 offset = (Vector3)(pos - RenderContext.CameraPosition);
			Matrix4 translate = Matrix4.CreateTranslation(offset);
			//translate.Transpose();
			return m * translate;
		}

		public override void OnResize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{
			_texVolRenderRaw?.Dispose();
			_texVolFiltered?.Dispose();
			_texVolFilteredHistory?.Dispose();

			_texVolRenderRaw = new Texture2D(Device, "VolumetricMapRaw", SizedInternalFormatGlob.RGBA16F, renderWidth / 2, renderHeight / 2, 1);
			_texVolFiltered = new Texture2D(Device, "VolumetricMapFiltered 1", SizedInternalFormatGlob.RGBA16F, _texVolRenderRaw.Width, _texVolRenderRaw.Height, 1);
			_texVolFilteredHistory = new Texture2D(Device, "VolumetricMapFiltered 2", _texVolFiltered.Format, _texVolFiltered.Width, _texVolFiltered.Height, 1);

			Glob.Utils.SetTextureParameters(Device, _texVolRenderRaw, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			Glob.Utils.SetTextureParameters(Device, _texVolFiltered, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			Glob.Utils.SetTextureParameters(Device, _texVolFilteredHistory, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
		}

		public void GenerateNoise()
		{
			using(Device.DebugMessageManager.PushGroupMarker("Volumetrics pre-generate"))
			{
				Device.BindPipeline(_psoGenNoise3D);
				Device.BindTexture(_texRandom, 0);
				Device.BindImage3D(0, _texNoise3D, TextureAccess.WriteOnly);
				Device.ShaderCompute.SetUniformF("sizercp", new Vector3(1f / _texNoise3D.Width));
				Device.ShaderCompute.SetUniformI("rndsize", _texRandom.Width, _texRandom.Height);

				for(int i = 0; i < _texNoise3D.Depth; i++)
				{
					Device.ShaderCompute.SetUniformI("offsetZ", i);
					Device.DispatchComputeThreads(_texNoise3D.Width, _texNoise3D.Height, 1);
				}

				Device.BindPipeline(_psoGenNoise2D);
				Device.BindTexture(_texRandom, 0);
				Device.BindImage2D(0, _texNoise2D, TextureAccess.WriteOnly);
				Device.ShaderCompute.SetUniformF("sizercp", new Vector2(1f / _texNoise2D.Width));
				Device.ShaderCompute.SetUniformI("rndsize", _texRandom.Width, _texRandom.Height);
				Device.ShaderCompute.SetUniformI("offsetXY", 0, 0);
				Device.DispatchComputeThreads(_texNoise2D.Width, _texNoise2D.Height);

				Device.BindPipeline(_psoGenCoverage);
				Device.BindTexture(_texRandom, 0);
				Device.BindImage2D(0, _texCoverage, TextureAccess.WriteOnly);
				Device.ShaderCompute.SetUniformF("sizercp", new Vector2(1f / _texCoverage.Width));
				Device.ShaderCompute.SetUniformI("rndsize", _texRandom.Width, _texRandom.Height);
				Device.DispatchComputeThreads(_texCoverage.Width, _texCoverage.Height);

				Device.BindPipeline(_psoGenHeightlut);
				Device.BindTexture(_texRandom, 0);
				Device.BindImage2D(0, _texHeightLut, TextureAccess.WriteOnly);
				Device.ShaderCompute.SetUniformF("sizercp", 1f / _texHeightLut.Width);
				Device.ShaderCompute.SetUniformI("rndsize", _texRandom.Width, _texRandom.Height);
				Device.ShaderCompute.SetUniformF("cloudMaxHeight", CloudMaxHeight);
				Device.DispatchComputeThreads(_texHeightLut.Width);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				Device.BindPipeline(_psoGenNoise3DMips);
				Device.BindImage3D(0, _texNoise3D, TextureAccess.ReadOnly, 0);
				Device.BindImage3D(1, _texNoise3D, TextureAccess.WriteOnly, 1);
				Device.BindImage3D(2, _texNoise3D, TextureAccess.WriteOnly, 2);
				Device.BindImage3D(3, _texNoise3D, TextureAccess.WriteOnly, 3);

				Device.DispatchComputeThreads(_texNoise3D.Width >> 1, _texNoise3D.Height >> 1, _texNoise3D.Depth >> 1);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				Device.BindImage3D(0, _texNoise3D, TextureAccess.ReadOnly, 3);
				Device.BindImage3D(1, _texNoise3D, TextureAccess.WriteOnly, 4);
				Device.BindImage3D(2, _texNoise3D, TextureAccess.WriteOnly, 5);
				Device.BindImage3D(3, _texNoise3D, TextureAccess.WriteOnly, 6);

				Device.DispatchComputeThreads(_texNoise3D.Width >> 4, _texNoise3D.Height >> 4, _texNoise3D.Depth >> 4);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
			}
		}

		Matrix4 _lastCloudShadowMatrix = Matrix4.Identity;
		double _lastCloudUpdate = 0;
		Vector3d _lastShadowCameraPos = Vector3d.Zero;
		Vector3 _lastSunDir = Vector3.Zero;

		void RenderCloudShadows(Vector2 posCoverage, Vector3 posNoise3D, float posY)
		{
			ShadowMatrix = GetCloudShadowMatrix(RenderContext.CameraPosition - _lastShadowCameraPos);

			bool skip = true;

			if(_lastSunDir != SceneData.SunDir)
				skip = false;

			if((RenderContext.CameraPosition - _lastShadowCameraPos).Length > 1000)
				skip = false;

			if(SceneData.Ellapsed - _lastCloudUpdate > 1)
				skip = false;

			if(skip)
				return;

			// TODO: use separate timers for scene animation and for rendering logic
			_lastCloudUpdate = SceneData.Ellapsed;
			_lastShadowCameraPos = RenderContext.CameraPosition;
			_lastSunDir = SceneData.SunDir;
			ShadowMatrix = GetCloudShadowMatrix(Vector3d.Zero);

			// Render shadows
			var queryShadows = new TimerQueryGPU(RenderContext.FeedbackManager, "CloudShadows");
			queryShadows.StartQuery();

			Device.BindPipeline(_psoRenderShadow);
			Device.BindTexture(_texNoise3D, 1);
			Device.BindTexture(_texNoise2D, 2);
			Device.BindTexture(_texCoverage, 3);
			Device.BindTexture(_texHeightLut, 4);
			Device.BindImage2D(0, _texShadowRaw, TextureAccess.WriteOnly);

			Device.ShaderCompute.SetUniformF("ellapsed", (float)SceneData.Ellapsed);
			Device.ShaderCompute.SetUniformF("resolutionRcp", new Vector2(1f / _texShadowRaw.Width, 1f / _texShadowRaw.Height));
			Device.ShaderCompute.SetUniformF("cameraHeight", (Vector3)RenderContext.CameraPosition);
			Device.ShaderCompute.SetUniformF("sunlightdir", SceneData.SunDir);

			Device.ShaderCompute.SetUniformF("cloudShadowRange", ShadowRange);
			Device.ShaderCompute.SetUniformF("cloudShadowMaxDist", ShadowMaxDist);
			Device.ShaderCompute.SetUniformF("shadowMatrix", ShadowMatrix);

			Device.ShaderCompute.SetUniformI("cloud_coverage_size", _texCoverage.Width, _texCoverage.Height);

			Device.ShaderCompute.SetUniformF("cloud_camera_pos_coverage", posCoverage);
			Device.ShaderCompute.SetUniformF("cloud_camera_pos_noise3D", posNoise3D);
			Device.ShaderCompute.SetUniformF("cloud_camera_pos_y", posY);

			//Device.ShaderCompute.SetUniformI("framenum64", (int)(RenderContext.FrameNumber & 0x3f));

			Device.ShaderCompute.SetUniformF("esm_c", ShadowEsmConstant);

			const int shadowComputeTileX = 8;
			const int shadowComputeTileY = 8;

			Device.DispatchComputeGroups((_texShadowRaw.Width + shadowComputeTileX - 1) / shadowComputeTileX,
				(_texShadowRaw.Height + shadowComputeTileY - 1) / shadowComputeTileY);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			// Downsample shadows
			Device.BindPipeline(_psoDownsampleShadow);
			Device.BindImage2D(0, _texShadow, TextureAccess.WriteOnly);
			Device.BindImage2D(1, _texShadowRaw, TextureAccess.ReadOnly);
			Device.DispatchComputeThreads(_texShadow.Width, _texShadow.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.TextureFetchBarrierBit);

			// Filter shadows

			Device.BindPipeline(_psoFilterShadowInit);
			Device.BindTexture(_texShadow, 0);
			Device.BindImage2D(0, _texShadowFilterBuffer, TextureAccess.WriteOnly);

			Device.ShaderCompute.SetUniformF("esm_c", ShadowEsmConstant);
			Device.ShaderCompute.SetUniformI("smSize", _texShadow.Width, _texShadow.Height);

			Device.DispatchComputeThreads(_texShadow.Width, _texShadow.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.TextureFetchBarrierBit);

			Device.BindPipeline(_psoFilterShadow);
			Device.BindTexture(_texShadowFilterBuffer, 0);
			Device.BindImage2D(0, _texShadow, TextureAccess.WriteOnly);

			Device.ShaderCompute.SetUniformF("esm_c", ShadowEsmConstant);
			Device.ShaderCompute.SetUniformI("smSize", _texShadow.Width, _texShadow.Height);

			Device.DispatchComputeThreads(_texShadow.Width, _texShadow.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.TextureFetchBarrierBit);

			queryShadows.EndQuery();
		}

		public void Render(GbufferModule gbuffer, EnvmapModule envmap, AtmosphereModule atmo)
		{
			using(Device.DebugMessageManager.PushGroupMarker("Volumetrics"))
			{
				// Compute position parameters
				// TODO: fix for spherical world
				var cam = SceneData.Camera.CameraCurrent.Position;
				Vector2 posCoverage = (Vector2) new Vector2d(cam.X % ScaleCoverage, cam.Z % ScaleCoverage);
				Vector2 posNoise3D2d = (Vector2) new Vector2d(cam.X % ScaleNoise3D, cam.Z % ScaleNoise3D);

				// Should be height above surface for spherical world
				float posY = (float) RenderContext.CameraPosition.Y;

				Vector3 posNoise3D = new Vector3(posNoise3D2d.X, 0.0f, posNoise3D2d.Y);

				posCoverage /= ScaleCoverage;
				posNoise3D /= ScaleNoise3D;

				posCoverage += new Vector2(0.5f);

				if(_updateNoise)
				{
					GenerateNoise();
					_updateNoise = false;
				}

				// Cloud shadows
				RenderCloudShadows(posCoverage, posNoise3D, posY);

				// Render clouds

				var queryRaytrace = new TimerQueryGPU(RenderContext.FeedbackManager, "VolumetricsRaytrace");
				queryRaytrace.StartQuery();

				Device.BindPipeline(_psoRender);
				Device.BindTexture(gbuffer.TextureLinearDepth, 0);
				Device.BindTexture(_texNoise3D, 1);
				Device.BindTexture(_texNoise2D, 2);
				Device.BindTexture(_texCoverage, 3);
				Device.BindTexture(envmap.TextureEnvmap, 4);
				Device.BindTexture(_texShadow, 5);
				Device.BindTexture(_texHeightLut, 6);
				Device.BindTexture(RenderContext.BlueNoise, 7);
				Device.BindImage2D(0, _texVolRenderRaw, TextureAccess.WriteOnly);

				Device.ShaderCompute.SetUniformF("exposure", SceneData.Exposure);
				Device.ShaderCompute.SetUniformF("ellapsed", (float) SceneData.Ellapsed);

				Device.ShaderCompute.SetUniformF("resolutionRcp", new Vector2(1f / _texVolRenderRaw.Width, 1f / _texVolRenderRaw.Height));

				Device.ShaderCompute.SetUniformF("ray00", RenderContext.CameraRay00);
				Device.ShaderCompute.SetUniformF("ray01", RenderContext.CameraRay01);
				Device.ShaderCompute.SetUniformF("ray10", RenderContext.CameraRay10);
				Device.ShaderCompute.SetUniformF("ray11", RenderContext.CameraRay11);
				Device.ShaderCompute.SetUniformF("cameraPosDebug", (Vector3) RenderContext.CameraPosition);

				Device.ShaderCompute.SetUniformF("diffuseCubeLod", EnvmapModule.TexCubeMaxLevel);

				Device.ShaderCompute.SetUniformF("sunlightdir", SceneData.SunDir);
				Device.ShaderCompute.SetUniformF("sunlightCloud", SceneData.SunLight2km);
				Device.ShaderCompute.SetUniformF("sunlightFog", SceneData.SunLight);

				Device.ShaderCompute.SetUniformF("cloudShadowMatrix", ShadowMatrix.Inverted());
				Device.ShaderCompute.SetUniformF("cloudShadowEsmConst", ShadowEsmConstant);

				Device.ShaderCompute.SetUniformI("cloud_coverage_size", _texCoverage.Width, _texCoverage.Height);

				Device.ShaderCompute.SetUniformF("cloud_camera_pos_coverage", posCoverage);
				Device.ShaderCompute.SetUniformF("cloud_camera_pos_noise3D", posNoise3D);
				Device.ShaderCompute.SetUniformF("cloud_camera_pos_y", posY);
				Device.ShaderCompute.SetUniformF("cloud_camera_shadow_delta", (Vector3)(_lastShadowCameraPos - RenderContext.CameraPosition));

				Device.ShaderCompute.SetUniformI("frameNum", (int) (RenderContext.FrameNumber & 0xffff));

				Device.DispatchComputeThreads(_texVolRenderRaw.Width / 2, _texVolRenderRaw.Height / 2);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				queryRaytrace.EndQuery();



				// Filter clouds

				// Swap
				var temp = _texVolFiltered;
				_texVolFiltered = _texVolFilteredHistory;
				_texVolFilteredHistory = temp;

				var queryFilter = new TimerQueryGPU(RenderContext.FeedbackManager, "VolumetricsFilter");
				queryFilter.StartQuery();

				Vector3 positionHistory = (Vector3)(RenderContext.CameraPositionHistory - RenderContext.CameraPosition);

				var plane = new Vector4(RenderContext.CameraMatrixHistory.Column2.Xyz,
					Vector3.Dot(RenderContext.CameraMatrixHistory.Column2.Xyz, positionHistory));

				var history = RenderContext.Camera.CameraHistory.ToCameraMatrix(positionHistory) *
							  RenderContext.ProjectionMatrixNoReverseDepthHistory;

				Device.BindPipeline(_psoFilterTemporal);
				Device.ShaderCompute.SetUniformF("filteredResolutionRcp", new Vector2(1f / _texVolFiltered.Width, 1f / _texVolFiltered.Height));
				Device.ShaderCompute.SetUniformF("matPrevious", history);
				Device.ShaderCompute.SetUniformF("cameraPlanePrevious", plane);

				Device.ShaderCompute.SetUniformF("cameraRay00", RenderContext.CameraRay00.Xyz);
				Device.ShaderCompute.SetUniformF("cameraRay01", RenderContext.CameraRay01.Xyz);
				Device.ShaderCompute.SetUniformF("cameraRay10", RenderContext.CameraRay10.Xyz);
				Device.ShaderCompute.SetUniformF("cameraRay11", RenderContext.CameraRay11.Xyz);

				Device.ShaderCompute.SetUniformI("frameNum", (int)(RenderContext.FrameNumber & 0xffff));
				Device.ShaderCompute.SetUniformI("rawTextureSize", _texVolRenderRaw.Width, _texVolRenderRaw.Height);
				Device.ShaderCompute.SetUniformI("depthTextureSize", gbuffer.TextureLinearDepth.Width, gbuffer.TextureLinearDepth.Height);

				Device.BindImage2D(0, _texVolFiltered, TextureAccess.WriteOnly);
				Device.BindTexture(gbuffer.TextureLinearDepth, 0);
				Device.BindTexture(_texVolRenderRaw, 1);
				Device.BindTexture(_texVolFilteredHistory, 2);
				Device.BindTexture(gbuffer.TextureLinearDepthHistory, 3);

				Device.DispatchComputeThreads(_texVolFiltered.Width, _texVolFiltered.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				queryFilter.EndQuery();



				// Apply clouds

				Device.BindPipeline(_psoApply);
				Device.BindImage2D(0, gbuffer.TextureSceneColorAlternative, TextureAccess.WriteOnly);
				Device.BindImage2D(1, gbuffer.TextureSceneColor, TextureAccess.ReadOnly);
				Device.BindTexture(_texVolFiltered, 0);
				Device.BindTexture(gbuffer.TextureLinearDepth, 2);

				Device.ShaderCompute.SetUniformF("resolutionRcp",
					new Vector2(1f / gbuffer.TextureSceneColor.Width, 1f / gbuffer.TextureSceneColor.Height));

				Device.DispatchComputeThreads(gbuffer.TextureSceneColor.Width, gbuffer.TextureSceneColor.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				gbuffer.SwapSceneColorBuffers();
			}
		}
	}
}
