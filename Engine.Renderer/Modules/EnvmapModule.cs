﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Renderer.Aurora;
using Engine.Renderer.Meshes;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class EnvmapModule : RenderModule
	{
		public const int BrdfLutSizeX = 512;
		public const int BrdfLutSizeY = BrdfLutSizeX;
		public const int TexSizeCube = 128;
		public const int TexCubeMaxLevel = 4;

		ComputePipeline _psoCubeMipGen, _psoConvoluteDiffuse, _psoConvoluteSpecular, _psoBrdfPrecompute;

		public Texture2D BrdfLut { get; private set; }
		public TextureCube TextureEnvmap { get; private set; }
		TextureCube _texCubePrepare;

		public EnvmapModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			BrdfLut = new Texture2D(Device, "BrdfLut", SizedInternalFormatGlob.RG16F, BrdfLutSizeX, BrdfLutSizeY, 1);

			Glob.Utils.SetTextureParameters(Device, BrdfLut, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);

			_psoBrdfPrecompute = new ComputePipeline(Device, Device.GetShader("brdfPrecompute.comp"));
			_psoCubeMipGen = new ComputePipeline(Device, Device.GetShader("cubemapMipGen.comp"));
			_psoConvoluteDiffuse = new ComputePipeline(Device, Device.GetShader("cubemapConvoluteDiffuse.comp"));
			_psoConvoluteSpecular = new ComputePipeline(Device, Device.GetShader("cubemapConvoluteSpecular.comp"));

			_texCubePrepare = new TextureCube(Device, "EnvmapPrepare", SizedInternalFormatGlob.RGBA16F, TexSizeCube, TexSizeCube, 0);
			TextureEnvmap = new TextureCube(Device, "Envmap", SizedInternalFormatGlob.RGBA16F, TexSizeCube, TexSizeCube, TexCubeMaxLevel + 1);

			Glob.Utils.SetTextureParameters(Device, TextureEnvmap, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.LinearMipmapLinear);

			PrecomputeBrdfLut();
		}

		void PrecomputeBrdfLut()
		{
			Device.BindPipeline(_psoBrdfPrecompute);
			Device.ShaderCompute.SetUniformI("lutsize", BrdfLutSizeX, BrdfLutSizeY);
			Device.ShaderCompute.SetUniformF("lutsizediv", new Vector2(1f / BrdfLutSizeX, 1f / BrdfLutSizeY));

			Device.BindImage2D(0, BrdfLut, TextureAccess.WriteOnly);

			Device.DispatchComputeThreads(BrdfLutSizeX, BrdfLutSizeY);
			
			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
		}

		static readonly int[] _specularSamples = { 4, 8, 48, 64 };
		static readonly int[] _specularSourceMips = { 0, 1, 4, 5 };

		public void RenderSkyEnvmap(AtmosphereModule atmosphereModule, AuroraModule auroraModule = null)
		{
			TimerQueryGPU tEnvmapFilter = new TimerQueryGPU(this.RenderContext.FeedbackManager, "EnvmapPrefilterAll");
			TimerQueryGPU tEnvmapFaceRender = new TimerQueryGPU(this.RenderContext.FeedbackManager, "EnvmapFaceRender");
			
			// Scene capture stage (sky only)
			using(Device.DebugMessageManager.PushGroupMarker("Envmap face render"))
			{
				tEnvmapFaceRender.StartQuery();
				if(auroraModule == null)
					atmosphereModule.RenderAtmosphereEnvmap(_texCubePrepare);
				else
					auroraModule.RenderAtmosphereAndAuroraEnvmap(atmosphereModule, _texCubePrepare);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);
				tEnvmapFaceRender.EndQuery();
			}

			// Envmap filtering stage
			using(Device.DebugMessageManager.PushGroupMarker("Envmap prefilter"))
			{
				tEnvmapFilter.StartQuery();

				// Mips
				Device.BindPipeline(_psoCubeMipGen);
				Device.BindImage3D(0, _texCubePrepare, TextureAccess.ReadOnly, 0);
				Device.BindImage3D(1, _texCubePrepare, TextureAccess.WriteOnly, 1);
				Device.BindImage3D(2, _texCubePrepare, TextureAccess.WriteOnly, 2);
				Device.BindImage3D(3, _texCubePrepare, TextureAccess.WriteOnly, 3);
				Device.BindImage3D(4, _texCubePrepare, TextureAccess.WriteOnly, 4);
				Device.DispatchComputeThreads(_texCubePrepare.Width / 2, _texCubePrepare.Height / 2, 6);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

				Device.BindImage3D(0, _texCubePrepare, TextureAccess.ReadOnly, 4);
				Device.BindImage3D(1, _texCubePrepare, TextureAccess.WriteOnly, 5);
				Device.BindImage3D(2, _texCubePrepare, TextureAccess.WriteOnly, 6);
				Device.BindImage3D(3, _texCubePrepare, TextureAccess.WriteOnly, 7);
				Device.BindImage3D(4, null, TextureAccess.WriteOnly, 0);

				Device.DispatchComputeThreads(_texCubePrepare.Width / 2 / 16, _texCubePrepare.Height / 2 / 16, 6);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				// Diffuse
				Device.BindTexture(_texCubePrepare, 0);
				Device.BindPipeline(_psoConvoluteDiffuse);
				int wDiffuse = TexSizeCube >> TexCubeMaxLevel;
				int hDiffuse = wDiffuse;

				_psoConvoluteDiffuse.ShaderCompute.SetUniformF("lod", TexCubeMaxLevel);
				_psoConvoluteDiffuse.ShaderCompute.SetUniformI("mipsize", wDiffuse, hDiffuse);
				_psoConvoluteDiffuse.ShaderCompute.SetUniformF("mipsizercp", new Vector2(1f / wDiffuse, 1f / hDiffuse));
				Device.BindImage3D(0, TextureEnvmap, TextureAccess.WriteOnly, TexCubeMaxLevel);
				Device.DispatchComputeThreads(wDiffuse, hDiffuse, 6);
				
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

				// Specular
				Device.BindPipeline(_psoConvoluteSpecular);

				for(int i = 0; i < TexCubeMaxLevel; i++)
				{
					int wSpec = TexSizeCube >> i;
					int hSpec = wSpec;

					float rough = (float)i / (TexCubeMaxLevel - 1);
					rough *= rough;

					_psoConvoluteSpecular.ShaderCompute.SetUniformI("numSamples", _specularSamples[i]);
					_psoConvoluteSpecular.ShaderCompute.SetUniformF("roughness", rough);
					_psoConvoluteSpecular.ShaderCompute.SetUniformI("mipsize", wSpec, hSpec);
					_psoConvoluteSpecular.ShaderCompute.SetUniformF("mipsizercp", new Vector2(1f / wSpec, 1f / hSpec));
					_psoConvoluteSpecular.ShaderCompute.SetUniformF("lod", (float)(_specularSourceMips[i]));
					Device.BindImage3D(0, TextureEnvmap, TextureAccess.WriteOnly, i);

					Device.DispatchComputeThreads(wSpec, hSpec, 6);
				}

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				tEnvmapFilter.EndQuery();
			}
		}
	}
}
