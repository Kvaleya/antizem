﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Renderer.Materials.VirtualTextures;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class VisualizeVTModule : RenderModule
	{
		GraphicsPipeline _pso;
		VirtualTextureManager _vtManager;

		public VisualizeVTModule(Renderer.RenderContext renderContext, VirtualTextureManager vtManager)
			: base(renderContext)
		{
			_vtManager = vtManager;
			_pso = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("vtVisualize.frag"), null, new RasterizerState(CullfaceState.Back), new DepthState(DepthFunction.Always, false));
			_pso.ShaderFragment.SetUniformI("tex", 0);
		}

		public void Render()
		{
			Device.BindPipeline(_pso);
			Device.BindTexture(_vtManager.TexPhysicalCacheBaseColor, 0);
			Device.BindTexture(_vtManager.TexPhysicalCacheNormalMap, 1);
			//Device.TextureUnit.BindTexture(RenderContext.AlphaTextureManager.Atlas, 0);
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
