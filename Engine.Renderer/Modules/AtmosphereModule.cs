﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class AtmosphereModule : RenderModule
	{
		const int LutTransmittanceSizeX = 64;
		const int LutTransmittanceSizeY = 64;
		const int LutScatteringSizeX = 32;
		const int LutScatteringSizeY = 256;
		const int LutScatteringSizeZ = 32;

		const int NumSamplesTransmittance = 40;
		const int NumSamplesTransmittanceFast = 10;
		const int NumSamplesScattering = 40;
		const int NumSamplesMultipleScattering = 10;

		public Vector3 SunIrradianceAtmosphereColored
		{
			get { return _sunColor * SunIrradianceTopOfAtmosphere; }
		}

		Vector3 _sunColor = new Vector3(1.0f, 0.99f, 0.975f);
		const float SunIrradianceTopOfAtmosphere = 1361;

		[InjectProperty]
		public TestSceneData SceneData { get; set; }

		GraphicsPipeline _psoAtmosphereRender;

		public Atmosphere AtmoData { get; private set; }

		Texture2D _texLutTransmittance;
		Texture3D _texLutScatteringSum;
		Texture3D _texLutScatteringSumSwap;
		Texture3D _texLutScatteringMultiple;
		Texture3D _texLutScatteringMultipleSwap;

		ComputePipeline _psoGenerateLutTransmittance;
		ComputePipeline _psoGenerateLutSingleScattering;
		ComputePipeline _psoGenerateLutMultipleScattering;
		ComputePipeline _psoAtmosphereRenderEnvmap;

		public AtmosphereModule(Renderer.RenderContext renderContext) : base(renderContext)
		{
			_psoAtmosphereRender = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("atmo.frag"), null, new RasterizerState(), new DepthState(DepthFunction.Equal, true));
			_psoAtmosphereRenderEnvmap = new ComputePipeline(Device, Device.GetShader("atmo_envmap.comp"));

			AtmoData = new Atmosphere();

			var definesTransmittance = new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("NUMSAMPLES_PRIMARY", NumSamplesScattering.ToString()),
				new Tuple<string, string>("NUMSAMPLES_SECONDARY", NumSamplesTransmittance.ToString()),
				new Tuple<string, string>("NUMSAMPLES_MULTIPLE", NumSamplesMultipleScattering.ToString()),
			};
			var definesScattering = new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("NUMSAMPLES_PRIMARY", NumSamplesScattering.ToString()),
				new Tuple<string, string>("NUMSAMPLES_SECONDARY", NumSamplesTransmittanceFast.ToString()),
				new Tuple<string, string>("NUMSAMPLES_MULTIPLE", NumSamplesMultipleScattering.ToString()),
			};

			_psoGenerateLutTransmittance = new ComputePipeline(Device, Device.GetShader("atmo_genTransmittanceLUT.comp", definesTransmittance));

			_psoGenerateLutSingleScattering = new ComputePipeline(Device, Device.GetShader("atmo_genSingleScatLUT.comp", definesScattering));
			_psoGenerateLutSingleScattering.ShaderCompute.SetUniformI("transmittanceLUT", 0);

			_psoGenerateLutMultipleScattering = new ComputePipeline(Device, Device.GetShader("atmo_genMultipleScatLUT.comp", definesScattering));
			_psoGenerateLutMultipleScattering.ShaderCompute.SetUniformI("texLutScattering", 0);

			PrecomputeAtmosphereLuts(4);
		}

		public void PrecomputeAtmosphereLuts(int scatteringOrders)
		{
			var device = RenderContext.Device;

			_texLutTransmittance?.Dispose();
			_texLutScatteringSum?.Dispose();
			_texLutScatteringSumSwap?.Dispose();
			_texLutScatteringMultiple?.Dispose();
			_texLutScatteringMultipleSwap?.Dispose();

			// Create textures
			_texLutTransmittance = new Texture2D(device, "AtmoTransmittanceLut", SizedInternalFormatGlob.RGBA16F, LutTransmittanceSizeX, LutTransmittanceSizeY, 1);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);

			_texLutScatteringSum = CreateScatteringTexture(device, "AtmosphericScatteringSumA");
			_texLutScatteringSumSwap = CreateScatteringTexture(device, "AtmosphericScatteringSumB");
			_texLutScatteringMultiple = CreateScatteringTexture(device, "AtmosphericScatteringMultipleA");
			_texLutScatteringMultipleSwap = CreateScatteringTexture(device, "AtmosphericScatteringMultipleB");

			// Precompute transmittance
			device.BindPipeline(_psoGenerateLutTransmittance);
			SetAtmoUniforms(_psoGenerateLutTransmittance.ShaderCompute);
			_psoGenerateLutTransmittance.ShaderCompute.SetUniformI("lutSize", LutTransmittanceSizeX, LutTransmittanceSizeY);
			device.BindImage2D(0, _texLutTransmittance, TextureAccess.WriteOnly);
			device.DispatchComputeThreads(LutTransmittanceSizeX, LutTransmittanceSizeY);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			// Single scattering
			device.BindPipeline(_psoGenerateLutSingleScattering);
			SetAtmoUniforms(_psoGenerateLutSingleScattering.ShaderCompute);
			_psoGenerateLutSingleScattering.ShaderCompute.SetUniformI("lutSize", LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ);
			device.BindImage3D(0, _texLutScatteringSum, TextureAccess.WriteOnly);
			RenderContext.Device.BindTexture(_texLutTransmittance, 0);
			device.DispatchComputeThreads(LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			var multipleSource = _texLutScatteringSum;
			var multipleTarget = _texLutScatteringMultiple;
			var sumSource = _texLutScatteringSum;
			var sumTarget = _texLutScatteringSumSwap;

			// Multiple scattering
			for(int i = 1; i < scatteringOrders; i++)
			{
				device.BindPipeline(_psoGenerateLutMultipleScattering);
				SetAtmoUniforms(_psoGenerateLutMultipleScattering.ShaderCompute);
				_psoGenerateLutMultipleScattering.ShaderCompute.SetUniformI("lutSize", LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ);
				device.BindImage3D(0, multipleTarget, TextureAccess.WriteOnly);
				device.BindImage3D(1, sumSource, TextureAccess.ReadOnly);
				device.BindImage3D(2, sumTarget, TextureAccess.WriteOnly);
				RenderContext.Device.BindTexture(multipleSource, 0);
				device.DispatchComputeThreads(LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

				Texture3D temp;

				if(i == 1)
				{
					multipleSource = _texLutScatteringMultiple;
					multipleTarget = _texLutScatteringMultipleSwap;
					sumSource = _texLutScatteringSumSwap;
					sumTarget = _texLutScatteringSum;
				}
				else
				{
					temp = multipleSource;
					multipleSource = multipleTarget;
					multipleTarget = temp;

					temp = sumSource;
					sumSource = sumTarget;
					sumTarget = temp;
				}

				temp = _texLutScatteringSum;
				_texLutScatteringSum = _texLutScatteringSumSwap;
				_texLutScatteringSumSwap = temp;

			}

			_texLutScatteringSumSwap?.Dispose();
			_texLutScatteringMultiple?.Dispose();
			_texLutScatteringMultipleSwap?.Dispose();
		}

		Texture3D CreateScatteringTexture(Device device, string name)
		{
			var tex = new Texture3D(device, name, SizedInternalFormatGlob.RGBA32F, LutScatteringSizeX, LutScatteringSizeY, LutScatteringSizeZ, 1);
			GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
			return tex;
		}

		public void RenderAtmosphere()
		{
			RenderContext.Device.BindPipeline(_psoAtmosphereRender);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("ray00", RenderContext.CameraRay00);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("ray01", RenderContext.CameraRay01);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("ray10", RenderContext.CameraRay10);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("ray11", RenderContext.CameraRay11);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("sunDir", SceneData.SunDir);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("starColor", SunIrradianceAtmosphereColored);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("exposure", SceneData.Exposure);
			_psoAtmosphereRender.ShaderFragment.SetUniformF("cameraHeight", (float)SceneData.Camera.CameraCurrent.Position.Y);
			SetAtmoUniforms(_psoAtmosphereRender.ShaderFragment);

			RenderContext.Device.BindTexture(_texLutScatteringSum, 0);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}

		public void RenderAtmosphereEnvmap(TextureCube tex)
		{
			RenderContext.Device.BindPipeline(_psoAtmosphereRenderEnvmap);

			_psoAtmosphereRenderEnvmap.ShaderCompute.SetUniformF("cubeSizeRcp", new Vector2(1f / tex.Width, 1f / tex.Height));
			SetupEnvmapRendering(_psoAtmosphereRenderEnvmap.ShaderCompute, 0);

			Device.BindImage3D(0, tex, TextureAccess.WriteOnly, 0);

			Device.DispatchComputeThreads(tex.Width, tex.Height, 6);
		}

		public void SetupEnvmapRendering(Shader shader, int lutScatteringTexBindingPoint = 0)
		{
			SetAtmoUniforms(shader);
			shader.SetUniformF("sunDir", SceneData.SunDir);
			shader.SetUniformF("starColor", SunIrradianceAtmosphereColored);
			RenderContext.Device.BindTexture(_texLutScatteringSum, lutScatteringTexBindingPoint);
		}

		void SetAtmoUniforms(Shader shader)
		{
			if(AtmoData == null)
				return;

			shader.SetUniformF("atmo_planet_radius", AtmoData.PlanetRadius);
			shader.SetUniformF("atmo_radius", AtmoData.Radius);

			shader.SetUniformF("atmo_coef_rayleigh", AtmoData.CoefRayleigh);
			shader.SetUniformF("atmo_coef_ozone", AtmoData.CoefOzone);

			shader.SetUniformF("atmo_coef_mie", AtmoData.CoefMie);
			shader.SetUniformF("atmo_g_mie", AtmoData.GMie);
			shader.SetUniformF("atmo_a_mie", AtmoData.AMie);

			shader.SetUniformF("atmo_h0_R", AtmoData.H0Rayleigh);
			shader.SetUniformF("atmo_h0_M", AtmoData.H0Mie);

			shader.SetUniformF("atmo_recover_mie", AtmoData.AtmoRecoverMie);
		}

		public Vector3 GetOutScattering(Vector3 direction, float height)
		{
			return AtmoData.GetOutScattering(height, direction);
		}

		public Vector3 GetColoredSun(Vector3 direction, float height = 0)
		{
			return SunIrradianceAtmosphereColored * GetOutScattering(direction, height);
		}
	}

	class Atmosphere
	{
		public float PlanetRadius = 6378000.00f;
		public float Radius = 100000.00f;

		public Vector3 CoefRayleigh = new Vector3(5.80000E-6f, 0.00001f, 0.00003f);
		public Vector3 CoefOzone = new Vector3(2.05560E-6f, 4.97880E-6f, 2.13600E-7f);

		public float CoefMie = 2.00000E-6f;
		public float GMie = -0.85f;
		public float AMie = 1.11f;

		public float H0Rayleigh = 8000.00f;
		public float H0Mie = 1200.00f;

		public Vector3 AtmoRecoverMie
		{
			get
			{
				double red = CoefRayleigh.X;
				return new Vector3((float)(red / CoefRayleigh.X), (float)(red / CoefRayleigh.Y),
					(float)(red / CoefRayleigh.Z));
			}
		}

		static float PhaseFuncRayleigh(float cosTheta)
		{
			return 0.75f * (1.0f + cosTheta * cosTheta);
		}

		static float PhaseFuncMie(float cosTheta, float g)
		{
			float g2 = g * g;
			float nom = 3.0f * (1.0f - g2) * (1.0f + cosTheta * cosTheta);
			float denom = 2.0f * (2.0f + g2) * (float)Math.Pow(1.0 + g2 - 2.0 * g * cosTheta, 1.5);
			return nom / denom;
		}

		Vector2 Density(Vector3 p)
		{
			float h = (p - new Vector3(0, -PlanetRadius, 0)).Length - PlanetRadius;
			return new Vector2((float)Math.Exp(-h / H0Rayleigh), (float)Math.Exp(-h / H0Mie));
		}

		Vector2 Transmittance(Vector3 pa, Vector3 pb)
		{
			const int samples = 5;
			//const float pi4 = 12.566370614359172953850573533118f;
			Vector3 add = (pb - pa) / samples;
			Vector2 temp = new Vector2(0);

			Vector3 p = pa;
			for(int i = 0; i < samples; i++)
			{
				p = pa + add * (i + 0.5f);
				temp += Density(p);
			}

			return temp * add.Length;
		}

		static Vector2 RaySphereIntersection(Vector3 center, float radius, Vector3 origin, Vector3 direction, float depth)
		{
			float r0 = 0;
			float r1 = 0;

			Vector3 distvec = (origin - center);
			float dirdotdist = Vector3.Dot(direction, distvec);
			float temp = dirdotdist * dirdotdist - distvec.LengthSquared + radius * radius;

			if(temp > 0)
			{
				temp = (float)Math.Sqrt(temp);
				r0 = -dirdotdist - temp;
				r1 = -dirdotdist + temp;
			}

			r0 = Math.Max(r0, 0.0f);
			r1 = Common.Utils.Math.Clamp(r1, 0.0f, depth);

			return new Vector2(r0, r1);
		}

		public Vector3 GetOutScattering(float height, Vector3 dir)
		{
			Vector3 p = new Vector3(0, height, 0);

			Vector2 r_atmo = RaySphereIntersection(new Vector3(0, -PlanetRadius, 0), PlanetRadius + Radius, p, dir, 1000000);

			Vector3 pc = p + dir * r_atmo.Y;

			Vector2 scat = Transmittance(p, pc);

			Vector3 outscat = new Vector3(1);

			outscat.X *= (float)Math.Exp(-scat.X * CoefRayleigh.X);
			outscat.Y *= (float)Math.Exp(-scat.X * CoefRayleigh.Y);
			outscat.Z *= (float)Math.Exp(-scat.X * CoefRayleigh.Z);

			outscat.X *= (float)Math.Exp(-scat.Y * CoefMie * AMie);
			outscat.Y *= (float)Math.Exp(-scat.Y * CoefMie * AMie);
			outscat.Z *= (float)Math.Exp(-scat.Y * CoefMie * AMie);

			outscat.X *= (float)Math.Exp(-scat.X * CoefOzone.X);
			outscat.Y *= (float)Math.Exp(-scat.X * CoefOzone.Y);
			outscat.Z *= (float)Math.Exp(-scat.X * CoefOzone.Z);

			return outscat;
		}

		static Vector3 ViewParamsToTc(float height, float rY, float sunY, float radius)
		{
			return new Vector3(
				(float)Math.Sqrt(height / radius),
				(1.0f + rY) * 0.5f,
				(1.0f - (float)Math.Exp(-2.8 * sunY - 0.8)) * 1.02809128080726f
				);
		}

		//Vector3 GetScattered(float height, Vector3 dir, SunSettings sun)
		//{
		//	Vector3 tc = ViewParamsToTc(height, dir.Y, sun.Direction.Y, Data.Radius);

		//	int x = (int)(tc.X * ScatteringLUTWidth);
		//	int y = (int)(tc.Y * ScatteringLUTHeight);
		//	int z = (int)(tc.Z * ScatteringLUTDepth);

		//	x = MathUtils.ClampI(x, 0, ScatteringLUTWidth - 1);
		//	y = MathUtils.ClampI(y, 0, ScatteringLUTHeight - 1);
		//	z = MathUtils.ClampI(z, 0, ScatteringLUTDepth - 1);

		//	Vector4 lut = LutScatteringData[x, y, z].ToVector4();

		//	Vector3 accumR = lut.Xyz;
		//	Vector3 accumM = lut.W * (lut.W / lut.X) * Data.AtmoRecoverMie;

		//	accumR *= PhaseFuncRayleigh(Vector3.Dot(dir, -sun.Direction));
		//	accumM *= PhaseFuncMie(Vector3.Dot(dir, -sun.Direction), Data.GMie);

		//	accumR *= Data.CoefRayleigh / (float)(4.0 * Math.PI);
		//	accumM *= Data.CoefMie / (float)(4.0 * Math.PI);

		//	return (accumR + accumM) * sun.IlluminanceTopOfAtmosphere;
		//}

		//public Vector3 GetAmbient(SunSettings sun)
		//{
		//	float y = 0.3f;
		//	float x = (float)Math.Sqrt(1.0 - y * y);
		//	Vector3 light = GetScattered(1, Vector3.UnitY, sun);
		//	light += GetScattered(1, new Vector3(x, y, 0), sun);
		//	light += GetScattered(1, new Vector3(-x, y, 0), sun);
		//	light += GetScattered(1, new Vector3(0, y, x), sun);
		//	light += GetScattered(1, new Vector3(0, y, -x), sun);
		//	light = light * 0.2f * (float)(2 * Math.PI);

		//	float max = Math.Max(Math.Max(light.X, light.Y), light.Z);

		//	if(max < 0 || float.IsNaN(max))
		//		light = new Vector3(0.02f);
		//	return light;
		//}
	}
}
