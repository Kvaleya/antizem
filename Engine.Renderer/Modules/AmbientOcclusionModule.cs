﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class AmbientOcclusionModule : RenderModule
	{
		ComputePipeline _psoAo;
		ComputePipeline _psoAoFilter;

		Texture2D _texAoNoisy;
		Texture2D _texAoFiltered;
		Texture2D _texAoFilteredHistory;

		public const int ResolutionShift = 1;

		public Texture2D TextureOcclusion { get { return _texAoFiltered; } }

		static readonly float[] TemporalAngleOffsets = new float[]
		{
			(float)(1 / 6f * Math.PI),
			(float)(5 / 6f * Math.PI),
			(float)(3 / 6f * Math.PI),
			(float)(4 / 6f * Math.PI),
			(float)(2 / 6f * Math.PI),
			(float)(0 / 6f * Math.PI),
		};

		public AmbientOcclusionModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			_psoAo = new ComputePipeline(Device, Device.GetShader("gtao.comp"));
			_psoAoFilter = new ComputePipeline(Device, Device.GetShader("gtao_temporal_filtering.comp"));
		}

		public override void OnResize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{
			base.OnResize(displayWidth, displayHeight, renderWidth, renderHeight);

			_texAoNoisy?.Dispose();
			_texAoFiltered?.Dispose();
			_texAoFilteredHistory?.Dispose();

			_texAoNoisy = new Texture2D(Device, "TexAoNoisy", SizedInternalFormatGlob.R8, renderWidth >> ResolutionShift, renderHeight >> ResolutionShift, 1);
			_texAoFiltered = new Texture2D(Device, "TexAoFiltered-A", SizedInternalFormatGlob.RG32F, renderWidth >> ResolutionShift, renderHeight >> ResolutionShift, 1);
			_texAoFilteredHistory = new Texture2D(Device, "TexAoFiltered-B", _texAoFiltered.Format, _texAoFiltered.Width, _texAoFiltered.Height, 1);

			Glob.Utils.SetTextureParameters(Device, _texAoNoisy, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			Glob.Utils.SetTextureParameters(Device, _texAoFiltered, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			Glob.Utils.SetTextureParameters(Device, _texAoFilteredHistory, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
		}

		public void ComputeAo(GbufferModule gbuffer)
		{
			var temp = _texAoFiltered;
			_texAoFiltered = _texAoFilteredHistory;
			_texAoFilteredHistory = temp;

			var timer = new TimerQueryGPU(RenderContext.FeedbackManager, "Ao");
			using(Device.DebugMessageManager.PushGroupMarker("Ao"))
			{
				var framenum = RenderContext.FeedbackManager.FrameNumber;
				//framenum = 0;

				timer.StartQuery();
				Device.BindPipeline(_psoAo);
				Device.BindImage2D(0, _texAoNoisy, TextureAccess.WriteOnly);
				Device.BindTexture(gbuffer.TextureLinearDepth, 0);
				_psoAo.ShaderCompute.SetUniformF("minMip", (float)ResolutionShift);
				_psoAo.ShaderCompute.SetUniformF("screenSizeRcp", new Vector2(1f / _texAoNoisy.Width, 1f / _texAoNoisy.Height));
				_psoAo.ShaderCompute.SetUniformF("hfovTan", (float)Math.Tan(RenderContext.CameraFov * 0.5 * Math.PI / 180));
				_psoAo.ShaderCompute.SetUniformF("spacialOffset", (framenum % 4) * 0.25f - 0.375f);
				_psoAo.ShaderCompute.SetUniformF("angleOffset",
					TemporalAngleOffsets[(framenum % TemporalAngleOffsets.Length)]);
				Device.DispatchComputeThreads(_texAoNoisy.Width, _texAoNoisy.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				Vector3 positionHistory = (Vector3)(RenderContext.CameraPositionHistory - RenderContext.CameraPosition);

				var plane = new Vector4(RenderContext.CameraMatrixHistory.Column2.Xyz,
					Vector3.Dot(RenderContext.CameraMatrixHistory.Column2.Xyz, positionHistory));

				var history = RenderContext.Camera.CameraHistory.ToCameraMatrix(positionHistory) *
				              RenderContext.ProjectionMatrixNoReverseDepthHistory;

				Device.BindPipeline(_psoAoFilter);
				_psoAoFilter.ShaderCompute.SetUniformF("sizeRcp", new Vector2(1f / _texAoFiltered.Width, 1f / _texAoFiltered.Height));
				_psoAoFilter.ShaderCompute.SetUniformI("depthSkip", ResolutionShift);
				_psoAoFilter.ShaderCompute.SetUniformF("matPrevious", history);
				_psoAoFilter.ShaderCompute.SetUniformF("cameraPlanePrevious", plane);

				_psoAoFilter.ShaderCompute.SetUniformF("cameraRay00", RenderContext.CameraRay00.Xyz);
				_psoAoFilter.ShaderCompute.SetUniformF("cameraRay01", RenderContext.CameraRay01.Xyz);
				_psoAoFilter.ShaderCompute.SetUniformF("cameraRay10", RenderContext.CameraRay10.Xyz);
				_psoAoFilter.ShaderCompute.SetUniformF("cameraRay11", RenderContext.CameraRay11.Xyz);

				Device.BindImage2D(0, _texAoFiltered, TextureAccess.WriteOnly);
				Device.BindTexture(_texAoNoisy, 1);
				Device.BindTexture(_texAoFilteredHistory, 2);
				Device.BindTexture(gbuffer.TextureLinearDepthHistory, 3);

				Device.DispatchComputeThreads(_texAoFiltered.Width, _texAoFiltered.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
				timer.EndQuery();
			}
		}
	}
}
