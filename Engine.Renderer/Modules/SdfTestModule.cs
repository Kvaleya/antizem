﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Renderer.Meshes;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class SdfTestModule : RenderModule
	{
		[InjectProperty]
		public TestSceneData SceneData { get; set; }

		ComputePipeline _psoGenSdf;
		ComputePipeline _psoPrepare;

		Texture3D _texSdf;

		public const int TestSize = 64;

		public SdfTestModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			_psoPrepare = new ComputePipeline(Device, Device.GetShader("sdfPrep.comp"));
			_psoGenSdf = new ComputePipeline(Device, Device.GetShader("sdfGen.comp"));
			_texSdf = new Texture3D(Device, "SdfTexture", SizedInternalFormatGlob.R16F, TestSize, TestSize, TestSize, 1);
		}

		bool _hasSdf = false;

		public void Render()
		{
			if(SceneData.Batches.Count > 0)
			{
				if(SceneData.Batches[0].Instances.Count > 0)
				{
					var instance = SceneData.Batches[0].Instances[0];
					var mesh = (Mesh)instance.Mesh;

					if(mesh == null)
						return;

					var renderable = mesh.MeshRenderable.Item;

					if(renderable == null || _hasSdf)
						return;

					//_hasSdf = true;
					GenerateSdf(renderable, 6, 0, 0, 0);
				}
			}
		}

		void GenerateSdf(MeshRenderable mesh, int resolutionPow, int offsetX, int offsetY, int offsetZ)
		{
			int res = 1 << resolutionPow;

			Vector3 scale = mesh.Description.PositionsScale;
			Vector3 offset = Vector3.Zero;

			// Normalize scale to 0 .. 1
			scale /= Math.Max(Math.Max(scale.X, scale.Y), scale.Z);
			// Reduce scale to make space for 1 voxel border
			scale = scale * (res - 2) / res;
			// Offset mesh by 1 voxel (border)
			offset += new Vector3(1f / res);

			var query3 = new TimerQueryGPU(RenderContext.FeedbackManager, "SdfAll");
			query3.StartQuery();

			int prepared = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer,
				new IntPtr(mesh.Description.Elements.Count * 16), IntPtr.Zero, BufferStorageFlags.None, "SdfTempBufferPositions");

			var query = new TimerQueryGPU(RenderContext.FeedbackManager, "SdfPrep");
			query.StartQuery();

			Device.BindPipeline(_psoPrepare);
			
			_psoPrepare.ShaderCompute.SetUniformI("numElements", mesh.Description.Elements.Count);
			_psoPrepare.ShaderCompute.SetUniformI("firstIndex", mesh.Description.Elements.Start);
			_psoPrepare.ShaderCompute.SetUniformI("baseVertex", mesh.Description.Vertices.Start);
			_psoPrepare.ShaderCompute.SetUniformF("meshPositionOffset", offset);
			_psoPrepare.ShaderCompute.SetUniformF("meshPositionScale", scale);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, mesh.Description.BufferPositions);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, mesh.Description.BufferIndices);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 2, prepared);

			Device.DispatchComputeThreads(mesh.Description.Elements.Count);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

			query.EndQuery();

			var query2 = new TimerQueryGPU(RenderContext.FeedbackManager, "SdfGen");
			query2.StartQuery();

			Device.BindPipeline(_psoGenSdf);

			_psoGenSdf.ShaderCompute.SetUniformI("numElements", mesh.Description.Elements.Count);
			_psoGenSdf.ShaderCompute.SetUniformI("texDestination", offsetX, offsetY, offsetZ);
			_psoGenSdf.ShaderCompute.SetUniformF("resolutionRcp", 1f / res);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, prepared);
			Device.BindImage3D(0, _texSdf, TextureAccess.WriteOnly);

			const int step = 8;
			for(int i = 0; i < res; i += step)
			{
				_psoGenSdf.ShaderCompute.SetUniformI("posOffset", 0, 0, i);
				Device.DispatchComputeThreads(res, res, step);
			}

			GL.MemoryBarrier(MemoryBarrierFlags.AllBarrierBits);

			query2.EndQuery();

			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, 0);
			GL.DeleteBuffer(prepared);

			query3.EndQuery();
		}
	}
}
