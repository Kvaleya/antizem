﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK.Audio;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class TonemapModule : RenderModule
	{
		GraphicsPipeline _psoTonemap;
		GraphicsPipeline _psoTonemapDisabled;

		int _samplerNearest;

		public TonemapModule(Renderer.RenderContext context) : base(context)
		{
			_psoTonemap = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("tonemap.frag"), null, new RasterizerState(), new DepthState());
			_psoTonemap.ShaderFragment.SetUniformI("texScene", 0);

			_psoTonemapDisabled = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("tonemap.frag", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("NO_TONEMAP_GAMMA", ""),
			}), null, new RasterizerState(), new DepthState());
			_psoTonemapDisabled.ShaderFragment.SetUniformI("texScene", 0);

			_samplerNearest = GL.GenSampler();
			GL.SamplerParameter(_samplerNearest, SamplerParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.SamplerParameter(_samplerNearest, SamplerParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
			GL.SamplerParameter(_samplerNearest, SamplerParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
			GL.SamplerParameter(_samplerNearest, SamplerParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
			GL.SamplerParameter(_samplerNearest, SamplerParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
		}

		// gamma parameter is just additional correction applied on top of the standart 2.2 gamma correction
		public void Tonemap(Texture2D sceneColorHdr, bool enabled = true, float gamma = 1)
		{
			if(enabled)
			{
				Device.BindPipeline(_psoTonemap);
			}
			else
			{
				Device.BindPipeline(_psoTonemapDisabled);
			}
			GL.BindSampler(0, _samplerNearest);
			Device.BindTexture(sceneColorHdr, 0);
			Device.ShaderFragment.SetUniformF("gamma", gamma);
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
			GL.BindSampler(0, 0);
		}
	}
}
