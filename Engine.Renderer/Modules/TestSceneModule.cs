﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using Engine.Renderer.ComputeMeshProcessor;
using Engine.Renderer.Meshes;
using Engine.Renderer.Modules;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Utils = Glob.Utils;

namespace Engine.Renderer.Modules
{
	class TestSceneModule : RenderModule
	{
		[InjectProperty]
		public TestSceneData SceneData { get; set; }

		GraphicsPipeline _psoMain;
		GraphicsPipeline _psoAlphaPrepass;
		GraphicsPipeline _psoAlphaMain;

		MeshProcessData _meshProcessDataOpaque;
		MeshProcessData _meshProcessDataAlphaTested;

		CameraPerspective _cullCamera = new CameraPerspective(Vector3d.Zero, Quaternion.Identity, 90);

		public TestSceneModule(Renderer.RenderContext renderContext) : base(renderContext)
		{
			_psoAlphaPrepass = new GraphicsPipeline(Device, Device.GetShader("instancedMesh.vert", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("DEPTH_PREPASS", "1")
			}), Device.GetShader("alphaTest.frag"), null, new RasterizerState(CullfaceState.None), new DepthState(DepthFunction.Greater, true));

			_psoMain = new GraphicsPipeline(Device, Device.GetShader("instancedMesh.vert"), Device.GetShader("texturedMesh.frag"), null, new RasterizerState(CullfaceState.None), new DepthState(DepthFunction.Greater, true));

			_psoAlphaMain = new GraphicsPipeline(Device, Device.GetShader("instancedMesh.vert"), Device.GetShader("texturedMesh.frag"), null, new RasterizerState(CullfaceState.None), new DepthState(DepthFunction.Equal, false));

			_psoMain.ShaderFragment.SetUniformI("tex", 0);
			_psoMain.ShaderFragment.SetUniformI("texVTIndirection", 1);
			_psoMain.ShaderFragment.SetUniformI("texVTPhysicalBaseColor", 2);
			_psoMain.ShaderFragment.SetUniformI("texVTPhysicalNormalMap", 3);
			_psoMain.ShaderFragment.SetUniformI("texAlphaAtlas", 4);

			_psoAlphaMain.ShaderFragment.SetUniformI("tex", 0);
			_psoAlphaMain.ShaderFragment.SetUniformI("texVTIndirection", 1);
			_psoAlphaMain.ShaderFragment.SetUniformI("texVTPhysicalBaseColor", 2);
			_psoAlphaMain.ShaderFragment.SetUniformI("texVTPhysicalNormalMap", 3);
			_psoAlphaMain.ShaderFragment.SetUniformI("texAlphaAtlas", 4);

			_psoAlphaPrepass.ShaderFragment.SetUniformI("texAlphaAtlas", 4);
		}

		public void Prepare()
		{
			_meshProcessDataAlphaTested = PrepareBatch(SceneData.BatchesAlphaTest);
			_meshProcessDataOpaque = PrepareBatch(SceneData.Batches);
		}

		MeshProcessData PrepareBatch(List<MeshBatch> inputBatches)
		{
			var batches = RenderContext.InstanceManager.GetBatches(inputBatches);
			return RenderContext.ComputeMeshProcessor.PrepareBatches(batches);
		}

		public void RenderMeshes(GbufferModule gbuffer)
		{
			if(!SceneData.VisualizeCulling)
			{
				_cullCamera = SceneData.Camera.CameraCurrent.Clone();
			}

			// Need these projection matrixes
			// Main camera
			// Culling matrix without reverse depth (for correct backface culling)
			// Matrix for reprojection previous frame's depth
			// ;
			var currentProjCamera = RenderContext.ProjectionCameraMatrix;
			Vector3 cameraOffset = (Vector3)(_cullCamera.Position - SceneData.Camera.CameraCurrent.Position);
			var cullCamera = _cullCamera.ToCameraMatrix(cameraOffset);
			var cullProj = cullCamera * RenderContext.ProjectionMatrix;
			var cullProjNoReverse = cullCamera * RenderContext.ProjectionMatrixNoReverseDepth;

			Action shared = () =>
			{
				RenderContext.Device.BindBufferBase(BufferRangeTarget.UniformBuffer, 0, RenderContext.MaterialUpdater.MaterialUbo);
				RenderContext.Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, RenderContext.VertexManager.MeshDataBuffer.Handle);
				RenderContext.Device.ShaderVertex.SetUniformF("projection", currentProjCamera);
			};

			Action sharedMaterial = () =>
			{
				RenderContext.Device.BindImage2D(0, RenderContext.VirtualTextureManager.TexFeedback, TextureAccess.ReadWrite);
				if(RenderContext.Device.ShaderFragment != null)
				{
					RenderContext.Device.ShaderFragment.SetUniformF("screenSize", new Vector4(RenderContext.RenderWidth, RenderContext.RenderHeight, 1f / RenderContext.RenderWidth, 1f / RenderContext.RenderHeight));
					RenderContext.Device.ShaderFragment.SetUniformF("vtTileBorderOffsetScale", RenderContext.VirtualTextureManager.VtTileBorderOffsetScale);
					RenderContext.Device.ShaderFragment.SetUniformF("vtTileScaleTexelScale", RenderContext.VirtualTextureManager.VtTileScaleTexelScale);
				}

				RenderContext.Device.BindTexture(RenderContext.VirtualTextureManager.TexIndirection, 1);
				RenderContext.Device.BindTexture(RenderContext.VirtualTextureManager.TexPhysicalCacheBaseColor, 2);
				RenderContext.Device.BindTexture(RenderContext.VirtualTextureManager.TexPhysicalCacheNormalMap, 3);

				GL.DrawBuffers(3, new DrawBuffersEnum[]
				{
					DrawBuffersEnum.ColorAttachment0, 
					DrawBuffersEnum.ColorAttachment1, 
					DrawBuffersEnum.ColorAttachment2, 
				});
			};

			Action onDrawAlphaPrepass = () =>
			{
				GL.DrawBuffer(DrawBufferMode.None);
				RenderContext.Device.BindPipeline(_psoAlphaPrepass);
				shared();
				RenderContext.Device.BindTexture(RenderContext.AlphaTextureManager.Atlas, 4);
			};

			Action onDrawAlphaMain = () =>
			{
				GL.DrawBuffers(3, new DrawBuffersEnum[]
				{
					DrawBuffersEnum.ColorAttachment0,
					DrawBuffersEnum.ColorAttachment1,
					DrawBuffersEnum.ColorAttachment2,
				});
				RenderContext.Device.BindPipeline(_psoAlphaMain);
				shared();
				sharedMaterial();
			};

			Action onDrawMain = () =>
			{
				GL.DrawBuffer(DrawBufferMode.ColorAttachment0);
				RenderContext.Device.BindPipeline(_psoMain);
				shared();
				sharedMaterial();
			};

			Action<MeshProcessData, Action<Action<int>>> dispatchMeshes = (data, draw) =>
			{
				var cullParams = new MeshProcessCullParams()
				{
					ProjectionCull = cullProj,
					ProjectionOrientationCull = cullProjNoReverse,
					OcclusionDepthTex = gbuffer.TextureDepthReprojected,
					CameraOffset = cameraOffset,
					CameraForward = -cullCamera.Column2.Xyz.Normalized(),
					CameraUp = cullCamera.Column1.Xyz.Normalized(),
					CameraNear = Renderer.ProjectionNear,
					Resolution = new Vector2(RenderContext.RenderWidth, RenderContext.RenderHeight)
				};

				Engine.Common.Utils.Math.GetPlanes(cullParams.ProjectionOrientationCull, out cullParams.Plane0, out cullParams.Plane1, out cullParams.Plane2, out cullParams.Plane3);

				RenderContext.ComputeMeshProcessor.ProcessMeshes(data, cullParams, draw);
			};

			TimerQueryGPU timerDrawAtest = new TimerQueryGPU(RenderContext.FeedbackManager, "DrawMeshesAtest");

			dispatchMeshes(_meshProcessDataAlphaTested, drawTriangles =>
			{
				timerDrawAtest.StartQuery();

				onDrawAlphaPrepass();
				drawTriangles(0);
				onDrawAlphaMain();
				drawTriangles(0);

				timerDrawAtest.EndQuery();
			});

			TimerQueryGPU timerDraw = new TimerQueryGPU(RenderContext.FeedbackManager, "DrawMeshes");

			dispatchMeshes(_meshProcessDataOpaque, drawTriangles =>
			{
				timerDraw.StartQuery();
				onDrawMain();
				drawTriangles(0);
				timerDraw.EndQuery();
			});
		}
	}
}
