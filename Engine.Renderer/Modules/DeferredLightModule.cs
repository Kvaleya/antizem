﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class DeferredLightModule : RenderModule
	{
		[InjectProperty]
		public TestSceneData SceneData { get; set; }

		ComputePipeline _psoDeferredLight;

		public DeferredLightModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			_psoDeferredLight = new ComputePipeline(Device, Device.GetShader("deferred_light.comp"));
		}

		public void ApplyLighting(GbufferModule gbuffer, EnvmapModule envmap, AmbientOcclusionModule aoModule, VoxelModule voxel, VolumetricModule volumetrics)
		{
			var queryDeferredLight = new TimerQueryGPU(RenderContext.FeedbackManager, "DeferredLight");

			queryDeferredLight.StartQuery();

			Device.BindPipeline(_psoDeferredLight);

			Device.ShaderCompute.SetUniformF("viewSizeRcp", new Vector2(1f / RenderContext.RenderWidth, 1f / RenderContext.RenderHeight));
			Device.ShaderCompute.SetUniformF("invProjCam", RenderContext.ProjectionCameraMatrix.Inverted());

			Device.ShaderCompute.SetUniformF("diffuseCubeLod", EnvmapModule.TexCubeMaxLevel);
			Device.ShaderCompute.SetUniformF("maxRoughnessLod", EnvmapModule.TexCubeMaxLevel - 1);
			Device.ShaderCompute.SetUniformF("exposure", SceneData.Exposure);
			Device.ShaderCompute.SetUniformF("cameraPlane", RenderContext.CameraPlane);

			Device.ShaderCompute.SetUniformF("sunlightdir", SceneData.SunDir);
			Device.ShaderCompute.SetUniformF("sunlight", SceneData.SunLight);

			Device.ShaderCompute.SetUniformF("cloudShadowRange", volumetrics.ShadowRange);
			Device.ShaderCompute.SetUniformF("cloudShadowMaxDist", volumetrics.ShadowMaxDist);
			Device.ShaderCompute.SetUniformF("cloudShadowMatrix", volumetrics.ShadowMatrix.Inverted());
			Device.ShaderCompute.SetUniformF("cloudShadowEsmConst", volumetrics.ShadowEsmConstant);

			Device.BindTexture(gbuffer.TextureGbufferBaseColorMetallic, 0);
			Device.BindTexture(gbuffer.TextureGbufferNormal, 1);
			Device.BindTexture(gbuffer.TextureGbufferMaterial, 2);
			Device.BindTexture(gbuffer.TextureDepthStencil, 3);
			Device.BindTexture(envmap.BrdfLut, 4);
			Device.BindTexture(envmap.TextureEnvmap, 5);
			Device.BindTexture(aoModule.TextureOcclusion, 6);
			if(voxel != null)
				Device.BindTexture(voxel.VoxelLight, 7);
			Device.BindTexture(volumetrics.TexCloudShadows, 8);

			Device.BindImage2D(0, gbuffer.TextureSceneColor, TextureAccess.WriteOnly);

			Device.DispatchComputeThreads(RenderContext.RenderWidth, RenderContext.RenderHeight);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			queryDeferredLight.EndQuery();
		}
	}
}
