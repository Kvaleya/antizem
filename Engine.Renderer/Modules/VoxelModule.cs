﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using Engine.Renderer.ComputeMeshProcessor;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class VoxelModule : RenderModule
	{
		const int VoxelMips = 7;
		const int VoxelResolutionPow = 6;
		const int VoxelBuildResMultiply = 2;
		const int VoxelResolution = 1 << VoxelResolutionPow;
		const int VoxelResolutionBuild = VoxelResolution * VoxelBuildResMultiply;
		const int VoxelizationMsaa = 4;
		const int VoxelLightResDiv = 4;
		public const int ClipmapLevels = 6;
		public const int NumFaces = 6;

		int _lastDebugClipmap = 0;

		static readonly float[] TemporalAngleOffsets = new float[]
		{
			(float)(1 / 6f * Math.PI / 3f),
			(float)(5 / 6f * Math.PI / 3f),
			(float)(3 / 6f * Math.PI / 3f),
			(float)(4 / 6f * Math.PI / 3f),
			(float)(2 / 6f * Math.PI / 3f),
			(float)(0 / 6f * Math.PI / 3f),
		};

		static readonly int[] DitherMatrix = new int[]
		{
			0, 8, 2, 10,
			12, 4, 14, 6,
			3, 11, 1, 9,
			15, 7, 13, 5,
		};

		[InjectProperty]
		public VoxelData VoxelData { get; set; }

		public Texture2D VoxelLight { get { return _texVoxelLightFiltered; } }

		FrameBuffer _fboVoxelize;
		Texture3D _texBuildOpacity;
		Texture3D _texBuildColor;
		Texture3D _texVoxelOpacity;
		Texture3D _texVoxelColor;

		Texture3D _texVoxelOpacityBuffer;

		Texture2D _texVoxelLight;
		Texture2D _texVoxelLightFiltered;
		Texture2D _texVoxelLightFilteredHistory;

		Texture2D _texVoxelTestDepth;
		FrameBuffer _fboVoxelTest;

		GraphicsPipeline _psoVoxelize;
		GraphicsPipeline _psoVoxelTest;
		GraphicsPipeline _psoVoxelVisualize;

		ComputePipeline _psoGenOpacity;
		ComputePipeline _psoFilterOpacity;
		ComputePipeline _psoVoxelLight;
		ComputePipeline _psoVoxelLightFilter;

		MeshProcessData _meshProcessData;

		Vec4i[] _clipmapPosSize = new Vec4i[ClipmapLevels];
		Vector4[] _clipmapPosSizeVec4 = new Vector4[ClipmapLevels];
		
		public VoxelModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			List<Tuple<string, string>> voxelDefines = new List<Tuple<string, string>>();
			voxelDefines.Add(new Tuple<string, string>("VOXEL_CLIPMAP_RES", VoxelResolution.ToString()));
			voxelDefines.Add(new Tuple<string, string>("NUM_VOXEL_CLIPMAPS", ClipmapLevels.ToString()));
				
			_fboVoxelize = FrameBuffer.GenerateEmptyFramebuffer(Device, VoxelResolutionBuild, VoxelResolutionBuild, 0, VoxelizationMsaa, false);
			Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);

			SizedInternalFormatGlob opacityFormat = SizedInternalFormatGlob.RGBA8;

			_texVoxelColor = new Texture3D(Device, "VoxelColor", SizedInternalFormatGlob.RGBA8, VoxelResolution * ClipmapLevels, VoxelResolution * NumFaces, VoxelResolution, VoxelMips);
			_texBuildColor = new Texture3D(Device, "VoxelBuildColor", SizedInternalFormatGlob.RGBA8, VoxelResolutionBuild, VoxelResolutionBuild, VoxelResolutionBuild, 1);

			_texVoxelOpacity = new Texture3D(Device, "VoxelOpacity", opacityFormat, VoxelResolution * ClipmapLevels, VoxelResolution, VoxelResolution, VoxelMips);
			Glob.Utils.SetTextureParameters(Device, _texVoxelOpacity, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.LinearMipmapLinear);

			_texVoxelOpacityBuffer = new Texture3D(Device, "VoxelOpacityBuffer", opacityFormat, VoxelResolution, VoxelResolution, VoxelResolution, 1);

			_texBuildOpacity = new Texture3D(Device, "VoxelBuildOpacity", SizedInternalFormatGlob.R32UI, VoxelResolutionBuild, VoxelResolutionBuild, VoxelResolutionBuild, 1);

			var defines = new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("VOXELIZE", "1"),
			};

			var definesGenOpacity = new List<Tuple<string, string>>()
			{
			};

			if(VoxelBuildResMultiply == 1)
			{
				definesGenOpacity.Add(new Tuple<string, string>("SAMERES", "1"));
			}

			_psoVoxelize = new GraphicsPipeline(Device, Device.GetShader("instancedMesh.vert", defines), Device.GetShader("texturedMesh.frag", defines), null, new RasterizerState(CullfaceState.None), new DepthState());
			_psoGenOpacity = new ComputePipeline(Device, Device.GetShader("voxelGenOpacity.comp", definesGenOpacity));
			_psoFilterOpacity = new ComputePipeline(Device, Device.GetShader("voxelFilterOpacity.comp"));
			//_psoVoxelTest = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("voxelTest.frag", voxelDefines), null, new RasterizerState(CullfaceState.None), new DepthState(DepthFunction.Always, false), new BlendState(BlendMode.AlphaBlending));

			_psoVoxelLight = new ComputePipeline(Device, Device.GetShader("voxelLight.comp", voxelDefines));
			_psoVoxelLightFilter = new ComputePipeline(Device, Device.GetShader("voxel_temporal_filtering.comp", voxelDefines));

			_psoVoxelVisualize = new GraphicsPipeline(Device, Device.GetShader("voxelVisualize.vert"), Device.GetShader("voxelVisualize.frag"), null, null, Device.GetShader("voxelVisualize.geom"), null, new RasterizerState(CullfaceState.None), new DepthState(DepthFunction.Gequal, true), new BlendState(BlendMode.AlphaBlending));
		}

		public override void OnResize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{
			base.OnResize(displayWidth, displayHeight, renderWidth, renderHeight);
			_texVoxelLight?.Dispose();
			_texVoxelLightFiltered?.Dispose();
			_texVoxelTestDepth?.Dispose();

			_texVoxelLight = new Texture2D(Device, "VoxelLight", SizedInternalFormatGlob.RGBA16F, renderWidth / VoxelLightResDiv, renderHeight / VoxelLightResDiv, 1);
			_texVoxelLightFiltered = new Texture2D(Device, "VoxelLightFiltered", SizedInternalFormatGlob.RGBA16F, renderWidth, renderHeight, 1);
			_texVoxelLightFilteredHistory = new Texture2D(Device, "VoxelLightFiltered", SizedInternalFormatGlob.RGBA16F, renderWidth, renderHeight, 1);
			_texVoxelTestDepth = new Texture2D(Device, "VoxelTestDepth", SizedInternalFormatGlob.DEPTH24_STENCIL8, renderWidth, renderHeight, 1);

			_fboVoxelTest?.Dispose();
			
			_fboVoxelTest = new FrameBuffer();
		}

		public void Prepare()
		{
			if(VoxelData == null)
				return;

			var batches = RenderContext.InstanceManager.GetBatches(VoxelData.Batches);
			_meshProcessData = RenderContext.ComputeMeshProcessor.PrepareBatchesVoxelize(batches);
		}

		/*
		void CopyTex(Vec3i src, Vec3i dst, Vec3i size, int clipmap)
		{
			int offset = clipmap * VoxelResolution;

			if(
				src.X < 0 || src.Y < 0 || src.Z < 0 ||
				dst.X < 0 || dst.Y < 0 || dst.Z < 0 ||
				src.X + size.X > VoxelResolution || 
				src.Y + size.Y > VoxelResolution || 
				src.Z + size.Z > VoxelResolution ||
				dst.X + size.X > VoxelResolution ||
				dst.Y + size.Y > VoxelResolution ||
				dst.Z + size.Z > VoxelResolution
				)
			{
				throw new IndexOutOfRangeException("Texture copy parameters are out of texture bounds: src: " + src.ToString() + " dst: " + dst.ToString() + " size: " + size.ToString() + " clipmap: " + clipmap.ToString());
			}

			if(
				(src.X >= dst.X && src.X < dst.X + size.X || dst.X >= src.X && dst.X < src.X + size.X) &&
				(src.Y >= dst.Y && src.Y < dst.Y + size.Y || dst.Y >= src.Y && dst.Y < src.Y + size.Y) &&
				(src.Z >= dst.Z && src.Z < dst.Z + size.Z || dst.Z >= src.Z && dst.Z < src.Z + size.Z)
				)
			{
				throw new Exception("Texture copy regions overlap! src: " + src.ToString() + " dst: " + dst.ToString() + " size: " + size.ToString() + " clipmap: " + clipmap.ToString());
			}

			// TODO: Copy all textures
			GL.CopyImageSubData(
				_texVoxelOpacity.Handle, ImageTarget.Texture3D, 0,
				src.X + offset, src.Y, src.Z,
				_texVoxelOpacity.Handle, ImageTarget.Texture3D, 0,
				dst.X + offset, dst.Y, dst.Z,
				size.X, size.Y, size.Z
			);
		}

		// Move by <move> texels (a texel at [0,0] will be at [move.X,move.Y])
		void Scroll(Vec3i move, int clipmap)
		{
			GL.MemoryBarrier(MemoryBarrierFlags.TextureUpdateBarrierBit);

			var moveAbs = move.Abs();

			for(int i = 0; i < 3; i++)
			{
				Vec3i copySize = new Vec3i(VoxelResolution);
				copySize[i] = Math.Abs(move[i]);

				if(Math.Abs(move[i]) >= VoxelResolution)
					continue; // No need to scroll

				// Do nothing if move[i] == 0
				if(move[i] < 0)
				{
					for(int pos = moveAbs[i]; pos < VoxelResolution; pos += moveAbs[i])
					{
						int overlap = pos + moveAbs[i] - VoxelResolution;
						if(overlap > 0)
							copySize[i] -= overlap;

						Vec3i src = new Vec3i(0);
						Vec3i dst = new Vec3i(0);
						src[i] = pos;
						dst[i] = pos - moveAbs[i];

						CopyTex(src, dst, copySize, clipmap);
					}
				}
				if(move[i] > 0)
				{
					for(int pos = VoxelResolution - moveAbs[i]; pos >= moveAbs[i]; pos -= moveAbs[i])
					{
						int overlap = moveAbs[i] - pos;
						if(overlap > 0)
							copySize[i] -= overlap;

						Vec3i src = new Vec3i(0);
						Vec3i dst = new Vec3i(0);
						src[i] = pos - moveAbs[i];
						dst[i] = pos;

						CopyTex(src, dst, copySize, clipmap);
					}
				}
			}

			GL.MemoryBarrier(MemoryBarrierFlags.TextureUpdateBarrierBit);
		}
		*/

		void Scroll(Vec3i move, int clipmap)
		{
			GL.MemoryBarrier(MemoryBarrierFlags.TextureUpdateBarrierBit);

			var moveAbs = move.Abs();
			Vec3i src = new Vec3i(0);
			Vec3i dst = new Vec3i(0);
			Vec3i size = new Vec3i(0);

			for(int i = 0; i < 3; i++)
			{
				size[i] = VoxelResolution - moveAbs[i];

				if(move[i] > 0)
				{
					dst[i] = moveAbs[i];
				}
				if(move[i] < 0)
				{
					src[i] = moveAbs[i];
				}
			}

			var offset = clipmap * VoxelResolution;

			// Copy the entire image to the temp cube
			GL.CopyImageSubData(
				_texVoxelOpacity.Handle, ImageTarget.Texture3D, 0,
				offset, 0, 0,
				_texVoxelOpacityBuffer.Handle, ImageTarget.Texture3D, 0,
				0, 0, 0,
				VoxelResolution, VoxelResolution, VoxelResolution
			);

			// Copy the relevant part back
			GL.CopyImageSubData(
				_texVoxelOpacityBuffer.Handle, ImageTarget.Texture3D, 0,
				src.X, src.Y, src.Z,
				_texVoxelOpacity.Handle, ImageTarget.Texture3D, 0,
				dst.X + offset, dst.Y, dst.Z,
				size.X, size.Y, size.Z
			);

			GL.MemoryBarrier(MemoryBarrierFlags.TextureUpdateBarrierBit);
		}

		void Dispatch(Vec3i move)
		{
			Vec3i[] offsets = new Vec3i[3];
			Vec3i[] sizes = new Vec3i[3];

			Vec3i cubeMin = new Vec3i(0);
			Vec3i cubeMax = new Vec3i(VoxelResolution);

			for(int i = 0; i < 3; i++)
			{
				if(move[i] == 0)
				{
					sizes[i] = new Vec3i(0);
					continue;
				}

				sizes[i] = new Vec3i(VoxelResolution);
				sizes[i][i] = Math.Abs(move[i]);

				if(move[i] < 0)
				{
					offsets[i][i] = VoxelResolution - sizes[i][i];
				}

				// Clamp the range by AABB, so that one texture location is not processed multiple times
				for(int j = 0; j < i; j++)
				{
					int diff = cubeMin[j] - offsets[i][j];
					if(diff > 0)
					{
						offsets[i][j] += diff;
						sizes[i][j] -= diff;
					}

					int diffSize = (offsets[i][j] + sizes[i][j]) - cubeMax[j];
					if(diffSize > 0)
					{
						sizes[i][j] -= diff;
					}
				}

				// Remove the range from AABB
				if(move[i] > 0)
					cubeMin[i] = sizes[i][i];
				if(move[i] < 0)
					cubeMax[i] = VoxelResolution - sizes[i][i]; 
			}

			for(int i = 0; i < 3; i++)
			{
				if(sizes[i].X == 0 || sizes[i].Y == 0 || sizes[i].Z == 0)
					continue;
				Device.ShaderCompute.SetUniformI("posOffset", offsets[i].X, offsets[i].Y, offsets[i].Z);
				Device.ShaderCompute.SetUniformI("posSize", sizes[i].X, sizes[i].Y, sizes[i].Z);
				Device.DispatchComputeThreads(sizes[i].X, sizes[i].Y, sizes[i].Z);
			}
		}

		public void UpdateVoxels()
		{
			if(VoxelData != null)
			{
				_clipmapPosSize[VoxelData.ClipmapLevel] = new Vec4i(
					VoxelData.CubeIncludeCenter.X,
					VoxelData.CubeIncludeCenter.Y,
					VoxelData.CubeIncludeCenter.Z,
					VoxelData.CubeIncludeSize
					);
			}

			for(int i = 0; i < _clipmapPosSizeVec4.Length; i++)
			{
				_clipmapPosSizeVec4[i] = new Vector4(
					_clipmapPosSize[i].X - (int)RenderContext.CameraPosition.X,
					_clipmapPosSize[i].Y - (int)RenderContext.CameraPosition.Y,
					_clipmapPosSize[i].Z - (int)RenderContext.CameraPosition.Z,
					_clipmapPosSize[i].W	
				);
			}
			
			if(VoxelData == null)
				return;

			TimerQueryGPU timerVoxelize = new TimerQueryGPU(RenderContext.FeedbackManager, "Voxelize");
			timerVoxelize.StartQuery();
			
			// In texels
			Vec3i move = ((VoxelData.CubeExcludeCenter - VoxelData.CubeIncludeCenter) * VoxelResolution) / VoxelData.CubeIncludeSize / 2;

			var moveAbs = move.Abs();
			var moveMax = Math.Max(Math.Max(moveAbs.X, moveAbs.Y), moveAbs.Z);

			if(moveMax > VoxelResolution)
				move = new Vec3i(VoxelResolution, 0, 0);

			Scroll(move, VoxelData.ClipmapLevel);

			_texBuildOpacity.Clear(0, PixelFormat.RedInteger, PixelType.UnsignedInt, new uint[] { 0 });

			Matrix4[] matrixes = new Matrix4[3];

			Vector3 includePos = new Vector3(
				VoxelData.CubeIncludeCenter.X - (int)RenderContext.CameraPosition.X,
				VoxelData.CubeIncludeCenter.Y - (int)RenderContext.CameraPosition.Y,
				VoxelData.CubeIncludeCenter.Z - (int)RenderContext.CameraPosition.Z
				);
			Vector3 excludePos = new Vector3(
				VoxelData.CubeExcludeCenter.X - (int)RenderContext.CameraPosition.X,
				VoxelData.CubeExcludeCenter.Y - (int)RenderContext.CameraPosition.Y,
				VoxelData.CubeExcludeCenter.Z - (int)RenderContext.CameraPosition.Z
				);

			//Matrix4 translate = Matrix4.Identity;
			//translate.Row3.Xyz = -includePos;

			Vector3 translate = includePos;

			for(int i = 0; i < 3; i++)
			{
				float s = 1f / VoxelData.CubeIncludeSize;
				Vector3 scale = new Vector3(s);
				scale.Z *= 0.9f;

				Matrix4 matrix = Matrix4.Zero;

				if(i == 0)
				{
					// X
					//matrix[0, 1] = s;
					//matrix[1, 2] = s;
					//matrix[3, 3] = 1;
					matrix = Glob.Utils.GetCameraMatrixOrtho(translate, Vector3.UnitX, scale);
				}
				if(i == 1)
				{
					// Y
					//matrix[0, 0] = s;
					//matrix[1, 2] = s;
					//matrix[3, 3] = 1;
					matrix = Glob.Utils.GetCameraMatrixOrtho(translate, Vector3.UnitY, scale);
				}
				if(i == 2)
				{
					// Z
					//matrix[0,0] = s;
					//matrix[1,1] = s;
					//matrix[3,3] = 1;
					matrix = Glob.Utils.GetCameraMatrixOrtho(translate, Vector3.UnitZ, scale);
				}
				//matrix.Row2 *= 0.5f;
				//matrix.Row3.Z += 0.5f;

				//matrix = translate * matrix;

				matrixes[i] = matrix;
			}

			Action shared = () =>
			{
				RenderContext.Device.BindBufferBase(BufferRangeTarget.UniformBuffer, 0, RenderContext.MaterialUpdater.MaterialUbo);
				RenderContext.Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, RenderContext.VertexManager.MeshDataBuffer.Handle);
			};

			Action sharedMaterial = () =>
			{
				if(RenderContext.Device.ShaderFragment != null)
				{
					RenderContext.Device.ShaderFragment.SetUniformF("screenSize", new Vector4(RenderContext.RenderWidth, RenderContext.RenderHeight, 1f / RenderContext.RenderWidth, 1f / RenderContext.RenderHeight));
					RenderContext.Device.ShaderFragment.SetUniformF("vtTileBorderOffsetScale", RenderContext.VirtualTextureManager.VtTileBorderOffsetScale);
					RenderContext.Device.ShaderFragment.SetUniformF("vtTileScaleTexelScale", RenderContext.VirtualTextureManager.VtTileScaleTexelScale);
				}

				RenderContext.Device.BindTexture(RenderContext.VirtualTextureManager.TexIndirection, 1);
				RenderContext.Device.BindTexture(RenderContext.VirtualTextureManager.TexPhysicalCacheBaseColor, 2);
				RenderContext.Device.BindTexture(RenderContext.VirtualTextureManager.TexPhysicalCacheNormalMap, 3);
				RenderContext.Device.BindTexture(RenderContext.AlphaTextureManager.Atlas, 4);

				GL.DrawBuffer(DrawBufferMode.None);
			};

			GL.ClipControl(ClipOrigin.LowerLeft, ClipDepthMode.NegativeOneToOne);
			GL.Enable(EnableCap.Multisample);
			using(Device.BindFrameBufferPushViewport(_fboVoxelize, 0, 0, VoxelResolutionBuild, VoxelResolutionBuild, "Voxelization"))
			{
				RenderContext.ComputeMeshProcessor.ProcessMeshes(_meshProcessData, new MeshProcessCullParams()
				{
					ExcludeBoxCenter = excludePos,
					ExcludeBoxSize = new Vector3(VoxelData.CubeExcludeSize),
					IncludeBoxCenter = includePos,
					IncludeBoxSize = new Vector3(VoxelData.CubeIncludeSize),
					Resolution = new Vector2(VoxelResolutionBuild, VoxelResolutionBuild),
					SizeCullMatrixes = matrixes,
				},
					(draw) =>
					{
						Device.BindPipeline(_psoVoxelize);
						shared();
						sharedMaterial();
						Device.BindImage3D(0, _texBuildOpacity, TextureAccess.ReadWrite);
						Device.ShaderFragment.SetUniformI("voxelResolution", VoxelResolutionBuild);
						Device.ShaderFragment.SetUniformF("cubePosScale", new Vector4(includePos, VoxelData.CubeIncludeSize));
						//Device.ShaderFragment.SetUniformF("numSamplesRcp", 1.0f / VoxelizationMsaa);
						for(int i = 0; i < 3; i++)
						{
							Device.ShaderFragment.SetUniformI("maskShift", i * 8);
							Device.ShaderVertex.SetUniformF("projection", matrixes[i]);
							Device.ShaderFragment.SetUniformI("majorAxis", i);
							draw(i);
						}
					});
			}
			WorldRenderer.SetClipControl();
			GL.Disable(EnableCap.Multisample);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			Device.BindPipeline(_psoGenOpacity);
			Device.BindImage3D(0, _texVoxelOpacity, TextureAccess.WriteOnly);
			Device.BindImage3D(1, _texBuildOpacity, TextureAccess.ReadOnly);
			Device.ShaderCompute.SetUniformI("writeOffset", VoxelResolution * VoxelData.ClipmapLevel, 0, 0);
			Dispatch(move);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit | MemoryBarrierFlags.TextureUpdateBarrierBit);

			// Generate mip levels 64..8
			Device.BindPipeline(_psoFilterOpacity);

			//Device.BindImage3D(0, _texVoxelOpacity, TextureAccess.ReadOnly, 0);
			//Device.BindImage3D(1, _texVoxelOpacity, TextureAccess.WriteOnly, 1);
			//Device.BindImage3D(2, _texVoxelOpacity, TextureAccess.WriteOnly, 2);
			//Device.BindImage3D(3, _texVoxelOpacity, TextureAccess.WriteOnly, 3);
			//Device.ShaderCompute.SetUniformI("offset", VoxelResolution * VoxelData.ClipmapLevel / 2, 0, 0);
			//Device.DispatchComputeThreads(VoxelResolution / 2, VoxelResolution / 2, VoxelResolution / 2);

			//GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit | MemoryBarrierFlags.TextureUpdateBarrierBit);

			// Size of a larger clipmap is always double than that of the smaller clipmap
			// Relative clipmap positions in texels are always a multiple of 2

			// Generate clipmap mips and copy mip1 of each clipmap to a region of mip0 of the next clipmap
			for(int i = 0; i < ClipmapLevels; i++)
			{
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit | MemoryBarrierFlags.TextureUpdateBarrierBit);

				Device.BindImage3D(0, _texVoxelOpacity, TextureAccess.ReadOnly, 0);
				Device.BindImage3D(1, _texVoxelOpacity, TextureAccess.WriteOnly, 1);
				Device.BindImage3D(2, _texVoxelOpacity, TextureAccess.WriteOnly, 2);
				Device.BindImage3D(3, _texVoxelOpacity, TextureAccess.WriteOnly, 3);
				Device.ShaderCompute.SetUniformI("offset", VoxelResolution * i / 2, 0, 0);
				Device.DispatchComputeThreads(VoxelResolution / 2, VoxelResolution / 2, VoxelResolution / 2);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit | MemoryBarrierFlags.TextureUpdateBarrierBit);

				if(i >= ClipmapLevels - 1)
					continue; // There is no succeeding clipmap
				if(_clipmapPosSize[i].W == 0 || _clipmapPosSize[i + 1].W == 0)
					continue;

				// World minmax of this clipmap
				Vec3i boxMin = _clipmapPosSize[i].Xyz - _clipmapPosSize[i].W;
				Vec3i boxMax = _clipmapPosSize[i].Xyz + _clipmapPosSize[i].W;

				Vec3i boxCorner = boxMin;

				Vec3i nextMin = _clipmapPosSize[i + 1].Xyz - _clipmapPosSize[i + 1].W;
				Vec3i nextMax = _clipmapPosSize[i + 1].Xyz + _clipmapPosSize[i + 1].W;

				boxMin = Vec3i.Clamp(boxMin, nextMin, nextMax);
				boxMax = Vec3i.Clamp(boxMax, nextMin, nextMax);

				Vec3i size = (boxMax - boxMin) * VoxelResolution / (_clipmapPosSize[i + 1].W * 2);

				if(size.X == 0 || size.Y == 0 || size.Z == 0)
					continue;

				Vec3i read = (boxMin - boxCorner) * VoxelResolution / (_clipmapPosSize[i + 1].W * 2);
				Vec3i write = (boxMin - nextMin) * VoxelResolution / (_clipmapPosSize[i + 1].W * 2);

				if(read.X >= VoxelResolution / 2 || read.Y >= VoxelResolution / 2 || read.Z >= VoxelResolution / 2)
					continue; // Read coords are outside the clipmap

				read.X += VoxelResolution / 2 * i;
				write.X += VoxelResolution * (i + 1);

				GL.CopyImageSubData(
					_texVoxelOpacity.Handle, ImageTarget.Texture3D, 1,
					read.X, read.Y, read.Z,
					_texVoxelOpacity.Handle, ImageTarget.Texture3D, 0,
					write.X, write.Y, write.Z,
					size.X, size.Y, size.Z
				);
			}

			// Finish mipmap generation 8..1
			Device.BindImage3D(0, _texVoxelOpacity, TextureAccess.ReadOnly, 3);
			Device.BindImage3D(1, _texVoxelOpacity, TextureAccess.WriteOnly, 4);
			Device.BindImage3D(2, _texVoxelOpacity, TextureAccess.WriteOnly, 5);
			Device.BindImage3D(3, _texVoxelOpacity, TextureAccess.WriteOnly, 6);
			Device.ShaderCompute.SetUniformI("offset", VoxelResolution * VoxelData.ClipmapLevel / 16, 0, 0);

			Device.DispatchComputeThreads(VoxelResolution * ClipmapLevels / 16, VoxelResolution / 16, VoxelResolution / 16);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit | MemoryBarrierFlags.TextureUpdateBarrierBit);

			timerVoxelize.EndQuery();
		}

		public void RenderOcclusion(GbufferModule gbuffer)
		{
			var temp = _texVoxelLightFilteredHistory;
			_texVoxelLightFilteredHistory = _texVoxelLightFiltered;
			_texVoxelLightFiltered = temp;

			Vec2i offset = new Vec2i(0);

			if(VoxelLightResDiv == 2)
			{
				offset = new Vec2i((int)(RenderContext.FrameNumber & 1), (int)(RenderContext.FrameNumber & 2) - 1);
			}
			if(VoxelLightResDiv == 4)
			{
				if((RenderContext.FrameNumber & 1) > 0)
					offset.Y += 2;
				if((RenderContext.FrameNumber & 2) >> 1 != (RenderContext.FrameNumber & 1))
					offset.X += 2;

				if((RenderContext.FrameNumber & 3) > 0)
					offset.Y += 1;
				if((RenderContext.FrameNumber & 4) >> 2 != (RenderContext.FrameNumber & 3))
					offset.X += 1;

			}
			//Vec2i offset = new Vec2i((int)(RenderContext.FrameNumber & 1), (int)(RenderContext.FrameNumber & 2) - 1);

			TimerQueryGPU timerLight = new TimerQueryGPU(RenderContext.FeedbackManager, "VoxelLight");
			TimerQueryGPU timerFilter = new TimerQueryGPU(RenderContext.FeedbackManager, "VoxelFilter");

			timerLight.StartQuery();

			Device.BindPipeline(_psoVoxelLight);
			Device.BindImage2D(0, _texVoxelLight, TextureAccess.WriteOnly);
			Device.ShaderCompute.SetUniformF("ray00", RenderContext.CameraRay00);
			Device.ShaderCompute.SetUniformF("ray01", RenderContext.CameraRay01);
			Device.ShaderCompute.SetUniformF("ray10", RenderContext.CameraRay10);
			Device.ShaderCompute.SetUniformF("ray11", RenderContext.CameraRay11);
			Device.BindTexture(gbuffer.TextureLinearDepth, 0);
			Device.BindTexture(gbuffer.TextureGbufferNormal, 1);
			Device.BindTexture(_texVoxelOpacity, 2);
			Glob.Utils.SetUniform(Device.ShaderCompute, "voxelClipmapParams", _clipmapPosSizeVec4);
			Device.ShaderCompute.SetUniformF("resolutionRcp", new Vector2(1f / _texVoxelLightFiltered.Width, 1f / _texVoxelLightFiltered.Height));

			Device.ShaderCompute.SetUniformI("resolutionOffsetScale", offset.X, offset.Y, VoxelLightResDiv);
			Device.ShaderCompute.SetUniformF("temporalAngleOffset", TemporalAngleOffsets[(int)(RenderContext.FrameNumber % TemporalAngleOffsets.Length)]);

			Device.DispatchComputeThreads(_texVoxelLight.Width, _texVoxelLight.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			timerLight.EndQuery();

			// Filter

			var prevPos = (Vector3)(RenderContext.CameraPositionHistory - RenderContext.CameraPosition);

			var plane = new Vector4(RenderContext.CameraMatrixHistory.Column2.Xyz,
				Vector3.Dot(RenderContext.CameraMatrixHistory.Column2.Xyz, prevPos));

			var history = RenderContext.Camera.CameraHistory.ToCameraMatrix(prevPos) *
						  RenderContext.ProjectionMatrixNoReverseDepthHistory;

			timerFilter.StartQuery();

			Device.BindPipeline(_psoVoxelLightFilter);

			Device.ShaderCompute.SetUniformF("sizeRcp", new Vector2(1f / _texVoxelLightFiltered.Width, 1f / _texVoxelLightFiltered.Height));
			Device.ShaderCompute.SetUniformF("matPrevious", history);
			Device.ShaderCompute.SetUniformF("cameraPlanePrevious", plane);

			Device.ShaderCompute.SetUniformI("resolutionOffsetScale", offset.X, offset.Y, VoxelLightResDiv);

			Device.ShaderCompute.SetUniformF("cameraRay00", RenderContext.CameraRay00.Xyz);
			Device.ShaderCompute.SetUniformF("cameraRay01", RenderContext.CameraRay01.Xyz);
			Device.ShaderCompute.SetUniformF("cameraRay10", RenderContext.CameraRay10.Xyz);
			Device.ShaderCompute.SetUniformF("cameraRay11", RenderContext.CameraRay11.Xyz);

			Device.BindImage2D(0, _texVoxelLightFiltered, TextureAccess.WriteOnly);
			Device.BindTexture(gbuffer.TextureLinearDepth, 0);
			Device.BindTexture(gbuffer.TextureGbufferNormal, 1);
			Device.BindTexture(_texVoxelLight, 2);
			Device.BindTexture(_texVoxelLightFilteredHistory, 3);
			Device.BindTexture(gbuffer.TextureLinearDepthHistory, 4);

			Device.ShaderCompute.SetUniformI("dataImageSize", _texVoxelLight.Width, _texVoxelLight.Height);
			Device.ShaderCompute.SetUniformI("depthImageSize", gbuffer.TextureLinearDepth.Width, gbuffer.TextureLinearDepth.Height);

			Device.ShaderCompute.SetUniformF("inTexel", new Vector2((float)offset.X / VoxelLightResDiv, (float)offset.Y / VoxelLightResDiv));

			Device.DispatchComputeThreads(_texVoxelLightFiltered.Width, _texVoxelLightFiltered.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			timerFilter.EndQuery();
		}

		public void RenderVoxelTest(Texture2D texSceneColor)
		{
			if(VoxelData != null)
				_lastDebugClipmap = VoxelData.DebugClipmapLevel;

			RenderContext.Device.BindFrameBuffer(_fboVoxelTest, FramebufferTarget.DrawFramebuffer);
			_fboVoxelTest.Attach(FramebufferAttachment.DepthStencilAttachment, _texVoxelTestDepth);
			_fboVoxelTest.Attach(FramebufferAttachment.ColorAttachment0, texSceneColor);

			using(Device.BindFrameBufferPushViewport(_fboVoxelTest, "Voxel Debug"))
			{
				GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

				Device.BindPipeline(_psoVoxelVisualize);
				Device.ShaderVertex.SetUniformI("voxelResolutionPow", VoxelResolutionPow);
				Device.ShaderGeometry.SetUniformI("voxelResolutionPow", VoxelResolutionPow);
				Device.ShaderGeometry.SetUniformI("voxelReadOffset", VoxelResolution * _lastDebugClipmap);
				Device.ShaderGeometry.SetUniformF("voxelClipmapParams", _clipmapPosSizeVec4[_lastDebugClipmap]);
				Device.ShaderGeometry.SetUniformF("projection", RenderContext.ProjectionCameraMatrix);
				Device.BindTexture(_texVoxelOpacity, 0);
				GL.DrawArrays(PrimitiveType.Points, 0, VoxelResolution * VoxelResolution * VoxelResolution);
			}

			//return;
			//Device.BindPipeline(_psoVoxelTest);
			//Device.BindTexture(_texVoxelOpacity, 0);
			//Glob.Utils.SetUniform(Device.ShaderFragment, "voxelClipmapParams", _clipmapPosSizeVec4);
			//Device.ShaderFragment.SetUniformF("cameraPos", RenderContext.CameraPosition.Offset);
			//Device.ShaderFragment.SetUniformF("ray00", RenderContext.CameraRay00);
			//Device.ShaderFragment.SetUniformF("ray01", RenderContext.CameraRay01);
			//Device.ShaderFragment.SetUniformF("ray10", RenderContext.CameraRay10);
			//Device.ShaderFragment.SetUniformF("ray11", RenderContext.CameraRay11);
			//GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
