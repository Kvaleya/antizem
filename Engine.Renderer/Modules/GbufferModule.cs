﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Structures;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class GbufferModule : RenderModule
	{
		public const int DepthReprojectedSizeX = 512;
		public const int DepthReprojectedSizeY = DepthReprojectedSizeX / 2;

		public FrameBuffer FrameBufferColorResult;
		public FrameBuffer FrameBufferColorResultAlternative;
		public FrameBuffer FrameBufferGbuffer;

		CyclicArray<Texture2D> _texLinearDepthArray;

		public Texture2D TextureLinearDepth { get { return _texLinearDepthArray[0]; } }
		public Texture2D TextureLinearDepthHistory { get { return _texLinearDepthArray[1]; } }
		public Texture2D TextureDepthStencil;
		public Texture2D TextureSceneColor;
		public Texture2D TextureSceneColorAlternative;
		public Texture2D TextureGbufferBaseColorMetallic;
		public Texture2D TextureGbufferNormal;
		public Texture2D TextureGbufferMaterial;

		public Texture2D TextureDepthReprojected;
		private Texture2D _textureDepthReprojectedInt;

		private ComputePipeline _psoClearImage;
		private ComputePipeline _psoDepthReproject;
		private ComputePipeline _psoDepthMipgenMin;
		private ComputePipeline _psoDepthMipgenAvg;
		private ComputePipeline _psoDepthLinearize;

		public GbufferModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			_texLinearDepthArray = new CyclicArray<Texture2D>(4);

			TextureDepthReprojected = new Texture2D(Device, "Gbuffer Depth Reprojected", SizedInternalFormatGlob.R32F, DepthReprojectedSizeX, DepthReprojectedSizeY, 0);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);

			_textureDepthReprojectedInt = new Texture2D(Device, "Gbuffer Depth Reprojeced (R16ui View)", SizedInternalFormatGlob.R32I, TextureDepthReprojected, 0, 1, 0);

			_psoClearImage = new ComputePipeline(Device, Device.GetShader("clearImage.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("FORMAT", "r32f"),
				new Tuple<string, string>("IMAGE", "image2D"),
				new Tuple<string, string>("VALUE", "vec4(DEPTH_NEAR)"),
				new Tuple<string, string>("SIZEX", "16"),
				new Tuple<string, string>("SIZEY", "16"),
				new Tuple<string, string>("SIZEZ", "1"),
			}));
			_psoDepthReproject = new ComputePipeline(Device, Device.GetShader("depth_reproject.comp"));
			_psoDepthReproject.ShaderCompute.SetUniformI("depth_previous", 0);
			_psoDepthMipgenMin = new ComputePipeline(Device, Device.GetShader("depthMips.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("FILTERFUNC(a, b, c, d)", "min(min(a, b), min(c, d))")
			}));
			_psoDepthMipgenAvg = new ComputePipeline(Device, Device.GetShader("depthMips.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("FILTERFUNC(a, b, c, d)", "((a + b + c + d) * 0.25)")
			}));
			_psoDepthLinearize = new ComputePipeline(Device, Device.GetShader("depth_linearize.comp"));
		}

		public override void OnResize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{
			base.OnResize(displayWidth, displayHeight, renderWidth, renderHeight);

			FrameBufferColorResult?.Dispose();
			FrameBufferColorResultAlternative?.Dispose();
			FrameBufferGbuffer?.Dispose();

			TextureDepthStencil?.Dispose();
			TextureSceneColor?.Dispose();
			TextureSceneColorAlternative?.Dispose();

			TextureGbufferBaseColorMetallic?.Dispose();
			TextureGbufferNormal?.Dispose();
			TextureGbufferMaterial?.Dispose();

			TextureDepthStencil = new Texture2D(RenderContext.Device, "Gbuffer Depth", SizedInternalFormatGlob.DEPTH32F_STENCIL8, renderWidth, renderHeight, 1);
			TextureSceneColor = new Texture2D(RenderContext.Device, "Gbuffer Color 1", SizedInternalFormatGlob.RGBA16F, renderWidth, renderHeight, 1);
			TextureSceneColorAlternative = new Texture2D(RenderContext.Device, "Gbuffer Color 2", SizedInternalFormatGlob.RGBA16F, renderWidth, renderHeight, 1);

			TextureGbufferBaseColorMetallic = new Texture2D(RenderContext.Device, "Gbuffer BaseColorMetallic", SizedInternalFormatGlob.RGBA8, renderWidth, renderHeight, 1);
			TextureGbufferNormal = new Texture2D(RenderContext.Device, "Gbuffer Normal", SizedInternalFormatGlob.RGB10_A2, renderWidth, renderHeight, 1);
			TextureGbufferMaterial = new Texture2D(RenderContext.Device, "Gbuffer Material", SizedInternalFormatGlob.RGBA8, renderWidth, renderHeight, 1);

			for(int i = 0; i < _texLinearDepthArray.Length; i++)
			{
				_texLinearDepthArray[i]?.Dispose();
				_texLinearDepthArray[i] = new Texture2D(RenderContext.Device, "Gbuffer Depth Linear " + i.ToString(), SizedInternalFormatGlob.R32F, renderWidth, renderHeight, 0);
			}

			Glob.Utils.SetTextureParameters(Device, TextureLinearDepth, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.LinearMipmapLinear);
			Glob.Utils.SetTextureParameters(Device, TextureLinearDepthHistory, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.LinearMipmapLinear);

			Glob.Utils.SetTextureParameters(Device, TextureSceneColor, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			Glob.Utils.SetTextureParameters(Device, TextureSceneColorAlternative, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);

			FrameBufferColorResult = new FrameBuffer(); // TODO: rewrite fbo so that it does not need to be bound when attaching images
			RenderContext.Device.BindFrameBuffer(FrameBufferColorResult, FramebufferTarget.DrawFramebuffer);
			FrameBufferColorResult.Attach(FramebufferAttachment.DepthStencilAttachment, TextureDepthStencil);
			FrameBufferColorResult.Attach(FramebufferAttachment.ColorAttachment0, TextureSceneColor);

			FrameBufferColorResultAlternative = new FrameBuffer();
			RenderContext.Device.BindFrameBuffer(FrameBufferColorResultAlternative, FramebufferTarget.DrawFramebuffer);
			FrameBufferColorResultAlternative.Attach(FramebufferAttachment.DepthStencilAttachment, TextureDepthStencil);
			FrameBufferColorResultAlternative.Attach(FramebufferAttachment.ColorAttachment0, TextureSceneColorAlternative);

			FrameBufferGbuffer = new FrameBuffer();
			RenderContext.Device.BindFrameBuffer(FrameBufferGbuffer, FramebufferTarget.DrawFramebuffer);
			FrameBufferGbuffer.Attach(FramebufferAttachment.DepthStencilAttachment, TextureDepthStencil);
			FrameBufferGbuffer.Attach(FramebufferAttachment.ColorAttachment0, TextureGbufferBaseColorMetallic);
			FrameBufferGbuffer.Attach(FramebufferAttachment.ColorAttachment1, TextureGbufferNormal);
			FrameBufferGbuffer.Attach(FramebufferAttachment.ColorAttachment2, TextureGbufferMaterial);

			Utils.CheckFramebufferErrors(RenderContext.Device);
		}

		public void ProcessDepth()
		{
			_texLinearDepthArray.Shift--;

			using(Device.DebugMessageManager.PushGroupMarker("DepthProcess"))
			{
				Device.BindPipeline(_psoDepthLinearize);
				Device.BindTexture(TextureDepthStencil, 0);
				Device.BindImage2D(0, TextureLinearDepth, TextureAccess.WriteOnly);
				Device.ShaderCompute.SetUniformF("viewSizeRcp", new Vector2(1f / TextureDepthStencil.Width, 1f / TextureDepthStencil.Height));
				Device.ShaderCompute.SetUniformF("invProjCam", RenderContext.ProjectionCameraMatrix.Inverted());
				Device.ShaderCompute.SetUniformF("cameraPlane", RenderContext.CameraPlane);

				Device.DispatchComputeThreads(TextureLinearDepth.Width, TextureLinearDepth.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

				Device.BindPipeline(_psoDepthMipgenAvg);
				RenderContext.Device.BindImage2D(4, TextureLinearDepth, TextureAccess.ReadOnly, 0);
				BindMipIfExists(0, TextureLinearDepth, TextureAccess.WriteOnly, 1);
				BindMipIfExists(1, TextureLinearDepth, TextureAccess.WriteOnly, 2);
				BindMipIfExists(2, TextureLinearDepth, TextureAccess.WriteOnly, 3);
				BindMipIfExists(3, TextureLinearDepth, TextureAccess.WriteOnly, 4);
				Device.DispatchComputeThreads(TextureLinearDepth.Width >> 1, TextureLinearDepth.Height >> 1);

				if(TextureLinearDepth.Levels > 5)
				{
					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

					RenderContext.Device.BindImage2D(4, TextureLinearDepth, TextureAccess.ReadOnly, 4);
					BindMipIfExists(0, TextureLinearDepth, TextureAccess.WriteOnly, 5);
					BindMipIfExists(1, TextureLinearDepth, TextureAccess.WriteOnly, 6);
					BindMipIfExists(2, TextureLinearDepth, TextureAccess.WriteOnly, 7);
					BindMipIfExists(3, TextureLinearDepth, TextureAccess.WriteOnly, 8);
					Device.DispatchComputeThreads(TextureLinearDepth.Width >> 5, TextureLinearDepth.Height >> 5);
				}

				if(TextureLinearDepth.Levels > 9)
				{
					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

					RenderContext.Device.BindImage2D(4, TextureLinearDepth, TextureAccess.ReadOnly, 8);
					BindMipIfExists(0, TextureLinearDepth, TextureAccess.WriteOnly, 9);
					BindMipIfExists(1, TextureLinearDepth, TextureAccess.WriteOnly, 10);
					BindMipIfExists(2, TextureLinearDepth, TextureAccess.WriteOnly, 11);
					BindMipIfExists(3, TextureLinearDepth, TextureAccess.WriteOnly, 12);
					Device.DispatchComputeThreads(TextureLinearDepth.Width >> 9, TextureLinearDepth.Height >> 9);
				}

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
			}
		}

		public void SwapSceneColorBuffers()
		{
			var temptex = TextureSceneColor;
			TextureSceneColor = TextureSceneColorAlternative;
			TextureSceneColorAlternative = temptex;

			var tempfbo = FrameBufferColorResult;
			FrameBufferColorResult = FrameBufferColorResultAlternative;
			FrameBufferColorResultAlternative = tempfbo;
		}

		void BindMipIfExists(int bindingPoint, Texture tex, TextureAccess access, int level)
		{
			if(level >= tex.Levels)
				tex = null;

			RenderContext.Device.BindImage2D(bindingPoint, tex, access, level);
		}

		// Must be called BEFORE clearing the depth buffer for rendering - GbufferModule does not store last frame's depth
		public void CreateReprojectedDepth()
		{
			// Clear reprojected depth to a big number
			RenderContext.Device.BindPipeline(_psoClearImage);
			RenderContext.Device.BindImage2D(0, TextureDepthReprojected, TextureAccess.WriteOnly);
			Device.DispatchComputeThreads(DepthReprojectedSizeX, DepthReprojectedSizeY);

			Vector3 historyPos = (Vector3)(RenderContext.CameraPositionHistory - RenderContext.CameraPosition);
			var history = RenderContext.Camera.CameraHistory.ToCameraMatrix(historyPos) * RenderContext.ProjectionMatrixHistory;

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			// Reproject last frame's depth
			RenderContext.Device.BindPipeline(_psoDepthReproject);
			RenderContext.Device.BindImage2D(0, _textureDepthReprojectedInt, TextureAccess.ReadWrite);
			RenderContext.Device.BindTexture(TextureDepthStencil, 0);
			RenderContext.Device.ShaderCompute.SetUniformF("resolution_source_rcp", new Vector2(1f / TextureDepthStencil.Width, 1f / TextureDepthStencil.Height));
			RenderContext.Device.ShaderCompute.SetUniformF("resolution_reprojected", new Vector2(_textureDepthReprojectedInt.Width, _textureDepthReprojectedInt.Height));
			RenderContext.Device.ShaderCompute.SetUniformF("proj_previous", history.Inverted());
			RenderContext.Device.ShaderCompute.SetUniformF("proj_current", RenderContext.ProjectionCameraMatrix);
			Device.DispatchComputeThreads(TextureDepthStencil.Width, TextureDepthStencil.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			// Generate mip chain of reprojected depth with min filter (choose the farther depth when using reverse z)
			RenderContext.Device.BindPipeline(_psoDepthMipgenMin);
			RenderContext.Device.BindImage2D(4, TextureDepthReprojected, TextureAccess.ReadOnly, 0);
			RenderContext.Device.BindImage2D(0, TextureDepthReprojected, TextureAccess.WriteOnly, 1);
			RenderContext.Device.BindImage2D(1, TextureDepthReprojected, TextureAccess.WriteOnly, 2);
			RenderContext.Device.BindImage2D(2, TextureDepthReprojected, TextureAccess.WriteOnly, 3);
			RenderContext.Device.BindImage2D(3, TextureDepthReprojected, TextureAccess.WriteOnly, 4);
			Device.DispatchComputeThreads(TextureDepthReprojected.Width >> 1, TextureDepthReprojected.Height >> 1);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			RenderContext.Device.BindImage2D(4, TextureDepthReprojected, TextureAccess.ReadOnly, 4);
			RenderContext.Device.BindImage2D(0, TextureDepthReprojected, TextureAccess.WriteOnly, 5);
			RenderContext.Device.BindImage2D(1, TextureDepthReprojected, TextureAccess.WriteOnly, 6);
			RenderContext.Device.BindImage2D(2, TextureDepthReprojected, TextureAccess.WriteOnly, 7);
			RenderContext.Device.BindImage2D(3, TextureDepthReprojected, TextureAccess.WriteOnly, 8);
			Device.DispatchComputeThreads(TextureDepthReprojected.Width >> 5, TextureDepthReprojected.Height >> 5);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
		}
	}
}
