﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class FxaaModule : RenderModule
	{
		GraphicsPipeline _psoFxaa;

		public FxaaModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			_psoFxaa = new GraphicsPipeline(Device, Device.GetShader("fxaa.vert"), Device.GetShader("fxaa.frag", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("CUSTOM_LUMA", "FxaaFloat FxaaLuma(FxaaFloat4 rgba) { return dot(rgba.rgb, FxaaFloat3(0.299, 0.587, 0.114)); }"),
				//new Tuple<string, string>("CUSTOM_LUMA", "FxaaFloat FxaaLuma(FxaaFloat4 rgba) { return rgba.g; }"),
				new Tuple<string, string>("FXAA_QUALITY__PRESET", "39"),
				new Tuple<string, string>("FXAA_GLSL_130", "1"),
				new Tuple<string, string>("FXAA_PC", "1"),
				//new Tuple<string, string>("FXAA_DISCARD", "1"),
				new Tuple<string, string>("FXAA_GATHER4_ALPHA", "0"),
			}), null, new RasterizerState(), new DepthState());
		}

		public void Render(Texture2D tex)
		{
			var queryFxaa = new TimerQueryGPU(RenderContext.FeedbackManager, "FXAA");
			queryFxaa.StartQuery();

			Device.BindPipeline(_psoFxaa);
			Device.ShaderFragment.SetUniformF("screenrcp", new Vector2(1f / tex.Width, 1f / tex.Height));
			Device.BindTexture(tex, 0);
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			queryFxaa.EndQuery();
		}
	}
}
