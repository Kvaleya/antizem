﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using GameFramework;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Modules
{
	class UIModule : RenderModule
	{
		[InjectProperty]
		public UIFrameData UiFrameData { get; set; }

		[InjectProperty]
		public FontTextureData FontTextureData { get; set; }

		StreamBuffer _streamBuffer;
		int _buffer;
		int _copyOffset;
		int _copySize;

		VertexBufferSource _vboSource;
		VertexBufferFormat _vboFormat;

		GraphicsPipeline _psoText;

		Texture2D _texFontAtlas;

		public UIModule(Renderer.RenderContext renderContext)
			: base(renderContext)
		{
			_streamBuffer = new StreamBuffer("UIUploadBuffer", BufferTarget.CopyReadBuffer, VertexUI.Size * VertexUI.MaxVertices);

			_buffer = Glob.Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(_streamBuffer.Size), IntPtr.Zero, BufferStorageFlags.None, "UIVertices");

			_vboSource = new VertexBufferSource(0, new VertexBufferBinding(_buffer, 0, VertexUI.Size, 0));
			_vboFormat = new VertexBufferFormat(
				new VertexAttribDescription(0, 0, 4, 0, VertexAttribType.UnsignedShort, VertexAttribClass.Float, true),
				new VertexAttribDescription(1, 0, 2, 8, VertexAttribType.UnsignedShort, VertexAttribClass.Float, true),
				new VertexAttribDescription(2, 0, 4, 12, VertexAttribType.UnsignedByte, VertexAttribClass.Float, true)
				);
			
			_psoText = new GraphicsPipeline(Device, Device.GetShader("ui.vert"), Device.GetShader("text.frag"), _vboFormat, new RasterizerState(CullfaceState.None), new DepthState(DepthFunction.Always, false), new BlendState(BlendMode.AlphaBlending));
			_psoText.ShaderFragment.SetUniformI("tex", 0);
		}

		public override void OnUploadPhase(FenceSync frameSync)
		{
			TimerQueryCPU query = new TimerQueryCPU(RenderContext.FeedbackManager, "UIUpload");
			query.StartQuery();

			base.OnUploadPhase(frameSync);

			if(_texFontAtlas == null && FontTextureData != null)
			{
				GL.BindBuffer(BufferTarget.PixelUnpackBuffer, 0);

				_texFontAtlas = new Texture2D(RenderContext.Device, "FontTexture", SizedInternalFormatGlob.R8,
					FontTextureData.WidthHeight, FontTextureData.WidthHeight);
				for(int i = 0; i < FontTextureData.Mips.Length; i++)
				{
					var mip = FontTextureData.Mips[i];
					_texFontAtlas.TexSubImage2D(RenderContext.Device, i, 0, 0, FontTextureData.WidthHeight >> i,
						FontTextureData.WidthHeight >> i, PixelFormat.Red, PixelType.UnsignedByte, mip);
				}
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapNearest);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMaxLevel, FontTextureData.Mips.Length - 1);
			}

			unsafe
			{
				fixed (VertexUI* ptrSource = &UiFrameData.Vertices[0])
				{
					Engine.Common.Utils.Unsafe.CisMemcpy(_streamBuffer.CurrentStart, (IntPtr)ptrSource, UiFrameData.VertexCount * VertexUI.Size);
				}
			}

			_copySize = UiFrameData.VertexCount * VertexUI.Size;
			_copyOffset = _streamBuffer.CurrentOffset;
			_streamBuffer.Advance(frameSync);

			query.EndQuery();
		}

		public override void OnCopyPhase()
		{
			Glob.Utils.CopyBuffer(_streamBuffer.Handle, _buffer, _copyOffset, 0, _copySize);
		}

		public void RenderUI()
		{
			var query = new TimerQueryGPU(RenderContext.FeedbackManager, "RenderUI");
			query.StartQuery();

			RenderContext.Device.BindPipeline(_psoText);
			RenderContext.Device.BindTexture(_texFontAtlas, 0);
			RenderContext.Device.BindVertexBufferSource(_vboSource);
			GL.DrawArrays(PrimitiveType.Triangles, 0, UiFrameData.VertexCount);

			query.EndQuery();
		}
	}
}
