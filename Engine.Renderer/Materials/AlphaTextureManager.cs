﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Materials;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Materials
{
	class AlphaTextureManager
	{
		// TODO: this could be used for any kind of textures with very little changes

		const int UploadTileSize = SmallestCell;
		const int UploadTileSizeBlocks = UploadTileSize / 4;
		const int UploadTileBytes = UploadTileSizeBlocks * UploadTileSizeBlocks * 8;
		const int SmallestCell = 64;
		const int MaxUploadsPerFrame = 16;

		class AlphaTextureItem : QuadtreeTextureItem
		{
			public IBcSurface Surface;
			public List<Resource<TextureMip>> MipResources;
			public int MinMip;

			public readonly int Mip0Size;

			public int LargestUsedMip
			{
				get
				{
					int largestMip = 0;
					int s = Mip0Size;

					while(s > NewSize)
					{
						largestMip++;
						s = s >> 1;
					}

					return largestMip;
				}
			}

			public AlphaTextureItem(IBcSurface surface, List<Resource<TextureMip>> tex)
			{
				Surface = surface;
				MipResources = tex;

				int s = Math.Max(surface.Width, surface.Height);

				Mip0Size = (s + SmallestCell - 1) / SmallestCell;

				Update();
			}

			public void Update()
			{
				SetDistance(MipResources[0].TopDistance);

				// Set max size to be no larger than the largest loaded mip
				int i = 0;

				int size = Mip0Size;

				while(i < MipResources.Count && MipResources[i].Item == null)
				{
					size = size >> 1;
					i++;
				}

				this.MaxSize = size;
			}

			public override void OnSizeChanged()
			{
				base.OnSizeChanged();
				Update();
			}

			public void SetDistance(float dist)
			{
				this.AreaImportance = 1 / (dist * dist);
			}
		}

		class UploadRequest
		{
			public int X, Y, Mip, TargetImageSize, TileSize = UploadTileSize;
			public bool Last = false;
			public AlphaTextureItem Target;
		}

		public Texture2D Atlas { get { return _quadTreeTexture.TextureAtlas; } }

		QuadtreeTexture<AlphaTextureItem> _quadTreeTexture;

		Dictionary<string, AlphaTextureItem> _fileToItemMap = new Dictionary<string, AlphaTextureItem>();

		List<AlphaTextureItem> _items = new List<AlphaTextureItem>();

		Renderer.RenderContext _context;
		Device _device;

		Heap<int, UploadRequest> _uploadHeap = new Heap<int, UploadRequest>(1);
		//Queue<UploadRequest> _uploadQueue = new Queue<UploadRequest>();
		List<UploadRequest> _uploadExecuted = new List<UploadRequest>();

		StreamBuffer _uploadBuffer;
		int _uploadOffset;

		public AlphaTextureManager(Renderer.RenderContext context, int texelSize, bool rectangular = false)
		{
			_context = context;
			_device = _context.Device;

			int levels = 1;
			int cellSize = 1;

			while(cellSize < (texelSize / SmallestCell))
			{
				levels++;
				cellSize *= 2;
			}

			_quadTreeTexture = new QuadtreeTexture<AlphaTextureItem>(_device, levels, rectangular, SmallestCell,
				SizedInternalFormatGlob.COMPRESSED_RED_RGTC1, "AlphaTextureAtlas", 0, 2048 / SmallestCell);
			_device.BindTexture(TextureTarget.Texture2D, _quadTreeTexture.TextureAtlas.Handle);
			Glob.Utils.SetTextureParameters(_device, _quadTreeTexture.TextureAtlas, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.LinearMipmapLinear);

			_uploadBuffer = new StreamBuffer("AlphaTextureUploadBuffer", BufferTarget.PixelUnpackBuffer, MaxUploadsPerFrame * UploadTileBytes);
		}

		public void GetSurfaceLocation(string filename, out int x, out int y, out int w, out int h, out int minMip, out int maxMip)
		{
			if(!_fileToItemMap.ContainsKey(filename))
			{
				AddSurface(filename);
			}

			GetSurfaceLocation(_fileToItemMap[filename], out x, out y, out w, out h, out minMip, out maxMip);
		}

		void GetSurfaceLocation(AlphaTextureItem item, out int x, out int y, out int w, out int h, out int minMip, out int maxMip)
		{
			x = 0;
			y = 0;
			w = 0;
			h = 0;
			minMip = 0;
			maxMip = 0;

			if(item == null)
				return;

			minMip = item.MinMip;

			double aspect = item.Surface.Width / (double)item.Surface.Height;

			int cellsize = _quadTreeTexture.SmallestCellTexels;

			x = item.X * cellsize;
			y = item.Y * cellsize;

			int s = item.Size * cellsize;

			if(aspect >= 1)
			{
				w = s;
				h = (int)(s / aspect);
			}
			else
			{
				w = (int)(s * aspect);
				h = s;
			}

			maxMip = 0;
			int maxMipTempSize = s;

			while(maxMipTempSize > 4)
			{
				maxMipTempSize /= 2;
				maxMip++;
			}
		}

		void AddSurface(string filename)
		{
			var surface = _context.MaterialManager.GetSurface(filename);

			if(surface == null)
			{
				// Surface cannot be loaded
				_fileToItemMap.Add(filename, null);
				return;
			}

			List<Resource<TextureMip>> mips = new List<Resource<TextureMip>>();

			for(int i = 0; i < surface.NumMips; i++)
			{
				mips.Add(_context.MaterialManager.GetMip(filename, i));
			}

			var item = new AlphaTextureItem(surface, mips);

			_items.Add(item);

			_fileToItemMap.Add(filename, item);
		}

		void QueueUploads(AlphaTextureItem item, int mip, int size)
		{
			int w = Math.Max(item.Surface.Width >> mip, 1);
			int h = Math.Max(item.Surface.Height >> mip, 1);

			int s = Math.Min(Math.Max(w, h), UploadTileSize);

			int cx = (w + UploadTileSize - 1) / UploadTileSize;
			int cy = (h + UploadTileSize - 1) / UploadTileSize;

			for(int y = 0; y < cy; y++)
			{
				for(int x = 0; x < cx; x++)
				{
					_uploadHeap.Add(mip, new UploadRequest()
					{
						X = x,
						Y = y,
						Mip = mip,
						Last = (x + 1 == cx && y + 1 == cy),
						TargetImageSize = size,
						Target = item,
						TileSize = s,
					});
				}
			}
		}

		public void Update()
		{
			for(int i = 0; i < _items.Count; i++)
			{
				_items[i].Update();
			}

			_quadTreeTexture.Update(_items);

			for(int i = 0; i < _items.Count; i++)
			{
				var item = _items[i];

				if(item.NewSize < item.Size)
				{
					int minMip = 0;
					int s = item.Size;

					while(s > item.NewSize)
					{
						s = s >> 1;
						minMip--;
					}

					item.MinMip += minMip;
				}

				if(item.NewSize > item.Size)
				{
					int minMip = 0;
					int s = item.Size;

					while(s < item.NewSize)
					{
						s = s << 1;
						if(s == 0)
							s = 1;
						minMip++;
					}

					int mipSize = item.Size << 1;

					int relativeMip = minMip;

					if(item.Size == 0)
					{
						mipSize = 1;

						// Queue the mip chain parts that are smaller that a single cell
						// 32, 16, 8, 4
						for(int j = 3; j >= 0; j--)
						{
							int m = relativeMip + j + item.LargestUsedMip;

							QueueUploads(item, m, 1);
						}

						item.MinMip += 4;
					}

					for(int j = relativeMip - 1; j >= 0; j--)
					{
						QueueUploads(item, j + item.LargestUsedMip, mipSize);
						mipSize = mipSize << 1;
					}

					item.MinMip += minMip;
				}

				item.MakeNewCellCurrent();
			}
		}

		public void UpdateUploads(FenceSync frameSync)
		{
			const int MaxTriesPerFrame = MaxUploadsPerFrame * 4;

			int uploads = 0;
			int attempts = 0;

			_uploadExecuted.Clear();

			List<UploadRequest> skipped = new List<UploadRequest>();

			while(attempts < MaxTriesPerFrame && uploads < MaxUploadsPerFrame && _uploadHeap.Count > 0)
			{
				attempts++;

				var u = _uploadHeap.Top.Value;

				_uploadHeap.RemoveTop();

				if(u.TargetImageSize > u.Target.Size)
				{
					// The item no longer uses this mip
					continue;
				}

				//if(u.Mip >= Atlas.Levels)
				//{
				//	// There are not enough mip levels in the actual texture
				//	continue;
				//}

				var mip = _context.MaterialManager.GetMip(u.Target.Surface.Filename, u.Mip).Item;

				if(mip == null)
				{
					// Mip is null, try again later
					skipped.Add(u);
					continue;
				}

				var bytes = mip.GetBlocksBytes(u.X * UploadTileSizeBlocks, u.Y * UploadTileSizeBlocks, u.TileSize / 4,
					u.TileSize / 4);

				Marshal.Copy(bytes, 0, IntPtr.Add(_uploadBuffer.CurrentStart, UploadTileBytes * _uploadExecuted.Count), bytes.Length);

				_uploadExecuted.Add(u);

				if(u.Last)
				{
					u.Target.MinMip--;
				}
				uploads++;
			}

			foreach(var u in skipped)
			{
				_uploadHeap.Add(u.Mip, u);
			}

			_uploadOffset = _uploadBuffer.CurrentOffset;
			_uploadBuffer.Advance(frameSync);
		}

		public void UpdateCopies()
		{
			using(_device.BindBuffer(BufferTarget.PixelUnpackBuffer, _uploadBuffer.Handle))
			{
				for(int i = 0; i < _uploadExecuted.Count; i++)
				{
					var u = _uploadExecuted[i];

					int level = u.Mip - u.Target.LargestUsedMip;

					// TODO: wtf

					int bytes = (u.TileSize / 4) * (u.TileSize / 4) * 8;

					int x = u.X * UploadTileSize + ((u.Target.X * _quadTreeTexture.SmallestCellTexels) >> level);
					int y = u.Y * UploadTileSize + ((u.Target.Y * _quadTreeTexture.SmallestCellTexels) >> level);

					_quadTreeTexture.TextureAtlas.CompressedTexSubImage2D(_device, level, x, y, u.TileSize, u.TileSize, bytes,
						new IntPtr(_uploadOffset + i * UploadTileBytes));
				}
			}
		}
	}
}
