﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Materials;

namespace Engine.Renderer.Materials.VirtualTextures
{
	class ImageMipRepository
	{
		VirtualTextureManager _vtManager;
		Renderer.RenderContext _context;

		public ImageMipRepository(Renderer.RenderContext context, VirtualTextureManager vtManager)
		{
			_context = context;
			_vtManager = vtManager;
		}

		public ImageMip GetMip(int x, int y, int mip)
		{
			// Try to add the page to load list
			var image = _vtManager.GetImage(x, y, mip);
			//var image = _vtManager.GetImage(entry.X, entry.Y);

			if(image == null)
				return null;

			TextureMip itemBaseColor = null, itemNormalMap = null;

			Resource<TextureMip> baseColorMip = _context.MaterialManager.GetMip(image.SourceFileBaseColor, mip);
			Resource<TextureMip> normalMapMip = _context.MaterialManager.GetMip(image.SourceFileNormalMap, mip);

			if(baseColorMip != null)
				itemBaseColor = baseColorMip.Item;
			if(normalMapMip != null)
				itemNormalMap = normalMapMip.Item;

			bool hasColor = baseColorMip != null;
			bool hasNormal = normalMapMip != null;

			if((itemBaseColor == null && hasColor) || (itemNormalMap == null && hasNormal))
				return null;

			return new ImageMip()
			{
				Image = image,
				BaseColorMip = itemBaseColor,
				NormalMapMip = itemNormalMap,
			};
		}
	}

	class ImageMip
	{
		public VirtualImage Image;
		public TextureMip BaseColorMip;
		public TextureMip NormalMapMip;
	}
}
