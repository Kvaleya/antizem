﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Materials;
using GameFramework;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Materials.VirtualTextures
{
	class VirtualImage : IQuadTreeItem
	{
		public readonly VirtualTextureManager Manager;
		readonly int _width, _height, _sizePow2;
		readonly int _tiles, _maxWholeMip;

		public readonly string SourceFileBaseColor;
		public readonly string SourceFileNormalMap;

		int _vtOffsetX = 0, _vtOffsetY = 0, _vtSize = 0;

		public bool IsAllocated { get { return _vtSize != 0; } }

		public int OffsetX { get { return _vtOffsetX; } }
		public int OffsetY { get { return _vtOffsetY; } }
		public int Size { get { return _vtSize; } }

		/// <summary>
		/// The last mip level that spans at least one whole tile
		/// </summary>
		public int MaxWholeMip { get { return _maxWholeMip; } }

		public double ScaleX { get { return _width / (double)_sizePow2; } }
		public double ScaleY { get { return _height / (double)_sizePow2; } }

		public float AreaImportance
		{
			get { return 1.0f; }
		}

		public int MaxSize
		{
			get { return _tiles; }
		}

		public VirtualImage(VirtualTextureManager manager, int w, int h, string fileBaseColor, string fileNormalMap)
		{
			SourceFileBaseColor = fileBaseColor;
			SourceFileNormalMap = fileNormalMap;

			Manager = manager;
			_width = w;
			_height = h;
			_sizePow2 = GreaterOrEqualPow2(Math.Max(w, h));
			_tiles = _sizePow2 / VirtualTextureManager.TileTexelsUsableSize;

			_maxWholeMip = 0;

			int t = _tiles;

			while(t > 1)
			{
				t /= 2;
				_maxWholeMip++;
			}
		}

		int GreaterOrEqualPow2(int x)
		{
			int p = 1;
			while(p < x)
				p = p << 1;
			return p;
		}

		public void SetNode(int x, int y, int size)
		{
			_vtOffsetX = x;
			_vtOffsetY = y;
			_vtSize = size;
		}

		public void SetNodeNull()
		{
			_vtOffsetX = 0;
			_vtOffsetY = 0;
			_vtSize = 0;
		}
	}

	class VirtualTextureManager
	{
		// TODO: generic virtual texture, reuse for terrain detail

		public const int TileBlocksUsableSize = 32;
		public const int TileBlocksBorder = 1;
		
		public const int TileBlocksPhysicalSize = TileBlocksUsableSize + TileBlocksBorder * 2;
		public const int TileTexelsUsableSize = TileBlocksUsableSize * 4;
		public const int TileTexelsBorderSize = TileBlocksBorder * 4;
		public const int TileTexelsPhysicalSize = TileBlocksPhysicalSize * 4;

		public const int TotalTileSizeBytesBc1 = TileBlocksPhysicalSize * TileBlocksPhysicalSize * 16 / 2;
		public const int TotalTileSizeBytesBc7 = TotalTileSizeBytesBc1 * 2;
		public const int TotalTileSizeBytesAllTextures = TotalTileSizeBytesBc1 * 3;
		const int UploadEntrySize = 4;

		const int FeedbackEntryBufferSize = 256;
		const int FeedbackBufferEntries = FeedbackEntryBufferSize / 4 - 1;

		QuadTreePacker<VirtualImage> _packer;

		public int CacheTexelSizeX { get; private set; }
		public int CacheTexelSizeY { get; private set; }

		public int PhysicalTilesX { get; private set; }
		public int PhysicalTilesY { get; private set; }
		
		public int VirtualPagesX { get; private set; }
		public int VirtualPagesY { get; private set; }

		public int VirtualLevels { get { return _packer.QuadTreeLevels; } }

		public Texture2D TexUsageMap { get { return _texUsageMap; } }
		public Texture2D TexFeedback { get { return _texFeedback; } }

		public Texture2D TexIndirection { get { return _texIndirection; } }
		public Texture2D TexPhysicalCacheBaseColor { get { return _texCacheBaseColor; } }
		public Texture2D TexPhysicalCacheNormalMap { get { return _texCacheNormalMap; } }

		public Vector4 VtTileBorderOffsetScale
		{
			get
			{
				return new Vector4(TileTexelsBorderSize / (float)_texCacheBaseColor.Width, TileTexelsBorderSize / (float)_texCacheBaseColor.Height,
					TileTexelsUsableSize / (float)_texCacheBaseColor.Width, TileTexelsUsableSize / (float)_texCacheBaseColor.Height);
			}
		}

		public Vector4 VtTileScaleTexelScale
		{
			get
			{
				return new Vector4(TileTexelsPhysicalSize / (float)_texCacheBaseColor.Width,
					TileTexelsPhysicalSize / (float)_texCacheBaseColor.Height, 1.0f / (float)_texCacheBaseColor.Width,
					1.0f / (float)_texCacheBaseColor.Height);
			}
		}

		readonly Renderer.RenderContext _context;
		Device _device { get { return _context.Device; } }

		Dictionary<string, VirtualImage> _imageMap = new Dictionary<string, VirtualImage>();
		List<VirtualImage> _allImages = new List<VirtualImage>();
		int _imageIndexToEternalize = 0;

		Texture2D _texIndirection;
		Texture2D _texIndirectionCopy;
		Texture2D _texFeedback;
		Texture2D _texUsageMap;
		Texture2D _texResidencyMap;

		Texture2D _texCacheBaseColor;
		Texture2D _texCacheNormalMap;

		ComputePipeline _psoMoveFeedback;
		ComputePipeline _psoGetFeedback;
		ComputePipeline _psoClearImage;
		ComputePipeline _psoGenMips;
		ComputePipeline _psoUpdateResidency;

		RingBufferSet _feedbackBuffers;
		int _currentFeedbackBuffer = 0;
		int _bufferFeedbackLocal;
		int _bufferZeroes;

		int _feedbackBufferOffset = 0;
		
		StreamBuffer _bufferIndirectionUpload;
		int _currentBufferIndirectionUploadOffset = 0;

		public UpdateLogic VtPageManager;
		IPageSource _pageSource;

		VTUpdateInfo _updateInfo;

		//List<Entry> _changes = new List<Entry>();

		public VirtualTextureManager(Renderer.RenderContext context, int virtualSize, int physicalSize, bool rectangular)
		{
			_pageSource = new TexturePageSource();

			_context = context;

			_packer = new QuadTreePacker<VirtualImage>(virtualSize + 1, false, int.MaxValue);

			VirtualPagesX = _packer.CellsX;
			VirtualPagesY = _packer.CellsY;

			_imageMap.Add(GetImageKey(null, null, null), null);

			CacheTexelSizeX = physicalSize;

			if(rectangular)
				CacheTexelSizeX *= 2;

			CacheTexelSizeY = physicalSize;

			PhysicalTilesX = CacheTexelSizeX / TileTexelsPhysicalSize;
			PhysicalTilesY = CacheTexelSizeY / TileTexelsPhysicalSize;

			VtPageManager = new UpdateLogic(context, this, new ImageMipRepository(context, this), _pageSource.MaxPageUpdates);

			// Textures
			_texFeedback = new Texture2D(_device, "VTFeedbackTex", SizedInternalFormatGlob.R32UI, 128, 64, 1);

			_texCacheBaseColor = new Texture2D(_device, "VTCacheBaseColor", SizedInternalFormatGlob.COMPRESSED_RGB_S3TC_DXT1_EXT, CacheTexelSizeX, CacheTexelSizeY, 1);
			_texCacheNormalMap = new Texture2D(_device, "VTCacheNormal", SizedInternalFormatGlob.COMPRESSED_RGBA_BPTC_UNORM, CacheTexelSizeX, CacheTexelSizeY, 1);

			Glob.Utils.SetTextureParameters(_device, _texCacheBaseColor, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			Glob.Utils.SetTextureParameters(_device, _texCacheNormalMap, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);

			_texIndirection = new Texture2D(_device, "VTIndirection", SizedInternalFormatGlob.R32UI, VirtualPagesX, VirtualPagesY, virtualSize + 1);
			_texIndirectionCopy = new Texture2D(_device, "VTIndirectionCopy", _texIndirection.Format, _texIndirection.Width,
				_texIndirection.Height, _texIndirection.Levels);

			_texIndirection.TexSubImage2D(_device, _texIndirection.Levels - 1, 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.UnsignedInt, new uint[]
			{
				0xF0000000,
			});

			_texUsageMap = new Texture2D(_device, "VTUsageMap", SizedInternalFormatGlob.R8UI,
				_texIndirection.Width + _texIndirection.Width / 2, _texIndirection.Height, 1);
			_texResidencyMap = new Texture2D(_device, "VTResidencyMap", _texUsageMap.Format, _texUsageMap.Width, _texUsageMap.Height,
				_texUsageMap.Levels);

			// One extra for null indirection
			_bufferIndirectionUpload = new StreamBuffer("VTEntryUploadBuffer", BufferTarget.ShaderStorageBuffer, UploadEntrySize * (_pageSource.MaxPageUpdates + 1));

			_feedbackBuffers = new RingBufferSet(BufferTarget.CopyWriteBuffer, FeedbackEntryBufferSize, "VTFeedbackBuffers", 5, BufferStorageFlags.MapReadBit | BufferStorageFlags.ClientStorageBit);

			uint[] zeroes = new uint[FeedbackEntryBufferSize / 4];

			_bufferZeroes = Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(FeedbackEntryBufferSize), zeroes,
				BufferStorageFlags.None, "VTFeedbackZeroes");
			_bufferFeedbackLocal = Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(FeedbackEntryBufferSize), IntPtr.Zero,
				BufferStorageFlags.None, "VTFeedbackWrite");
			
			Utils.CopyBuffer(_bufferZeroes, _bufferFeedbackLocal, 0, 0, FeedbackEntryBufferSize);

			// Shaders
			_psoGenMips = new ComputePipeline(_context.Device, _context.Device.GetShader("vt_indirection_mips.comp"));
			_psoMoveFeedback = new ComputePipeline(_context.Device, _context.Device.GetShader("vt_move_feedback.comp"));
			_psoGetFeedback = new ComputePipeline(_context.Device, _context.Device.GetShader("vt_get_feedback.comp"));
			_psoUpdateResidency = new ComputePipeline(_context.Device, _context.Device.GetShader("vt_update_residency.comp"));
			_psoClearImage = new ComputePipeline(_context.Device, _context.Device.GetShader("clearImage.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("IMAGE", "uimage2D"),
				new Tuple<string, string>("FORMAT", "r8ui"),
				new Tuple<string, string>("VALUE", "uvec4(0)"),
				new Tuple<string, string>("SIZEX", "16"),
				new Tuple<string, string>("SIZEY", "16"),
				new Tuple<string, string>("SIZEZ", "1"),
			}));
		}

		public VirtualImage GetImage(MaterialInject material)
		{
			var key = GetImageKey(material);

			if(_imageMap.ContainsKey(key))
				return _imageMap[key];

			var image = new VirtualImage(this, material.ImageWidth, material.ImageHeight, material.TextureBaseColor, material.TextureNormalRoughnessMetallic);
			_allImages.Add(image);
			_imageMap.Add(key, image);
			return image;
		}

		string GetImageKey(MaterialInject material)
		{
			if(material.ImageWidth == 0 || material.ImageHeight == 0)
				return GetImageKey(null, null, null);

			return GetImageKey(material.TextureBaseColor, material.TextureNormalRoughnessMetallic, material.TextureHeightAlpha);
		}

		string GetImageKey(string baseColor, string normal, string alpha)
		{
			return baseColor ?? String.Empty + "_" +
				   normal ?? String.Empty + "_" +
				   alpha ?? String.Empty;
		}

		public VirtualImage[,] GetImages(int x, int y, int mip)
		{
			var corner = GetImage(x, y, mip);
			int downsize = mip - corner.MaxWholeMip;

			if(downsize <= 0)
			{
				return new VirtualImage[1, 1]
				{
					{corner}
				};
			}

			var images00 = GetImages(x * 2, y * 2, mip - 1);
			var images10 = GetImages(x * 2 + 1, y * 2, mip - 1);
			var images01 = GetImages(x * 2, y * 2 + 1, mip - 1);
			var images11 = GetImages(x * 2 + 1, y * 2 + 1, mip - 1);

			int size = Math.Max(Math.Max(images00.GetLength(0), images01.GetLength(0)), Math.Max(images10.GetLength(0), images11.GetLength(0)));

			VirtualImage[,] result = new VirtualImage[size * 2, size * 2];

			Action<VirtualImage[,], int, int> upsample = (source, offsetX, offsetY) =>
			{
				int factor = size / source.GetLength(0);

				for(int ly = 0; ly < size; ly++)
				{
					for(int lx = 0; lx < size; lx++)
					{
						result[lx + offsetY, ly + offsetY] = source[lx / factor, ly / factor];
					}
				}
			};

			upsample(images00, 0, 0);
			upsample(images10, size, 0);
			upsample(images01, 0, size);
			upsample(images11, size, size);

			return result;
		}

		public VirtualImage GetImage(int x, int y, int mip)
		{
			return GetImage(x << mip, y << mip);
		}

		public VirtualImage GetImage(int x, int y)
		{
			var result = _packer.FindUsage(x, y);

			if(result == null)
			{
				//_context.TextOutput.Print("Warning: trying to access empty virtual texture region", OutputType.PerformanceWarning);
			}

			return result;
		}

		public void Update(FenceSync frameSync)
		{
			TimerQueryCPU query = new TimerQueryCPU(_context.FeedbackManager, "VTUpdate");
			query.StartQuery();

			_packer.Update(_allImages);

			// When the first few mips of a new image are loaded, make its first entire-tile level always loaded
			while(_allImages.Count > _imageIndexToEternalize)
			{
				var image = _allImages[_imageIndexToEternalize];

				var mip = _context.MaterialManager.GetMip(image.SourceFileBaseColor, image.MaxWholeMip);

				if(mip != null && mip.Item != null)
				{
					int x = image.OffsetX;
					int y = image.OffsetY;

					x = x >> image.MaxWholeMip;
					y = y >> image.MaxWholeMip;

					this.VtPageManager.MakePageEternal(x, y, image.MaxWholeMip);

					_imageIndexToEternalize++;
				}
				else
				{
					break;
				}
			}

			int[] feedbackEntiresCountArray = new int[1];

			// Download the feedback
			GL.BindBuffer(BufferTarget.CopyWriteBuffer, _feedbackBuffers.Current);
			var ptr = GL.MapBuffer(BufferTarget.CopyWriteBuffer, BufferAccess.ReadOnly);
			Marshal.Copy(ptr, feedbackEntiresCountArray, 0, 1);
			var feedbackEntries = new int[Math.Max(Math.Min(feedbackEntiresCountArray[0], FeedbackBufferEntries), 0)];
			Marshal.Copy(IntPtr.Add(ptr, 4), feedbackEntries, 0, feedbackEntries.Length);
			GL.UnmapBuffer(BufferTarget.CopyWriteBuffer);
			// Readback with persistent mapping does not seem work with renderdoc for some reason :(

			Entry[] entries = new Entry[feedbackEntries.Length];

			for(int i = 0; i < entries.Length; i++)
			{
				uint packed = (uint)feedbackEntries[i];
				entries[i] = Entry.UnpackFeedback(packed, VirtualPagesX);
			}

			_updateInfo = VtPageManager.Update(entries);

			if(feedbackEntries.Length > 0)
			{
				//_context.TextOutput.Print("Count: " + feedbackEntiresCountArray[0].ToString() + " VTUsageChanges: " + feedbackEntries.Length.ToString(), OutputType.Notify);
			}

			unsafe
			{
				int* buffer = (int*)_bufferIndirectionUpload.CurrentStart;
				buffer[_pageSource.MaxPageUpdates] = unchecked((int)0x00000000);

				for(int i = 0; i < _pageSource.MaxPageUpdates; i++)
				{
					if(i < _updateInfo.Uploads.Count)
					{
						var upload = _updateInfo.Uploads[i];
						buffer[i] = unchecked((int)(0xF0000000 | (upload.PhysicalX & 0x7F) | ((upload.PhysicalY & 0x7F) << 7)));
					}
					else
					{
						buffer[i] = 0;
					}
				}
			}

			_pageSource.UpdateUploads(frameSync, _updateInfo.Uploads);

			_currentFeedbackBuffer = _feedbackBuffers.Current;
			_feedbackBuffers.Advance();

			_currentBufferIndirectionUploadOffset = _bufferIndirectionUpload.CurrentOffset;
			_bufferIndirectionUpload.Advance(frameSync);

			query.EndQuery();
		}

		public void UpdateCopies()
		{
			using(_device.DebugMessageManager.PushGroupMarker("VTUpdates"))
			{
				TimerQueryGPU query = new TimerQueryGPU(_context.FeedbackManager, "VTUpdateGPU");
				query.StartQuery();

				// Update indirection
				using(_device.BindBuffer(BufferTarget.PixelUnpackBuffer, _bufferIndirectionUpload.Handle))
				{
					GL.BindTexture(TextureTarget.Texture2D, _texIndirection.Handle);

					for(int i = 0; i < _updateInfo.ClearedIndirection.Count; i++)
					{
						var clear = _updateInfo.ClearedIndirection[i];
						GL.TexSubImage2D(TextureTarget.Texture2D, clear.Mip, clear.X, clear.Y, 1, 1, PixelFormat.RedInteger,
							PixelType.UnsignedInt, new IntPtr(_currentBufferIndirectionUploadOffset + _pageSource.MaxPageUpdates * 4));
					}

					for(int i = 0; i < _updateInfo.Uploads.Count; i++)
					{
						var upload = _updateInfo.Uploads[i];
						GL.TexSubImage2D(TextureTarget.Texture2D, upload.VirtualMip, upload.VirtualX, upload.VirtualY, 1, 1,
							PixelFormat.RedInteger, PixelType.UnsignedInt, new IntPtr(_currentBufferIndirectionUploadOffset + i * 4));
					}

					// Update physical storage

					_pageSource.UpdateCopies(_device, _texCacheBaseColor, _texCacheNormalMap);

					GL.BindTexture(TextureTarget.Texture2D, 0);
				}

				GL.MemoryBarrier(MemoryBarrierFlags.TextureUpdateBarrierBit);

				for(int i = 0; i < VirtualLevels; i++)
				{
					int size = 1 << i;
					int level = VirtualLevels - i - 1;
					GL.CopyImageSubData(_texIndirection.Handle, ImageTarget.Texture2D, level, 0, 0, 0, _texIndirectionCopy.Handle, ImageTarget.Texture2D, level, 0, 0, 0, size, size, 1);
				}

				_device.BindPipeline(_psoGenMips);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

				for(int i = 1; i < VirtualLevels; i++)
				{
					int size = 1 << (i - 1);
					int level = VirtualLevels - i - 1;
					_device.BindImage2D(0, _texIndirection, TextureAccess.ReadOnly, level + 1);
					_device.BindImage2D(1, _texIndirectionCopy, TextureAccess.ReadOnly, level);
					_device.BindImage2D(2, _texIndirection, TextureAccess.WriteOnly, level);
					_device.ShaderCompute.SetUniformI("size", size, size);
					_device.DispatchComputeThreads(size, size);
					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);
				}

				GL.MemoryBarrier(MemoryBarrierFlags.TextureFetchBarrierBit);

				query.EndQuery();
			}

			GL.BindTexture(TextureTarget.Texture2D, 0);
		}

		public void UpdateVTFeedback()
		{
			var queryGpu = new TimerQueryGPU(_context.FeedbackManager, "VTFeedback");
			// clear the feedback buffer
			Utils.CopyBuffer(_bufferZeroes, _bufferFeedbackLocal, 0, 0, FeedbackEntryBufferSize);

			// TODO simplify (remove move step that is used to make it compatible with old feedback pipe)
			// Move the feedback from rendertarget to usage texture
			_device.BindPipeline(_psoMoveFeedback);
			_device.BindImage2D(0, _texFeedback, TextureAccess.ReadOnly);
			_device.BindImage2D(1, _texUsageMap, TextureAccess.WriteOnly);
			_device.DispatchComputeThreads(_texFeedback.Width, _texFeedback.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

			// Compare usage and residency texture and write entries into feedback buffer
			_device.BindPipeline(_psoGetFeedback);
			_device.BindBufferRange(BufferRangeTarget.ShaderStorageBuffer, 2,
				_bufferFeedbackLocal, IntPtr.Zero, new IntPtr(FeedbackEntryBufferSize));
			_device.BindImage2D(0, _texUsageMap, TextureAccess.ReadOnly);
			_device.BindImage2D(1, _texResidencyMap, TextureAccess.ReadOnly);
			_device.ShaderCompute.SetUniformI("maxEntries", FeedbackBufferEntries);
			_device.DispatchComputeThreads(_texUsageMap.Width, _texUsageMap.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

			// Update residency tex with the written feedback values
			_device.BindPipeline(_psoUpdateResidency);
			_device.BindImage2D(0, _texResidencyMap, TextureAccess.WriteOnly);
			_device.DispatchComputeThreads(FeedbackBufferEntries);

			// Clear usage texture
			_device.BindPipeline(_psoClearImage);
			_device.BindImage2D(0, _texUsageMap, TextureAccess.WriteOnly);
			_device.DispatchComputeThreads(_texUsageMap.Width, _texUsageMap.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

			// Copy feedback into readback buffer
			Utils.CopyBuffer(_bufferFeedbackLocal, _currentFeedbackBuffer, 0, _feedbackBufferOffset, FeedbackEntryBufferSize);
			queryGpu.EndQuery();

			_device.BindImage2D(0, null, TextureAccess.ReadOnly);
			_device.BindImage2D(1, null, TextureAccess.ReadOnly);

			uint zero = 0;
			GL.ClearTexImage(_texFeedback.Handle, 0, PixelFormat.RedInteger, PixelType.UnsignedInt, ref zero);
		}
	}
}
