﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Materials.VirtualTextures
{
	class TexturePageSource : IPageSource
	{
		const int MaxUpdates = 8;

		public int MaxPageUpdates
		{
			get { return MaxUpdates; }
		}

		StreamBuffer _bufferTileUpload;
		int _currentBufferTileUploadOffset = 0;

		List<UploadInstruction> _uploads = new List<UploadInstruction>();

		public TexturePageSource()
		{
			_bufferTileUpload = new StreamBuffer("VTTileUploadBuffer", BufferTarget.PixelPackBuffer, VirtualTextureManager.TotalTileSizeBytesAllTextures * MaxUpdates);
		}

		public void UpdateUploads(FenceSync frameSync, List<UploadInstruction> uploads)
		{
			_uploads = uploads;

			for(int i = 0; i < _uploads.Count; i++)
			{
				var upload = _uploads[i];

				int offsetX = upload.VirtualX - upload.Image.OffsetX;
				int offsetY = upload.VirtualY - upload.Image.OffsetY;

				byte[] bytesBaseColor, bytesNormalMap;

				if(upload.TextureMipBaseColor != null)
				{
					bytesBaseColor = upload.TextureMipBaseColor.GetBlocksBytes(
						offsetX * VirtualTextureManager.TileBlocksUsableSize - VirtualTextureManager.TileBlocksBorder,
						offsetY * VirtualTextureManager.TileBlocksUsableSize - VirtualTextureManager.TileBlocksBorder,
						VirtualTextureManager.TileBlocksPhysicalSize, VirtualTextureManager.TileBlocksPhysicalSize);
				}
				else
				{
					bytesBaseColor = new byte[VirtualTextureManager.TotalTileSizeBytesBc1];
				}

				if(upload.TextureMipNormalMap != null)
				{
					bytesNormalMap = upload.TextureMipNormalMap.GetBlocksBytes(
						offsetX * VirtualTextureManager.TileBlocksUsableSize - VirtualTextureManager.TileBlocksBorder,
						offsetY * VirtualTextureManager.TileBlocksUsableSize - VirtualTextureManager.TileBlocksBorder,
						VirtualTextureManager.TileBlocksPhysicalSize, VirtualTextureManager.TileBlocksPhysicalSize);
				}
				else
				{
					bytesNormalMap = new byte[VirtualTextureManager.TotalTileSizeBytesBc7];
				}

				Marshal.Copy(bytesBaseColor, 0, IntPtr.Add(_bufferTileUpload.CurrentStart, i * VirtualTextureManager.TotalTileSizeBytesAllTextures), bytesBaseColor.Length);
				Marshal.Copy(bytesNormalMap, 0, IntPtr.Add(_bufferTileUpload.CurrentStart, i * VirtualTextureManager.TotalTileSizeBytesAllTextures + VirtualTextureManager.TotalTileSizeBytesBc1), bytesNormalMap.Length);
			}

			_currentBufferTileUploadOffset = _bufferTileUpload.CurrentOffset;
			_bufferTileUpload.Advance(frameSync);
		}

		public void UpdateCopies(Device device, Texture2D baseColor, Texture2D normalMap)
		{
			using(device.BindBuffer(BufferTarget.PixelUnpackBuffer, _bufferTileUpload.Handle))
			{
				for(int i = 0; i < _uploads.Count; i++)
				{
					var upload = _uploads[i];

					baseColor.CompressedTexSubImage2D(device, 0, upload.PhysicalX * VirtualTextureManager.TileTexelsPhysicalSize,
						upload.PhysicalY * VirtualTextureManager.TileTexelsPhysicalSize, VirtualTextureManager.TileTexelsPhysicalSize,
						VirtualTextureManager.TileTexelsPhysicalSize, VirtualTextureManager.TotalTileSizeBytesBc1,
						new IntPtr(_currentBufferTileUploadOffset + i * VirtualTextureManager.TotalTileSizeBytesAllTextures));

					normalMap.CompressedTexSubImage2D(device, 0, upload.PhysicalX * VirtualTextureManager.TileTexelsPhysicalSize,
						upload.PhysicalY * VirtualTextureManager.TileTexelsPhysicalSize, VirtualTextureManager.TileTexelsPhysicalSize,
						VirtualTextureManager.TileTexelsPhysicalSize, VirtualTextureManager.TotalTileSizeBytesBc7,
						new IntPtr(_currentBufferTileUploadOffset + i * VirtualTextureManager.TotalTileSizeBytesAllTextures +
						           VirtualTextureManager.TotalTileSizeBytesBc1));
				}
			}
		}

		public void UpdateRenders()
		{
			// Unused
		}
	}
}
