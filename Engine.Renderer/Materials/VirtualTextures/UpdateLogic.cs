﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Materials;

namespace Engine.Renderer.Materials.VirtualTextures
{
	class Entry
	{
		public bool Usage;
		public int X;
		public int Y;
		public int Mip;

		public Entry(int x, int y, int mip, bool usage)
		{
			X = x;
			Y = y;
			Mip = mip;
			Usage = usage;
		}

		public static Entry UnpackFeedback(uint packed, int virtualSize)
		{
			Entry e = new Entry(0, 0, 0, false);

			e.Usage = (packed & 0x80000000) > 0;
			e.X = (int)(packed & 0x0000FFFF);
			e.Y = (int)((packed >> 16) & 0x00007FFF);
			e.Mip = 0;

			if(e.X >= virtualSize)
			{
				e.X -= virtualSize;
				e.Mip++;
			}

			int size = virtualSize / 2;
			if(e.Mip > 0)
			{
				while(e.Y >= size)
				{
					e.Y -= size;
					e.Mip++;
					size /= 2;
				}
			}

			return e;
		}

		public int PackForSort()
		{
			int data = 0;
			data = data | ((X & 0xFFF));
			data = data | ((Y & 0xFFF) << 12);
			data = data | ((Mip & 0xFF) << 24);
			return data;
		}

		public static Entry UnpackSort(int data)
		{
			return new Entry(
				(data & 0xFFF),
				((data >> 12) & 0xFFF),
				((data >> 24) & 0xFF),
				false);
		}
	}

	class VTUpdateInfo
	{
		public List<UploadInstruction> Uploads;
		public List<Entry> ClearedIndirection;
	}

	class UploadInstruction
	{
		public int VirtualX, VirtualY, VirtualMip, PhysicalX, PhysicalY;
		public TextureMip TextureMipBaseColor;
		public TextureMip TextureMipNormalMap;
		public VirtualImage Image;
	}

	class UpdateLogic
	{
		public int MaxAllocationPerUpdate { get; private set; } = 16;

		const byte TokenEmpty = 0;
		const byte TokenMip = 1;
		const byte TokenUsed = 2;

		const int KeepOldPagesMillis = 50;

		Stopwatch _stopwatch;

		List<uint> _allPages = new List<uint>();
		HashSet<int> _neededPages = new HashSet<int>();
		Dictionary<int, long> _removePages = new Dictionary<int, long>();
		Dictionary<int, uint> _entryToPageMap = new Dictionary<int, uint>();
		Dictionary<uint, int> _pageToEntryMap = new Dictionary<uint, int>();
		Dictionary<uint, long> _lastPageUsage = new Dictionary<uint, long>();
		Dictionary<uint, bool> _pageUsed = new Dictionary<uint, bool>();
		HashSet<int> _eternalPages = new HashSet<int>();
			
		VirtualTextureManager _vtManager;

		Renderer.RenderContext _context;

		ImageMipRepository _mipRepository;

		byte[][,] _indirectionUsageMips;

		public UpdateLogic(Renderer.RenderContext context, VirtualTextureManager vtManager, ImageMipRepository mipRepository, int maxUploadsPerUpdate)
		{
			_mipRepository = mipRepository;

			_stopwatch = new Stopwatch();
			_stopwatch.Start();

			_context = context;
			_vtManager = vtManager;

			MaxAllocationPerUpdate = maxUploadsPerUpdate;

			_indirectionUsageMips = new byte[_vtManager.VirtualLevels][,];

			for(int i = 0; i < _indirectionUsageMips.Length; i++)
			{
				_indirectionUsageMips[_indirectionUsageMips.Length - i - 1] = new byte[1 << i, 1 << i];
			}

			for(int y = 0; y < _vtManager.PhysicalTilesY; y++)
			{
				for(int x = 0; x < _vtManager.PhysicalTilesX; x++)
				{
					uint p = PackPageCoords(x, y);
					_allPages.Add(p);
					_pageUsed.Add(p, false);
				}
			}
		}

		uint PackPageCoords(int x, int y)
		{
			return (uint)(x | (y << 16));
		}

		void UnpackPageCoords(uint packed, out int x, out int y)
		{
			x = (int)(packed & 0xFFFF);
			y = (int)((packed >> 16) & 0xFFFF);
		}

		// This page will always be loaded
		public void MakePageEternal(int x, int y, int mip)
		{
			Entry e = new Entry(x, y, mip, true);

			_indirectionUsageMips[e.Mip][e.X, e.Y] = TokenUsed;

			int key = e.PackForSort();
			_eternalPages.Add(key);
			_neededPages.Add(key);

			if(_removePages.ContainsKey(key))
				_removePages.Remove(key);
		}

		// TODO: optimize to remove stutter
		public VTUpdateInfo Update(Entry[] entires)
		{
			TimerQueryCPU querySlow = new TimerQueryCPU(_context.FeedbackManager, "VTUpdateSlow");
			TimerQueryCPU queryLogic = new TimerQueryCPU(_context.FeedbackManager, "VTUpdateLogic");
			queryLogic.StartQuery();

			const bool allowUpdate = true;

			VTUpdateInfo info = new VTUpdateInfo();
			info.ClearedIndirection = new List<Entry>();
			info.Uploads = new List<UploadInstruction>();

			long millis = _stopwatch.ElapsedMilliseconds;

			Action<int> addPage = i =>
			{
				_removePages.Remove(i);
				_neededPages.Add(i);
			};

			Action<int> removePage = i =>
			{
				if(_removePages.ContainsKey(i))
				{
					_removePages[i] = millis;
				}
				else
				{
					_removePages.Add(i, millis);
				}
			};

			foreach(var sortKey in _removePages.Keys.ToList())
			{
				if(millis - _removePages[sortKey] > KeepOldPagesMillis)
				{
					_neededPages.Remove(sortKey);
					_removePages.Remove(sortKey);
				}
			}

			List<uint>[] updateLevels = new List<uint>[_vtManager.VirtualLevels];

			for(int i = 0; i < updateLevels.Length; i++)
			{
				updateLevels[i] = new List<uint>();
			}

			for(int i = 0; i < entires.Length; i++)
			{
				var entry = entires[i];

				updateLevels[entry.Mip].Add(PackPageCoords(entry.X, entry.Y));
				
				if(entry.Usage)
				{
					_indirectionUsageMips[entry.Mip][entry.X, entry.Y] = TokenUsed;
					addPage(entry.PackForSort());
				}
				else
				{
					if(_eternalPages.Contains(entry.PackForSort()))
						continue;

					_indirectionUsageMips[entry.Mip][entry.X, entry.Y] = TokenEmpty;
					removePage(entry.PackForSort());
				}
			}

			TimerQueryCPU queryMips = new TimerQueryCPU(_context.FeedbackManager, "VTUpdateMips");
			
			queryMips.StartQuery();

			for(int i = 0; i < updateLevels.Length; i++)
			{
				foreach(uint packed in updateLevels[i])
				{
					int x, y;
					UnpackPageCoords(packed, out x, out y);
					Entry e = new Entry(x, y, i, true);

					byte here = _indirectionUsageMips[i][x, y];
					byte updated = here;

					bool empty = true;

					if(i > 0)
					{
						bool hasChild = false;
						hasChild = hasChild || _indirectionUsageMips[i - 1][x * 2 + 0, y * 2 + 0] != TokenEmpty;
						hasChild = hasChild || _indirectionUsageMips[i - 1][x * 2 + 1, y * 2 + 0] != TokenEmpty;
						hasChild = hasChild || _indirectionUsageMips[i - 1][x * 2 + 0, y * 2 + 1] != TokenEmpty;
						hasChild = hasChild || _indirectionUsageMips[i - 1][x * 2 + 1, y * 2 + 1] != TokenEmpty;

						empty = !hasChild;
					}

					if(here != TokenUsed)
					{
						if(empty)
						{
							updated = TokenEmpty;
						}
						else
						{
							updated = TokenMip;
						}
					}

					if(updated != here)
					{
						if(updated == TokenEmpty)
						{
							if(!_eternalPages.Contains(e.PackForSort()))
								removePage(e.PackForSort());
						}
						else
						{
							addPage(e.PackForSort());
						}
					}

					if(i < updateLevels.Length - 1)
						updateLevels[i + 1].Add(PackPageCoords(x / 2, y / 2));

					_indirectionUsageMips[i][x, y] = updated;
				}
			}

			queryMips.EndQuery();

			/*
			// Free all pages that are not used anymore
			foreach(int key in _entryToPageMap.Keys.ToList())
			{
				var entry = Entry.UnpackSort(key);

				//if(!_indirectionUsageMips[entry.Mip][entry.X, entry.Y])
				if(_indirectionUsageMips[entry.Mip][entry.X, entry.Y] == TokenEmpty)
				{
					if(allowUpdate)
					{
						info.ClearedIndirection.Add(entry);
						_entryToPageMap.Remove(key);
					}
				}
			}
			*/

			

			// The mip is packed into the most significant bits
			var entrySort = _neededPages.ToList();
			entrySort.Sort();
			entrySort.Reverse();
			// Sorted from most important to least important
			
			//HashSet<uint> freePages = new HashSet<uint>(_allPages);

			for(int i = 0; i < _allPages.Count; i++)
			{
				_pageUsed[_allPages[i]] = false;
			}

			for(int i = 0; i < entrySort.Count; i++)
			{
				var sortKey = entrySort[i];
				if(_entryToPageMap.ContainsKey(sortKey))
				{
					if(i < _allPages.Count)
					{
						// Remove all reused pages from free pages
						_pageUsed[_entryToPageMap[sortKey]] = true;
						//freePages.Remove(_entryToPageMap[sortKey]);
						_lastPageUsage[_entryToPageMap[sortKey]] = _stopwatch.ElapsedMilliseconds;
					}
					else
					{
						// Clear page usage for entries that will not get a page
						var entry = Entry.UnpackSort(sortKey);

						if(allowUpdate)
						{
							info.ClearedIndirection.Add(entry);
							_pageToEntryMap.Remove(_entryToPageMap[sortKey]);
							_entryToPageMap.Remove(sortKey);
						}
					}
				}
			}
			querySlow.StartQuery();
			List<uint> freePages = new List<uint>();
			for(int i = 0; i < _allPages.Count; i++)
			{
				if(!_pageUsed[_allPages[i]])
					freePages.Add(_allPages[i]);
			}

			var remainings = freePages.OrderBy(x =>
			{
				if(_lastPageUsage.ContainsKey(x))
					return _lastPageUsage[x];
				return 0;
			}).ToList();

			querySlow.EndQuery();


			int loadedPages = 0;
			int allocations = 0;

			

			for(int i = 0; i < entrySort.Count && i < _allPages.Count; i++)
			{
				var sortKey = entrySort[i];
				var entry = Entry.UnpackSort(sortKey);

				if(allocations < MaxAllocationPerUpdate && loadedPages < remainings.Count)
				{
					if(!_entryToPageMap.ContainsKey(sortKey))
					{
						var mipImage = _mipRepository.GetMip(entry.X, entry.Y, entry.Mip);

						if(mipImage != null && allowUpdate)
						{
							var free = remainings[loadedPages];
							_entryToPageMap.Add(sortKey, free);

							if(_pageToEntryMap.ContainsKey(free))
							{
								var oldEntry = _pageToEntryMap[free];
								_pageToEntryMap[free] = sortKey;
								info.ClearedIndirection.Add(Entry.UnpackSort(oldEntry));
								_entryToPageMap.Remove(oldEntry);
							}
							else
							{
								_pageToEntryMap.Add(free, sortKey);
							}

							if(!_lastPageUsage.ContainsKey(free))
							{
								_lastPageUsage.Add(free, _stopwatch.ElapsedMilliseconds);
							}
							else
							{
								_lastPageUsage[free] = _stopwatch.ElapsedMilliseconds;
							}

							int physX, physY;
							UnpackPageCoords(free, out physX, out physY);

							info.Uploads.Add(new UploadInstruction()
							{
								PhysicalX = physX,
								PhysicalY = physY,
								VirtualX = entry.X,
								VirtualY = entry.Y,
								VirtualMip = entry.Mip,
								TextureMipBaseColor = mipImage.BaseColorMip,
								TextureMipNormalMap = mipImage.NormalMapMip,
								Image = mipImage.Image,
							});

							loadedPages++;
							allocations++;
						}
					}
				}
			}

			queryLogic.EndQuery();


			return info;
		}
	}
}
