﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;

namespace Engine.Renderer.Materials.VirtualTextures
{
	interface IPageSource
	{
		int MaxPageUpdates { get; }
		void UpdateUploads(FenceSync frameSync, List<UploadInstruction> uploads);
		void UpdateCopies(Device device, Texture2D baseColor, Texture2D normalMap);
		void UpdateRenders();
	}
}
