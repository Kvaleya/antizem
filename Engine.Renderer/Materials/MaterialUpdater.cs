﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Materials;
using Engine.Renderer.Materials.VirtualTextures;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Utils = Engine.Common.Utils;

namespace Engine.Renderer.Materials
{
	enum MaterialFlags
	{
		HasBaseColorMap = 1,
		HasNormalMap = 2,
		HasAlphaMap = 4,
		HasExtraMap = 8,
	}

	struct MaterialStruct
	{
		static readonly Vector4 _offsetMin = new Vector4(-8);
		static readonly Vector4 _offsetMax = new Vector4(8);
		static readonly Vector4 _scaleMin = new Vector4(-8);
		static readonly Vector4 _scaleMax = new Vector4(8);

		static readonly Vector4 _defaultBaseColor = new Vector4(0.5f);
		static readonly Vector4 _defaultMaterial = new Vector4(0);

		public const int Size = 64;
		// 0 bytes
		// (16 bits per component each)
		public ulong BaseColorOffset;
		public ulong BaseColorScale;
		// 16 bytes
		public ulong MaterialOffset;
		public ulong MaterialScale;
		// 32 bytes
		// (16 bit X and Y offset, 8 bit size, 8 bits transition)
		public ushort VTOffsetX;
		public ushort VTOffsetY;
		// 36 bytes
		public byte VTSize;
		public byte VTMaxMip;
		public ushort VTUvScaleX;
		// 40 bytes
		public ushort VTUvScaleY;
		public ushort DetailMapOffsetX;
		// 44 bytes
		public ushort DetailMapOffsetY;
		public byte DetailMapSize;
		public byte Flags;

		// 48 bytes
		public ushort AlphaOffsetX, AlphaOffsetY, AlphaScaleX, AlphaScaleY;
		public byte AlphaMinMip;
		public byte AlphaMaxMip;
		public ushort Unused4;
		public uint Unused5;
		// 64 bytes

		public static MaterialStruct FromMaterialAndImage(AlphaTextureManager alphaManager, MaterialInject material, VirtualImage image = null)
		{
			MaterialStruct data = new MaterialStruct();

			MaterialFlags flags = (MaterialFlags)0;

			data.BaseColorOffset = Common.Utils.Pack.PackVec4(material.BaseColorOffset, _offsetMin, _offsetMax);
			data.BaseColorScale = Common.Utils.Pack.PackVec4(material.BaseColorScale, _scaleMin, _scaleMax);
			data.MaterialOffset = Common.Utils.Pack.PackVec4(material.MaterialOffset, _offsetMin, _offsetMax);
			data.MaterialScale = Common.Utils.Pack.PackVec4(material.MaterialScale, _scaleMin, _scaleMax);

			if(image != null && image.IsAllocated)
			{
				byte maxMip = 0;
				int size = image.Size;

				while(size > 1)
				{
					maxMip++;
					size = size >> 1;
				}

				data.VTOffsetX = (ushort)image.OffsetX;
				data.VTOffsetY = (ushort)image.OffsetY;
				data.VTSize = (byte)image.Size;
				data.VTMaxMip = maxMip;

				data.VTUvScaleX = (ushort)(65535 * image.ScaleX);
				data.VTUvScaleY = (ushort)(65535 * image.ScaleY);

				if(!string.IsNullOrEmpty(image.SourceFileBaseColor))
				{
					flags = flags | MaterialFlags.HasBaseColorMap;
				}

				if(!string.IsNullOrEmpty(image.SourceFileNormalMap))
				{
					flags = flags | MaterialFlags.HasNormalMap;
				}
			}
			else
			{
				data.BaseColorOffset = Common.Utils.Pack.PackVec4(_defaultBaseColor + material.BaseColorOffset, _offsetMin, _offsetMax);
				data.BaseColorScale = Common.Utils.Pack.PackVec4(Vector4.Zero, _scaleMin, _scaleMax);
				data.MaterialOffset = Common.Utils.Pack.PackVec4(_defaultMaterial + material.MaterialOffset, _offsetMin, _offsetMax);
				data.MaterialScale = Common.Utils.Pack.PackVec4(Vector4.Zero, _scaleMin, _scaleMax);
			}

			if(!string.IsNullOrEmpty(material.TextureHeightAlpha))
			{
				int x, y, w, h, minMip, maxMip;
				alphaManager.GetSurfaceLocation(material.TextureHeightAlpha, out x, out y, out w, out h, out minMip, out maxMip);

				float ox, oy, sx, sy;
				ox = x / (float)alphaManager.Atlas.Width;
				oy = y / (float)alphaManager.Atlas.Height;
				sx = w / (float)alphaManager.Atlas.Width;
				sy = h / (float)alphaManager.Atlas.Height;

				data.AlphaOffsetX = Utils.Pack.PackUnorm(ox);
				data.AlphaOffsetY = Utils.Pack.PackUnorm(oy);
				data.AlphaScaleX = Utils.Pack.PackUnorm(sx);
				data.AlphaScaleY = Utils.Pack.PackUnorm(sy);
				data.AlphaMinMip = (byte)minMip;
				data.AlphaMaxMip = (byte)maxMip;

				flags = flags | MaterialFlags.HasAlphaMap;
			}

			data.Flags = (byte)flags;

			return data;
		}

		public void ToBytes(byte[] array, int offset)
		{
			AddToBytes(array, ref offset, BitConverter.GetBytes(BaseColorOffset));
			AddToBytes(array, ref offset, BitConverter.GetBytes(BaseColorScale));
			AddToBytes(array, ref offset, BitConverter.GetBytes(MaterialOffset));
			AddToBytes(array, ref offset, BitConverter.GetBytes(MaterialScale));

			AddToBytes(array, ref offset, BitConverter.GetBytes(VTOffsetX));
			AddToBytes(array, ref offset, BitConverter.GetBytes(VTOffsetY));
			AddToBytes(array, ref offset, VTSize);
			AddToBytes(array, ref offset, VTMaxMip);
			AddToBytes(array, ref offset, BitConverter.GetBytes(VTUvScaleX));
			AddToBytes(array, ref offset, BitConverter.GetBytes(VTUvScaleY));

			AddToBytes(array, ref offset, BitConverter.GetBytes(DetailMapOffsetX));
			AddToBytes(array, ref offset, BitConverter.GetBytes(DetailMapOffsetY));
			AddToBytes(array, ref offset, DetailMapSize);
			AddToBytes(array, ref offset, Flags);

			AddToBytes(array, ref offset, BitConverter.GetBytes(AlphaOffsetX));
			AddToBytes(array, ref offset, BitConverter.GetBytes(AlphaOffsetY));
			AddToBytes(array, ref offset, BitConverter.GetBytes(AlphaScaleX));
			AddToBytes(array, ref offset, BitConverter.GetBytes(AlphaScaleY));
			AddToBytes(array, ref offset, BitConverter.GetBytes(AlphaMinMip));
			AddToBytes(array, ref offset, BitConverter.GetBytes(AlphaMaxMip));
			AddToBytes(array, ref offset, BitConverter.GetBytes(Unused4));
			AddToBytes(array, ref offset, BitConverter.GetBytes(Unused5));
		}

		static void AddToBytes(byte[] array, ref int offset, byte[] add)
		{
			for(int i = 0; i < add.Length; i++)
			{
				array[i + offset] = add[i];
			}
			offset += add.Length;
		}

		static void AddToBytes(byte[] array, ref int offset, byte add)
		{
			array[offset] = add;
			offset++;
		}
	}

	class MaterialUpdater
	{
		[InjectProperty]
		public MaterialData MaterialData { get; set; }

		VirtualTextureManager _vtManager;
		Renderer.RenderContext _context;

		public readonly int MaxMaterials;

		public int MaterialUbo
		{
			get { return _materialUbo; }
		}

		StreamBuffer _materialUploadBuffer;
		int _offset;
		int _materialUbo;

		public MaterialUpdater(Renderer.RenderContext context, VirtualTextureManager vtManager, int maxMaterials)
		{
			MaxMaterials = maxMaterials;

			_context = context;
			_vtManager = vtManager;

			_materialUploadBuffer = new StreamBuffer("MaterialUploadBuffer", BufferTarget.CopyReadBuffer, MaxMaterials * MaterialStruct.Size);
			_materialUbo = Glob.Utils.CreateBuffer(BufferTarget.UniformBuffer, new IntPtr(MaxMaterials * MaterialStruct.Size),
				IntPtr.Zero, BufferStorageFlags.None, "MaterialUbo");
		}

		public void Update(FenceSync sync)
		{
			if(MaterialData == null)
				return;

			var materials = (MaterialInject[])MaterialData.InjectMaterials;

			var data = new byte[materials.Length * MaterialStruct.Size];

			int offset = 0;
			for(int i = 0; i < materials.Length; i++)
			{
				var material = materials[i];

				if(material != null)
				{
					var s = MaterialStruct.FromMaterialAndImage(_context.AlphaTextureManager, material, _vtManager.GetImage(material));
					s.ToBytes(data, offset);
				}
				
				offset += MaterialStruct.Size;
			}

			Marshal.Copy(data, 0, _materialUploadBuffer.CurrentStart, data.Length);

			_offset = _materialUploadBuffer.CurrentOffset;

			_materialUploadBuffer.Advance(sync);
		}

		public void UpdateCopies()
		{
			Glob.Utils.CopyBuffer(_materialUploadBuffer.Handle, _materialUbo, _offset, 0, _materialUploadBuffer.Size);
		}
	}
}
