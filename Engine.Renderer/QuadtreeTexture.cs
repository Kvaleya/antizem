﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Materials;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer
{
	abstract class QuadtreeTextureItem : IQuadTreeItem
	{
		public float AreaImportance { get; protected set; }

		public int MaxSize { get; protected set; }

		public int X { get; private set; } = 0;
		public int Y { get; private set; } = 0;
		public int Size { get; private set; } = 0;

		public int NewX { get; private set; }
		public int NewY { get; private set; }
		public int NewSize { get; private set; }

		public void SetNode(int x, int y, int size)
		{
			NewX = x;
			NewY = y;
			NewSize = size;
		}

		public void MakeNewCellCurrent()
		{
			X = NewX;
			Y = NewY;
			Size = NewSize;
		}

		public void SetNodeNull()
		{
			SetNode(0, 0, 0);
			MakeNewCellCurrent();
		}

		// Called when cell size changed or when the cell is lost (then Size = 0)
		public virtual void OnSizeChanged()
		{
		}
	}

	class QuadtreeTexture<T>
		where T : QuadtreeTextureItem
	{
		QuadTreePacker<T> _packer;

		Texture2D _tex;
		Texture2D _texBuffer;

		public Texture2D TextureAtlas { get { return _tex; } }

		public int MipLevels { get; private set; }
		public int MaxCellSize { get; private set; }
		public int SmallestCellTexels { get; private set; }

		public bool Mipmapped { get { return MipLevels > 1; } }

		public QuadtreeTexture(Device device, int levels, bool rectangular, int smallestCellTexels, SizedInternalFormatGlob format, string name, int mipLevels, int maxCellSize)
		{
			// TODO: fbo copies for shadowmaps

			_packer = new QuadTreePacker<T>(levels, rectangular, 1 << (levels * 2));
			_tex = new Texture2D(device, name, format, _packer.CellsX * smallestCellTexels, _packer.CellsY * smallestCellTexels, mipLevels);
			_texBuffer = new Texture2D(device, name + "-Buffer", format, maxCellSize * smallestCellTexels, maxCellSize * smallestCellTexels, mipLevels);
			MipLevels = _tex.Levels;

			SmallestCellTexels = smallestCellTexels;
			MaxCellSize = maxCellSize; // In quadtree cells
			
		}

		// DO:
		// - move relocated cells
		// - reduce cell size OR tell owner to reduce it
		// - tell owner to increase cell size
		// - tell owner that cell no longer exists

		public void Update(List<T> items)
		{
			// First, update the quadtree
			_packer.Update(items);

			List<MoveCommand> moves = new List<MoveCommand>();

			// Then gather all the needed texture movements
			for(int i = 0; i < items.Count; i++)
			{
				var item = items[i];
				// We don't care about move if:
				// - the item no longer exists, because its data is now useless
				// - the item's cell did not change at all
				if(item.Size == 0)
					continue;
				if(item.X == item.NewX && item.Y == item.NewY && item.Size == item.NewSize)
				{
					item.MakeNewCellCurrent();
					continue;
				}
				else
				{
					item.OnSizeChanged();
				}

				moves.Add(new MoveCommand(
					new CellCoords(item.X, item.Y, item.Size, 0),
					new CellCoords(item.NewX, item.NewY, item.NewSize, 0)));
			}

			// Modify movements to remove conflicts
			var solved = SolveMovement(moves);

			// Execute the movements
			ExecuteMovements(solved);
		}

		List<MoveCommand> SolveMovement(List<MoveCommand> moves)
		{
			List<MoveCommand> actualMoves = new List<MoveCommand>();

			// Sort move commands by src.Size
			var orderedMoves = moves.OrderByDescending(x => x.Src.Size).ToList();

			// Solving moves to equal or lesser size cell:
			// Repeat while there are unprocessed moves of this kind left:
			// - find unprocessed move with greatest src.Size
			// - if it has no dependencies, then execute it, else:
			// - move the src into temp buffer
			// - move everything from dst into the original src
			// - move from buffer to dst
			// - update locations of moved stuff
			for(int i = 0; i < orderedMoves.Count; i++)
			{
				var move = orderedMoves[i];

				// Only process moves into equal or smaller size cells
				if(move.Src.Size < move.Dst.Size)
					continue;

				var tempCoords = new CellCoords(0, 0, move.Src.Size, 1);

				var src = move.Src;
				var dst = move.Dst;

				actualMoves.Add(new MoveCommand(src, tempCoords));
				actualMoves.Add(new MoveCommand(dst, new CellCoords(src.X, src.Y, dst.Size, 0)));
				actualMoves.Add(new MoveCommand(tempCoords, dst));

				// Update locations
				foreach(var m in moves)
				{
					if(m.Src.IsInside(dst))
					{
						m.Src = new CellCoords(src.X + m.Src.X - dst.X, src.Y + m.Src.Y - dst.Y, m.Src.Size, m.Src.Tex);
					}
				}
			}

			// What about moves to a larger cell size?
			// For mipmapped textures, just go from smallest mip to second largest, using the larger mip as a data source
			// For non-mipmapped (shadowmaps), regenerate the larger data and only allow a certain number of most important items to become larger per frame
			for(int i = 0; i < orderedMoves.Count; i++)
			{
				var move = orderedMoves[i];

				if(move.Src.Size < move.Dst.Size)
					actualMoves.Add(move);
			}

			return actualMoves;
		}

		void ExecuteMovements(List<MoveCommand> moves)
		{
			for(int i = 0; i < moves.Count; i++)
			{
				var move = moves[i];

				int size = move.Src.Size;
				int difference = 0;

				while(size != move.Dst.Size)
				{
					if(size < move.Dst.Size)
					{
						size = size << 1;
						difference++;
					}
					else
					{
						size = size >> 1;
						difference--;
					}
				}

				if(move.Src.Size >= move.Dst.Size)
				{
					// Move is to a equal or smaller cell
					for(int level = 0; level + difference < MipLevels; level++)
					{
						int srcLevel = level - difference;
						int dstLevel = level;

						CopyImage(move, srcLevel, dstLevel);
					}
				}
				else
				{
					// Move is to a larger cell
					for(int level = difference; level < MipLevels; level++)
					{
						int srcLevel = level - difference;
						int dstLevel = level;

						CopyImage(move, srcLevel, dstLevel);
					}
				}
			}
		}

		void CopyImage(MoveCommand move, int srcLevel, int dstLevel)
		{
			int srcTex = move.Src.Tex == 1 ? _texBuffer.Handle : _tex.Handle;
			int dstTex = move.Dst.Tex == 1 ? _texBuffer.Handle : _tex.Handle;

			int srcCellX = (move.Src.X * SmallestCellTexels) >> srcLevel;
			int srcCellY = (move.Src.Y * SmallestCellTexels) >> srcLevel;
			int dstCellX = (move.Dst.X * SmallestCellTexels) >> dstLevel;
			int dstCellY = (move.Dst.Y * SmallestCellTexels) >> dstLevel;

			int moveSize = (move.Src.Size * SmallestCellTexels) >> srcLevel;

			if(moveSize < 4)
				return;

			GL.CopyImageSubData(srcTex, ImageTarget.Texture2D, srcLevel, srcCellX, srcCellY, 0, dstTex, ImageTarget.Texture2D, dstLevel, dstCellX, dstCellY, 0, moveSize, moveSize, 1);
		}

		class MoveCommand
		{
			public CellCoords Src, Dst;

			public MoveCommand(CellCoords src, CellCoords dst)
			{
				Src = src;
				Dst = dst;
			}
		}

		class CellCoords : IEquatable<CellCoords>
		{
			public readonly int X, Y, Size, Tex;

			public CellCoords(int x, int y, int size, int tex)
			{
				X = x;
				Y = y;
				Size = size;
				Tex = tex;
			}

			public bool IsInside(CellCoords c)
			{
				if(X >= c.X + c.Size || Y >= c.Y + c.Size || X + Size <= c.X || Y + Size <= c.Y)
					return false;
				return true;
			}

			public bool Equals(CellCoords other)
			{
				if(ReferenceEquals(null, other)) return false;
				if(ReferenceEquals(this, other)) return true;
				return X == other.X && Y == other.Y && Size == other.Size && Tex == other.Tex;
			}

			public override bool Equals(object obj)
			{
				if(ReferenceEquals(null, obj)) return false;
				if(ReferenceEquals(this, obj)) return true;
				if(obj.GetType() != this.GetType()) return false;
				return Equals((CellCoords)obj);
			}

			public override int GetHashCode()
			{
				unchecked
				{
					var hashCode = X;
					hashCode = (hashCode * 397) ^ Y;
					hashCode = (hashCode * 397) ^ Size;
					hashCode = (hashCode * 397) ^ Tex;
					return hashCode;
				}
			}

			public static bool operator ==(CellCoords left, CellCoords right)
			{
				return Equals(left, right);
			}

			public static bool operator !=(CellCoords left, CellCoords right)
			{
				return !Equals(left, right);
			}
		}
	}
}
