﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer
{
	class TimerQueryGPU : ITimerQuery
	{
		public static bool Allow = true;

		public double Start { get; private set; }
		public double End { get; private set; }
		public string Name { get; private set; }

		public TimerQueryKind Kind { get { return TimerQueryKind.GFX; } }

		public bool ValidMeasurement { get { return _validMeasurement; } }

		public double Ellapsed { get { return End - Start; } }
		public double DisplayEllapsed { get { return Ellapsed; } }

		int _query0;
		int _query1;
		bool _ended = false;
		bool _validMeasurement = false;

		/// <summary>
		/// Timer query should only be created by FeedbackManager
		/// </summary>
		/// <param name="name"></param>
		public TimerQueryGPU(FeedbackManager feedbackManager, string name)
		{
			Name = name;
			feedbackManager.AddQueryGPU(this);
		}

		public void StartQuery()
		{
			if(!Allow)
				return;

			_query0 = GL.GenQuery();
			_query1 = GL.GenQuery();
			GL.QueryCounter(_query0, QueryCounterTarget.Timestamp);
		}

		public void EndQuery()
		{
			_ended = true;
			if(_query0 == 0 || _query1 == 0)
				return;
			GL.QueryCounter(_query1, QueryCounterTarget.Timestamp);
		}

		public void MeasureAndDisposeQueries()
		{
			if(_query0 == 0 || _query1 == 0)
				return;

			if(!_ended)
			{
				throw new Exception("GPU Timer Query was started but never ended!");
			}

			// wait until the query results are available
			int i = 0;
			while(i == 0)
			{
				GL.GetQueryObject(_query1, GetQueryObjectParam.QueryResultAvailable, out i);
			}

			long start, end;

			// get the query results
			GL.GetQueryObject(_query0, GetQueryObjectParam.QueryResult, out start);
			GL.GetQueryObject(_query1, GetQueryObjectParam.QueryResult, out end);
			Start = start * 1E-9;
			End = end * 1E-9;

			GL.DeleteQuery(_query0);
			GL.DeleteQuery(_query1);

			_query0 = 0;
			_query1 = 0;

			_validMeasurement = true;
		}

		public override string ToString()
		{
			return Name + ": " + Math.Round(Ellapsed * 1000, 2).ToString() + " ms";
		}
	}
}
