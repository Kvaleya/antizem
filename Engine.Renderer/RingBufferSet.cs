﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer
{
	class RingBufferSet
	{
		int[] _buffers;

		int _position;

		public int Previous
		{
			get
			{
				int p = _position - 1;
				if(p < 0)
					p += _buffers.Length;
				return _buffers[p];
			}
		}

		public int Current { get { return _buffers[_position]; } }
		public int Size { get; private set; }

		public RingBufferSet(BufferTarget target, int size, string name, int count, BufferStorageFlags flags = (BufferStorageFlags)0)
		{
			Size = size;
			_buffers = new int[count];
			for(int i = 0; i < _buffers.Length; i++)
			{
				_buffers[i] = Glob.Utils.CreateBuffer(target, new IntPtr(size), IntPtr.Zero, flags,
					name + " (" + i + ")");
			}
		}

		public void Advance()
		{
			_position++;
			if(_position == _buffers.Length)
				_position = 0;
		}
	}
}
