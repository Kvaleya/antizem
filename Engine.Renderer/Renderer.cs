﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using Engine.Materials;
using Engine.Meshes;
using Engine.Renderer.ComputeMeshProcessor;
using Engine.Renderer.Materials;
using Engine.Renderer.Materials.VirtualTextures;
using Engine.Renderer.Meshes;
using Engine.Renderer.Meshes.Buffers;
using Engine.Renderer.Modules;
using GameFramework;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using Utils = Glob.Utils;

namespace Engine.Renderer
{
	public class Renderer : IRenderer
	{
		public const float ProjectionNear = 1f / 64; // 0.0625f;
		public const int VoxelClipmapCount = VoxelModule.ClipmapLevels;

		internal RenderContext Context;

		WorldRenderer _worldRenderer;
		Device _device;
		TextOutput _textOutput;
		IFileManager _fileManager;
		DataInjector _dataInjector;

		// Resources
		MeshResourceManager _meshResourceManager;

		[InjectProperty]
		public BasicFrameData FrameData { get; set; }

		[InjectProperty]
		public TimerQueryCPUManager TimerQueryCPUManager { get; set; }

		[InjectProperty]
		public TestSceneData SceneData { get; set; }

		// Fences are inserted at the end of the frame
		int _maxFences = 16;
		// Keep the fences of last several frames
		// Other systems WILL rely on this (for triple buffering)
		Queue<FenceSync> _fencesFrameEnd = new Queue<FenceSync>();


		int _feedbackWaitFrames = 4;
		Queue<FeedbackManager> _feedbackQueue = new Queue<FeedbackManager>();

		public Renderer(TextOutput textOutput, IFileManager fileManager, bool debug)
		{
			_textOutput = textOutput;
			_fileManager = fileManager;
			var globText = new TextOutputGlob(textOutput);
			var globFileManager = new FileManagerGlob(_fileManager, "src/Shaders", "src/Shaders/Resolved", "src/Shaders/Compiled");
			_device = new Device(globText, globFileManager);
			if(debug)
				DebugMessageManager.SetupDebugCallback(globText);
		}

		public void Inicialize(DataInjector dataInjector, object meshBinaryResourceManager, object materialManager, out IMeshManager meshManager)
		{
			_dataInjector = dataInjector;
			_dataInjector.AddModule(this);
			Context = new RenderContext()
			{
				Device = _device,
				TextOutput = _textOutput,
				DisplayWidth = 1280,
				DisplayHeight = 720,
				RenderWidth = 1280,
				RenderHeight = 720,
				MeshUploader = new MeshUploader(2097152),
				InstanceManager = new InstanceManager(_device),
				//FilteredMeshRenderer = new FilteredMeshRenderer(_device),
				FileManager = _fileManager,
			};

			Context.BlueNoise = LoadBlueNoise();

			Context.ComputeMeshProcessor = new MeshProcessor(Context);

			bool allowSparse = _device.DeviceInfo.CheckExtensionAvailable(VertexManager.SparseBufferExtensionName);

			Context.Renderdoc = !allowSparse; // TODO: detect renderdoc properly

			if(allowSparse)
			{
				_textOutput.Print(OutputType.LogOnly, "VertexManager: vertex storage is using sparse buffers");
			}
			else
			{
				_textOutput.Print(OutputType.PerformanceWarning, "VertexManager: vertex storage is using expanding buffers (fallback)");
			}

			Context.VertexManager = new VertexManager(Context, allowSparse);

			//TODO: proper memory budget for GPU meshes
			_meshResourceManager = new MeshResourceManager(1024 * 1024 * 256, (MeshBinaryResourceManager)meshBinaryResourceManager, Context.VertexManager);

			meshManager = _meshResourceManager;

			var engineMaterialManager = (MaterialManager)materialManager;

			Context.MaterialManager = engineMaterialManager;

			//Context.VirtualTextureManager = new VirtualTextureManager(Context, 10, 4096);
			Context.VirtualTextureManager = new VirtualTextureManager(Context, 10, 4096, false);
			Context.MaterialUpdater = new MaterialUpdater(Context, Context.VirtualTextureManager, engineMaterialManager.MaxMaterials);
			_dataInjector.AddModule(Context.MaterialUpdater);

			Context.AlphaTextureManager = new AlphaTextureManager(Context, 1024, true);

			_worldRenderer = new WorldRenderer(dataInjector, Context);
		}

		// Takes care of support functions such as data uploads, then invokes the WorldRenderer
		public FrameOutput RenderFrame()
		{
			// Frame begin
			_dataInjector.Inject();

			CreateCameraMatrixes();

			FenceSync frameSync = new FenceSync(FrameData.FrameNumber);
			Context.FeedbackManager = new FeedbackManager(FrameData.FrameNumber, TimerQueryCPUManager);
			_device.Update();

			Context.Device.Invalidate();

			_worldRenderer.Prepare();

			var queryGpuFrame = new TimerQueryGPU(Context.FeedbackManager, "Frame");
			var queryCpuFrame = new TimerQueryCPU(Context.FeedbackManager, "Frame");
			var queryCpuRender = new TimerQueryCPU(Context.FeedbackManager, "Render");
			var queryCpuUpload = new TimerQueryCPU(Context.FeedbackManager, "Upload");
			var queryCpuCopy = new TimerQueryCPU(Context.FeedbackManager, "Copy");
			var queryGpuUpload = new TimerQueryGPU(Context.FeedbackManager, "Upload");
			queryGpuFrame.StartQuery();
			queryCpuFrame.StartQuery();

			queryCpuUpload.StartQuery();
			queryGpuUpload.StartQuery();
			// Update resource managers
			_meshResourceManager.UpdateGL();
			Context.VertexManager.UpdateStorage();

			// Do all uploads
			Context.InstanceManager.UpdateUploads(frameSync);
			Context.MeshUploader.UpdateUploads(frameSync);
			//Context.FilteredMeshRenderer.Upload(frameSync);
			
			Context.ComputeMeshProcessor.Upload(frameSync);

			// Must update in this order
			Context.VirtualTextureManager.Update(frameSync);
			
			Context.AlphaTextureManager.Update();
			Context.AlphaTextureManager.UpdateUploads(frameSync);
			Context.MaterialUpdater.Update(frameSync);
			
			_worldRenderer.OnUploadPhase(frameSync);

			// Make uploads visible
			GL.MemoryBarrier(MemoryBarrierFlags.ClientMappedBufferBarrierBit);

			queryCpuUpload.EndQuery();
			queryCpuCopy.StartQuery();

			// Copy data from uploads
			Context.MeshUploader.UpdateCopies();
			Context.InstanceManager.UpdateCopies();
			//Context.FilteredMeshRenderer.UpdateCopies();
			Context.ComputeMeshProcessor.UpdateCopies();
			Context.MaterialUpdater.UpdateCopies();
			Context.VirtualTextureManager.UpdateCopies();
			Context.AlphaTextureManager.UpdateCopies();

			_worldRenderer.OnCopyPhase();

			queryCpuCopy.EndQuery();
			queryGpuUpload.EndQuery();

			// The main renderer - takes care of visuals
			queryCpuRender.StartQuery();
			_worldRenderer.RenderFrame(Context);
			queryCpuRender.EndQuery();

			// Update VT feedback
			Context.VirtualTextureManager.UpdateVTFeedback();

			// Frame end
			GL.MemoryBarrier(MemoryBarrierFlags.ClientMappedBufferBarrierBit);
			frameSync.Create();

			queryGpuFrame.EndQuery();
			queryCpuFrame.EndQuery();

			_fencesFrameEnd.Enqueue(frameSync);
			if(_fencesFrameEnd.Count > _maxFences)
			{
				var fenceDelete = _fencesFrameEnd.Dequeue();
				fenceDelete.Dispose();
			}

			_feedbackQueue.Enqueue(Context.FeedbackManager);

			FrameOutput output = new FrameOutput();

			if(_feedbackQueue.Count > _feedbackWaitFrames)
			{
				var fb = _feedbackQueue.Dequeue();
				var queries = fb.FinishQueries();
				output.Timers.AddRange(queries);
				//StringBuilder sb = new StringBuilder();
				//sb.Append(fb.FrameNumber);
				//foreach(var q in queries)
				//{
				//	sb.Append(' ');
				//	sb.Append(q.ToString());
				//}
				//_textOutput.Print(sb.ToString(), OutputType.LogOnly);
			}

			if(Context.FrameNumber == 100)
			{
				Context.TextOutput.Print(OutputType.Debug, "StreamBuffer bytes: " + Glob.StreamBuffer.TotalBytesPerFrame);
			}

			return output;
		}

		void CreateCameraMatrixes()
		{
			Context.ProjectionMatrixHistory = Context.ProjectionMatrix;
			Context.ProjectionMatrixNoReverseDepthHistory = Context.ProjectionMatrixNoReverseDepth;
			Context.CameraMatrixHistory = Context.CameraMatrix;
			Context.CameraPositionHistory = Context.CameraPosition;

			Context.Camera = SceneData.Camera;

			Context.ProjectionMatrix = Utils.GetProjectionPerspectiveReverseZ(Context.RenderWidth, Context.RenderHeight, SceneData.Camera.CameraCurrent.Fov, ProjectionNear);
			Context.ProjectionMatrixNoReverseDepth = Utils.GetProjectionPerspective(Context.RenderWidth, Context.RenderHeight, SceneData.Camera.CameraCurrent.Fov, 0.0625f, 1 << 16);
			Context.CameraMatrix = SceneData.Camera.CameraCurrent.ToCameraMatrix(Vector3.Zero);
			Context.ProjectionCameraMatrix = Context.CameraMatrix * Context.ProjectionMatrix;
			Context.CameraPosition = SceneData.Camera.CameraCurrent.Position;
			Context.CameraFov = SceneData.Camera.CameraCurrent.Fov;

			Context.CameraPlane = new Vector4(Context.CameraMatrix.Column2.Xyz, 0f);

			Utils.GetCameraCornerRays(Context.ProjectionMatrixNoReverseDepth, Context.CameraMatrix, out Context.CameraRay00, out Context.CameraRay10, out Context.CameraRay11, out Context.CameraRay01);
		}

		public void Resize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{
			Context.DisplayWidth = displayWidth;
			Context.DisplayHeight = displayHeight;
			Context.RenderWidth = renderWidth;
			Context.RenderHeight = renderHeight;
			Context.Device.Invalidate();
			_worldRenderer.OnResize(displayWidth, displayHeight, renderWidth, renderHeight);
		}

		internal class RenderContext
		{
			internal IFileManager FileManager;
			internal TextOutput TextOutput;
			internal Device Device;
			internal int DisplayWidth, DisplayHeight, RenderWidth, RenderHeight;
			internal MeshUploader MeshUploader;
			internal InstanceManager InstanceManager;
			internal FeedbackManager FeedbackManager;
			internal VertexManager VertexManager;
			internal MeshProcessor ComputeMeshProcessor;
			internal MaterialManager MaterialManager;
			internal MaterialUpdater MaterialUpdater;
			internal VirtualTextureManager VirtualTextureManager;
			internal AlphaTextureManager AlphaTextureManager;
			internal Matrix4 ProjectionMatrix = Matrix4.Identity;
			internal Matrix4 ProjectionMatrixHistory = Matrix4.Identity;
			internal Matrix4 ProjectionMatrixNoReverseDepth = Matrix4.Identity;
			internal Matrix4 ProjectionMatrixNoReverseDepthHistory = Matrix4.Identity;
			internal Matrix4 CameraMatrix = Matrix4.Identity;
			internal Matrix4 CameraMatrixHistory = Matrix4.Identity;
			internal Matrix4 ProjectionCameraMatrix;
			internal Vector3d CameraPosition = new Vector3d();
			internal Vector3d CameraPositionHistory = new Vector3d();
			internal Vector4 CameraRay00, CameraRay01, CameraRay10, CameraRay11;
			internal Vector4 CameraPlane;
			internal CameraData Camera;
			internal float CameraFov;
			internal long FrameNumber { get { return FeedbackManager.FrameNumber; } }
			internal WorkerThread IOThread;
			internal bool Renderdoc;
			internal Texture2D BlueNoise;
		}

		// Can be passed to Device
		class TextOutputGlob : ITextOutputGlob
		{
			TextOutput _textOutput;

			public TextOutputGlob(TextOutput textOutput)
			{
				_textOutput = textOutput;
			}

			public void Print(OutputTypeGlob type, string message)
			{
				OutputType ot = OutputType.Notify;

				if(type == OutputTypeGlob.LogOnly)
					ot = OutputType.LogOnly;
				if(type == OutputTypeGlob.Debug)
					ot = OutputType.Debug;
				if(type == OutputTypeGlob.Notify)
					ot = OutputType.Notify;
				if(type == OutputTypeGlob.Warning)
					ot = OutputType.Warning;
				if(type == OutputTypeGlob.PerformanceWarning)
					ot = OutputType.PerformanceWarning;
				if(type == OutputTypeGlob.Error)
					ot = OutputType.Error;

				_textOutput.Print(ot, message);
			}
		}

		// Can be passed to GlobContext
		class FileManagerGlob : IFileManager, Glob.IFileManagerGlob
		{
			IFileManager _fileManager;

			string _shaderSourcePath;
			string _shaderResolvedPath;
			string _shaderCompilePath;

			public FileManagerGlob(IFileManager fileManager, string shaderSourcePath, string shaderResolvedPath, string shaderCompiledPath)
			{
				_fileManager = fileManager;
				_shaderSourcePath = shaderSourcePath;
				_shaderResolvedPath = shaderResolvedPath;
				_shaderCompilePath = shaderCompiledPath;
			}

			public string GetPathShaderSource(string file)
			{
				if(string.IsNullOrEmpty(file))
					return _shaderSourcePath;
				return Path.Combine(_shaderSourcePath, file);
			}

			public string GetPathShaderCacheResolved(string file)
			{
				if(string.IsNullOrEmpty(file))
					return _shaderResolvedPath;
				return Path.Combine(_shaderResolvedPath, file);
			}

			public string GetPathShaderCacheCompiled(string file)
			{
				if(string.IsNullOrEmpty(file))
					return _shaderCompilePath;
				return Path.Combine(_shaderCompilePath, file);
			}

			public void MountArchive(string archive)
			{
				_fileManager.MountArchive(archive);
			}

			public bool FileExists(string file)
			{
				return _fileManager.FileExists(file);
			}

			Stream IFileManager.GetStream(string file)
			{
				return _fileManager.GetStream(file);
			}

			Stream IFileManagerGlob.GetStream(string file)
			{
				return _fileManager.GetStream(file);
			}
		}

		Texture2D LoadBlueNoise()
		{
			using(var stream = this._fileManager.GetStream("Textures/bluenoise_LDR_RGBA_15.png"))
			{
				if(stream == null)
					return new Texture2D(this.Context.Device, "BlueNoiseINVALID", SizedInternalFormatGlob.RGBA8, 1, 1, 1);

				using(var bitmap = (Bitmap)Image.FromStream(stream))
				{
					var data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly,
						System.Drawing.Imaging.PixelFormat.Format32bppRgb);
					var tex = new Texture2D(this.Context.Device, "BlueNoise", SizedInternalFormatGlob.RGBA8, bitmap.Width,
						bitmap.Height, 1);
					tex.TexSubImage2D(this._device, 0, 0, 0, tex.Width, tex.Height, PixelFormat.Rgba, PixelType.UnsignedByte,
						data.Scan0);
					bitmap.UnlockBits(data);
					return tex;
				}
			}
		}
	}
}
