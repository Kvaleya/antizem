﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Glob;

namespace Engine.Renderer
{
	abstract class RenderModule
	{
		protected readonly Renderer.RenderContext RenderContext;
		protected Device Device { get { return RenderContext.Device; } }

		public RenderModule(Renderer.RenderContext renderContext)
		{
			RenderContext = renderContext;
		}

		public virtual void OnResize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{
			
		}

		public virtual void OnUploadPhase(FenceSync frameSync)
		{
			
		}

		public virtual void OnCopyPhase()
		{

		}
	}
}
