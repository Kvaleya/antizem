﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer
{
	class ExpandingStreamBuffer
	{
		int _handleGPU = 0;

		StreamBuffer _streamBuffer;

		string _name;
		BufferTarget _target;
		BufferStorageFlags _flags;

		int _currentSize;

		int _currentUploadSize;
		int _currentUploadOffset;

		public ExpandingStreamBuffer(string name, BufferTarget target, int inicialSize = 1 << 12, BufferStorageFlags flags = BufferStorageFlags.MapWriteBit, int bufferingLevel = 3)
		{
			_name = name;
			_target = target;
			_currentSize = inicialSize;
			_flags = flags;

			ResizeToCurrentSize();
		}

		/// <summary>
		/// Requests upload of certain amount of bytes. Returns pointer to the buffer's storage.
		/// </summary>
		/// <param name="size">Amount of bytes to upload</param>
		/// <returns>Pointer to the buffer's storage</returns>
		public IntPtr GetPtrUpload(int size)
		{
			if(size > _currentSize)
			{
				_currentSize = size;
				ResizeToCurrentSize();
			}

			_currentUploadSize = size;
			_currentUploadOffset = _streamBuffer.CurrentOffset;
			return _streamBuffer.CurrentStart;
		}

		public void Advance(FenceSync fence)
		{
			_streamBuffer.Advance(fence);
		}

		public int CopyAndGetGPUBuffer()
		{
			GL.BindBuffer(BufferTarget.CopyReadBuffer, _streamBuffer.Handle);
			GL.BindBuffer(BufferTarget.CopyWriteBuffer, _handleGPU);
			GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, new IntPtr(_currentUploadOffset), IntPtr.Zero, _currentUploadSize);
			GL.BindBuffer(BufferTarget.CopyReadBuffer, 0);
			GL.BindBuffer(BufferTarget.CopyWriteBuffer, 0);

			return _handleGPU;
		}

		void ResizeToCurrentSize()
		{
			ResizeStreamBuffer();
			ResizeGpuBuffer();
		}

		void ResizeStreamBuffer()
		{
			_streamBuffer?.Dispose();
			_streamBuffer = new StreamBuffer(_name + " Upload buffer", BufferTarget.CopyReadBuffer, _currentSize);
		}

		void ResizeGpuBuffer()
		{
			if(_handleGPU != 0)
				GL.DeleteBuffer(_handleGPU);

			_handleGPU = GL.GenBuffer();
			GL.BindBuffer(_target, _handleGPU);
			GL.BufferStorage(_target, new IntPtr(_currentSize), IntPtr.Zero, _flags);
			Glob.Utils.SetObjectLabel(ObjectLabelIdentifier.Buffer, _handleGPU, _name);
			GL.BindBuffer(_target, 0);
		}
	}
}
