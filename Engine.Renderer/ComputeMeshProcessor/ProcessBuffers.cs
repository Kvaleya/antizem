﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Engine.Meshes;
using Engine.Renderer.Meshes;
using Glob;
using OpenTK.Graphics.OpenGL;
using Utils = Engine.Common.Utils;

namespace Engine.Renderer.ComputeMeshProcessor
{
	class ProcessBuffers
	{
		// Mesh processor settings
		public const int MaxClusterCount = 262144; // Enough for about 67M triangles
		public const int IndexBufferSize = 1 << 25; // 32mb
		public const int MaxDraws = InstanceManager.MaxInstances;
		public const int BufferingLevel = 4;

		// Structure sizes
		public const int ClusterBufferSize = MaxClusterCount * ClusterData.Size;

		public const int DrawCallSize = 20;
		public const int DrawWorkGroupDataSize = 8;
		public const int DrawCompactionWorkGroupSize = 64;
		public const int DrawCompactionWorkGroupMaxCount = MaxDraws / DrawCompactionWorkGroupSize * 2; // Why 2?
		public const int DrawCompactionWorkGroupBufferSize = DrawCompactionWorkGroupMaxCount * DrawWorkGroupDataSize;

		public const int IndicesPerCluster = MeshBinary.ClusterSize * 3; // 3: for each vertex in a triangle
		public const int MaxClusterBucketCountTotal = 256 / 2; // ???

		public const int ClusterBucketOffsetBufferSize = MaxClusterBucketCountTotal * 4; // 4: sizeof(int)
		public const int IndirectDispatchBufferSize = MaxClusterBucketCountTotal * 16; // 16: size of indirect dispatch parameters

		public const int MaxPassDrawBatches = 1024; // Draw count params buffer size

		// Buffers

		public RingBufferSet BuffersCulledIndices;
		public RingBufferSet BuffersCompactedDraws;
		public RingBufferSet BuffersDrawParams;

		public int BufferDraws;
		public int BufferDrawCompactionWorkGroups;
		public int BufferClustersCulled;
		public int BufferClusterBucketOffsets;
		public int BufferIndirectDispatchParams; // Surviving cluster counts for each bucket in numThreadsX

		public int BufferClusterDescriptions { get; private set; }

		public List<ClusterData> Clusters = new List<ClusterData>();
		//int _clusterCount = 0;
		public int ClusterBucketCountTotal = 0;
		public int[] ClusterBucketOffsets = new int[MaxClusterBucketCountTotal];

		public long[] DrawWorkGroups = new long[DrawCompactionWorkGroupMaxCount];
		public int DrawCompactionWorkGroupCount = 0;

		ExpandingStreamBuffer _streamUploadClusters;
		StreamBuffer _streamUploadClusterOffsets;
		StreamBuffer _streamUploadDrawCompactionWorkGroups;

		int _uploadOffsetClusterOffsets;
		int _uploadOffsetDrawCompatcionWorkGroups;

		// Get List<T>'s internal array using reflection
		static readonly FieldInfo _fieldListItems = typeof(List<ClusterData>).GetField("_items",
			BindingFlags.Instance | BindingFlags.NonPublic);

		public int GetIndexBufferSize(int targets)
		{
			int size = IndexBufferSize / targets;
			size -= size % 4;
			return size;
		}

		public ProcessBuffers()
		{
			BuffersCulledIndices = new RingBufferSet(BufferTarget.ElementArrayBuffer, IndexBufferSize, "MeshProcessing-Buffer-CulledIndices", BufferingLevel);
			BuffersCompactedDraws = new RingBufferSet(BufferTarget.ShaderStorageBuffer, MaxDraws * DrawCallSize, "MeshProcessing-Buffer-DrawsCompacted", BufferingLevel);
			BuffersDrawParams = new RingBufferSet(BufferTarget.ShaderStorageBuffer, MaxPassDrawBatches * 4, "MeshProcessing-Buffer-DrawParams", BufferingLevel);

			BufferDraws = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(MaxDraws * DrawCallSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-DrawsRaw");
			BufferDrawCompactionWorkGroups = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(DrawCompactionWorkGroupBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-DrawCompactionWorkGroups");

			BufferClusterBucketOffsets = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(ClusterBucketOffsetBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-ClusterBucketOffsets");
			BufferClustersCulled = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(ClusterBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-ClustersCulled");
			BufferIndirectDispatchParams = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(IndirectDispatchBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-IndirectDispatch");

			_streamUploadClusters = new ExpandingStreamBuffer("MeshProcessing-Buffer-ClusterDescriptions", BufferTarget.CopyReadBuffer);
			_streamUploadClusterOffsets = new StreamBuffer("MeshProcessing-Buffer-ClusterBucketOffsets", BufferTarget.CopyReadBuffer, ClusterBucketOffsetBufferSize);
			_streamUploadDrawCompactionWorkGroups = new StreamBuffer("MeshProcessing-Buffer-DrawCompactionWorkGroups-Upload", BufferTarget.CopyReadBuffer, DrawCompactionWorkGroupBufferSize);
		}

		public unsafe void Upload(FenceSync frameSync)
		{
			_uploadOffsetClusterOffsets = _streamUploadClusterOffsets.CurrentOffset;
			_uploadOffsetDrawCompatcionWorkGroups = _streamUploadDrawCompactionWorkGroups.CurrentOffset;

			if(Clusters.Count > 0)
			{
				fixed (ClusterData* ptrSource = &((ClusterData[])_fieldListItems.GetValue(Clusters))[0]) // Get List<T>'s internal array using reflection
				{
					Utils.Unsafe.CisMemcpy(_streamUploadClusters.GetPtrUpload(Clusters.Count * ClusterData.Size), (IntPtr)ptrSource, Clusters.Count * ClusterData.Size);
				}
			}

			fixed (int* ptrSource = &ClusterBucketOffsets[0])
			{
				Utils.Unsafe.CisMemcpy(_streamUploadClusterOffsets.CurrentStart, (IntPtr)ptrSource, ClusterBucketCountTotal * 4);
			}

			fixed (long* ptrSource = &DrawWorkGroups[0])
			{
				Utils.Unsafe.CisMemcpy(_streamUploadDrawCompactionWorkGroups.CurrentStart, (IntPtr)ptrSource, DrawCompactionWorkGroupCount * DrawWorkGroupDataSize);
			}

			_streamUploadClusters.Advance(frameSync);
			_streamUploadClusterOffsets.Advance(frameSync);
			_streamUploadDrawCompactionWorkGroups.Advance(frameSync);
		}

		public void UpdateCopies()
		{
			BufferClusterDescriptions = _streamUploadClusters.CopyAndGetGPUBuffer();
			Glob.Utils.CopyBuffer(_streamUploadClusterOffsets.Handle, BufferClusterBucketOffsets, _uploadOffsetClusterOffsets, 0, ClusterBucketCountTotal * 4);
			Glob.Utils.CopyBuffer(_streamUploadDrawCompactionWorkGroups.Handle, BufferDrawCompactionWorkGroups, _uploadOffsetDrawCompatcionWorkGroups, 0, DrawCompactionWorkGroupBufferSize);
			Clusters.Clear();
			ClusterBucketCountTotal = 0;
			DrawCompactionWorkGroupCount = 0;
		}
	}
}
