﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Meshes;

namespace Engine.Renderer.ComputeMeshProcessor
{
	[Flags]
	enum ClusterFlags
	{
		None = 0,
		TwoSided = 1,
	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	struct ClusterData
	{
		public const int Size = 20;

		public static readonly ClusterData Invalid = new ClusterData(0, 0, 0, 0, 0, 0, 0, 0, false);

		public ushort InstanceIndex; // Used to fetch instance data
		public ushort DrawIndex;

		public int BaseVertex; // Mesh first vertex in shared vertex buffer
		public int FirstIndex; // Cluster first index in shared index buffer, 28bit, 4bit cull flags
		public int WriteDestinationOffset; // 24bit, per instance offset to which surviving indices are written, 8bit triangle count
		public int ClusterIndex; // 24bit, pointer to cluster bounding sphere and normal cone, 8bit cluster bucket index

		public ClusterData(int instanceIndex, int triangleCount, int baseVertex, int firstIndex, int writeOffsetDestination,
			int clusterIndex, int clusterBucketIndex, int drawIndex, bool valid = true)
		{
			Debug.Assert(MeshBinary.ClusterSize <= 256,
				"Triangle count in a cluster cannot be encoded using a single byte, change ClusterData struct!");

			InstanceIndex = (ushort)Math.Min(instanceIndex, 0xffff);
			DrawIndex = (ushort)Math.Min(drawIndex, 0xffff);
			BaseVertex = baseVertex & 0x0fffffff;
			FirstIndex = firstIndex;

			if(!valid)
			{
				BaseVertex = (int)((uint)BaseVertex | 0xf0000000);
			}

			WriteDestinationOffset = writeOffsetDestination & 0x00ffffff;
			ClusterIndex = clusterIndex & 0x00ffffff;

			int tris = Math.Min(Math.Max(triangleCount, 1), 256) - 1;
			WriteDestinationOffset |= tris << 24;

			ClusterIndex |= (clusterBucketIndex & 0xff) << 24;
		}
	}
}
