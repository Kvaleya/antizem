﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Renderer.Meshes;
using Engine.Renderer.Modules;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Utils = Engine.Common.Utils;

namespace Engine.Renderer.ComputeMeshProcessor
{
	class MeshComputeDispatcher
	{
		// OpenGL constants
		const string ExtensionIndirectDrawParameters = "GL_ARB_indirect_parameters";
		const int GL_PARAMETER_BUFFER_ARB = 0x80EE;

		readonly bool _allowDrawCountParams;

		public int ClusterWorkGroupSize { get { return _psoCullClusters.ShaderCompute.WorkGroupSizeX; } }

		ComputePipeline[] _psoCullTrianglesVariants;
		ComputePipeline[] _psoCullTrianglesVoxelVariants;
		ComputePipeline _psoCullClusters;
		ComputePipeline _psoCullClustersVoxels;
		ComputePipeline _psoCullDraws; // Should be rather called "CompactDraws", but since it does pretty much the same thing as the other compute shaders involved in MeshProcessor, it is called similarly to them
		ComputePipeline _psoClearBuffer;
		ComputePipeline _psoClearIndirectDispatchBuffer;

		Device Device { get { return _context.Device; } }
		Renderer.RenderContext _context;

		public MeshComputeDispatcher(Renderer.RenderContext context)
		{
			_context = context;
			_allowDrawCountParams = _context.Device.DeviceInfo.CheckExtensionAvailable(ExtensionIndirectDrawParameters);

			var defineDepthTexRes = new Tuple<string, string>("DEPTHTEXRES", "ivec2(" + GbufferModule.DepthReprojectedSizeX + ", " + GbufferModule.DepthReprojectedSizeY + ")");

			_psoCullTrianglesVariants = new ComputePipeline[(int)MeshProcessFormat.All + 1];
			_psoCullTrianglesVoxelVariants = new ComputePipeline[(int)MeshProcessFormat.All + 1];

			for(int i = 0; i <= (int)MeshProcessFormat.All; i++)
			{
				MeshProcessFormat flags = (MeshProcessFormat)i;
				var defines = new List<Tuple<string, string>>()
				{
					defineDepthTexRes,
				};

				if(flags.HasFlag(MeshProcessFormat.Int32))
				{
					defines.Add(new Tuple<string, string>("INDICES32", "1"));
				}
				if(flags.HasFlag(MeshProcessFormat.Float32))
				{
					//defines.Add(new Tuple<string, string>("POSITIONS32", "1"));
					continue; // Not supported (TODO)
				}

				var voxelDefines = defines.ToList();
				voxelDefines.AddRange(new List<Tuple<string, string>>()
				{
					//new Tuple<string, string>("CULL_BACKFACE", "1"),
					//new Tuple<string, string>("CULL_SIZE", "1"),
					//new Tuple<string, string>("CULL_FRUSTUM", "1"), // broken/unreliable
					//new Tuple<string, string>("CULL_OCCLUSION", "1"),
					new Tuple<string, string>("CULL_CUBE", "1"),
					new Tuple<string, string>("CULL_MSAA", "4"),
					new Tuple<string, string>("SORT_VOXELS", "1"),
					new Tuple<string, string>("CULL_SIZE", "1"),
				});

				defines.AddRange(new List<Tuple<string, string>>()
				{
					new Tuple<string, string>("CULL_BACKFACE", "1"),
					new Tuple<string, string>("CULL_SIZE", "1"),
					//new Tuple<string, string>("CULL_FRUSTUM", "1"), // broken/unreliable
					new Tuple<string, string>("CULL_OCCLUSION", "1"),
					//new Tuple<string, string>("CULL_CUBE", "1"),
				});

				_psoCullTrianglesVariants[i] = new ComputePipeline(Device, Device.GetShader("cullTriangles.comp", defines));
				_psoCullTrianglesVoxelVariants[i] = new ComputePipeline(Device, Device.GetShader("cullTriangles.comp", voxelDefines));
			}

			_psoCullClusters = new ComputePipeline(Device, Device.GetShader("cullClusters.comp", new List<Tuple<string, string>>()
			{
				defineDepthTexRes,
				//new Tuple<string, string>("CULL_BACKFACE", "1"), // broken/unreliable
				new Tuple<string, string>("CULL_FRUSTUM", "1"),
				new Tuple<string, string>("CULL_OCCLUSION", "1"),
				new Tuple<string, string>("KEEP_NEAR_CLUSTERS", "1"),
				//new Tuple<string, string>("CULL_CUBE", "1"),
			}));

			_psoCullClustersVoxels = new ComputePipeline(Device, Device.GetShader("cullClusters.comp", new List<Tuple<string, string>>()
			{
				defineDepthTexRes,
				//new Tuple<string, string>("CULL_BACKFACE", "1"), // broken/unreliable
				//new Tuple<string, string>("CULL_FRUSTUM", "1"),
				//new Tuple<string, string>("CULL_OCCLUSION", "1"),
				//new Tuple<string, string>("KEEP_NEAR_CLUSTERS", "1"),
				new Tuple<string, string>("CULL_CUBE", "1"),
			}));

			_psoCullDraws = new ComputePipeline(Device, Device.GetShader("cullDraws.comp", new List<Tuple<string, string>>()
			{
				//new Tuple<string, string>("COMPACTION_GENERIC", "1"),
			}));

			_psoClearBuffer = new ComputePipeline(Device, Device.GetShader("clearBuffer.comp"));
			_psoClearIndirectDispatchBuffer = new ComputePipeline(Device, Device.GetShader("clearBufferIndirectDispatch.comp"));
		}

		void ClearBuffers(ProcessBuffers buffers)
		{
			// Clear dispatch buffer
			Device.BindPipeline(_psoClearIndirectDispatchBuffer);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, buffers.BufferIndirectDispatchParams);
			Device.ShaderCompute.SetUniformI("count", ProcessBuffers.IndirectDispatchBufferSize);
			Device.DispatchComputeThreads(ProcessBuffers.IndirectDispatchBufferSize);

			// Clear draws buffer
			Device.BindPipeline(_psoClearBuffer);
			int clearCountDraws = ProcessBuffers.DrawCallSize * ProcessBuffers.MaxDraws / 4; // Buffer is interpreted as int array, thus the division by 4
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, buffers.BufferDraws);
			Device.ShaderCompute.SetUniformI("count", clearCountDraws);
			Device.DispatchComputeThreads(clearCountDraws);
			// Also clear the compacted draws buffer
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, buffers.BuffersCompactedDraws.Current);
			Device.DispatchComputeThreads(clearCountDraws);
			// And the draw params buffer
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, buffers.BuffersDrawParams.Current);
			Device.ShaderCompute.SetUniformI("count", ProcessBuffers.MaxPassDrawBatches);
			Device.DispatchComputeThreads(ProcessBuffers.MaxPassDrawBatches);
		}

		void BindOcclusionDepthTexture(MeshProcessCullParams cullParams)
		{
			if(cullParams.OcclusionDepthTex != null)
			{
				Device.BindTexture(cullParams.OcclusionDepthTex, 0);
				Device.ShaderCompute.SetUniformI("maxDepthLevel", cullParams.OcclusionDepthTex.Levels - 1);
			}
			else
			{
				Device.BindTexture(TextureTarget.Texture2D, 0, 0);
			}
		}

		void ClusterCull(ProcessBuffers buffers, MeshProcessCullParams cullParams, MeshProcessPass meshProcessPass)
		{
			bool frustumCull = true;
			bool cubeCull = false;

			if(cullParams.Voxels)
			{
				Device.BindPipeline(_psoCullClustersVoxels);
				frustumCull = false;
				cubeCull = true;
			}
			else
			{
				Device.BindPipeline(_psoCullClusters);
			}

			Device.ShaderCompute.SetUniformI("clusterDataCount", meshProcessPass.ClusterCount);
			Device.ShaderCompute.SetUniformI("clusterDataOffset", meshProcessPass.ClusterOffset);
			if(frustumCull)
			{
				Device.ShaderCompute.SetUniformF("frustumPlane0", cullParams.Plane0);
				Device.ShaderCompute.SetUniformF("frustumPlane1", cullParams.Plane1);
				Device.ShaderCompute.SetUniformF("frustumPlane2", cullParams.Plane2);
				Device.ShaderCompute.SetUniformF("frustumPlane3", cullParams.Plane3);
				Device.ShaderCompute.SetUniformF("cameraOffset", cullParams.CameraOffset);
				Device.ShaderCompute.SetUniformF("cameraForward", cullParams.CameraForward);
				Device.ShaderCompute.SetUniformF("cameraUp", cullParams.CameraUp);
				Device.ShaderCompute.SetUniformF("cameraNear", cullParams.CameraNear);
				Device.ShaderCompute.SetUniformF("projection", cullParams.ProjectionCull);
				BindOcclusionDepthTexture(cullParams);
			}

			if(cubeCull)
			{
				Device.ShaderCompute.SetUniformF("includeBoxCenter", cullParams.IncludeBoxCenter);
				Device.ShaderCompute.SetUniformF("includeBoxSize", cullParams.IncludeBoxSize);
				Device.ShaderCompute.SetUniformF("excludeBoxCenter", cullParams.ExcludeBoxCenter);
				Device.ShaderCompute.SetUniformF("excludeBoxSize", cullParams.ExcludeBoxSize);
			}

			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _context.VertexManager.ClusterBufferHandle);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, buffers.BufferClusterBucketOffsets);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 2, buffers.BufferIndirectDispatchParams);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 3, buffers.BufferClusterDescriptions);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 4, _context.InstanceManager.BufferInstanceData);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 5, buffers.BufferClustersCulled);

			// TODO: proper orientation cull for clusters

			Device.DispatchComputeThreads(meshProcessPass.ClusterCount);
		}

		void DispatchMeshProcessBatch(ProcessBuffers buffers, MeshProcessData data, MeshProcessCullParams cullParams, MeshProcessBatch meshProcessBatch, ref ComputePipeline currentPipeline)
		{
			bool indices32 = meshProcessBatch.Format.HasFlag(MeshProcessFormat.Int32);
			int indicesStride = indices32 ? 4 : 2;

			ComputePipeline pipeline;

			if(cullParams.Voxels)
			{
				pipeline = _psoCullTrianglesVoxelVariants[(int)meshProcessBatch.Format];
			}
			else
			{
				pipeline = _psoCullTrianglesVariants[(int)meshProcessBatch.Format];
			}

			if(pipeline != currentPipeline)
			{
				Device.BindPipeline(pipeline);
				currentPipeline = pipeline;

				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 2, buffers.BuffersCulledIndices.Current);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 3, buffers.BufferDraws);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 4, _context.InstanceManager.BufferInstanceData);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 5, buffers.BufferClustersCulled);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 6, _context.VertexManager.MeshDataBuffer.Handle);

				Device.ShaderCompute.SetUniformI("multiTargetDrawsOffset", data.MultiTargetDrawsOffset);

				Device.ShaderCompute.SetUniformF("resolution", cullParams.Resolution);

				if(!cullParams.Voxels)
				{
					Device.ShaderCompute.SetUniformF("projection", cullParams.ProjectionCull);
					Device.ShaderCompute.SetUniformF("projectionOrientationCull", cullParams.ProjectionOrientationCull);
					BindOcclusionDepthTexture(cullParams);
				}

				if(cullParams.Voxels)
				{
					Device.ShaderCompute.SetUniformF("includeBoxCenter", cullParams.IncludeBoxCenter);
					Device.ShaderCompute.SetUniformF("includeBoxSize", cullParams.IncludeBoxSize);
					Device.ShaderCompute.SetUniformF("excludeBoxCenter", cullParams.ExcludeBoxCenter);
					Device.ShaderCompute.SetUniformF("excludeBoxSize", cullParams.ExcludeBoxSize);
				}

				if(cullParams.SizeCullMatrixes != null)
				{
					Device.ShaderCompute.SetUniformF("voxelMatrix0", cullParams.SizeCullMatrixes[0]);
					Device.ShaderCompute.SetUniformF("voxelMatrix1", cullParams.SizeCullMatrixes[1]);
					Device.ShaderCompute.SetUniformF("voxelMatrix2", cullParams.SizeCullMatrixes[2]);
				}
			}

			Device.ShaderCompute.SetUniformI("multiTargetIndicesOffset", buffers.GetIndexBufferSize(data.NumTargets) / indicesStride);

			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, meshProcessBatch.BufferPositions);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, meshProcessBatch.BufferIndices);
			Device.ShaderCompute.SetUniformI("clusterOffset", meshProcessBatch.ClusterOffset);

			GL.DispatchComputeIndirect(new IntPtr(meshProcessBatch.ClusterBucketIndex * 16));
		}

		void CompactDraws(ProcessBuffers buffers, MeshProcessData data, MeshProcessPass meshProcessPass)
		{
			Device.BindPipeline(_psoCullDraws);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, buffers.BufferDraws);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, buffers.BuffersCompactedDraws.Current);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 2, buffers.BufferDrawCompactionWorkGroups);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 3, buffers.BuffersDrawParams.Current);
			Device.ShaderCompute.SetUniformI("workGroupOffset", meshProcessPass.DrawCompactionWorkGroupFirst);
			Device.ShaderCompute.SetUniformI("multiTargetDrawsOffset", data.MultiTargetDrawsOffset);
			Device.ShaderCompute.SetUniformI("multiTargetDrawBatchOffset", data.MultiTargetDrawBatchOffset);

			Device.DispatchComputeThreads(meshProcessPass.DrawCompactionWorkGroupCount * ProcessBuffers.DrawCompactionWorkGroupSize, data.NumTargets);
		}

		Action<int> GetDrawTrianglesAction(MeshProcessData data, MeshProcessPass meshProcessPass, Action<MeshRenderBatch> onBatchRender)
		{
			return (target) =>
			{
				foreach(var meshProcessBatch in meshProcessPass.MeshProcessBatches)
				{
					string key = "";

					foreach(var drawBatch in meshProcessBatch.DrawBatches)
					{
						var firstMeshDesc = ((Mesh)(drawBatch.Batch.Batch.Instances[0]).Mesh).MeshRenderable.Item.Description;

						if(drawBatch.Batch.Batch.BatchKey != key)
						{
							key = drawBatch.Batch.Batch.BatchKey;
							Device.BindVertexFormat(firstMeshDesc.VertexBufferFormat);
							Device.BindVertexBufferSource(firstMeshDesc.VertexBufferSource);
						}

						DrawElementsType indicesType = DrawElementsType.UnsignedShort;

						if(firstMeshDesc.ElementsBinding.Stride == 4)
							indicesType = DrawElementsType.UnsignedInt;

						if(onBatchRender != null)
							onBatchRender(drawBatch.Batch);

						// TODO: multitarget
						// TODO: multiple indices and positions formats support
						var drawOffset = new IntPtr((drawBatch.DrawOffset + data.MultiTargetDrawsOffset * target) * ProcessBuffers.DrawCallSize);

						if(_allowDrawCountParams)
						{
							GL.MultiDrawElementsIndirectCount(PrimitiveType.Triangles, (Version46)indicesType, drawOffset, new IntPtr((drawBatch.BatchIndex + target * data.MultiTargetDrawBatchOffset) * 4), drawBatch.DrawCount, 0);
						}
						else
						{
							GL.MultiDrawElementsIndirect(PrimitiveType.Triangles, indicesType, drawOffset, drawBatch.DrawCount, 0);
						}
					}
				}
			};
		}

		public void ProcessMeshes(ProcessBuffers buffers, MeshProcessData data, MeshProcessCullParams cullParams, Action<Action<int>> onTrianglesReady, Action<MeshRenderBatch> onBatchRender = null)
		{
			CounterQuery counterPasses = new CounterQuery(_context.FeedbackManager, "CullPasses");

			foreach(var meshProcessPass in data.MeshProcessPasses)
			{
				counterPasses.Add(1);
				var timer = new TimerQueryGPU(_context.FeedbackManager, "CullGPU");

				timer.StartQuery();

				// Clear buffers
				ClearBuffers(buffers);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

				// Cull clusters
				ClusterCull(buffers, cullParams, meshProcessPass);

				GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, buffers.BufferIndirectDispatchParams);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit | MemoryBarrierFlags.CommandBarrierBit);

				// Cull triangles
				ComputePipeline currentPipeline = null;
				foreach(var meshProcessBatch in meshProcessPass.MeshProcessBatches)
				{
					DispatchMeshProcessBatch(buffers, data, cullParams, meshProcessBatch, ref currentPipeline);
				}

				GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, 0);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

				// Compact draws buffer
				CompactDraws(buffers, data, meshProcessPass);

				timer.EndQuery();

				if(_allowDrawCountParams)
				{
					GL.BindBuffer((BufferTarget)GL_PARAMETER_BUFFER_ARB, buffers.BuffersDrawParams.Current);
				}

				GL.BindBuffer(BufferTarget.ElementArrayBuffer, buffers.BuffersCulledIndices.Current);
				GL.BindBuffer(BufferTarget.DrawIndirectBuffer, buffers.BuffersCompactedDraws.Current);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit | MemoryBarrierFlags.ElementArrayBarrierBit | MemoryBarrierFlags.CommandBarrierBit);

				// Draw triangles
				onTrianglesReady(GetDrawTrianglesAction(data, meshProcessPass, onBatchRender));

				if(_allowDrawCountParams)
				{
					GL.BindBuffer((BufferTarget)GL_PARAMETER_BUFFER_ARB, 0);
				}

				buffers.BuffersCulledIndices.Advance();
				buffers.BuffersCompactedDraws.Advance();
				buffers.BuffersDrawParams.Advance();
			}

			GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, 0);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
			GL.BindBuffer(BufferTarget.DrawIndirectBuffer, 0);
		}
	}
}
