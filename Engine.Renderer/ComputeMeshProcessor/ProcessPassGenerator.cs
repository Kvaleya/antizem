﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Renderer.Meshes;
using GameFramework;

namespace Engine.Renderer.ComputeMeshProcessor
{
	[Flags]
	enum ProcessPassGenerateResults
	{
		ClusterBucketOverflow = 1,
		DrawCompactionBufferOverflow = 2,
		DrawBatchBufferOverflow = 4,
	}

	class ProcessPassGenerator
	{
		int _indicesBytesUsed = 0;
		int _drawsUsed = 0;
		int _drawBatches = 0;

		MeshProcessPass _currentPass = null;
		MeshProcessBatch _currentProcessBatch = null;
		MeshProcessDrawBatch _currentDrawBatch = null;
		MeshProcessData _result = null;

		MeshProcessor _meshProcessor;
		ProcessBuffers _buffers { get { return _meshProcessor.Buffers; } }

		ProcessPassGenerateResults _resultFlags = (ProcessPassGenerateResults)0;

		private ProcessPassGenerator(MeshProcessor meshProcessor)
		{
			_meshProcessor = meshProcessor;
		}

		void CreatePass()
		{
			// Generate invalid clusters so that the first cluster of the next pass is aligned to work group size
			while(_buffers.Clusters.Count % _meshProcessor.ClusterWorkGroupSize != 0)
			{
				_buffers.Clusters.Add(ClusterData.Invalid);
			}

			_indicesBytesUsed = 0;
			_drawsUsed = 0;
			_currentPass = new MeshProcessPass();
			_currentPass.ClusterOffset = _buffers.Clusters.Count;
			_currentPass.ClusterCount = 0;
			_result.MeshProcessPasses.Add(_currentPass);

			_currentProcessBatch = null;
			_currentDrawBatch = null;
		}

		void CreateProcessBatch(MeshRenderBatch batch)
		{
			// Generate invalid clusters so that the first cluster of the next batch is aligned to work group size
			while(_buffers.Clusters.Count % _meshProcessor.ClusterWorkGroupSize != 0)
			{
				_buffers.Clusters.Add(ClusterData.Invalid);
				_currentPass.ClusterCount++;
			}

			var processBatch = new MeshProcessBatch();

			var meshDesc = ((Mesh)(batch.Batch.Instances[0]).Mesh).MeshRenderable.Item.Description;

			processBatch.BufferPositions = meshDesc.BufferPositions;
			processBatch.BufferIndices = meshDesc.BufferIndices;
			processBatch.ClusterOffset = _buffers.Clusters.Count;
			processBatch.Format = meshDesc.MeshProcessFormat;
			processBatch.ClusterBucketIndex = _buffers.ClusterBucketCountTotal;
			_buffers.ClusterBucketOffsets[_buffers.ClusterBucketCountTotal] = _buffers.Clusters.Count;
			_buffers.ClusterBucketCountTotal++;

			if(_buffers.ClusterBucketCountTotal >= ProcessBuffers.MaxClusterBucketCountTotal)
			{
				// Cluster bucket count overflow
				_resultFlags = _resultFlags | ProcessPassGenerateResults.ClusterBucketOverflow;
				_buffers.ClusterBucketCountTotal = ProcessBuffers.MaxClusterBucketCountTotal - 1;
			}

			_currentPass.MeshProcessBatches.Add(processBatch);
			_currentProcessBatch = processBatch;

			_currentDrawBatch = null;
		}

		void CreateDrawBatch(MeshRenderBatch batch, int targets)
		{
			_currentDrawBatch = new MeshProcessDrawBatch();
			_currentDrawBatch.Batch = batch;
			_currentDrawBatch.DrawCount = 0;
			_currentDrawBatch.DrawOffset = _drawsUsed;
			_currentDrawBatch.BatchIndex = _drawBatches;
			_currentProcessBatch.DrawBatches.Add(_currentDrawBatch);
			_drawBatches++;
			if(_drawBatches * targets >= ProcessBuffers.MaxPassDrawBatches)
				_resultFlags = _resultFlags | ProcessPassGenerateResults.DrawBatchBufferOverflow;
		}

		static long PackDrawCompactionWorkGroup(int start, int count, int batch, int writeIndex)
		{
			long result = 0;
			result |= (uint)(start & 0xffff);
			result |= (uint)(((count - 1) & 0x3f) << 16);
			result |= (uint)((batch & 0x3ff) << 22);
			result |= (long)writeIndex << 32;
			return result;
		}

		void CreateDrawCompactionWorkGroups()
		{
			foreach(var meshProcessPass in _result.MeshProcessPasses)
			{
				meshProcessPass.DrawCompactionWorkGroupFirst = _buffers.DrawCompactionWorkGroupCount;
				foreach(var meshProcessBatch in meshProcessPass.MeshProcessBatches)
				{
					foreach(var meshProcessDrawBatch in meshProcessBatch.DrawBatches)
					{
						for(int i = 0; i < meshProcessDrawBatch.DrawCount; i += 64)
						{
							_buffers.DrawWorkGroups[_buffers.DrawCompactionWorkGroupCount] = PackDrawCompactionWorkGroup(
								meshProcessDrawBatch.DrawOffset + i, Math.Min(meshProcessDrawBatch.DrawCount - i, 64),
								meshProcessDrawBatch.BatchIndex, meshProcessDrawBatch.DrawOffset);
							_buffers.DrawCompactionWorkGroupCount++;
							meshProcessPass.DrawCompactionWorkGroupCount++;

							if(_buffers.DrawCompactionWorkGroupCount >= ProcessBuffers.DrawCompactionWorkGroupMaxCount)
								_resultFlags = _resultFlags | ProcessPassGenerateResults.DrawCompactionBufferOverflow;
						}
					}
				}
			}
		}

		MeshProcessData PrepareBatches(List<MeshRenderBatch> batches, int targets, ProcessBuffers buffers, CounterQuery counterTriangles = null)
		{
			if(_result != null)
				throw new Exception("Cannot use same process pass generator multiple times!");

			_result = new MeshProcessData();
			_result.NumTargets = targets;

			int indexBufferBytes = buffers.GetIndexBufferSize(targets);

			if(batches.Count == 0)
				return _result;

			CreatePass();

			for(int batchIndex = 0; batchIndex < batches.Count; batchIndex++)
			{
				var batch = batches[batchIndex];

				var firstMeshDesc = ((Mesh)(batch.Batch.Instances[0]).Mesh).MeshRenderable.Item.Description;
				int elementSize = firstMeshDesc.MeshProcessFormat.HasFlag(MeshProcessFormat.Int32) ? 4 : 2;

				if(_currentPass.MeshProcessBatches.Count == 0 ||
				   !_currentPass.MeshProcessBatches[_currentPass.MeshProcessBatches.Count - 1].UsesSameBuffersAs(firstMeshDesc))
				{
					// Buffers or shaders need changing -> use another MeshProcessBatch
					CreateProcessBatch(batch);
				}

				CreateDrawBatch(batch, targets);

				// Generate cluster data for each instance
				for(int i = 0; i < batch.Batch.Instances.Count; i++)
				{
					var instanceData = batch.Batch.Instances[i];
					var mesh = ((Mesh)instanceData.Mesh);

					// TODO: it should be guaranteed that non-loaded meshes will never be rendered but cmvalley crashes on this!?
					if(mesh.MeshRenderable.Item == null)
						continue;

					var instanceMeshDesc = mesh.MeshRenderable.Item.Description;

					if(_drawsUsed * targets + targets >= ProcessBuffers.MaxDraws) // TODO: really break pass due to too many draws? Can there even be more draws?
					{
						// Break pass due to too many draws
						CreatePass();
						CreateProcessBatch(batch);
						CreateDrawBatch(batch, targets);
					}

					int writeDest = _indicesBytesUsed / elementSize;

					for(int clusterIndex = 0; clusterIndex < instanceMeshDesc.Clusters.Count; clusterIndex++)
					{
						int indicesOffset = clusterIndex * ProcessBuffers.IndicesPerCluster;
						int indicesCount = Math.Min(ProcessBuffers.IndicesPerCluster, instanceMeshDesc.Elements.Count - indicesOffset);
						int indicesNewBytes = indicesCount * elementSize;

						if(_indicesBytesUsed + indicesNewBytes >= indexBufferBytes)// || _currentPass.ClusterCount >= ProcessBuffers.MaxClusterCount)
						{
							// Break pass due to too many indices
							// or too many clusters
							// Support mid-draw pass break for very large meshes
							_currentDrawBatch.DrawCount++;
							CreatePass();
							CreateProcessBatch(batch);
							CreateDrawBatch(batch, targets);
							writeDest = 0;
						}

						_buffers.Clusters.Add(new ClusterData(
							batch.FirstInstanceIndex + i,
							indicesCount / 3,
							instanceMeshDesc.Vertices.Start,
							instanceMeshDesc.Elements.Start + indicesOffset,
							writeDest,
							instanceMeshDesc.Clusters.Start + clusterIndex,
							_currentProcessBatch.ClusterBucketIndex,
							_drawsUsed
						));

						counterTriangles?.Add(indicesCount / 3);

						_currentPass.ClusterCount++;

						_indicesBytesUsed += indicesNewBytes;
					}

					_currentDrawBatch.DrawCount++;
					_drawsUsed++;

					while(_indicesBytesUsed % 4 != 0)
					{
						_indicesBytesUsed++;
					}
				}
			}

			// Create draw compaction work groups
			CreateDrawCompactionWorkGroups();

			return _result;
		}

		public static MeshProcessData PrepareBatches(out ProcessPassGenerateResults genResult, MeshProcessor meshProcessor,
			List<MeshRenderBatch> batches, int targets, ProcessBuffers buffers,
			CounterQuery counterTriangles = null)
		{
			var generator = new ProcessPassGenerator(meshProcessor);
			var result = generator.PrepareBatches(batches, targets, buffers, counterTriangles);
			genResult = generator._resultFlags;
			return result;
		}
	}
}