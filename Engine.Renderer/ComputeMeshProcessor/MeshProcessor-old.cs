﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Meshes;
using Engine.Renderer.Meshes;
using Engine.Renderer.Modules;
using GameFramework;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Utils = Engine.Common.Utils;

namespace Engine.Renderer.ComputeMeshProcessor
{
	class MeshProcessor
	{
		const string ExtensionIndirectDrawParameters = "GL_ARB_indirect_parameters";
		const int GL_PARAMETER_BUFFER_ARB = 0x80EE;

		const int ClusterBufferSize = MaxClusterCount * ClusterData.Size;
		const int MaxClusterCount = 262144; // Enough for about 67M triangles

		const int DrawCallSize = 20;

		const int IndexBufferSize = 1 << 25; // 32mb
		const int MaxDraws = InstanceManager.MaxInstances;

		//const int IndexBufferSize = 1 << 10;
		//const int MaxDraws = 256;

		const int DrawCompactionWorkGroupSize = 64; // TODO: zase další buffer, fuj
		const int DrawCompactionWorkGroupBufferSize = MaxDraws / DrawCompactionWorkGroupSize * 2 * DrawWorkGroupDataSize;

		const int BufferingLevel = 4;

		const int IndicesPerCluster = MeshBinary.ClusterSize * 3;

		const int MaxClusterBucketCountTotal = 256 / 2;
		const int ClusterBucketOffsetBufferSize = MaxClusterBucketCountTotal * 4;
		const int IndirectDispatchBufferSize = MaxClusterBucketCountTotal * 16;

		const int MaxPassDrawBatches = 1024; // Draw count params buffer size

		const int DrawWorkGroupDataSize = 8;

		/**/
		RingBufferSet _buffersCulledIndices;
		RingBufferSet _buffersCompactedDraws;
		RingBufferSet _buffersDrawParams;

		int _bufferDraws;
		int _bufferDrawCompactionWorkGroups;
		int _bufferClusters;
		int _bufferClustersCulled;
		int _bufferClusterBucketOffsets;
		int _bufferIndirectDispatchParams; // Surviving cluster counts for each bucket in numThreadsX

		ClusterData[] _clusterData;
		int _clusterCount = 0;
		int _clusterBucketCountTotal = 0;
		int[] _clusterBucketOffsets = new int[MaxClusterBucketCountTotal];

		long[] _drawWorkGroups = new long[DrawCompactionWorkGroupBufferSize / DrawWorkGroupDataSize];
		int _drawCompactionWorkGroupCount = 0;

		StreamBuffer _streamUploadClusters;
		StreamBuffer _streamUploadClusterOffsets;
		StreamBuffer _streamUploadDrawCompactionWorkGroups;

		int _uploadOffsetClusters;
		int _uploadOffsetClusterOffsets;
		int _uploadOffsetDrawCompatcionWorkGroups;
		/**/

		bool _bufferOverflow;

		bool _allowDrawCountParams;

		ComputePipeline[] _psoCullTrianglesVariants;
		ComputePipeline[] _psoCullTrianglesVoxelVariants;
		ComputePipeline _psoCullClusters;
		ComputePipeline _psoCullClustersVoxels;
		ComputePipeline _psoCullDraws; // Should be rather called "CompactDraws", but since it does pretty much the same thing as the other compute shaders involved in MeshProcessor, it is called similarly to them
		ComputePipeline _psoClearBuffer;
		ComputePipeline _psoClearIndirectDispatchBuffer;

		Device Device { get { return _context.Device; } }
		Renderer.RenderContext _context;

		public MeshProcessor(Renderer.RenderContext context)
		{
			_context = context;

			_allowDrawCountParams = _context.Device.DeviceInfo.CheckExtensionAvailable(ExtensionIndirectDrawParameters);
			//_allowDrawCountParams = false;

			_buffersCulledIndices = new RingBufferSet(BufferTarget.ElementArrayBuffer, IndexBufferSize, "MeshProcessing-Buffer-CulledIndices", BufferingLevel);
			_buffersCompactedDraws = new RingBufferSet(BufferTarget.ShaderStorageBuffer, MaxDraws * DrawCallSize, "MeshProcessing-Buffer-DrawsCompacted", BufferingLevel);
			_buffersDrawParams = new RingBufferSet(BufferTarget.ShaderStorageBuffer, MaxPassDrawBatches * 4, "MeshProcessing-Buffer-DrawParams", BufferingLevel);

			_bufferDraws = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(MaxDraws * DrawCallSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-DrawsRaw");
			_bufferDrawCompactionWorkGroups = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(DrawCompactionWorkGroupBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-DrawCompactionWorkGroups");

			_bufferClusters = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(ClusterBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-ClusterDescriptions");
			_bufferClustersCulled = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(ClusterBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-ClustersCulled");
			_bufferClusterBucketOffsets = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(ClusterBucketOffsetBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-ClusterBucketOffsets");
			_bufferIndirectDispatchParams = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(IndirectDispatchBufferSize), IntPtr.Zero, BufferStorageFlags.None, "MeshProcessing-Buffer-IndirectDispatch");

			_streamUploadClusters = new StreamBuffer("MeshProcessing-Buffer-ClusterDescriptions-Upload", BufferTarget.CopyReadBuffer, ClusterBufferSize);
			_streamUploadClusterOffsets = new StreamBuffer("MeshProcessing-Buffer-ClusterBucketOffsets-Upload", BufferTarget.CopyReadBuffer, ClusterBucketOffsetBufferSize);
			_streamUploadDrawCompactionWorkGroups = new StreamBuffer("MeshProcessing-Buffer-DrawCompactionWorkGroups-Upload", BufferTarget.CopyReadBuffer, DrawCompactionWorkGroupBufferSize);

			// Shaders:

			_clusterData = new ClusterData[MaxClusterCount];

			var defineDepthTexRes = new Tuple<string, string>("DEPTHTEXRES", "ivec2(" + GbufferModule.DepthReprojectedSizeX + ", " + GbufferModule.DepthReprojectedSizeY + ")");

			_psoCullTrianglesVariants = new ComputePipeline[(int)MeshProcessFormat.All + 1];
			_psoCullTrianglesVoxelVariants = new ComputePipeline[(int)MeshProcessFormat.All + 1];

			for(int i = 0; i <= (int)MeshProcessFormat.All; i++)
			{
				MeshProcessFormat flags = (MeshProcessFormat)i;
				var defines = new List<Tuple<string, string>>()
				{
					defineDepthTexRes,
				};

				if(flags.HasFlag(MeshProcessFormat.Int32))
				{
					defines.Add(new Tuple<string, string>("INDICES32", "1"));
				}
				if(flags.HasFlag(MeshProcessFormat.Float32))
				{
					//defines.Add(new Tuple<string, string>("POSITIONS32", "1"));
					continue; // Not supported (TODO)
				}

				var voxelDefines = defines.ToList();
				voxelDefines.AddRange(new List<Tuple<string, string>>()
				{
					//new Tuple<string, string>("CULL_BACKFACE", "1"),
					//new Tuple<string, string>("CULL_SIZE", "1"),
					//new Tuple<string, string>("CULL_FRUSTUM", "1"), // broken/unreliable
					//new Tuple<string, string>("CULL_OCCLUSION", "1"),
					new Tuple<string, string>("CULL_CUBE", "1"),
					new Tuple<string, string>("CULL_MSAA", "4"),
					new Tuple<string, string>("SORT_VOXELS", "1"),
					new Tuple<string, string>("CULL_SIZE", "1"),
				});

				defines.AddRange(new List<Tuple<string, string>>()
				{
					new Tuple<string, string>("CULL_BACKFACE", "1"),
					new Tuple<string, string>("CULL_SIZE", "1"),
					//new Tuple<string, string>("CULL_FRUSTUM", "1"), // broken/unreliable
					new Tuple<string, string>("CULL_OCCLUSION", "1"),
					//new Tuple<string, string>("CULL_CUBE", "1"),
				});

				_psoCullTrianglesVariants[i] = new ComputePipeline(Device, Device.GetShader("cullTriangles.comp", defines));
				_psoCullTrianglesVoxelVariants[i] = new ComputePipeline(Device, Device.GetShader("cullTriangles.comp", voxelDefines));
			}

			_psoCullClusters = new ComputePipeline(Device, Device.GetShader("cullClusters.comp", new List<Tuple<string, string>>()
			{
				defineDepthTexRes,
				//new Tuple<string, string>("CULL_BACKFACE", "1"), // broken/unreliable
				new Tuple<string, string>("CULL_FRUSTUM", "1"),
				new Tuple<string, string>("CULL_OCCLUSION", "1"),
				new Tuple<string, string>("KEEP_NEAR_CLUSTERS", "1"),
				//new Tuple<string, string>("CULL_CUBE", "1"),
			}));

			_psoCullClustersVoxels = new ComputePipeline(Device, Device.GetShader("cullClusters.comp", new List<Tuple<string, string>>()
			{
				defineDepthTexRes,
				//new Tuple<string, string>("CULL_BACKFACE", "1"), // broken/unreliable
				//new Tuple<string, string>("CULL_FRUSTUM", "1"),
				//new Tuple<string, string>("CULL_OCCLUSION", "1"),
				//new Tuple<string, string>("KEEP_NEAR_CLUSTERS", "1"),
				new Tuple<string, string>("CULL_CUBE", "1"),
			}));

			_psoCullDraws = new ComputePipeline(Device, Device.GetShader("cullDraws.comp", new List<Tuple<string, string>>()
			{
				//new Tuple<string, string>("COMPACTION_GENERIC", "1"),
			}));

			_psoClearBuffer = new ComputePipeline(Device, Device.GetShader("clearBuffer.comp"));
			_psoClearIndirectDispatchBuffer = new ComputePipeline(Device, Device.GetShader("clearBufferIndirectDispatch.comp"));
		}

		long PackDrawCompactionWorkGroup(int start, int count, int batch, int writeIndex)
		{
			long result = 0;
			result |= (uint)(start & 0xffff);
			result |= (uint)(((count - 1) & 0x3f) << 16);
			result |= (uint)((batch & 0x3ff) << 22);
			result |= (long)writeIndex << 32;
			return result;
		}

		int GetIndexBufferSize(int targets)
		{
			int size = IndexBufferSize / targets;
			size -= size % 4;
			return size;
		}

		// TODO: change instancing to use a global instance buffer and only upload instance indices for rendering
		// TODO: unfuck batching

		public MeshProcessData PrepareBatches(List<MeshRenderBatch> batches)
		{
			return PrepareBatches(batches, 1);
		}

		public MeshProcessData PrepareBatchesVoxelize(List<MeshRenderBatch> batches)
		{
			return PrepareBatches(batches, 3);
		}

		public MeshProcessData PrepareBatchesShadow(List<MeshRenderBatch> batches, int numCascades)
		{
			// TODO: implement this in shaders (4 bits per cluster)
			return PrepareBatches(batches, numCascades);
		}

		// Process large amounts of geometry with few compute dispatches
		// Assumes that the batch list is sorted so that meshes with same format are next to each other
		// targets: how many actual culled instances are generated from each instance, 1 for usual rendering, 3 for voxelisation (one for each axis)
		MeshProcessData PrepareBatches(List<MeshRenderBatch> batches, int targets)
		{
			TimerQueryCPU timer = new TimerQueryCPU(_context.FeedbackManager, "ClusterGen");
			timer.StartQuery();

			CounterQuery counterTris = new CounterQuery(_context.FeedbackManager, "Tris");

			MeshProcessData result = new MeshProcessData();
			result.NumTargets = targets;

			int indicesBytesUsed = 0;
			int drawsUsed = 0;
			int drawBatches = 0;
			MeshProcessPass currentPass = null;
			MeshProcessBatch currentProcessBatch = null;
			MeshProcessDrawBatch currentDrawBatch = null;

			int indexBufferBytes = GetIndexBufferSize(targets);

			if(batches.Count == 0)
				return result;

			Action createPass = () =>
			{
				while(_clusterCount % _psoCullClusters.ShaderCompute.WorkGroupSizeX != 0)
				{
					_clusterData[_clusterCount] = ClusterData.Invalid;
					_clusterCount++;
				}

				indicesBytesUsed = 0;
				drawsUsed = 0;
				currentPass = new MeshProcessPass();
				currentPass.ClusterOffset = _clusterCount;
				currentPass.ClusterCount = 0;
				result.MeshProcessPasses.Add(currentPass);

				currentProcessBatch = null;
				currentDrawBatch = null;
			};

			Action<MeshRenderBatch> createProcessBatch = (batch) =>
			{
				while(_clusterCount % _psoCullClusters.ShaderCompute.WorkGroupSizeX != 0)
				{
					_clusterData[_clusterCount] = ClusterData.Invalid;
					_clusterCount++;
					currentPass.ClusterCount++;
				}

				var processBatch = new MeshProcessBatch();

				var meshDesc = ((Mesh)(batch.Batch.Instances[0]).Mesh).MeshRenderable.Item.Description;

				processBatch.BufferPositions = meshDesc.BufferPositions;
				processBatch.BufferIndices = meshDesc.BufferIndices;
				processBatch.ClusterOffset = _clusterCount;
				processBatch.Format = meshDesc.MeshProcessFormat;
				processBatch.ClusterBucketIndex = _clusterBucketCountTotal;
				_clusterBucketOffsets[_clusterBucketCountTotal] = _clusterCount;
				_clusterBucketCountTotal++;

				if(_clusterBucketCountTotal >= MaxClusterBucketCountTotal)
				{
					_clusterBucketCountTotal = MaxClusterBucketCountTotal - 1;
					_bufferOverflow = true;
					// TODO: crash to desktop or something
				}

				currentPass.MeshProcessBatches.Add(processBatch);
				currentProcessBatch = processBatch;

				currentDrawBatch = null;
			};

			Action<MeshRenderBatch> createDrawBatch = (batch) =>
			{
				currentDrawBatch = new MeshProcessDrawBatch();
				currentDrawBatch.Batch = batch;
				currentDrawBatch.DrawCount = 0;
				currentDrawBatch.DrawOffset = drawsUsed;
				currentDrawBatch.BatchIndex = drawBatches;
				currentProcessBatch.DrawBatches.Add(currentDrawBatch);
				drawBatches++;
				if(drawBatches * targets >= MaxPassDrawBatches)
					_context.TextOutput.Print(OutputType.PerformanceWarning, "Draw batch buffer overflow!");
			};
			
			createPass();

			for(int batchIndex = 0; batchIndex < batches.Count; batchIndex++)
			{
				var batch = batches[batchIndex];

				var firstMeshDesc = ((Mesh)(batch.Batch.Instances[0]).Mesh).MeshRenderable.Item.Description;
				int elementSize = firstMeshDesc.MeshProcessFormat.HasFlag(MeshProcessFormat.Int32) ? 4 : 2;

				if(currentPass.MeshProcessBatches.Count == 0 ||
				   !currentPass.MeshProcessBatches[currentPass.MeshProcessBatches.Count - 1].UsesSameBuffersAs(firstMeshDesc))
				{
					// Buffers or shaders need changing -> use another MeshProcessBatch
					createProcessBatch(batch);
				}

				createDrawBatch(batch);

				// Generate cluster data for each instance
				for(int i = 0; i < batch.Batch.Instances.Count; i++)
				{
					var instanceData = batch.Batch.Instances[i];
					var mesh = ((Mesh) instanceData.Mesh);

					var instanceMeshDesc = mesh.MeshRenderable.Item.Description;

					if(drawsUsed * targets + targets >= MaxDraws)
					{
						// Break pass due to too many draws
						createPass();
						createProcessBatch(batch);
						createDrawBatch(batch);
					}

					int writeDest = indicesBytesUsed / elementSize;

					for(int clusterIndex = 0; clusterIndex < instanceMeshDesc.Clusters.Count; clusterIndex++)
					{
						int indicesOffset = clusterIndex * IndicesPerCluster;
						int indicesCount = Math.Min(IndicesPerCluster, instanceMeshDesc.Elements.Count - indicesOffset);
						int indicesNewBytes = indicesCount * elementSize;

						if(indicesBytesUsed + indicesNewBytes >= indexBufferBytes)
						{
							// Break pass due to too many indices
							// Support mid-draw pass break for very large meshes
							currentDrawBatch.DrawCount++;
							createPass();
							createProcessBatch(batch);
							createDrawBatch(batch);
							writeDest = 0;
						}

						if(_clusterCount >= _clusterData.Length)
						{
							_bufferOverflow = true; // TODO: do this?
							_context.TextOutput.Print(OutputType.PerformanceWarning, "Cluster buffer overflow!");
							break;
						}

						_clusterData[_clusterCount] = new ClusterData(
							batch.FirstInstanceIndex + i,
							indicesCount / 3,
							instanceMeshDesc.Vertices.Start,
							instanceMeshDesc.Elements.Start + indicesOffset,
							writeDest,
							instanceMeshDesc.Clusters.Start + clusterIndex,
							currentProcessBatch.ClusterBucketIndex,
							drawsUsed
						);

						counterTris.Add(indicesCount / 3);
						
						_clusterCount++;
						currentPass.ClusterCount++;

						indicesBytesUsed += indicesNewBytes;
					}

					currentDrawBatch.DrawCount++;
					drawsUsed++;

					while(indicesBytesUsed % 4 != 0)
					{
						indicesBytesUsed++;
					}
				}
			}

			// Create draw compaction work groups
			foreach(var meshProcessPass in result.MeshProcessPasses)
			{
				meshProcessPass.DrawCompactionWorkGroupFirst = _drawCompactionWorkGroupCount;
				foreach(var meshProcessBatch in meshProcessPass.MeshProcessBatches)
				{
					foreach(var meshProcessDrawBatch in meshProcessBatch.DrawBatches)
					{
						for(int i = 0; i < meshProcessDrawBatch.DrawCount; i += 64)
						{
							_drawWorkGroups[_drawCompactionWorkGroupCount] = PackDrawCompactionWorkGroup(
								meshProcessDrawBatch.DrawOffset + i, Math.Min(meshProcessDrawBatch.DrawCount - i, 64),
								meshProcessDrawBatch.BatchIndex, meshProcessDrawBatch.DrawOffset);
							_drawCompactionWorkGroupCount++;
							meshProcessPass.DrawCompactionWorkGroupCount++;

							if(_drawCompactionWorkGroupCount == DrawCompactionWorkGroupBufferSize / DrawWorkGroupDataSize)
								_context.TextOutput.Print(OutputType.PerformanceWarning, "Draw compaction work group buffer overflow!");
						}
					}
				}
			}

			timer.EndQuery();
			return result;
		}

		public unsafe void Upload(FenceSync frameSync)
		{
			_uploadOffsetClusters = _streamUploadClusters.CurrentOffset;
			_uploadOffsetClusterOffsets = _streamUploadClusterOffsets.CurrentOffset;
			_uploadOffsetDrawCompatcionWorkGroups = _streamUploadDrawCompactionWorkGroups.CurrentOffset;

			fixed (ClusterData* ptrSource = &_clusterData[0])
			{
				Utils.Unsafe.CopyMemory(_streamUploadClusters.CurrentStart, (IntPtr)ptrSource, (uint)_clusterCount * ClusterData.Size);
			}

			fixed (int* ptrSource = &_clusterBucketOffsets[0])
			{
				Utils.Unsafe.CopyMemory(_streamUploadClusterOffsets.CurrentStart, (IntPtr)ptrSource, (uint)_clusterBucketCountTotal * 4);
			}

			fixed (long* ptrSource = &_drawWorkGroups[0])
			{
				Utils.Unsafe.CopyMemory(_streamUploadDrawCompactionWorkGroups.CurrentStart, (IntPtr)ptrSource, (uint)_drawCompactionWorkGroupCount * DrawWorkGroupDataSize);
			}

			_streamUploadClusters.Advance(frameSync);
			_streamUploadClusterOffsets.Advance(frameSync);
			_streamUploadDrawCompactionWorkGroups.Advance(frameSync);
		}

		public void UpdateCopies()
		{
			Glob.Utils.CopyBuffer(_streamUploadClusters.Handle, _bufferClusters, _uploadOffsetClusters, 0, _clusterCount * ClusterData.Size);
			Glob.Utils.CopyBuffer(_streamUploadClusterOffsets.Handle, _bufferClusterBucketOffsets, _uploadOffsetClusterOffsets, 0, _clusterBucketCountTotal * 4);
			Glob.Utils.CopyBuffer(_streamUploadDrawCompactionWorkGroups.Handle, _bufferDrawCompactionWorkGroups, _uploadOffsetDrawCompatcionWorkGroups, 0, DrawCompactionWorkGroupBufferSize);
			_clusterCount = 0;
			_clusterBucketCountTotal = 0;
			_drawCompactionWorkGroupCount = 0;
		}

		public void ProcessMeshes(MeshProcessData data, Renderer.RenderContext context, MeshProcessCullParams cullParams, Action<Action<int>> onTrianglesReady, Action<MeshRenderBatch> onBatchRender = null)
		{
			var device = context.Device;

			int indexBufferSize = GetIndexBufferSize(data.NumTargets);
			int multiTargetDrawsOffset = MaxDraws / data.NumTargets;
			int multiTargetDrawBatchOffset = MaxPassDrawBatches / data.NumTargets;

			bool frustumCull = true;
			bool cubeCull = false;

			if(cullParams.MeshCullMode == MeshCullMode.Voxel)
			{
				frustumCull = false;
				cubeCull = true;
			}

			// TODO: support for non-perspective camera frustum cull
			Vector4 plane0 = Vector4.Zero, plane1 = Vector4.Zero, plane2 = Vector4.Zero, plane3 = Vector4.Zero;

			if(frustumCull)
			{
				Utils.Math.GetPlanes(cullParams.ProjectionOrientationCull, out plane0, out plane1, out plane2, out plane3);
			}

			CounterQuery counterPasses = new CounterQuery(_context.FeedbackManager, "CullPasses");

			foreach(var meshProcessPass in data.MeshProcessPasses)
			{
				counterPasses.Add(1);
				var timer = new TimerQueryGPU(_context.FeedbackManager, "CullGPU");

				timer.StartQuery();

				// TODO: do all cluster culling at once for all passes using a single dispatch?
				int maxDepthMip = 0;
				if(cullParams.OcclusionDepthTex != null)
				{
					device.BindTexture(cullParams.OcclusionDepthTex, 0);
					maxDepthMip = cullParams.OcclusionDepthTex.Levels - 1;
				}
				else
				{
					device.BindTexture(TextureTarget.Texture2D, 0, 0);
				}

				// Clear dispatch buffer
				device.BindPipeline(_psoClearIndirectDispatchBuffer);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _bufferIndirectDispatchParams);
				device.ShaderCompute.SetUniformI("count", IndirectDispatchBufferSize);
				device.DispatchComputeThreads(IndirectDispatchBufferSize);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

				// Cluster cull
				if(cullParams.MeshCullMode == MeshCullMode.Voxel)
				{
					device.BindPipeline(_psoCullClustersVoxels);
				}
				else
				{
					device.BindPipeline(_psoCullClusters);
				}
				
				device.ShaderCompute.SetUniformI("clusterDataCount", meshProcessPass.ClusterCount);
				device.ShaderCompute.SetUniformI("clusterDataOffset", meshProcessPass.ClusterOffset);
				if(frustumCull)
				{
					device.ShaderCompute.SetUniformF("frustumPlane0", plane0);
					device.ShaderCompute.SetUniformF("frustumPlane1", plane1);
					device.ShaderCompute.SetUniformF("frustumPlane2", plane2);
					device.ShaderCompute.SetUniformF("frustumPlane3", plane3);
					device.ShaderCompute.SetUniformF("cameraOffset", cullParams.CameraOffset);
					device.ShaderCompute.SetUniformF("cameraForward", cullParams.CameraForward);
					device.ShaderCompute.SetUniformF("cameraUp", cullParams.CameraUp);
					device.ShaderCompute.SetUniformF("cameraNear", cullParams.CameraNear);
					device.ShaderCompute.SetUniformF("projection", cullParams.ProjectionCull);
					device.ShaderCompute.SetUniformI("maxDepthLevel", maxDepthMip);
				}

				if(cubeCull)
				{
					device.ShaderCompute.SetUniformF("includeBoxCenter", cullParams.IncludeBoxCenter);
					device.ShaderCompute.SetUniformF("includeBoxSize", cullParams.IncludeBoxSize);
					device.ShaderCompute.SetUniformF("excludeBoxCenter", cullParams.ExcludeBoxCenter);
					device.ShaderCompute.SetUniformF("excludeBoxSize", cullParams.ExcludeBoxSize);
				}

				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, context.VertexManager.ClusterBufferHandle);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, _bufferClusterBucketOffsets);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 2, _bufferIndirectDispatchParams);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 3, _bufferClusters);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 4, context.InstanceManager.BufferInstanceData);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 5, _bufferClustersCulled);

				// TODO: proper orientation cull for clusters

				device.DispatchComputeThreads(meshProcessPass.ClusterCount);

				// Clear draws buffer
				context.Device.BindPipeline(_psoClearBuffer);
				int clearCountDraws = DrawCallSize * MaxDraws / 4; // Buffer is interpreted as int array, thus the division by 4
				context.Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _bufferDraws);
				context.Device.ShaderCompute.SetUniformI("count", clearCountDraws);
				device.DispatchComputeThreads(clearCountDraws);
				// Also clear the compacted draws buffer
				context.Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _buffersCompactedDraws.Current);
				device.DispatchComputeThreads(clearCountDraws);
				// And the draw params buffer
				context.Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _buffersDrawParams.Current);
				context.Device.ShaderCompute.SetUniformI("count", MaxPassDrawBatches);
				device.DispatchComputeThreads(MaxPassDrawBatches);

				GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, _bufferIndirectDispatchParams);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit | MemoryBarrierFlags.CommandBarrierBit);

				ComputePipeline currentPipeline = null;

				foreach(var meshProcessBatch in meshProcessPass.MeshProcessBatches)
				{
					bool indices32 = meshProcessBatch.Format.HasFlag(MeshProcessFormat.Int32);
					int indicesStride = indices32 ? 4 : 2;

					ComputePipeline pipeline;

					if(cullParams.MeshCullMode == MeshCullMode.Voxel)
					{
						pipeline = _psoCullTrianglesVoxelVariants[(int)meshProcessBatch.Format];
					}
					else
					{
						pipeline = _psoCullTrianglesVariants[(int)meshProcessBatch.Format];
					}

					if(pipeline != currentPipeline)
					{
						device.BindPipeline(pipeline);
						currentPipeline = pipeline;

						device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 2, _buffersCulledIndices.Current);
						device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 3, _bufferDraws);
						device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 4, context.InstanceManager.BufferInstanceData);
						device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 5, _bufferClustersCulled);
						device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 6, context.VertexManager.MeshDataBuffer.Handle);

						device.ShaderCompute.SetUniformI("multiTargetDrawsOffset", multiTargetDrawsOffset);

						device.ShaderCompute.SetUniformF("resolution", cullParams.Resolution);

						if(frustumCull)
						{
							device.ShaderCompute.SetUniformF("projection", cullParams.ProjectionCull);
							device.ShaderCompute.SetUniformF("projectionOrientationCull", cullParams.ProjectionOrientationCull);
							device.ShaderCompute.SetUniformI("maxDepthLevel", maxDepthMip);
						}

						if(cubeCull)
						{
							device.ShaderCompute.SetUniformF("includeBoxCenter", cullParams.IncludeBoxCenter);
							device.ShaderCompute.SetUniformF("includeBoxSize", cullParams.IncludeBoxSize);
							device.ShaderCompute.SetUniformF("excludeBoxCenter", cullParams.ExcludeBoxCenter);
							device.ShaderCompute.SetUniformF("excludeBoxSize", cullParams.ExcludeBoxSize);
						}

						if(cullParams.SizeCullMatrixes != null)
						{
							device.ShaderCompute.SetUniformF("voxelMatrix0", cullParams.SizeCullMatrixes[0]);
							device.ShaderCompute.SetUniformF("voxelMatrix1", cullParams.SizeCullMatrixes[1]);
							device.ShaderCompute.SetUniformF("voxelMatrix2", cullParams.SizeCullMatrixes[2]);
						}
					}

					int multiTargetIndicesOffset = indexBufferSize / indicesStride;

					device.ShaderCompute.SetUniformI("multiTargetIndicesOffset", multiTargetIndicesOffset);

					device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, meshProcessBatch.BufferPositions);
					device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, meshProcessBatch.BufferIndices);
					device.ShaderCompute.SetUniformI("clusterOffset", meshProcessBatch.ClusterOffset);

					GL.DispatchComputeIndirect(new IntPtr(meshProcessBatch.ClusterBucketIndex * 16));
				}

				
				GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, 0);
				
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);
				
				device.BindPipeline(_psoCullDraws);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _bufferDraws);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, _buffersCompactedDraws.Current);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 2, _bufferDrawCompactionWorkGroups);
				device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 3, _buffersDrawParams.Current);
				device.ShaderCompute.SetUniformI("workGroupOffset", meshProcessPass.DrawCompactionWorkGroupFirst);
				device.ShaderCompute.SetUniformI("multiTargetDrawsOffset", multiTargetDrawsOffset);
				device.ShaderCompute.SetUniformI("multiTargetDrawBatchOffset", multiTargetDrawBatchOffset);

				device.DispatchComputeThreads(meshProcessPass.DrawCompactionWorkGroupCount * DrawCompactionWorkGroupSize, data.NumTargets);
				
				//Glob.Utils.CopyBuffer(_bufferDraws, _buffersCompactedDraws.Current, 0 ,0, MaxDraws * DrawCallSize);

				if(_allowDrawCountParams)
				{
					GL.BindBuffer((BufferTarget)GL_PARAMETER_BUFFER_ARB, _buffersDrawParams.Current);
				}
				
				GL.BindBuffer(BufferTarget.ElementArrayBuffer, _buffersCulledIndices.Current);
				GL.BindBuffer(BufferTarget.DrawIndirectBuffer, _buffersCompactedDraws.Current);
				GL.MemoryBarrier(MemoryBarrierFlags.ElementArrayBarrierBit | MemoryBarrierFlags.CommandBarrierBit);

				timer.EndQuery();

				Action<int> drawTriangles = (target) =>
				{
					foreach(var meshProcessBatch in meshProcessPass.MeshProcessBatches)
					{
						string key = "";

						foreach(var drawBatch in meshProcessBatch.DrawBatches)
						{
							var firstMeshDesc = ((Mesh)(drawBatch.Batch.Batch.Instances[0]).Mesh).MeshRenderable.Item.Description;

							if(drawBatch.Batch.Batch.BatchKey != key)
							{
								key = drawBatch.Batch.Batch.BatchKey;
								device.BindVertexFormat(firstMeshDesc.VertexBufferFormat);
								device.BindVertexBufferSource(firstMeshDesc.VertexBufferSource);
							}

							DrawElementsType indicesType = DrawElementsType.UnsignedShort;

							if(firstMeshDesc.ElementsBinding.Stride == 4)
								indicesType = DrawElementsType.UnsignedInt;

							if(onBatchRender != null)
								onBatchRender(drawBatch.Batch);

							// TODO: multitarget
							// TODO: multiple indices and positions formats support
							var drawOffset = new IntPtr((drawBatch.DrawOffset + multiTargetDrawsOffset * target) * DrawCallSize);

							if(_allowDrawCountParams)
							{
								GL.MultiDrawElementsIndirectCount(PrimitiveType.Triangles, (Version46)indicesType, drawOffset, new IntPtr((drawBatch.BatchIndex + target * multiTargetDrawBatchOffset) * 4), drawBatch.DrawCount, 0);
							}
							else
							{
								GL.MultiDrawElementsIndirect(PrimitiveType.Triangles, indicesType, drawOffset, drawBatch.DrawCount, 0);
							}
						}
					}
				};

				onTrianglesReady(drawTriangles);

				if(_allowDrawCountParams)
				{
					GL.BindBuffer((BufferTarget)GL_PARAMETER_BUFFER_ARB, 0);
				}

				_buffersCulledIndices.Advance();
				_buffersCompactedDraws.Advance();
				_buffersDrawParams.Advance();
			}

			GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, 0);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
			GL.BindBuffer(BufferTarget.DrawIndirectBuffer, 0);
		}
	}
}
