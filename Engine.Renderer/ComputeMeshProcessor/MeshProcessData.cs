﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Meshes;
using Engine.Renderer.Meshes;
using Engine.Renderer.Meshes.Buffers;
using Glob;
using OpenTK;

namespace Engine.Renderer.ComputeMeshProcessor
{
	[Flags]
	enum MeshProcessFormat
	{
		Unorm16Int16 = 0,
		Unorm16Int32 = Int32,
		Float32Int16 = Float32,
		Float32Int32 = Int32 | Float32,
		Float32 = 2,
		Int32 = 1,
		All = 3,
		// TODO: animated formats (skinned and trees)
	}

	class MeshProcessDrawBatch
	{
		public MeshRenderBatch Batch;
		public int BatchIndex;
		public int DrawCount;
		public int DrawOffset;
	}

	// One MeshProcessBatch -> one compute dispatch to cull triangles (because of required buffer switch)
	class MeshProcessBatch
	{
		public int ClusterBucketIndex = 0;
		public int ClusterOffset = 0;
		public int BufferPositions = 0;
		public int BufferIndices = 0;
		public MeshProcessFormat Format = MeshProcessFormat.Unorm16Int16;
		public List<MeshProcessDrawBatch> DrawBatches = new List<MeshProcessDrawBatch>();

		public bool UsesSameBuffersAs(MeshBufferDescription desc)
		{
			return desc.BufferPositions == BufferPositions && desc.BufferIndices == BufferIndices;
		}
	}

	class MeshProcessPass
	{
		public int ClusterOffset = 0;
		public int ClusterCount = 0;
		public int DrawCompactionWorkGroupFirst = 0;
		public int DrawCompactionWorkGroupCount = 0;
		public List<MeshProcessBatch> MeshProcessBatches = new List<MeshProcessBatch>();
	}

	// Needed to launch culling of a collection of mesh instances
	class MeshProcessData
	{
		public int NumTargets;
		public List<MeshProcessPass> MeshProcessPasses = new List<MeshProcessPass>();

		public int MultiTargetDrawsOffset
		{
			get { return ProcessBuffers.MaxDraws / NumTargets; }
		}

		public int MultiTargetDrawBatchOffset
		{
			get { return ProcessBuffers.MaxPassDrawBatches / NumTargets; }
		}
	}

	class MeshProcessCullParams
	{
		//public MeshCullMode MeshCullMode = MeshCullMode.Default;
		public bool Voxels = false;
		// Default cull
		public Matrix4 ProjectionCull = Matrix4.Identity;
		public Matrix4 ProjectionOrientationCull = Matrix4.Identity;
		public Texture2D OcclusionDepthTex = null;
		public Vector3 CameraOffset = Vector3.Zero;
		public Vector3 CameraForward = Vector3.UnitZ;
		public Vector3 CameraUp = Vector3.UnitY;
		public float CameraNear = 0;
		public Vector2 Resolution = Vector2.Zero;
		// Voxel cull params
		public Vector3 IncludeBoxCenter = Vector3.Zero;
		public Vector3 IncludeBoxSize = Vector3.Zero; // Half of box extends (radius-like)
		public Vector3 ExcludeBoxCenter = Vector3.Zero;
		public Vector3 ExcludeBoxSize = Vector3.Zero;
		public Vector4 Plane0 = Vector4.Zero;
		public Vector4 Plane1 = Vector4.Zero;
		public Vector4 Plane2 = Vector4.Zero;
		public Vector4 Plane3 = Vector4.Zero;
		public Matrix4[] SizeCullMatrixes = null;
	}
}
