﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Renderer.Meshes;
using Engine.Renderer.Modules;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Utils = Engine.Common.Utils;

namespace Engine.Renderer.ComputeMeshProcessor
{
	class MeshProcessor
	{
		public int ClusterWorkGroupSize { get { return _computeDispatcher.ClusterWorkGroupSize; } }

		public ProcessBuffers Buffers;
		MeshComputeDispatcher _computeDispatcher;

		Renderer.RenderContext _context;

		public MeshProcessor(Renderer.RenderContext context)
		{
			_context = context;

			Buffers = new ProcessBuffers();
			_computeDispatcher = new MeshComputeDispatcher(context);
		}

		// Old API
		public MeshProcessData PrepareBatches(List<MeshRenderBatch> batches)
		{
			return PrepareBatches(batches, 1);
		}

		public MeshProcessData PrepareBatchesVoxelize(List<MeshRenderBatch> batches)
		{
			return PrepareBatches(batches, 3);
		}

		public MeshProcessData PrepareBatchesShadow(List<MeshRenderBatch> batches, int numCascades)
		{
			// TODO: implement this in shaders (4 bits per cluster)
			return PrepareBatches(batches, numCascades);
		}

		private MeshProcessData PrepareBatches(List<MeshRenderBatch> batches, int targets)
		{
			CounterQuery counterTris = new CounterQuery(_context.FeedbackManager, "Tris");
			ProcessPassGenerateResults genResult;
			return ProcessPassGenerator.PrepareBatches(out genResult, this, batches, targets, Buffers, counterTris);
		}

		public void Upload(FenceSync fence)
		{
			Buffers.Upload(fence);
		}

		public void UpdateCopies()
		{
			Buffers.UpdateCopies();
		}

		public void ProcessMeshes(MeshProcessData data, MeshProcessCullParams cullParams, Action<Action<int>> onTrianglesReady, Action<MeshRenderBatch> onBatchRender = null)
		{
			_computeDispatcher.ProcessMeshes(Buffers, data, cullParams, onTrianglesReady, onBatchRender);
		}
	}
}
