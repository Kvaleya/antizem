﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Renderer.ComputeMeshProcessor;
using Glob;
using OpenTK;

namespace Engine.Renderer.Meshes.Buffers
{
	class MeshBufferDescription
	{
		public readonly string FormatKey;
		public VertexBufferFormat VertexBufferFormat;
		public VertexBufferSource VertexBufferSource;
		public Segment Elements;
		public Segment Vertices;
		public Segment Clusters;

		public MeshProcessFormat MeshProcessFormat;

		public VertexBufferBinding ElementsBinding;
		public VertexBufferBinding ClustersBinding;

		public Vector3 PositionsOffset = Vector3.Zero;
		public Vector3 PositionsScale = Vector3.One;

		public Vector2 TexCoordsOffset = Vector2.Zero;
		public Vector2 TexCoordsScale = Vector2.One;

		public Vector4 BoundingSphere = Vector4.Zero;

		public int Index;

		public int BufferPositions { get { return VertexBufferSource.GetBinding(0).Buffer; } }
		public int BufferIndices { get { return ElementsBinding.Buffer; } }

		public MeshBufferDescription(string key, MeshProcessFormat meshProcessFormat)
		{
			FormatKey = key;
			MeshProcessFormat = meshProcessFormat;
		}
	}
}
