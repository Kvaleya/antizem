﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Renderer.Meshes.Buffers
{
	interface IBuffer
	{
		int Handle { get; }
	}
}
