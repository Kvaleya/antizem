﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Meshes;

namespace Engine.Renderer.Meshes.Buffers
{
	class BufferSet
	{
		public readonly Dictionary<VertexStreamUsage, IBufferDynamic> Buffers;
		List<IBufferDynamic> _buffers = new List<IBufferDynamic>();
		HashSet<Segment> _segments;

		public BufferSet(List<Tuple<VertexStreamUsage, IBufferDynamic>> buffers)
		{
			Buffers = new Dictionary<VertexStreamUsage, IBufferDynamic>();
			foreach(var tuple in buffers)
			{
				_buffers.Add(tuple.Item2);
				Buffers.Add(tuple.Item1, tuple.Item2);
			}
			_segments = new HashSet<Segment>();
		}

		public Segment Alloc(int elementCount)
		{
			int start = 0;
			foreach(var buffer in _buffers)
			{
				// All buffers should return the same value
				start = buffer.AllocElements(elementCount);
			}
			var segment = new Segment()
			{
				Start = start,
				Count = elementCount,
			};
			_segments.Add(segment);
			return segment;
		}

		public void Delete(Segment mesh)
		{
			_segments.Remove(mesh);
		}

		public void Defrag()
		{
			// Order meshes by their first vertex
			var ordered = _segments.OrderBy(x => x.Start).ToList();

			// Merge succesing meshes into the same chunk
			List<Chunk> chunks = new List<Chunk>();
			chunks.Add(new Chunk()
			{
				VertexStart = 0,
				VertexCount = 0,
				Segments = new List<Segment>(),
			});
			for(int i = 0; i < ordered.Count; i++)
			{
				var segment = ordered[i];
				var last = chunks[chunks.Count - 1];

				if(segment.Start == last.VertexStart + last.VertexCount)
				{
					last.VertexCount += segment.Count;
					last.Segments.Add(segment);
				}
				else
				{
					chunks.Add(new Chunk()
					{
						VertexStart = segment.Start,
						VertexCount = segment.Count,
						Segments = new List<Segment>()
							{
								segment
							},
					});
				}
			}

			// Use the merged chunks for defrag
			int position = 0;
			int index = 0;

			while(index < chunks.Count)
			{
				var chunk = chunks[index];

				if(chunk.VertexStart != position)
				{
					foreach(var buffer in _buffers)
					{
						buffer.MoveDataBack(chunk.VertexStart, position, chunk.VertexCount);
					}
					int move = chunk.VertexStart - position;
					chunk.VertexStart = position;
					foreach(var segment in chunk.Segments)
					{
						segment.Start -= move;
					}
				}

				position += chunk.VertexCount;
				index++;
			}

			foreach(var buffer in _buffers)
			{
				buffer.UpdateStorage();
			}
		}

		class Chunk
		{
			public int VertexStart;
			public int VertexCount;
			public List<Segment> Segments;
		}
	}
}
