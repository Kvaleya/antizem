﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Meshes.Buffers
{
	class MeshDataBuffer : IBuffer
	{
		public const int MaxMeshes = 1 << 14;
		public const int DataSize = 64;

		public int Handle { get; private set; }
		Stack<int> _freeIndices;

		public MeshDataBuffer()
		{
			Handle = Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(MaxMeshes * DataSize), IntPtr.Zero,
				BufferStorageFlags.None, "MeshDataBuffer");
			_freeIndices = new Stack<int>();

			for(int i = MaxMeshes - 1; i >= 0; i--)
			{
				_freeIndices.Push(i);
			}
		}

		public void AllocateMesh(MeshBufferDescription description, MeshUploader uploader, Action onUploadDone)
		{
			int index = _freeIndices.Pop();
			description.Index = index;

			byte[] bytes = new byte[DataSize];
			int offset = 0;
			
			AddBytes(BitConverter.GetBytes(description.PositionsOffset.X), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.PositionsOffset.Y), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.PositionsOffset.Z), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(0f), bytes, ref offset);

			AddBytes(BitConverter.GetBytes(description.PositionsScale.X), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.PositionsScale.Y), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.PositionsScale.Z), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(0f), bytes, ref offset);

			AddBytes(BitConverter.GetBytes(description.TexCoordsOffset.X), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.TexCoordsOffset.Y), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.TexCoordsScale.X), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.TexCoordsScale.Y), bytes, ref offset);

			AddBytes(BitConverter.GetBytes(description.BoundingSphere.X), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.BoundingSphere.Y), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.BoundingSphere.Z), bytes, ref offset);
			AddBytes(BitConverter.GetBytes(description.BoundingSphere.W), bytes, ref offset);

			uploader.AddUpload(bytes, this, new Segment()
			{
				Count = 1,
				Start = index,
			}, DataSize, onUploadDone);
		}

		static void AddBytes(byte[] source, byte[] target, ref int offset)
		{
			for(int i = 0; i < source.Length; i++)
			{
				target[offset] = source[i];
				offset++;
			}
		}

		public void FreeMesh(int index)
		{
			_freeIndices.Push(index);
		}
	}
}
