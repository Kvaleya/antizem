﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Meshes;
using Engine.Renderer.ComputeMeshProcessor;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Meshes.Buffers
{
	class VertexManager
	{
		class FormatData
		{
			public VertexBufferFormat Format;
			public BufferSet BufferSet;
			public VertexBufferSource Source;
		}

		public const string SparseBufferExtensionName = "GL_ARB_sparse_buffer";

		const GetPName SPARSE_BUFFER_PAGE_SIZE_ARB = (GetPName)0x82F8;
		const int MaxSparseBufferSize = 1024 * 1024 * 256; // ~256mb
		public const int InstanceAttributeIndex = 6;
		public const int InstanceBufferBindingPoint = 15;

		Dictionary<string, FormatData> _formats = new Dictionary<string, FormatData>();
		List<BufferSet> _bufferSets = new List<BufferSet>();

		public int SparseBufferPageSize { get; private set; }
		public int ClusterBufferHandle { get { return _bufferClusters.Buffers[VertexStreamUsage.Clusters].Handle; } }

		public MeshDataBuffer MeshDataBuffer;

		Func<int, string, IBufferDynamic> _bufferFactory;

		Renderer.RenderContext _context;

		BufferSet _bufferIndicesShort;
		BufferSet _bufferIndicesUint;
		BufferSet _bufferClusters;

		public VertexManager(Renderer.RenderContext context, bool sparseStorage)
		{
			_context = context;

			if(sparseStorage)
			{
				SparseBufferPageSize = GL.GetInteger(SPARSE_BUFFER_PAGE_SIZE_ARB);

				_bufferFactory = (stride, name) =>
				{
					return new SparseBuffer(this, MaxSparseBufferSize, stride, name);
				};
			}
			else
			{
				_bufferFactory = (stride, name) =>
				{
					return new ExpandingBuffer(stride, name);
				};
			}

			MeshDataBuffer = new MeshDataBuffer();

			_bufferIndicesShort = new BufferSet(new List<Tuple<VertexStreamUsage, IBufferDynamic>>()
			{
				new Tuple<VertexStreamUsage, IBufferDynamic>(VertexStreamUsage.Indices, _bufferFactory(2, "MeshBuffer Indices Ushort")),
			});

			_bufferIndicesUint = new BufferSet(new List<Tuple<VertexStreamUsage, IBufferDynamic>>()
			{
				new Tuple<VertexStreamUsage, IBufferDynamic>(VertexStreamUsage.Indices, _bufferFactory(4, "MeshBuffer Indices Uint")),
			});

			_bufferClusters = new BufferSet(new List<Tuple<VertexStreamUsage, IBufferDynamic>>()
			{
				new Tuple<VertexStreamUsage, IBufferDynamic>(VertexStreamUsage.Clusters, _bufferFactory(MeshBinary.ClusterDataSize, "MeshBuffer Clusters")),
			});

			_bufferSets.Add(_bufferIndicesShort);
			_bufferSets.Add(_bufferIndicesUint);
			_bufferSets.Add(_bufferClusters);
		}

		FormatData CreateFormat(List<VertexStream> streams)
		{
			FormatData data = new FormatData();

			// Create description string and vertex buffer bindings
			List<VertexBufferBinding> bindings = new List<VertexBufferBinding>();
			List<Tuple<VertexStreamUsage, IBufferDynamic>> buffers= new List<Tuple<VertexStreamUsage, IBufferDynamic>>();

			// Gather buffers for regular rendering
			for(int i = 0; i < streams.Count; i++)
			{
				if(streams[i].Usage == VertexStreamUsage.Indices || streams[i].Usage == VertexStreamUsage.IndicesShadow || streams[i].Usage == VertexStreamUsage.Clusters)
					continue;
				var buffer = _bufferFactory(streams[i].Stride, "MeshBuffer " + streams[i].Usage.ToString());
				var binding = new VertexBufferBinding(buffer.Handle, 0, streams[i].Stride, 0);
				buffer.AddUsage(binding); // Register the binding to the IBufferDynamic, so that it can be autoupdated when buffer handle changes
				bindings.Add(binding);
				buffers.Add(new Tuple<VertexStreamUsage, IBufferDynamic>(streams[i].Usage, buffer));
			}

			// Add instance buffer
			bindings.Add(new VertexBufferBinding(_context.InstanceManager.BufferInstanceData, 0, InstanceManager.InstanceDataSize, 1));

			// Create vertex buffer source
			data.Source = new VertexBufferSource(0, bindings.ToArray());

			// Create new format
			List<VertexAttribDescription> attribs = new List<VertexAttribDescription>();

			int bindingPoint = 0;

			for(int i = 0; i < streams.Count; i++)
			{
				var stream = streams[i];

				if(stream.Usage == VertexStreamUsage.Indices || stream.Usage == VertexStreamUsage.IndicesShadow || stream.Usage == VertexStreamUsage.Clusters)
					continue;

				attribs.Add(new VertexAttribDescription(GetAttributeIndex(stream.Usage), bindingPoint, (int)stream.ComponentType, 0, (VertexAttribType)stream.Type, (VertexAttribClass)stream.DataClass, stream.Normalized));

				bindingPoint++;
			}

			// Instance indices - use the last binding point for that
			attribs.Add(new VertexAttribDescription(InstanceAttributeIndex + 0, bindingPoint, 4, 0, VertexAttribType.Float, VertexAttribClass.Float, false));
			attribs.Add(new VertexAttribDescription(InstanceAttributeIndex + 1, bindingPoint, 4, 16, VertexAttribType.Float, VertexAttribClass.Float, false));
			attribs.Add(new VertexAttribDescription(InstanceAttributeIndex + 2, bindingPoint, 4, 32, VertexAttribType.Float, VertexAttribClass.Float, false));
			attribs.Add(new VertexAttribDescription(InstanceAttributeIndex + 3, bindingPoint, 4, 48, VertexAttribType.Int, VertexAttribClass.Integer, false));

			data.Format = new VertexBufferFormat(attribs.ToArray());

			data.BufferSet = new BufferSet(buffers);
			_bufferSets.Add(data.BufferSet);

			return data;
		}

		public MeshBufferDescription AllocateMesh(List<VertexStream> streams, Action onUploadDone)
		{
			// TODO: separate formats for regular, shadow and alpha-tested shadow variants

			StringBuilder descriptionBuilder = new StringBuilder();

			VertexStream positions = null, texCoords = null, indices = null, clusters = null;

			// Get the format key
			for(int i = 0; i < streams.Count; i++)
			{
				if(streams[i].Usage == VertexStreamUsage.Positions)
					positions = streams[i];
				if(streams[i].Usage == VertexStreamUsage.TexCoords)
					texCoords = streams[i];
				if(streams[i].Usage == VertexStreamUsage.Indices)
					indices = streams[i];
				if(streams[i].Usage == VertexStreamUsage.Clusters)
					clusters = streams[i];

				if(streams[i].Usage == VertexStreamUsage.Clusters)
					continue;
				descriptionBuilder.Append(streams[i].FormatDescription);
				descriptionBuilder.Append(' ');
			}

			string key = descriptionBuilder.ToString();

			// TODO: animation MeshProcessFormat
			bool indices32 = indices.Stride == 4;
			bool positions32 = positions.Type == VertexStreamType.Float;

			ComputeMeshProcessor.MeshProcessFormat meshProcessFormat = MeshProcessFormat.Unorm16Int16;

			if(indices32)
				meshProcessFormat = meshProcessFormat | MeshProcessFormat.Int32;
			if(positions32)
				meshProcessFormat = meshProcessFormat | MeshProcessFormat.Float32;

			MeshBufferDescription description = new MeshBufferDescription(key, meshProcessFormat);

			// Get format data or create new
			FormatData data;

			if(_formats.ContainsKey(key))
				data = _formats[key];
			else
			{
				data = CreateFormat(streams);
				_formats.Add(key, data);
			}

			description.VertexBufferFormat = data.Format;
			description.VertexBufferSource = data.Source;

			// Allocate space in buffers
			description.Vertices = data.BufferSet.Alloc(positions.AttributeCount);
			description.Clusters = _bufferClusters.Alloc(clusters.AttributeCount);

			if(indices32)
			{
				description.Elements = _bufferIndicesUint.Alloc(indices.AttributeCount);
				description.ElementsBinding = new VertexBufferBinding(_bufferIndicesUint.Buffers[VertexStreamUsage.Indices].Handle, 0, 4, 0);
				_bufferIndicesUint.Buffers[VertexStreamUsage.Indices].AddUsage(description.ElementsBinding);
			}
			else
			{
				description.Elements = _bufferIndicesShort.Alloc(indices.AttributeCount);
				description.ElementsBinding = new VertexBufferBinding(_bufferIndicesShort.Buffers[VertexStreamUsage.Indices].Handle, 0, 2, 0);
				_bufferIndicesShort.Buffers[VertexStreamUsage.Indices].AddUsage(description.ElementsBinding);
			}

			description.ClustersBinding = new VertexBufferBinding(_bufferClusters.Buffers[VertexStreamUsage.Clusters].Handle, 0, MeshBinary.ClusterDataSize, 0);
			_bufferClusters.Buffers[VertexStreamUsage.Clusters].AddUsage(description.ClustersBinding);

			description.PositionsOffset = positions.ValueOffset.Xyz;
			description.PositionsScale = positions.ValueScale.Xyz;

			description.BoundingSphere = positions.BoundingSphere;

			if(texCoords != null)
			{
				description.TexCoordsOffset = texCoords.ValueOffset.Xy;
				description.TexCoordsScale = texCoords.ValueScale.Xy;
			}

			// Upload mesh
			for(int i = 0; i < streams.Count; i++)
			{
				var stream = streams[i];

				IBuffer buffer = null;
				Segment segment = null;

				switch(stream.Usage)
				{
					case VertexStreamUsage.Indices:
					{
						if(indices32)
						{
							buffer = _bufferIndicesUint.Buffers[VertexStreamUsage.Indices];
						}
						else
						{
							buffer = _bufferIndicesShort.Buffers[VertexStreamUsage.Indices];
						}
						
						segment = description.Elements;
						break;
					}
					case VertexStreamUsage.IndicesShadow:
					{
						// No separate buffer for shadows -> buffer upload is done since nothing to upload -> invoke onUploadDone
						onUploadDone.Invoke();
						break;
					}
					case VertexStreamUsage.Clusters:
					{
						buffer = _bufferClusters.Buffers[VertexStreamUsage.Clusters];
						segment = description.Clusters;
						break;
					}
					default:
					{
						buffer = data.BufferSet.Buffers[stream.Usage];
						segment = description.Vertices;
						break;
					}
				}

				if(buffer == null || segment == null)
					continue;
				
				_context.MeshUploader.AddUpload(stream.Data, buffer, segment, stream.Stride, onUploadDone);
			}

			// Upload mesh data
			MeshDataBuffer.AllocateMesh(description, _context.MeshUploader, onUploadDone);

			return description;
		}

		public void DeleteMesh(MeshBufferDescription description)
		{
			MeshDataBuffer.FreeMesh(description.Index);

			if(description.ElementsBinding.Stride == 2)
			{
				_bufferIndicesShort.Delete(description.Elements);
			}
			else
			{
				_bufferIndicesUint.Delete(description.Elements);
			}

			_bufferClusters.Delete(description.Clusters);
			_formats[description.FormatKey].BufferSet.Delete(description.Vertices);
		}

		public void UpdateStorage()
		{
			foreach(var set in _bufferSets)
			{
				set.Defrag();
			}
		}

		int GetAttributeIndex(VertexStreamUsage usage)
		{
			switch(usage)
			{
				case VertexStreamUsage.Indices:
					{
						return -1;
					}
				case VertexStreamUsage.IndicesShadow:
					{
						return -1;
					}
				case VertexStreamUsage.Clusters:
					{
						return -1;
					}
				case VertexStreamUsage.Positions:
					{
						return 0;
					}
				case VertexStreamUsage.Normals:
					{
						return 1;
					}
				case VertexStreamUsage.TexCoords:
					{
						return 2;
					}
				case VertexStreamUsage.Tangents:
					{
						return 3;
					}
				default:
					{
						return -1;
					}
			}
		}
	}
}
