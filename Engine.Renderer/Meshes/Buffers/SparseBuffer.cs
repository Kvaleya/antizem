﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Meshes.Buffers
{
	class SparseBuffer : IBufferDynamic
	{
		const BufferStorageFlags SPARSE_STORAGE_BIT_ARB = (BufferStorageFlags)0x0400;

		public int Handle { get { return _handle; } }

		public int ResidentPages = 0;
		readonly int _handle;
		public int UsedBytes = 0;
		public readonly int Stride;
		readonly VertexManager _manager;

		public SparseBuffer(VertexManager manager, int size, int stride, string name)
		{
			Stride = stride;
			_handle = Glob.Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(size), IntPtr.Zero, SPARSE_STORAGE_BIT_ARB,
				name);
			_manager = manager;
		}

		public int AllocElements(int elementCount)
		{
			int bytes = elementCount * Stride;

			int start = UsedBytes / Stride;
			UsedBytes += bytes;
			if(ResidentPages * _manager.SparseBufferPageSize < UsedBytes)
			{
				int newBytes = UsedBytes - ResidentPages * _manager.SparseBufferPageSize;
				int newPages = (newBytes + _manager.SparseBufferPageSize - 1) / _manager.SparseBufferPageSize;

				GL.BindBuffer(BufferTarget.CopyReadBuffer, _handle);
				GL.Arb.BufferPageCommitment((ArbSparseBuffer)BufferTarget.CopyReadBuffer, new IntPtr(ResidentPages * _manager.SparseBufferPageSize), newPages * _manager.SparseBufferPageSize, true);
				ResidentPages += newPages;
			}
			return start;
		}

		// Target must be lesser than source
		// Will also uncommit unused pages
		public void MoveDataBack(int sourceElement, int targetElement, int countElements)
		{
			MoveDataBack(sourceElement, targetElement, countElements, Stride, _handle);

			// At the end of the copying, "source" is set to the first byte that comes after the moved segment
			if(sourceElement * Stride == UsedBytes)
			{
				UsedBytes -= (sourceElement - targetElement) * Stride;
				int neededPages = (UsedBytes + _manager.SparseBufferPageSize - 1) / _manager.SparseBufferPageSize;
				int extraPages = ResidentPages - neededPages;

				if(extraPages > 0)
				{
					GL.BindBuffer(BufferTarget.CopyReadBuffer, _handle);
					GL.Arb.BufferPageCommitment((ArbSparseBuffer)BufferTarget.CopyReadBuffer, new IntPtr(neededPages * _manager.SparseBufferPageSize), extraPages * _manager.SparseBufferPageSize, false);
				}
			}
		}

		public void UpdateStorage()
		{

		}

		public void AddUsage(VertexBufferBinding bidning)
		{
			
		}

		public static void MoveDataBack(int sourceElement, int targetElement, int countElements, int stride, int handle)
		{
			int bytes = countElements * stride;
			int bytesSource = sourceElement * stride;
			int bytesTarget = targetElement * stride;

			GL.BindBuffer(BufferTarget.CopyReadBuffer, handle);
			GL.BindBuffer(BufferTarget.CopyWriteBuffer, handle);
			int gap = bytesSource - bytesTarget;

			while(bytes > 0)
			{
				int move = Math.Min(gap, bytes);

				GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, new IntPtr(bytesSource), new IntPtr(bytesTarget), new IntPtr(move));

				bytesSource += move;
				bytesTarget += move;
				bytes -= move;
			}
		}
	}
}
