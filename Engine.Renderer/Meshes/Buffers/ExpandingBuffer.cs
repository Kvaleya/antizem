﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Meshes.Buffers
{
	class ExpandingBuffer : IBufferDynamic
	{
		const int MinimalSize = 1024 * 64;

		public int Handle
		{
			get { return _handle; }
		}

		int _handle;
		public readonly int Stride;
		int _bytesUsed = 0;
		int _bytesAllocated = 0;
		string _name;
		List<VertexBufferBinding> _usages = new List<VertexBufferBinding>();

		public ExpandingBuffer(int stride, string name)
		{
			Stride = stride;
			_bytesAllocated = MinimalSize;
			_name = name;
			_handle = CreateBuffer(_bytesAllocated);
		}

		public int AllocElements(int elementCount)
		{
			int bytes = elementCount * Stride;

			int start = _bytesUsed / Stride;
			_bytesUsed += bytes;

			//TryResize();

			return start;
		}

		public void MoveDataBack(int sourceElement, int targetElement, int countElements)
		{
			SparseBuffer.MoveDataBack(sourceElement, targetElement, countElements, Stride, _handle);

			if(sourceElement * Stride == _bytesUsed)
			{
				_bytesUsed -= (sourceElement - targetElement) * Stride;
			}
		}

		public void UpdateStorage()
		{
			TryResize();
		}

		public void AddUsage(VertexBufferBinding bidning)
		{
			_usages.Add(bidning);
		}

		void TryResize()
		{
			int idealSize = _bytesAllocated;
			while(_bytesUsed < idealSize)
				idealSize /= 2;
			while(_bytesUsed > idealSize)
				idealSize *= 2;
			if(idealSize < MinimalSize)
				idealSize = MinimalSize;

			if(idealSize != _bytesAllocated)
			{
				int oldBuffer = _handle;
				_handle = CreateBuffer(idealSize);
				Glob.Utils.CopyBuffer(oldBuffer, _handle, 0, 0, Math.Min(idealSize, _bytesAllocated));
				_bytesAllocated = idealSize;
				
				foreach(var usage in _usages)
				{
					usage.Buffer = _handle;
				}

				GL.DeleteBuffer(oldBuffer);
			}
		}

		int CreateBuffer(int size)
		{
			return Glob.Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(size), IntPtr.Zero,
				BufferStorageFlags.None, _name + "(size: " + size + ")");
		}
	}
}
