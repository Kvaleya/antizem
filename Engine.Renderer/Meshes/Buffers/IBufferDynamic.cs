﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;

namespace Engine.Renderer.Meshes.Buffers
{
	interface IBufferDynamic : IBuffer
	{
		int AllocElements(int elementCount);
		void MoveDataBack(int sourceElement, int targetElement, int countElements);
		void UpdateStorage();
		void AddUsage(VertexBufferBinding bidning);
	}
}
