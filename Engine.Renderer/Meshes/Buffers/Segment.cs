﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Renderer.Meshes.Buffers
{
	class Segment
	{
		public int Start;
		public int Count;
	}
}
