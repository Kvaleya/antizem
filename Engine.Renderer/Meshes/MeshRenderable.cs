﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Meshes;
using Engine.Renderer.Meshes.Buffers;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Meshes
{
	class MeshRenderable : IDisposable
	{
		MeshResourceManager _manager;
		VertexManager _vertexManager;
		public MeshBufferDescription Description;

		public MeshRenderable(MeshResourceManager manager, VertexManager vertexManager)
		{
			_manager = manager;
			_vertexManager = vertexManager;
		}

		public void AllocBuffers(List<VertexStream> streams, Action onUploadDone)
		{
			Description = _vertexManager.AllocateMesh(streams, onUploadDone);
		}

		public void Dispose()
		{
			_manager.AddUpdateGLAction((() =>
			{
				_vertexManager.DeleteMesh(Description);
			}));
		}
	}
}
