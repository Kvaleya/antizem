﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Renderer.Meshes.Buffers;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Meshes
{
	class MeshUploader
	{
		StreamBuffer _streamBuffer;

		ConcurrentQueue<UploadCommand> _uploads = new ConcurrentQueue<UploadCommand>();
		List<CopyCommand> _copies = new List<CopyCommand>();

		public MeshUploader(int size)
		{
			_streamBuffer = new StreamBuffer("MeshUploadBuffer", BufferTarget.CopyReadBuffer, size);
		}

		public void UpdateUploads(FenceSync frameSync)
		{
			int usedBytes = 0;

			while(usedBytes < _streamBuffer.Size && _uploads.Count > 0)
			{
				UploadCommand upload;
				if(!_uploads.TryPeek(out upload))
					break;

				int move = Math.Min(_streamBuffer.Size - usedBytes, upload.Length);

				Marshal.Copy(upload.SourceBytes, upload.SourceOffset, _streamBuffer.CurrentStart + usedBytes, move);

				_copies.Add(new CopyCommand()
				{
					Length = move,
					SourceOffset = _streamBuffer.CurrentOffset + usedBytes,
					TargetOffset = upload.TargetOffset + upload.TargetSegment.Start * upload.Stride,
					TargetBuffer = upload.TargetBuffer.Handle,
				});

				upload.Length -= move;
				upload.SourceOffset += move;
				upload.TargetOffset += move;

				UploadCommand dequeue;

				if(upload.Length == 0)
				{
					_uploads.TryDequeue(out dequeue);
					dequeue?.OnUploadFinished.Invoke();
				}

				usedBytes += move;
			}

			_streamBuffer.Advance(frameSync);
		}

		public void UpdateCopies()
		{
			GL.BindBuffer(BufferTarget.CopyReadBuffer, _streamBuffer.Handle);
			foreach(CopyCommand copy in _copies)
			{
				GL.BindBuffer(BufferTarget.CopyWriteBuffer, copy.TargetBuffer);
				GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, new IntPtr(copy.SourceOffset),
					new IntPtr(copy.TargetOffset), copy.Length);
			}

			GL.BindBuffer(BufferTarget.CopyReadBuffer, 0);
			GL.BindBuffer(BufferTarget.CopyWriteBuffer, 0);

			_copies.Clear();
		}

		public void AddUpload(byte[] bytes, IBuffer targetBuffer, Segment targetSegment, int stride, Action onUploadFinished)
		{
			_uploads.Enqueue(new UploadCommand()
			{
				SourceBytes = bytes,
				Length = bytes.Length,
				TargetBuffer = targetBuffer,
				TargetSegment = targetSegment,
				Stride = stride,
				SourceOffset = 0,
				TargetOffset = 0,
				OnUploadFinished = onUploadFinished
			});
		}

		class UploadCommand
		{
			public byte[] SourceBytes;

			public int Length;

			public IBuffer TargetBuffer;
			public Segment TargetSegment;

			public int SourceOffset;
			public int TargetOffset;
			public int Stride;
			
			public Action OnUploadFinished;
		}

		class CopyCommand
		{
			public int Length;

			public int TargetBuffer;

			public int SourceOffset;
			public int TargetOffset;
		}
	}
}
