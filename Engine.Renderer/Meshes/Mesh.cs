﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Meshes;
using OpenTK;

namespace Engine.Renderer.Meshes
{
	class Mesh : IMesh
	{
		public Resource<MeshRenderable> MeshRenderable;
		public Resource<MeshBinary> MeshBinary;
		Vector4 _boundingSphere = Vector4.Zero;
		bool _hasBoundingSphere = false;

		public int MeshID
		{
			get
			{
				var item = MeshRenderable.Item;
				if(item == null)
					return 0;
				return item.Description.Index;
			}
		}

		public string FileName
		{
			get
			{
				string f, m;
				Utils.Meshes.NameToPathAndMeshName(MeshRenderable.Name, out f, out m);
				return f;
			}
		}

		public string MeshName
		{
			get
			{
				string f, m;
				Utils.Meshes.NameToPathAndMeshName(MeshRenderable.Name, out f, out m);
				return m;
			}
		}

		public string BatchKey
		{
			get
			{
				var item = MeshRenderable.Item;
				if(item == null)
					return string.Empty;
				return item.Description.FormatKey;
			}
		}

		public bool IsLoaded
		{
			get { return MeshRenderable.Item != null && MeshBinary != null; }
		}

		public Vector4 BoundingSphere
		{
			get
			{
				if(_hasBoundingSphere)
				{
					return _boundingSphere;
				}

				var item = MeshBinary.Item;

				if(item == null)
				{
					return _boundingSphere;
				}

				_boundingSphere = item.ComputeBoundingSphere();
				_hasBoundingSphere = true;
				return _boundingSphere;
			}
		}

		public void SetDistance(object caller, float d)
		{
			MeshRenderable.SetDistance(caller, d);
			MeshBinary.SetDistance(caller, d);
		}

		public void RemoveDistance(object caller)
		{
			MeshRenderable.RemoveDistance(caller);
			MeshBinary.RemoveDistance(caller);
		}
	}
}
