﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Common.Structures;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Meshes
{
	class InstanceManager
	{
		public const int MaxInstances = 1 << 18;
		public const int InstanceDataSize = 64;

		public int BufferInstanceData;
		StreamBuffer _streamInstanceData;

		List<MeshInstanceData> _instances = new List<MeshInstanceData>();
		int _uploadDataCount;
		int _uploadDataOffset;

		Device _device;

		public InstanceManager(Device device)
		{
			BufferInstanceData = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer,
				new IntPtr(MaxInstances * InstanceDataSize), IntPtr.Zero, BufferStorageFlags.None, "InstanceData");
			
			_streamInstanceData = new StreamBuffer("UploadInstanceData", BufferTarget.CopyReadBuffer, MaxInstances * InstanceDataSize);

			_device = device;
		}

		/// <summary>
		/// Queues instances for upload to GPU and returns the index of the first element of "instances" in the GPU buffer
		/// </summary>
		public int AddInstances(IReadOnlyList<MeshInstanceData> instances)
		{
			int first = _instances.Count;

			for(int i = 0; i < instances.Count; i++)
			{
				var instance = instances[i];
				_instances.Add(instance);
			}

			return first;
		}

		public void UpdateUploads(FenceSync frameSync)
		{
			_uploadDataCount = Math.Min(MaxInstances, _instances.Count);

			for(int i = 0; i < _uploadDataCount; i++)
			{
				var instance = _instances[i];
				IntPtr ptr = IntPtr.Add(_streamInstanceData.CurrentStart, i * InstanceDataSize);
				instance.MarshalTo(ptr, 0);
			}

			_instances.Clear();

			_uploadDataOffset = _streamInstanceData.CurrentOffset;
			_streamInstanceData.Advance(frameSync);
		}

		public void UpdateCopies()
		{
			//_device.BindPipeline(_psoPatchInstances);
			//_device.ShaderCompute.SetUniformI("offset", _uploadDataOffset / InstanceDataSize);
			//_device.ShaderCompute.SetUniformI("count", _uploadDataCount);
			//_device.BindBufferBase(0, BufferRangeTarget.ShaderStorageBuffer, _streamInstanceData.Handle);
			//_device.BindBufferBase(1, BufferRangeTarget.ShaderStorageBuffer, BufferInstanceData);
			//_device.DispatchComputeThreads(_uploadDataCount);

			//GL.BindBuffer(BufferTarget.ArrayBuffer, BufferInstanceData);
			//GL.MemoryBarrier(MemoryBarrierFlags.VertexAttribArrayBarrierBit);
			//GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

			// Copy indices
			if(_uploadDataCount > 0)
			{
				GL.BindBuffer(BufferTarget.CopyReadBuffer, _streamInstanceData.Handle);
				GL.BindBuffer(BufferTarget.CopyWriteBuffer, BufferInstanceData);
				GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, new IntPtr(_uploadDataOffset),
					IntPtr.Zero, new IntPtr(_uploadDataCount * InstanceDataSize));
			}
		}

		/// <summary>
		/// Queues all mesh instances from "batches" for upload to GPU and returns render-ready batches.
		/// </summary>
		public List<MeshRenderBatch> GetBatches(List<MeshBatch> batches)
		{
			List<MeshRenderBatch> renderBatches = new List<MeshRenderBatch>();

			for(int i = 0; i < batches.Count; i++)
			{
				var batch = batches[i];

				var mesh = (Mesh) batch.Instances[0].Mesh;
				var renderable = mesh.MeshRenderable.Item;

				if(renderable == null)
					continue;

				int first = AddInstances(batch.Instances);

				renderBatches.Add(new MeshRenderBatch()
				{
					Batch = batch,
					FirstInstanceIndex = first,
				});
			}

			return renderBatches;
		}
	}
}
