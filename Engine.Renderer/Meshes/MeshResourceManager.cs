﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Meshes;
using Engine.Renderer.Meshes.Buffers;

namespace Engine.Renderer.Meshes
{
	class MeshResourceManager : ResourceManager<MeshRenderable>, IMeshManager
	{
		Thread _waitingForLoadedThread;

		Dictionary<string, Mesh> _meshes = new Dictionary<string, Mesh>();
		
		ResourceManager<MeshBinary> _meshBinaryResourceManager;
		VertexManager _vertexManager;

		ProducerConsumerBag<Action> _onUpdateActions = new ProducerConsumerBag<Action>();

		public long BytesUsed { get { return MemoryBytesUsed; } }

		public MeshResourceManager(long memoryBudgetBytes, MeshBinaryResourceManager meshBinaryResourceManager, VertexManager vertexManager)
			: base(memoryBudgetBytes)
		{
			_vertexManager = vertexManager;
			_meshBinaryResourceManager = meshBinaryResourceManager;
		}

		public void AddUpdateGLAction(Action a)
		{
			_onUpdateActions.Add(a);
		}

		protected override void LoadResource(Resource<MeshRenderable> resource)
		{
			var resourceBinary = _meshBinaryResourceManager.GetResource(resource.Name);
			resourceBinary.AddOnLoadAction(resourceMeshBinary =>
			{
				var binary = resourceMeshBinary.Item;

				int counter = binary.VertexStreams.Count;
				counter += 1; // Also wait for upload of mesh data
				MeshRenderable renderable = null;

				_onUpdateActions.Add((() =>
				{
					renderable = new MeshRenderable(this, _vertexManager);
					renderable.AllocBuffers(binary.VertexStreams, () =>
					{
						counter--;
						if(counter == 0)
						{
							resource.Update(renderable);
							OnResourceLoadFinished(resource);
						}
					});
				}));
			});
		}

		protected override void GetResourceSize(Resource<MeshRenderable> resource)
		{
			var binary = _meshBinaryResourceManager.GetResource(resource.Name);
			binary.AddOnLoadAction(resource1 =>
			{
				OnResourceSizeKnown(resource, resource1.Item.BytesUsed);
			});			
		}

		internal void UpdateGL()
		{
			List<Action> list = new List<Action>();
			_onUpdateActions.TryCollectAndClear(list);

			foreach(var action in list)
			{
				action.Invoke();
			}
		}

		public IMesh GetMesh(string name)
		{
			if(_meshes.ContainsKey(name))
				return _meshes[name];

			var mesh = new Mesh();
			mesh.MeshBinary = _meshBinaryResourceManager.GetResource(name);
			mesh.MeshRenderable = GetResource(name);
			_meshes.Add(name, mesh);

			return mesh;
		}
	}
}
