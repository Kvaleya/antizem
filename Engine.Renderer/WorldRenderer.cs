﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.Renderer.Modules;
using Glob;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer
{
	class WorldRenderer
	{
		[InjectProperty]
		public TestSceneData SceneData { get; set; }

		List<RenderModule> _modules = new List<RenderModule>();
		DataInjector _dataInjector;

		Aurora.AuroraModule _moduleAurora;
		AtmosphereModule _moduleAtmosphere;
		TestSceneModule _moduleTestScene;
		GbufferModule _moduleGbuffer;
		UIModule _moduleUI;
		VisualizeVTModule _moduleVisualizeVT;
		TonemapModule _moduleTonemap;
		DeferredLightModule _moduleDeferredLight;
		EnvmapModule _moduleEnvmap;
		AmbientOcclusionModule _moduleAo;
		VoxelModule _moduleVoxel;
		VolumetricModule _moduleVolumetric;
		//FxaaModule _moduleFxaa;
		//SdfTestModule _moduleSdfTest;
		SmaaModule _moduleSmaa;

		GraphicsPipeline _psoClear;
		GraphicsPipeline _psoDrawTex;

		public WorldRenderer(DataInjector dataInjector, Renderer.RenderContext context)
		{
			_psoClear = new GraphicsPipeline(context.Device, null, null, null, new RasterizerState(),
				new DepthState(DepthFunction.Always, true));
			_psoDrawTex = new GraphicsPipeline(context.Device, context.Device.GetShader("fullscreenSimple.vert"), context.Device.GetShader("simpleTexture.frag"), null, new RasterizerState(), new DepthState(DepthFunction.Always, false));

			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.StencilTest);
			GL.Enable(EnableCap.TextureCubeMapSeamless);
			//GL.Enable(EnableCap.Texture2D);
			//GL.Enable(EnableCap.Texture3DExt);
			SetClipControl();

			_dataInjector = dataInjector;
			// Create and register modules
			_dataInjector.AddModule(this);

			_moduleAtmosphere = new AtmosphereModule(context);
			RegisterModule(_moduleAtmosphere);

			_moduleTestScene = new TestSceneModule(context);
			RegisterModule(_moduleTestScene);

			_moduleGbuffer = new GbufferModule(context);
			RegisterModule(_moduleGbuffer);

			_moduleUI = new UIModule(context);
			RegisterModule(_moduleUI);

			_moduleVisualizeVT = new VisualizeVTModule(context, context.VirtualTextureManager);
			RegisterModule(_moduleVisualizeVT);

			_moduleTonemap = new TonemapModule(context);
			RegisterModule(_moduleTonemap);

			_moduleDeferredLight = new DeferredLightModule(context);
			RegisterModule(_moduleDeferredLight);

			_moduleEnvmap = new EnvmapModule(context);
			RegisterModule(_moduleEnvmap);

			_moduleAo = new AmbientOcclusionModule(context);
			RegisterModule(_moduleAo);

			_moduleVoxel = new VoxelModule(context);
			RegisterModule(_moduleVoxel);

			_moduleVolumetric = new VolumetricModule(context);
			RegisterModule(_moduleVolumetric);

			_moduleAurora = new Aurora.AuroraModule(context);
			RegisterModule(_moduleAurora);

			_moduleSmaa = new SmaaModule(context);
			RegisterModule(_moduleSmaa);
			
			//_moduleFxaa = new FxaaModule(context);
			//RegisterModule(_moduleFxaa);

			//_moduleSdfTest = new SdfTestModule(context);
			//RegisterModule(_moduleSdfTest);
		}

		public static void SetClipControl()
		{
			GL.ClipControl(ClipOrigin.LowerLeft, ClipDepthMode.ZeroToOne);
		}

		public void OnResize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{
			foreach(RenderModule module in _modules)
			{
				module.OnResize(displayWidth, displayHeight, renderWidth, renderHeight);
			}
		}

		public void OnUploadPhase(FenceSync frameSync)
		{
			foreach(RenderModule module in _modules)
			{
				module.OnUploadPhase(frameSync);
			}
		}

		public void OnCopyPhase()
		{
			foreach(RenderModule module in _modules)
			{
				module.OnCopyPhase();
			}
		}

		public void Prepare()
		{
			_moduleTestScene.Prepare();
			_moduleVoxel.Prepare();
		}

		public void RenderFrame(Renderer.RenderContext context)
		{
			context.Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);

			GL.Viewport(0, 0, context.DisplayWidth, context.DisplayHeight);
			GL.ClearColor(0f, 0f, 0f, 1f);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			//_moduleAtmosphere.RenderAtmosphere();

			SceneData.SunLight = _moduleAtmosphere.GetColoredSun(SceneData.SunDir);
			SceneData.SunLight2km = _moduleAtmosphere.GetColoredSun(SceneData.SunDir, 2e3f);
			SceneData.SunLight4km = _moduleAtmosphere.GetColoredSun(SceneData.SunDir, 4e3f);
			SceneData.SunLight8km = _moduleAtmosphere.GetColoredSun(SceneData.SunDir, 8e3f);
			SceneData.SunLight16km = _moduleAtmosphere.GetColoredSun(SceneData.SunDir, 16e3f);
			SceneData.SunLight32km = _moduleAtmosphere.GetColoredSun(SceneData.SunDir, 32e3f);
			SceneData.SunLight64km = _moduleAtmosphere.GetColoredSun(SceneData.SunDir, 64e3f);
			SceneData.SunLight128km = _moduleAtmosphere.GetColoredSun(SceneData.SunDir, 128e3f);
			SceneData.Exposure = GetExposureFromSun(SceneData.SunDir.Y, SceneData.SunLight.X);

			// Generate reprojected depth
			if(!SceneData.VisualizeCulling)
			{
				_moduleGbuffer.CreateReprojectedDepth();
			}

			using(context.Device.BindFrameBufferPushViewport(_moduleGbuffer.FrameBufferGbuffer, "Main render"))
			{
				context.Device.BindPipeline(_psoClear);
				GL.ClearDepth(0.0f);
				GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
				
				_moduleTestScene.RenderMeshes(_moduleGbuffer);
				
				//_moduleSdfTest.Render();
			}
			
			//_moduleVoxel.UpdateVoxels();
			

			
			_moduleGbuffer.ProcessDepth();

			_moduleAo.ComputeAo(_moduleGbuffer);
			//_moduleVoxel.RenderOcclusion(_moduleGbuffer);

			_moduleAurora.Update(false);

			_moduleEnvmap.RenderSkyEnvmap(_moduleAtmosphere, _moduleAurora);

			_moduleDeferredLight.ApplyLighting(_moduleGbuffer, _moduleEnvmap, _moduleAo, _moduleVoxel, _moduleVolumetric);
			
			using(context.Device.BindFrameBufferPushViewport(_moduleGbuffer.FrameBufferColorResult))
			{
				_moduleAtmosphere.RenderAtmosphere();
				_moduleAurora?.Render();
			}

			_moduleVolumetric.Render(_moduleGbuffer, _moduleEnvmap, _moduleAtmosphere);

			//_moduleVoxel.RenderVoxelTest(_moduleGbuffer.TextureSceneColor);

			if(SceneData.EnableAA)
			{
				using(context.Device.BindFrameBufferPushViewport(_moduleGbuffer.FrameBufferColorResult))
				{
					_moduleTonemap.Tonemap(_moduleGbuffer.TextureSceneColor, _moduleTestScene.SceneData.EnableTonemap);
				}

				_moduleSmaa.Render(_moduleGbuffer);

				context.Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.DrawFramebuffer);
				context.Device.BindPipeline(_psoDrawTex);
				context.Device.BindTexture(_moduleGbuffer.TextureSceneColor, 0);
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
			}
			else
			{
				context.Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.DrawFramebuffer);
				_moduleTonemap.Tonemap(_moduleGbuffer.TextureSceneColor, _moduleTestScene.SceneData.EnableTonemap);
			}
			
			if(SceneData.VisualizeVirtualTexture)
				_moduleVisualizeVT.Render();

			if(SceneData.EnableUI)
				_moduleUI.RenderUI();
			

			//context.Device.BindFrameBuffer(_moduleGbuffer.FrameBufferColorResult, FramebufferTarget.ReadFramebuffer);
			//context.Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.DrawFramebuffer);
			//FrameBuffer.Blit(0, 0, context.RenderWidth, context.RenderHeight, 0, 0, context.DisplayWidth, context.DisplayHeight, ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Linear);
		}

		void RegisterModule(RenderModule module)
		{
			_modules.Add(module);
			_dataInjector.AddModule(module);
		}

		// Instead of calculating the exposure from the previous frame's histogram,
		// let's simply use the Y component of the sunlight vector to guess the
		// correct exposure, which is good enough for the purposes of this project
		float GetExposureFromSun(float sunVectorY, float sunRed)
		{
			float pow = 0;
			/*
			float red = (float)Math.Log(sunRed, 2);
			red = Math.Min(Math.Max(red, -2), 8);

			float angle = (float)(Math.Asin(sunVectorY) / Math.PI * 90);

			pow = -red;
			

			if(red < -2)
			{
				pow = -2;
			}
			*/
			
			float d = Math.Min(Math.Max((sunVectorY + 0.23f) / 0.25f, 0), 1); // 0 .. night, 1 .. day
			float nightPow = 2f;
			float dayPow = -9f;

			d = SmoothStep(0, 1, d);

			pow = nightPow * (1 - d) + dayPow * d;
			
			return (float)Math.Pow(2, pow);
		}

		// https://en.wikipedia.org/wiki/Smoothstep
		float SmoothStep(float edge0, float edge1, float x)
		{
			// Scale, bias and saturate x to 0..1 range
			x = Math.Min(Math.Max((x - edge0) / (edge1 - edge0), 0.0f), 1.0f);
			// Evaluate polynomial
			return x * x * (3 - 2 * x);
		}
	}
}
