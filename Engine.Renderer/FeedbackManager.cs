﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;

namespace Engine.Renderer
{
	class FeedbackManager : ITimerQueryCPUManager
	{
		List<TimerQueryGPU> _queries = new List<TimerQueryGPU>();
		TimerQueryCPUManager _cpuQueryManager;

		public readonly long FrameNumber;

		public FeedbackManager(long frameNumber, TimerQueryCPUManager cpuQueryManager)
		{
			_cpuQueryManager = cpuQueryManager;
			FrameNumber = frameNumber;
			if(_cpuQueryManager == null)
			{
				_cpuQueryManager = new TimerQueryCPUManager();
			}
		}

		public void AddQueryCPU(ITimerQuery query)
		{
			var cpu = query as TimerQueryCPU;
			var coutner = query as CounterQuery;

			if(cpu != null)
				AddQueryCPU(cpu);
			if(coutner != null)
				AddQueryCPU(coutner);
		}

		public void AddQueryCPU(CounterQuery query)
		{
			_cpuQueryManager.AddQueryCPU(query);
			query.Kind = TimerQueryKind.OpenGL;
		}

		public void AddQueryCPU(TimerQueryCPU query)
		{
			_cpuQueryManager.AddQueryCPU(query);
			query.Kind = TimerQueryKind.OpenGL;
		}

		public void AddQueryGPU(TimerQueryGPU query)
		{
			_queries.Add(query);
		}

		public List<ITimerQuery> FinishQueries()
		{
			List<ITimerQuery> result = new List<ITimerQuery>();
			foreach(TimerQueryGPU query in _queries)
			{
				query.MeasureAndDisposeQueries();
				result.Add(query);
			}
			result.AddRange(_cpuQueryManager.GetQueries());
			return result;
		}
	}
}
