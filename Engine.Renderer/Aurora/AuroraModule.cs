﻿using System;
using System.Collections.Generic;
using System.Globalization;
using AuroraRenderer;
using Engine.Common.Rendering;
using Engine.Renderer.Modules;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Aurora
{
	// Based on:
	// Interactive Volume Rendering Aurora on the GPU 
	// https://www.researchgate.net/publication/220200705_Interactive_Volume_Rendering_Aurora_on_the_GPU
	class AuroraModule : RenderModule
	{
		const int FluidSimSizeX = 256;
		const int FluidSimSizeY = 64;
		const int FluidSimSizeZ = 3;

		const int SplatTexSize = 4096;

		const int NumRandomValues = 1024;

		[InjectProperty]
		public TestSceneData SceneData { get; set; }

		FluidSim _fluidSim;
		SplineGen _splineGen;

		ComputePipeline _psoUpdate;
		ComputePipeline _psoRender;
		ComputePipeline _psoEnvmap;

		GraphicsPipeline _psoSplineSplat;
		GraphicsPipeline _psoTextureViewSpline;
		GraphicsPipeline _psoTextureViewFluid;
		GraphicsPipeline _psoAuroraBlur;

		Texture2D _texAurora;

		Texture2D _texSplines;
		FrameBuffer _fboSplines;

		// Random values used for spiral spline movement
		float[] _randomness;

		// Scales the aurora spirals, higher values mean that the spirals will cover more space
		const float PlaneScale = 5;

		List<AuroraSpline> _splinesCurrent = new List<AuroraSpline>();
		List<AuroraSpiral> _spirals = new List<AuroraSpiral>();

		AuroraFileLoader _loader;

		static readonly Matrix4 _ditherMatrix = new Matrix4(
			new Vector4(1, 9, 3, 11) / 17,
			new Vector4(13, 5, 15, 7) / 17,
			new Vector4(4, 12, 2, 10) / 17,
			new Vector4(16, 8, 14, 6) / 17
			);

		public AuroraModule(Renderer.RenderContext rendererContext) : base(rendererContext)
		{
			_texSplines = new Texture2D(Device, "AuroraSplat", SizedInternalFormatGlob.RGBA8, SplatTexSize, SplatTexSize, 1);
			Glob.Utils.SetTextureParameters(Device, _texSplines, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);

			_fboSplines = new FrameBuffer();
			Device.BindFrameBuffer(_fboSplines, FramebufferTarget.DrawFramebuffer);
			_fboSplines.Attach(FramebufferAttachment.ColorAttachment0, _texSplines);

			// Note that the viscousity parameter is set to 0
			_fluidSim = new FluidSim(Device, FluidSimSizeX, FluidSimSizeY, FluidSimSizeZ, 0);
			_splineGen = new SplineGen();

			_psoUpdate = new ComputePipeline(Device, Device.GetShader("aurora_fluid_update.comp"));

			_psoSplineSplat = new GraphicsPipeline(Device, Device.GetShader("spline.vert"), Device.GetShader("spline.frag"), _splineGen.SplineVboFormat, new RasterizerState(CullfaceState.None), new DepthState(), new BlendState(BlendMode.Additive));
			_psoSplineSplat.ShaderVertex.SetUniformF("posOffset", new Vector2(0f));
			_psoSplineSplat.ShaderVertex.SetUniformF("posScale", new Vector2(1f));

			_psoTextureViewSpline = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("auroraFluidDisplay.frag"), null, new RasterizerState(), new DepthState(DepthFunction.Always, false), new BlendState(BlendMode.Additive));
			_psoTextureViewFluid = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("auroraFluidDisplay.frag", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("ARRAY", "")
			}), null, new RasterizerState(), new DepthState(DepthFunction.Always, false));

			_psoAuroraBlur = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"), Device.GetShader("auroraBlur.frag"), null, new RasterizerState(), new DepthState(DepthFunction.Equal, true), new BlendState(BlendMode.Additive));

			Random r = new Random(1);

			_randomness = new float[NumRandomValues];

			for(int i = 0; i < NumRandomValues; i++)
			{
				_randomness[i] = (float)r.NextDouble();
			}

			//new Spiral().SaveToFile("auroraFile.xml");
			//new AuroraCurtain().SaveToFile("auroraCurtains.xml");
			_loader = new AuroraFileLoader(rendererContext, this);
			_loader.LoadAuroraFiles();
			_loader.StartFileWatcher();
		}

		public override void OnResize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{
			base.OnResize(displayWidth, displayHeight, renderWidth, renderHeight);

			const int divide = 6;

			_texAurora?.Dispose();

			_texAurora = new Texture2D(Device, "SceneAurora", SizedInternalFormatGlob.RGBA16F, renderWidth / divide, renderHeight / divide);
			Glob.Utils.SetTextureParameters(Device, _texAurora, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
		}

		public void SetAuroraSpirlas(List<AuroraSpiral> spirals)
		{
			if(spirals == null)
				throw new ArgumentNullException(nameof(spirals));

			_spirals = spirals;
		}

		public void SetAuroraCurtains(AuroraCurtain aurora0, AuroraCurtain aurora1, AuroraCurtain aurora2,
			AuroraCurtain aurora3)
		{
			_psoRender?.Dispose();
			_psoEnvmap?.Dispose();

			List<Tuple<string, string>> macros = new List<Tuple<string, string>>();

			macros.AddRange(GenerateParamMacros(0, aurora0));
			macros.AddRange(GenerateParamMacros(1, aurora1));
			macros.AddRange(GenerateParamMacros(2, aurora2));
			macros.AddRange(GenerateParamMacros(3, aurora3));

			_psoRender = new ComputePipeline(Device, Device.GetShader("aurora_render.comp", macros));
			_psoEnvmap = new ComputePipeline(Device, Device.GetShader("aurora_envmap.comp", macros));
		}

		string Vec3ToString(Vector3 vec)
		{
			return "vec3(" +
				vec.X.ToString(CultureInfo.InvariantCulture) + ", " +
				vec.Y.ToString(CultureInfo.InvariantCulture) + ", " +
				vec.Z.ToString(CultureInfo.InvariantCulture) + ")";
		}

		List<Tuple<string, string>> GenerateParamMacros(int number, AuroraCurtain curtain)
		{
			List<Tuple<string, string>> list = new List<Tuple<string, string>>();
			list.Add(new Tuple<string, string>("PARAM_COLOR_BOTTOM_" + number.ToString(), Vec3ToString(curtain.ColorBottom)));
			list.Add(new Tuple<string, string>("PARAM_COLOR_TOP_" + number.ToString(), Vec3ToString(curtain.ColorTop)));
			list.Add(new Tuple<string, string>("PARAM_BRIGHTNESS_" + number.ToString(),
				curtain.Brightness.ToString(CultureInfo.InvariantCulture)));
			list.Add(new Tuple<string, string>("PARAM_HEIGHT_BOTTOM_" + number.ToString(),
				curtain.HeightBottom.ToString(CultureInfo.InvariantCulture)));
			list.Add(new Tuple<string, string>("PARAM_HEIGHT_CENTER_" + number.ToString(),
				curtain.HeightCenter.ToString(CultureInfo.InvariantCulture)));
			list.Add(new Tuple<string, string>("PARAM_HEIGHT_TOP_" + number.ToString(),
				curtain.HeightTop.ToString(CultureInfo.InvariantCulture)));
			list.Add(new Tuple<string, string>("PARAM_POWER_COLOR_" + number.ToString(),
				curtain.PowerColor.ToString(CultureInfo.InvariantCulture)));
			list.Add(new Tuple<string, string>("PARAM_POWER_DATA_" + number.ToString(),
				curtain.PowerData.ToString(CultureInfo.InvariantCulture)));
			list.Add(new Tuple<string, string>("PARAM_POWER_HEIGHT_BOTTOM_" + number.ToString(),
				curtain.PowerHeightBottom.ToString(CultureInfo.InvariantCulture)));
			list.Add(new Tuple<string, string>("PARAM_POWER_HEIGHT_TOP_" + number.ToString(),
				curtain.PowerHeightTop.ToString(CultureInfo.InvariantCulture)));
			return list;
		}

		public override void OnUploadPhase(FenceSync frameSync)
		{
			TimerQueryCPU query = new TimerQueryCPU(RenderContext.FeedbackManager, "AuroraUpload");
			query.StartQuery();

			base.OnUploadPhase(frameSync);

			_loader.Update();

			// Generate and upload spline geometry
			_splinesCurrent.Clear();

			for(int i = 0; i < _spirals.Count; i++)
			{
				_splinesCurrent.AddRange(_spirals[i].GenSplines(_splineGen, _randomness, SceneData.Ellapsed, i * 97));
			}

			_splineGen.Upload(frameSync);
			query.EndQuery();
		}

		public void Update(bool debugPlane)
		{
			TimerQueryGPU timerUpdate = new TimerQueryGPU(RenderContext.FeedbackManager, "AuroraSim");

			timerUpdate.StartQuery();

			// Update fluid sim and render splines into texture

			// Copy uploaded spline geometry into a buffer from which it will be renderer
			_splineGen.Copy();

			float deltaT = (float)SceneData.TimeDelta;

			// Update fluid simulation
			Action<Texture2DArray, Texture2DArray, Texture2DArray, Texture2DArray> fluidUpdate =
				(texVelocityIn, texQuantityIn, texVelocityOut, texQuantityOut) =>
				{
					var gridSizeRcp = new Vector2(1f / _fluidSim.Width, 1f / _fluidSim.Height);

					Device.BindPipeline(_psoUpdate);
					Device.ShaderCompute.SetUniformF("gridSizeRcp", gridSizeRcp);
					Device.ShaderCompute.SetUniformF("deltaT", deltaT);
					Device.ShaderCompute.SetUniformF("ellapsed", (float)SceneData.Ellapsed);
					Device.BindTexture(texVelocityIn, 0);
					Device.BindTexture(texQuantityIn, 1);
					Device.BindImage3D(0, texVelocityOut, TextureAccess.WriteOnly);
					Device.BindImage3D(1, texQuantityOut, TextureAccess.WriteOnly);
					Device.DispatchComputeThreads(_fluidSim.Width, _fluidSim.Height, _fluidSim.Layers);

					GL.MemoryBarrier(MemoryBarrierFlags.FramebufferBarrierBit);
				};

			_fluidSim.Update(Math.Min(deltaT * 0.2f, 0.1f), fluidUpdate);

			// Splat all aurora splines into a single texture
			using(Device.BindFrameBufferPushViewport(_fboSplines, "AuroraSplat"))
			{
				Device.BindPipeline(_psoSplineSplat);

				GL.ClearColor(0f, 0f, 0f, 0f);
				GL.Clear(ClearBufferMask.ColorBufferBit);

				Device.BindVertexBufferSource(_splineGen.SplineVboSource);
				GL.BindBuffer(BufferTarget.ElementArrayBuffer, _splineGen.SplineEboBinding.Buffer);

				Device.BindTexture(_fluidSim.Quantity, 0);

				float planeScale = PlaneScale;

				// Makes all the spirals/splines be visible at once for debugging and setting the spiral parameters
				if(debugPlane)
				{
					planeScale = 0.5f;
				}

				// Camera movement
				Vector2 positionOffset = (Vector2)(RenderContext.CameraPosition.Xz * -0.000005) + new Vector2(1.8f, 0);

				for(int i = 0; i < _splinesCurrent.Count; i++)
				{
					const int border = 0;

					for(int y = -border; y <= border; y++)
					{
						for(int x = -border; x <= border; x++)
						{
							Vector2 offset = positionOffset + _splinesCurrent[i].Offset + new Vector2(x, y);
							_psoSplineSplat.ShaderVertex.SetUniformF("posOffset", offset);
							_psoSplineSplat.ShaderVertex.SetUniformF("posScale", new Vector2(planeScale));
							_psoSplineSplat.ShaderFragment.SetUniformF("arrayLayer", _splinesCurrent[i].TexLayer);
							_psoSplineSplat.ShaderFragment.SetUniformF("primaryColor", _splinesCurrent[i].PrimaryColor);
							_psoSplineSplat.ShaderFragment.SetUniformF("secondaryColor", _splinesCurrent[i].SecondaryColor);
							_psoSplineSplat.ShaderFragment.SetUniformF("texCoordScale", _splinesCurrent[i].TexCoordScale);

							GL.DrawElementsBaseVertex(PrimitiveType.Triangles, _splinesCurrent[i].ElementCount, DrawElementsType.UnsignedShort, IntPtr.Zero, _splinesCurrent[i].BaseVertex);
						}
					}
				}
			}

			timerUpdate.EndQuery();
		}

		public void Render()
		{
			TimerQueryGPU timerRender = new TimerQueryGPU(RenderContext.FeedbackManager, "AuroraRender");

			if(_psoRender == null)
				return;

			timerRender.StartQuery();

			var result = _texAurora;

			Device.BindPipeline(_psoRender);
			
			Device.BindImage2D(0, result, TextureAccess.WriteOnly);
			Device.BindTexture(_texSplines, 0);

			Device.ShaderCompute.SetUniformF("ditherMatrix", _ditherMatrix);
			Device.ShaderCompute.SetUniformF("ray00", RenderContext.CameraRay00);
			Device.ShaderCompute.SetUniformF("ray01", RenderContext.CameraRay01);
			Device.ShaderCompute.SetUniformF("ray10", RenderContext.CameraRay10);
			Device.ShaderCompute.SetUniformF("ray11", RenderContext.CameraRay11);
			Device.ShaderCompute.SetUniformF("exposure", SceneData.Exposure);

			Device.ShaderCompute.SetUniformF("screenSizeRcp", new Vector2(1f / result.Width, 1f / result.Height));

			Device.DispatchComputeThreads(result.Width, result.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			Device.BindPipeline(_psoAuroraBlur);
			Device.BindTexture(_texAurora, 0);
			Device.ShaderFragment.SetUniformF("texelSize", new Vector2(1f / _texAurora.Width, 1f / _texAurora.Height));
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			timerRender.EndQuery();
		}

		public void RenderAtmosphereAndAuroraEnvmap(AtmosphereModule _atmo, TextureCube tex)
		{
			if(_psoEnvmap == null)
			{
				_atmo.RenderAtmosphereEnvmap(tex);
				return;
			}

			RenderContext.Device.BindPipeline(_psoEnvmap);

			_psoEnvmap.ShaderCompute.SetUniformF("cubeSizeRcp", new Vector2(1f / tex.Width, 1f / tex.Height));
			_atmo.SetupEnvmapRendering(_psoEnvmap.ShaderCompute, 0);
			Device.BindTexture(_texSplines, 1);

			Device.BindImage3D(0, tex, TextureAccess.WriteOnly, 0);

			Device.DispatchComputeThreads(tex.Width, tex.Height, 6);
		}

		// Displays a single layer of aurora fluid simulation
		public void RenderDebugFluidTexture(int layer, int windowW, int windowH)
		{
			int l = (layer + _fluidSim.Layers) % (_fluidSim.Layers + 1);
			if(l == _fluidSim.Layers)
				return;

			var tex = _fluidSim.Quantity;

			Device.BindPipeline(_psoTextureViewFluid);
			Device.BindTexture(tex, 0);
			_psoTextureViewFluid.ShaderFragment.SetUniformF("scale", 0.25f);
			_psoTextureViewFluid.ShaderFragment.SetUniformF("layer", l);
			_psoTextureViewFluid.ShaderFragment.SetUniformF("aspect", (tex.Width / (float)tex.Height) / ((float)windowW / windowH));
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}

		public void RenderDebugSplineTexture(int windowW, int windowH)
		{
			var tex = _texSplines;

			Device.BindPipeline(_psoTextureViewSpline);
			Device.BindTexture(tex, 0);
			_psoTextureViewSpline.ShaderFragment.SetUniformF("scale", 1f);
			_psoTextureViewSpline.ShaderFragment.SetUniformF("aspect", (tex.Width / (float)tex.Height) / ((float)windowW / windowH));
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
