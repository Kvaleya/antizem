﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using AuroraRenderer;
using GameFramework;

namespace Engine.Renderer.Aurora
{
	class AuroraFileLoader
	{
		AuroraModule _module;
		FileSystemWatcher _watcher;
		Renderer.RenderContext _engineContext;
		bool _doReload = false;
		Object _sync = new Object();

		public AuroraFileLoader(Renderer.RenderContext context, AuroraModule auroraModule)
		{
			_engineContext = context;
			_module = auroraModule;
		}

		public void StartFileWatcher()
		{
			_watcher = new FileSystemWatcher();
			_watcher.Path = Path.Combine(System.Environment.CurrentDirectory, "src/Auroras");
			_watcher.Filter = "*.xml";
			_watcher.Changed += (sender, args) =>
			{
				lock(_sync)
				{
					_doReload = true;
				}
			};

			_watcher.EnableRaisingEvents = true;
		}

		public void Update()
		{
			if(_doReload)
			{
				lock(_sync)
				{
					_doReload = false;
					// Watcher may detect file change before the text editor is done writing it
					Thread.Sleep(10);
					LoadAuroraFiles();
				}
			}
		}

		public void LoadAuroraFiles()
		{
			List<AuroraCurtain> curtains = null;
			List<AuroraSpiral> newSpirals = null;

			for(int i = 0; i < 20; i++)
			{
				try
				{
					newSpirals = AuroraSpiral.LoadFromFile(_engineContext.FileManager, "src/Auroras/auroraSpirals.xml");
					curtains = AuroraCurtain.LoadFromFile(_engineContext.FileManager, "src/Auroras/auroraCurtains.xml");
					break;
				}
				catch(ArgumentNullException e)
				{
					Thread.Sleep(10);
					curtains = null;
				}
				catch(Exception e)
				{
					_engineContext.TextOutput.PrintException(OutputType.Error, "Error loading auroras", e);
					curtains = null;
					newSpirals = null;
				}
			}

			if(newSpirals != null)
			{
				_module.SetAuroraSpirlas(newSpirals);
			}

			if(curtains != null)
			{
				_module.SetAuroraCurtains(curtains[0], curtains[1], curtains[2], curtains[3]);
			}
		}
	}
}
