﻿using System;
using System.Collections.Generic;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Aurora
{
	// Based on GPU Gems Chapter 38. Fast Fluid Dynamics Simulation on the GPU
	// http://developer.download.nvidia.com/books/HTML/gpugems/gpugems_ch38.html
	class FluidSim
	{
		public Texture2DArray Quantity { get { return _texQuantity; } }

		public int Width { get { return _width; } }
		public int Height { get { return _height; } }
		public int Layers { get { return _layers; } }

		ComputePipeline _psoAdvect, _psoJacobiDiffuse, _psoJacobiPressure, _psoDivergence, _psoGradientSubtract, _psoClearPressure;

		Texture2DArray _texQuantity, _texVelocityRead, _texPressure, _texDivergence;
		Texture2DArray _texQuantitySwap, _texVelocityWrite, _texVelocitySwap2, _texPressureSwap;

		Device _device;

		int _width, _height, _layers;

		// OpenGL sampler object
		int _sampler;

		float _viscousity;

		public FluidSim(Device device, int width, int height, int layers, float viscousity = 1e-11f)
		{
			_viscousity = viscousity;

			_device = device;

			// Several different fluid simulations are done at once using array textures
			// This uses fewer compute dispatches than doing separate fluid sims
			// This way the dispatches also contain more threads which is helps to occupy the GPU when using low resolution simulation
			_width = width;
			_height = height;
			_layers = layers;

			_psoAdvect = new ComputePipeline(_device, _device.GetShader("fluid_advect.comp"));

			_psoJacobiDiffuse = new ComputePipeline(_device, _device.GetShader("fluid_jacobi.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("FORMAT", "rg16f")
			}));

			_psoJacobiPressure = new ComputePipeline(_device, _device.GetShader("fluid_jacobi.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("FORMAT", "r16f")
			}));

			_psoDivergence = new ComputePipeline(_device, _device.GetShader("fluid_divergence.comp"));

			_psoGradientSubtract = new ComputePipeline(_device, _device.GetShader("fluid_gradient_subtract.comp"));

			_psoClearPressure = new ComputePipeline(_device, _device.GetShader("clearImage.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("FORMAT", "r32f"),
				new Tuple<string, string>("IMAGE", "image2D"),
				new Tuple<string, string>("VALUE", "vec4(0.0)"),
				new Tuple<string, string>("SIZEX", "8"),
				new Tuple<string, string>("SIZEY", "8"),
				new Tuple<string, string>("SIZEZ", "1"),
			}));

			_texQuantity = new Texture2DArray(_device, "TexFluidQuantity", SizedInternalFormatGlob.RG16F, width, height, layers, 1);
			_texQuantitySwap = new Texture2DArray(_device, "TexFluidQuantitySwap", SizedInternalFormatGlob.RG16F, width, height, layers, 1);
			_texVelocityRead = new Texture2DArray(_device, "TexFluidVelocity", SizedInternalFormatGlob.RG16F, width, height, layers, 1);
			_texVelocityWrite = new Texture2DArray(_device, "TexFluidVelocitySwap", SizedInternalFormatGlob.RG16F, width, height, layers, 1);
			_texVelocitySwap2 = new Texture2DArray(_device, "TexFluidVelocitySwap2", SizedInternalFormatGlob.RG16F, width, height, layers, 1);
			_texPressure = new Texture2DArray(_device, "TexFluidPressure", SizedInternalFormatGlob.R16F, width, height, layers, 1);
			_texPressureSwap = new Texture2DArray(_device, "TexFluidPressureSwap", SizedInternalFormatGlob.R16F, width, height, layers, 1);
			_texDivergence = new Texture2DArray(_device, "TexFluidVelocityDivergence", SizedInternalFormatGlob.R16F, width, height, layers, 1);

			_sampler = GL.GenSampler();
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureWrapR, (int)TextureWrapMode.Repeat);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
		}

		public void Update(float deltaT, Action<Texture2DArray, Texture2DArray, Texture2DArray, Texture2DArray> onUpdate = null)
		{
			using(_device.DebugMessageManager.PushGroupMarker("Fluid simulation"))
			{
				const float simulationSize = 1;

				GL.BindSampler(0, _sampler);
				GL.BindSampler(1, _sampler);

				Vector2 gridSizeRcp = new Vector2(1f / _width, 1f / _height);
				float gridSpacing = simulationSize / Math.Max(_width, _height);
				float halfGridScaleRcp = 0.5f / gridSpacing;

				// Advection
				_device.BindPipeline(_psoAdvect);
				_device.ShaderCompute.SetUniformF("deltaT", deltaT);
				_device.ShaderCompute.SetUniformF("gridSizeRcp", gridSizeRcp);
				_device.ShaderCompute.SetUniformF("gridSpacingRcp", 1.0f / gridSpacing);
				_device.BindTexture(_texVelocityRead, 0);
				_device.BindTexture(_texQuantity, 1);
				_device.BindImage3D(0, _texVelocityWrite, TextureAccess.WriteOnly);
				_device.BindImage3D(1, _texQuantitySwap, TextureAccess.WriteOnly);
				_device.DispatchComputeThreads(_width, _height, _layers);

				// Shaders always write into the "Write" textures
				// Every time a texture is updated by a shader, the read and write variants are swapped...
				var temp = _texVelocityRead;
				_texVelocityRead = _texVelocityWrite;
				_texVelocityWrite = temp;

				// .. for every texture updated
				temp = _texQuantity;
				_texQuantity = _texQuantitySwap;
				_texQuantitySwap = temp;

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				// Diffusion
				if(_viscousity != 0f)
				{
					// In auroras viscousity is unused (always zero), thus this never executes
					using(_device.DebugMessageManager.PushGroupMarker("Jacobi iterations for diffusion"))
					{
						float ndt = _viscousity * deltaT;

						_device.BindPipeline(_psoJacobiDiffuse);
						_device.ShaderCompute.SetUniformF("gridSizeRcp", gridSizeRcp);
						_device.ShaderCompute.SetUniformF("alpha", gridSpacing * gridSpacing / ndt);
						_device.ShaderCompute.SetUniformF("betaRcp", 1.0f / (4 + gridSpacing * gridSpacing / ndt));
						_device.BindTexture(_texVelocityRead, 1);

						for(int i = 0; i < 40; i++)
						{
							_device.BindTexture(_texVelocityWrite, 0);
							_device.BindImage3D(0, _texVelocitySwap2, TextureAccess.WriteOnly);

							_device.DispatchComputeThreads(_width, _height, _layers);

							temp = _texVelocityWrite;
							_texVelocityWrite = _texVelocitySwap2;
							_texVelocitySwap2 = temp;

							GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
						}

						temp = _texVelocityRead;
						_texVelocityRead = _texVelocityWrite;
						_texVelocityWrite = temp;
					}
				}

				// Add forces
				if(onUpdate != null)
				{
					onUpdate.Invoke(_texVelocityRead, _texQuantity, _texVelocityWrite, _texQuantitySwap);
				}
				
				temp = _texVelocityRead;
				_texVelocityRead = _texVelocityWrite;
				_texVelocityWrite = temp;

				temp = _texQuantity;
				_texQuantity = _texQuantitySwap;
				_texQuantitySwap = temp;

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				// Pressure projection

				// Compute velocity divergence
				_device.BindPipeline(_psoDivergence);
				_device.ShaderCompute.SetUniformF("halfGridScaleRcp", halfGridScaleRcp);
				_device.ShaderCompute.SetUniformF("gridSizeRcp", gridSizeRcp);
				_device.BindTexture(_texVelocityRead, 0);
				_device.BindImage3D(0, _texDivergence, TextureAccess.WriteOnly);
				_device.DispatchComputeThreads(_width, _height, _layers);

				// Clear the pressure texture
				_device.BindPipeline(_psoClearPressure);
				_device.BindImage3D(0, _texPressure, TextureAccess.WriteOnly);
				_device.DispatchComputeThreads(_width, _height, _layers);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				// Jacobi iterations for pressure
				using(_device.DebugMessageManager.PushGroupMarker("Jacobi iterations for pressure"))
				{
					_device.BindPipeline(_psoJacobiPressure);
					_device.ShaderCompute.SetUniformF("gridSizeRcp", gridSizeRcp);
					_device.ShaderCompute.SetUniformF("alpha", -gridSpacing * gridSpacing);
					_device.ShaderCompute.SetUniformF("betaRcp", 0.25f);
					_device.BindTexture(_texDivergence, 1);

					for(int i = 0; i < 20; i++)
					{
						_device.BindTexture(_texPressure, 0);
						_device.BindImage3D(0, _texPressureSwap, TextureAccess.WriteOnly);

						_device.DispatchComputeThreads(_width, _height, _layers);

						temp = _texPressure;
						_texPressure = _texPressureSwap;
						_texPressureSwap = temp;

						GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
					}
				}

				// Subtract gradient

				_device.BindPipeline(_psoGradientSubtract);
				_device.ShaderCompute.SetUniformF("gridSizeRcp", gridSizeRcp);
				_device.ShaderCompute.SetUniformF("halfGridScaleRcp", halfGridScaleRcp);
				_device.BindTexture(_texVelocityRead, 0);
				_device.BindTexture(_texPressure, 1);
				_device.BindImage3D(0, _texVelocityWrite, TextureAccess.WriteOnly);
				_device.DispatchComputeThreads(_width, _height, _layers);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				temp = _texVelocityRead;
				_texVelocityRead = _texVelocityWrite;
				_texVelocityWrite = temp;

				GL.BindSampler(0, 0);
				GL.BindSampler(1, 0);
			}
		}
	}
}
