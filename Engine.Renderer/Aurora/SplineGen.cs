﻿using System;
using System.Runtime.InteropServices;
using Glob;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Renderer.Aurora
{
	// Based on:
	// http://www.math.ucla.edu/~baker/149.1.02w/handouts/dd_splines.pdf
	class SplineGen
	{
		const int VertexSize = 12;
		const int MaxVertices = 2 << 17;
		
		public readonly VertexBufferFormat SplineVboFormat;
		public readonly VertexBufferSource SplineVboSource;
		public readonly VertexBufferBinding SplineEboBinding;

		int _vertexBuffer;
		int _indexBuffer;

		StreamBuffer _vertexUploadBuffer;

		int _vertexUploadCount = 0;
		int[] _verticesToUpload;

		int _copyOffset;
		int _copyCount;

		public SplineGen(int maxIndices = 65536)
		{
			ushort[] indices = new ushort[maxIndices];

			// Spline rendered as a triangle strip:
			// 0---2---4---6
			// |  /|  /|  /|
			// | / | / | / |
			// |/  |/  |/  |
			// 1---3---5---7
			// Indices: 0,1,2,  2,1,3,  2,3,4,  4,3,5,  ...
			for(int i = 0; i < maxIndices; i++)
			{
				int quadIndex = i / 6;
				int inQuad = i % 6;

				int result = quadIndex * 2;

				if(inQuad == 1 || inQuad == 4)
					result += 1;
				if(inQuad == 2 || inQuad == 3)
					result += 2;
				if(inQuad == 5)
					result += 3;
				
				indices[i] = (ushort)result;
			}

			_verticesToUpload = new int[MaxVertices * 3];

			_indexBuffer = Utils.CreateBuffer(BufferTarget.ElementArrayBuffer, new IntPtr(2 * maxIndices), indices,
				BufferStorageFlags.None, "BezierIndexBuffer");
			_vertexBuffer = Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(VertexSize * MaxVertices), _verticesToUpload, 
				BufferStorageFlags.None, "BezierVertexBuffer");
			_vertexUploadBuffer = new StreamBuffer("BezierVertexUploadBuffer", BufferTarget.CopyReadBuffer, VertexSize * MaxVertices);

			// 12 bytes per vertex: 2x float position coordinate, 2x unorm16 texcoord
			SplineVboFormat = new VertexBufferFormat(new VertexAttribDescription[]
			{
				new VertexAttribDescription(0, 0, 2, 0, VertexAttribType.Float, VertexAttribClass.Float, false), 
				new VertexAttribDescription(1, 0, 2, 8, VertexAttribType.UnsignedShort, VertexAttribClass.Float, true), 
			});

			SplineVboSource = new VertexBufferSource(0, new VertexBufferBinding(_vertexBuffer, 0, VertexSize, 0));

			SplineEboBinding = new VertexBufferBinding(_indexBuffer, 0, 2, 0);
		}

		// First item: base vertex
		// Second item: index count (triangle count * 3)
		public Tuple<int, int> AddSpline(int innerPoints, float width, params Vector2[] controlPolygon)
		{
			if(controlPolygon.Length < 2)
				throw new Exception("The spline control polygon must cointain at least two vertices!");

			var vertices = GenerateSpline(controlPolygon, innerPoints, width);

			int baseVertex = _vertexUploadCount;

			for(int i = 0; i < vertices.Length && _vertexUploadCount < _verticesToUpload.Length; i++)
			{
				_verticesToUpload[_vertexUploadCount * 3 + 0] = Unsafe.SingleToInt32Bits(vertices[i].X);
				_verticesToUpload[_vertexUploadCount * 3 + 1] = Unsafe.SingleToInt32Bits(vertices[i].Y);
				_verticesToUpload[_vertexUploadCount * 3 + 2] = PackVec2(vertices[i].Zw);
				_vertexUploadCount++;
			}

			return new Tuple<int, int>(baseVertex, (vertices.Length - 2) * 3);
		}

		void GetCubicBezier(float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, out Vector2 result,
			out Vector2 derivative)
		{
			float u = 1 - t;
			result = u * u * u * p0 + 3 * u * u * t * p1 + 3 * u * t * t * p2 + t * t * t * p3;
			derivative = 3 * u * u * (p1 - p0) + 6 * u * t * (p2 - p1) + 3 * t * t * (p3 - p2);
		}

		Vector4[] GenerateSpline(Vector2[] controlPolygon, int innerPoints, float width)
		{
			// Find the control points of each cubic curve that makes up the spline
			// The start and end point of each curve is shared, except for the very first and very last point
			int cubicCurves = (controlPolygon.Length - 1);
			Vector2[] controlPoints = new Vector2[cubicCurves * 3 + 1];

			for(int i = 0; i < controlPolygon.Length - 1; i++)
			{
				Vector2 sFirst = controlPolygon[0];

				Vector2 third1 = (controlPolygon[i] * 2 + controlPolygon[i + 1]) / 3;
				Vector2 third2 = (controlPolygon[i] + controlPolygon[i + 1] * 2) / 3;

				if(i > 0)
				{
					Vector2 prevThird = (controlPolygon[i - 1] + controlPolygon[i] * 2) / 3;
					sFirst = (prevThird + third1) / 2;
				}

				controlPoints[i * 3 + 0] = sFirst;
				controlPoints[i * 3 + 1] = third1;
				controlPoints[i * 3 + 2] = third2;
			}

			controlPoints[controlPoints.Length - 1] = controlPolygon[controlPolygon.Length - 1];

			// Generate vertices
			Vector4[] vertices = new Vector4[(cubicCurves * (innerPoints + 1) + 1) * 2];

			int vertexIndex = 0;

			for(int i = 0; i < cubicCurves; i++)
			{
				int starting = 1;

				if(i == 0)
					starting = 0;

				var p0 = controlPoints[i * 3 + 0];
				var p1 = controlPoints[i * 3 + 1];
				var p2 = controlPoints[i * 3 + 2];
				var p3 = controlPoints[i * 3 + 3];

				for(int j = starting; j <= innerPoints + 1; j++)
				{
					float t = j / (float)(innerPoints + 1);
					Vector2 pos, dir;
					GetCubicBezier(t, p0, p1, p2, p3, out pos, out dir);
					Vector2 normal = new Vector2(dir.Y, -dir.X);
					normal.Normalize();

					float alongSpline = vertexIndex / (float)(vertices.Length - 2);

					// TODO: fix: texture gets too compressed on auroras near the circle center
					vertices[vertexIndex] = new Vector4(pos.X + normal.X * width * 0.5f, pos.Y + normal.Y * width * 0.5f, 0.0f, alongSpline);
					vertexIndex++;
					vertices[vertexIndex] = new Vector4(pos.X - normal.X * width * 0.5f, pos.Y - normal.Y * width * 0.5f, 1.0f, alongSpline);
					vertexIndex++;
				}
			}

			return vertices;
		}

		// Copy data required for this frame into persistently mapped buffers
		public void Upload(FenceSync frameSync)
		{
			_copyCount = _vertexUploadCount * VertexSize;
			_copyOffset = _vertexUploadBuffer.CurrentOffset;

			Unsafe.Copy(_verticesToUpload, _vertexUploadBuffer.CurrentStart, 0, (uint)(_vertexUploadCount * 3));

			_vertexUploadBuffer.Advance(frameSync);

			_vertexUploadCount = 0;
		}

		// Copy the data from persistently mapped buffer into a GPU buffer
		public void Copy()
		{
			Utils.CopyBuffer(_vertexUploadBuffer.Handle, _vertexBuffer, _copyOffset, 0, _copyCount);
		}

		static int PackVec2(Vector2 vec)
		{
			int u = 0;
			u += PackUnorm(vec.Y) << 16;
			u += PackUnorm(vec.X) << 0;
			return u;
		}

		static ushort PackUnorm(float v)
		{
			return (ushort)((System.Math.Min(System.Math.Max(v, 0), 1)) * 65535);
		}

		static unsafe class Unsafe
		{
			// https://stackoverflow.com/questions/20981551/c-sharp-marshal-copy-intptr-to-16-bit-managed-unsigned-integer-array
			public static void Copy(int[] source, IntPtr ptrDest, int start, uint elements)
			{
				fixed (int* ptrSource = &source[start])
				{
					CopyMemory(ptrDest, (IntPtr)ptrSource, elements * 8);
				}
			}

			public static int SingleToInt32Bits(float value)
			{
				return *(int*)(&value);
			}
			public static float Int32BitsToSingle(int value)
			{
				return *(float*)(&value);
			}

			[DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory", SetLastError = false)]
			static extern void CopyMemory(IntPtr destination, IntPtr source, uint length);
		}
	}
}
