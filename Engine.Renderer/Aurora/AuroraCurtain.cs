﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Xml;
using GameFramework;
using OpenTK;

namespace AuroraRenderer
{
	// Describes the visual features of aurora curtains, such as the color gradient
	// This structure is inlined into the GPU shader via define macros (for performance reasons)
	// See the aurora shader for exact parameter meaning
	[Serializable]
	[StructLayout(LayoutKind.Sequential, Pack = 4)]
	struct AuroraCurtain
	{
		public Vector3 ColorBottom;

		// Aurora light intensity scale
		public float Brightness;

		public Vector3 ColorTop;

		// Height of the curtain bottom, center and top
		// In range 0.0 .. 1.0, where 0.0 is the bottom of aurora layer and 1.0 is the top
		public float HeightBottom;
		public float HeightCenter;
		public float HeightTop;

		// Controls the color gradient
		public float PowerColor;
		// Power of the source aurora data (fluid sim + splines), higher values lead to higher contrast
		public float PowerData;

		// Sharpness of fade/gradient of the bottom/top aurora edge
		public float PowerHeightBottom;
		public float PowerHeightTop;

		public static List<AuroraCurtain> LoadFromFile(IFileManager fileManager, string filename)
		{
			using(var stream = fileManager.GetStream(filename))
			{
				XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas());
				DataContractSerializer ser = new DataContractSerializer(typeof(List<AuroraCurtain>));

				return (List<AuroraCurtain>)ser.ReadObject(reader, true);
			}
		}

		public void SaveToFile(string filename)
		{
			var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };

			DataContractSerializer ds = new DataContractSerializer(typeof(List<AuroraCurtain>));

			using(var writer = XmlWriter.Create(filename, settings))
			{
				ds.WriteObject(writer, new List<AuroraCurtain>()
					{
						this,
					});
			}
		}
	}
}
