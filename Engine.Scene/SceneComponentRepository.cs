﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Engine.Common;
using Engine.Materials;
using Engine.Scene.Descriptions;
using Engine.Scene.Serializable;
using GameFramework;

namespace Engine.Scene
{
	public class SceneComponentRepository
	{
		IFileManager _fileManager;
		TextOutput _textOutput;
		IMeshManager _meshManager;
		MaterialManager _materialManager;

		LazyCache<string, SceneComponent> _cache;

		public SceneComponentRepository(IFileManager fileManager, TextOutput textOutput, IMeshManager meshManager, MaterialManager materialManager)
		{
			_fileManager = fileManager;
			_textOutput = textOutput;
			_meshManager = meshManager;
			_materialManager = materialManager;
			_cache = new LazyCache<string, SceneComponent>(LoadComponent);
		}

		SceneComponent LoadComponent(string filename)
		{
			SceneComponentSerializable data = null;

			try
			{
				if(_fileManager.FileExists(filename))
				{
					using(var stream = _fileManager.GetStream(filename))
					{
						XmlSerializer xmlSerializer = new XmlSerializer(typeof(SceneComponentSerializable));
						data = (SceneComponentSerializable)xmlSerializer.Deserialize(stream);

						if(data.LodDistances.Length != data.Lods.Length)
							throw new Exception("Lod count and lod distance count does not match!");
					}
				}
				else
				{
					throw new FileNotFoundException("Cannot load material file " + filename);
				}
			}
			catch(Exception e)
			{
				_textOutput.PrintException(OutputType.Warning, "Error loading scene component" + filename, e);
				return new SceneComponent(new List<PrimitiveMeshStatic>[1]
				{
					new List<PrimitiveMeshStatic>()
				}, new float[1]
				{
					0f,
				});
			}

			var primitives = new List<PrimitiveMeshStatic>[data.Lods.Length];

			for(int i = 0; i < data.Lods.Length; i++)
			{
				var list = new List<PrimitiveMeshStatic>();

				for(int j = 0; j < data.Lods[i].Primitives.Count; j++)
				{
					var p = data.Lods[i].Primitives[j];
					list.Add(new PrimitiveMeshStatic()
					{
						Material = _materialManager.AddOrGetMaterial(p.MaterialFile, true),
						Mesh = _meshManager.GetMesh(Utils.Meshes.MeshFileAndNameCombine(p.MeshFile, p.MeshName)),
						Position = p.Position,
						Rotation = p.Rotation,
						Scale = p.Scale,
						Flags = p.Flags,
					});
				}

				primitives[i] = list;
			}

			return new SceneComponent(primitives, data.LodDistances.ToArray());
		}

		public SceneComponent GetComponent(string filename)
		{
			return _cache.GetItem(filename);
		}
	}
}
