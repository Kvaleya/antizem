﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Scene.Descriptions;
using OpenTK;

namespace Engine.Scene
{
	public class SceneComponentInstance : IDisposable
	{
		InstanceDataCache _dataCache;
		int _dataIndex;
		SceneMeshManager _manager;

		internal ComponentInstanceData Data
		{
			get { return _dataCache[_dataIndex]; }
			set { _dataCache[_dataIndex] = value; }
		}

		internal int DataIndex
		{
			get { return _dataIndex; }
			set { _dataIndex = value; }
		}

		public SceneComponent Component
		{
			get { return Data.Component; }
			set
			{
				var data = Data;
				data.Component = value;
				Data = data;
			}
		}

		public Vector3d Position
		{
			get { return Data.Position; }
			set
			{
				var data = Data;
				data.Position = value;
				Data = data;
			}
		}

		public Quaternion Rotation
		{
			get { return Data.Rotation; }
			set
			{
				var data = Data;
				data.Rotation = value;
				Data = data;
			}
		}

		public float Scale
		{
			get { return Data.Scale; }
			set
			{
				var data = Data;
				data.Scale = value;
				Data = data;
			}
		}

		public int CurrentLod
		{
			get { return Data.CurrentLod; }
			internal set
			{
				var data = Data;
				data.CurrentLod = value;
				Data = data;
			}
		}

		public SceneComponentInstance(SceneMeshManager manager, SceneComponent component)
		{
			_manager = manager;
			_manager.AddInstance(this);
			_dataCache = _manager.ComponentInstanceDataCache;
			CurrentLod = component.LodDistances.Length - 1;
			Component = component;
		}

		public void UpdateTransform(Vector3d position, Quaternion rotation, float scale)
		{
			var data = Data;
			data.Position = position;
			data.Rotation = rotation;
			data.Scale = scale;
			Data = data;
		}

		public void Dispose()
		{
			_manager.RemoveInstance(this);
		}
	}
}
