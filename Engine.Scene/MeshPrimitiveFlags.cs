﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Scene
{
	[Flags]
	public enum MeshPrimitiveFlags
	{
		TwoSided = 1,
		// Ideas: skinned, disable triangle culling, tesselated
	}
}
