﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Engine.Scene
{
	class InstanceDataCache
	{
		ComponentInstanceData[] _array;
		int _count;

		public int Count { get { return _count; } }

		public ComponentInstanceData this[int i]
		{
			get { return _array[i]; }
			set { _array[i] = value; }
		}

		public InstanceDataCache(int capacity)
		{
			_array = new ComponentInstanceData[capacity];
			_count = 0;
		}

		// Concurrent add
		public void Add(SceneComponentInstance instance)
		{
			int index = Interlocked.Increment(ref _count) - 1;
			instance.DataIndex = index;
			_array[index].Instance = instance;
		}

		// Non-parallel remove
		public void Remove(SceneComponentInstance instance)
		{
			int index = instance.DataIndex;
			instance.DataIndex = -1;

			_count--;

			if(index == _count)
			{
				return;
			}

			var last = _array[_count];
			_array[index] = last;
			last.Instance.DataIndex = index;
		}
	}
}
