﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Scene.Descriptions;
using OpenTK;

namespace Engine.Scene
{
	struct ComponentInstanceData
	{
		public SceneComponent Component;
		public SceneComponentInstance Instance;
		public Vector3d Position;
		public Quaternion Rotation;
		public float Scale;
		public int CurrentLod;
	}
}
