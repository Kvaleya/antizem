﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using Engine.Materials;
using Engine.Scene.Serializable;
using OpenTK;

namespace Engine.Scene.Descriptions
{
	public class PrimitiveMeshStatic
	{
		public Matrix4 LocalTransform { get { return _transform; } }

		public MeshPrimitiveFlags Flags { get { return _flags; } set { _flags = value; } }

		public Vector3 Position
		{
			get
			{
				return _position;
			}
			set
			{
				_position = value;
				RefreshMatrix();
			}
		}

		public Quaternion Rotation
		{
			get
			{
				return _rotation;
			}
			set
			{
				_rotation = value;
				RefreshMatrix();
			}
		}

		public float Scale
		{
			get
			{
				return _scale;
			}
			set
			{
				_scale = value;
				RefreshMatrix();
			}
		}

		public IMesh Mesh;
		public Material Material;

		Vector3 _position = Vector3.Zero;
		Quaternion _rotation = Quaternion.Identity;
		float _scale = 1.0f;
		Matrix4 _transform = Matrix4.Identity;
		MeshPrimitiveFlags _flags = (MeshPrimitiveFlags)0;

		public PrimitiveMeshStatic()
		{
		}

		internal PrimitiveMeshStaticSerializable ToSerializable()
		{
			var s = new PrimitiveMeshStaticSerializable();

			s.MaterialFile = Material.FileString;
			s.MeshFile = Mesh.FileName;
			s.MeshName = Mesh.MeshName;
			s.Position = _position;
			s.Rotation = _rotation;
			s.Scale = _scale;
			s.Flags = _flags;

			return s;
		}

		void RefreshMatrix()
		{
			_transform = Utils.Math.GetWorldMatrix(Position, Rotation, Scale);
		}

		internal bool Loaded { get { return Mesh.IsLoaded; } }

		//internal void AddToData(SceneData data, List<IntWrapper> indices, Matrix4 transform, int offsetX, int offsetY, int offsetZ)
		//{
		//	Matrix4 localTransform = LocalTransform * transform;
		//	var instance = new MeshInstance(Mesh, localTransform, offsetX, offsetY, offsetZ);
		//	indices.Add(data.MeshStaticInstances.Add(instance));
		//}

		internal MeshInstanceData GetMesh(Matrix4 transform)
		{
			int materialID = 0;

			if(Material != null)
				materialID = Material.ID;

			Matrix4 finalTransform = _transform * transform;
			return new MeshInstanceData(finalTransform, Mesh, materialID, (int)_flags);
		}

		internal void SetDistance(object caller, float meshDistance, float materialDistance)
		{
			Mesh.SetDistance(caller, meshDistance);
			Material?.SetDistance(caller, materialDistance);
		}

		internal void RemoveDistance(object caller)
		{
			Mesh.RemoveDistance(caller);
			Material?.RemoveDistance(caller);
		}
	}
}
