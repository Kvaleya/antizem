﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Engine.Common;
using Engine.Materials;
using Engine.Scene.Serializable;
using OpenTK;

namespace Engine.Scene.Descriptions
{
	public class SceneComponent
	{
		internal List<PrimitiveMeshStatic>[] Primitives; // Each list is one LOD
		internal float[] LodDistances;

		public SceneComponent(List<PrimitiveMeshStatic>[] primitives, float[] lodDistances)
		{
			Primitives = primitives;
			LodDistances = lodDistances;
		}

		internal SceneComponentSerializable ToSerializable()
		{
			SceneComponentSerializable s = new SceneComponentSerializable();
			s.LodDistances = LodDistances.ToArray();
			s.Lods = new ComponentLodSerializable[Primitives.Length];

			for(int i = 0; i < s.Lods.Length; i++)
			{
				var lod = new ComponentLodSerializable();
				lod.Primitives = new List<PrimitiveMeshStaticSerializable>();

				for(int j = 0; j < Primitives[i].Count; j++)
				{
					lod.Primitives.Add(Primitives[i][j].ToSerializable());
				}

				s.Lods[i] = lod;
			}

			return s;
		}

		public void SaveToFile(string filename)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(SceneComponentSerializable));

			using(var stream = new FileStream(filename, FileMode.Create))
			{
				xmlSerializer.Serialize(stream, this.ToSerializable());
			}
		}

		internal bool LodLoaded(int lod)
		{
			foreach(var primitive in Primitives[lod])
			{
				if(!primitive.Loaded)
					return false;
			}
			return true;
		}
	}
}
