﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Engine.Scene.Descriptions;
using Engine.Scene.Serializable;
using GameFramework;
using OpenTK;

namespace Engine.Scene.ArchAZDOScene
{
	public static class ArchAZDOConverter
	{
		public static void Convert(string fileOut, string fileIn, string workingFolder, IFileManager fileManager)
		{
			HashSet<string> materials = new HashSet<string>();

			SceneComponentSerializable data;

			using(var stream = fileManager.GetStream(Path.Combine(workingFolder, fileIn)))
			{
				XmlSerializer xmlDeserializer = new XmlSerializer(typeof(SceneComponentSerializable));
				data = (SceneComponentSerializable)xmlDeserializer.Deserialize(stream);
			}

			ComponentLodSerializable[] primitives = new ComponentLodSerializable[data.Lods.Length];
			float[] lodDistances = new float[data.Lods.Length];

			for(int i = 0; i < data.Lods.Length; i++)
			{
				lodDistances[i] = data.Lods[i].Distance;
				var list = new List<PrimitiveMeshStaticSerializable>();

				for(int j = 0; j < data.Lods[i].StaticMeshes.Count; j++)
				{
					var mesh = data.Lods[i].StaticMeshes[j];
					list.Add(new PrimitiveMeshStaticSerializable()
					{
						MaterialFile = mesh.MaterialFile,
						MeshFile = mesh.MeshFile,
						MeshName = mesh.MeshName,
						Position = mesh.Position,
						Rotation = mesh.Rotation,
						Scale = Math.Max(Math.Max(mesh.Scale.X, mesh.Scale.Y), mesh.Scale.Z),
					});

					materials.Add(mesh.MaterialFile);
				}

				primitives[i] = new ComponentLodSerializable()
				{
					Primitives = list,
				};
			}

			Serializable.SceneComponentSerializable componentResult = new Serializable.SceneComponentSerializable();
			componentResult.LodDistances = lodDistances;
			componentResult.Lods = primitives;

			var type = typeof(Serializable.SceneComponentSerializable);

			XmlSerializer xmlSerializer = new XmlSerializer(type);

			using(var stream = new FileStream(fileOut, FileMode.Create))
			{
				xmlSerializer.Serialize(stream, componentResult);
			}

			foreach(string materialFile in materials)
			{
				ArchAZDOScene.MaterialSerializable materialData;

				string file = Path.Combine("Materials", materialFile) + ".kvmat";

				using(var stream = fileManager.GetStream(Path.Combine(workingFolder, file)))
				{
					XmlSerializer xmlDeserializer = new XmlSerializer(typeof(ArchAZDOScene.MaterialSerializable));
					materialData = (MaterialSerializable)xmlDeserializer.Deserialize(stream);
				}

				Materials.MaterialSerializable materialResult = new Materials.MaterialSerializable();
				materialResult.BaseColorScale = new Vector4(materialData.BaseColorR, materialData.BaseColorG, materialData.BaseColorB, materialData.BaseColorA);
				materialResult.RoughnessScale = materialData.Roughness;
				materialResult.MetallicScale = materialData.Metallic;
				materialResult.TextureBaseColor = materialData.TextureBaseColor;
				materialResult.TextureNormalRoughnessMetallic = materialData.TextureNormal;
				materialResult.TextureHeightAlpha = materialData.TextureAlpha;

				XmlSerializer xmlSerializerMat = new XmlSerializer(typeof(Materials.MaterialSerializable));

				using(var stream = new FileStream(Path.Combine("Materials", materialFile) + ".xml", FileMode.Create))
				{
					xmlSerializerMat.Serialize(stream, materialResult);
				}
			}
		}
	}
}
