﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Scene.ArchAZDOScene
{
	public enum MaterialType
	{
		Default,
		Alphatest,
	}

	public class MaterialSerializable
	{
		public MaterialType Type = MaterialType.Default;

		public string TextureBaseColor = "";
		public string TextureAlpha = "";
		public string TextureNormal = "";
		public string TextureMetallic = "";
		public string TextureRoughness = "";

		public float BaseColorR = 1.0f;
		public float BaseColorG = 1.0f;
		public float BaseColorB = 1.0f;
		public float BaseColorA = 1.0f;

		public float Roughness = 0.0f;
		public float Metallic = 0.0f;
		public float Glow = 0.0f;
		public float HeightScale = 0.0f;
	}

	public class SceneComponentSerializable
	{
		public SceneComponentLodSerializable[] Lods;
		public List<StaticMeshActorSerializable> StaticMeshesCollision;
	}

	public class SceneComponentLodSerializable
	{
		public float Distance;
		public List<StaticMeshActorSerializable> StaticMeshes;
		public List<StaticMeshActorSerializable> StaticMeshesShadow;
	}

	public class StaticMeshActorSerializable
	{
		public Vector3 Position;
		public Quaternion Rotation;
		public Vector3 Scale;
		public string MeshFile;
		public string MeshName;
		public string MaterialFile;
	}
}
