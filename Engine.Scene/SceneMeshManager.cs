﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using Engine.Common.Structures;
using Engine.Scene.Descriptions;
using OpenTK;
using Vector3 = OpenTK.Vector3;
using Vector4 = OpenTK.Vector4;

namespace Engine.Scene
{
	public class SceneMeshManager
	{
		const int LodMod = 32;
		const int MaxComponentInstances = 1 << 17;
		const int MaxMeshInstances = 1 << 18;

		InstanceDataCache _componentInstanceData = new InstanceDataCache(MaxComponentInstances);

		ParallelBuffer<SceneComponentInstance> _instancesAdded = new ParallelBuffer<SceneComponentInstance>(MaxComponentInstances);
		//ParallelBuffer<int> _instancesUpdated = new ParallelBuffer<int>(MaxInstances);
		ParallelBuffer<SceneComponentInstance> _instancesRemoved = new ParallelBuffer<SceneComponentInstance>(MaxComponentInstances);

		PrimitiveMeshStatic[] _meshPrimitives = new PrimitiveMeshStatic[MaxMeshInstances];
		MeshInstanceData[] _meshData = new MeshInstanceData[MaxMeshInstances];
		Vector4[] _meshBoundingSpheres = new Vector4[MaxMeshInstances];
		int _meshCount;

		internal InstanceDataCache ComponentInstanceDataCache { get { return _componentInstanceData; } }

		public ICamera MainCamera { get; private set; }

		int _lodUpdateIndex = 0;
		
		public SceneMeshManager(ICamera mainCamera)
		{
			MainCamera = mainCamera;
		}

		internal void AddInstance(SceneComponentInstance instance)
		{
			_componentInstanceData.Add(instance);
			_instancesAdded.Add(instance);
		}

		//public void UpdateInstance(SceneComponentInstanceWrapper instance)
		//{
		//	_instancesUpdated.Add(instance.DataIndex);
		//}

		internal void RemoveInstance(SceneComponentInstance instance)
		{
			_instancesRemoved.Add(instance);
		}

		void UpdateLod(SceneComponentInstance instance)
		{
			float distanceSquare = (float)(Vector3d.Dot(instance.Position, MainCamera.Position) / instance.Scale);
			int lod = instance.CurrentLod;

			var component = instance.Component;

			while(lod > 0 && distanceSquare < component.LodDistances[lod] && component.LodLoaded(lod - 1))
				lod--;
			while(lod < component.LodDistances.Length - 1 && distanceSquare > component.LodDistances[lod] &&
				  component.LodLoaded(lod + 1))
				lod++;

			instance.CurrentLod = lod;

			for(int i = 0; i < component.Primitives.Length; i++)
			{
				float resourceImportance = distanceSquare / component.LodDistances[i];
				if(float.IsInfinity(component.LodDistances[i]))
				{
					resourceImportance = 0;
				}

				foreach(var primitive in component.Primitives[i])
				{
					primitive.SetDistance(instance, resourceImportance, distanceSquare);
				}
			}
		}

		public void Update(bool updateLods)
		{
			// Remove instances
			for(int i = 0; i < _instancesRemoved.Count; i++)
			{
				var instance = _instancesRemoved[i];
				_componentInstanceData.Remove(instance);
			}

			int lowLods = 0;
			int highLods = 0;

			for(int i = 0; i < _instancesAdded.Count; i++)
			{
				var instance = _instancesAdded[i];
				UpdateLod(instance);
				if(instance.CurrentLod == 0)
					highLods++;
				else
					lowLods++;
			}

			// Update LODs
			if(updateLods)
			{
				int lodUpdateCount = (_componentInstanceData.Count / LodMod) + 1;
				int last = Math.Min(_lodUpdateIndex + lodUpdateCount, _componentInstanceData.Count);

				for(; _lodUpdateIndex < last; _lodUpdateIndex++)
				{
					UpdateLod(_componentInstanceData[_lodUpdateIndex].Instance);
				}

				if(_lodUpdateIndex >= _componentInstanceData.Count)
					_lodUpdateIndex = 0;
			}

			// Create meshes
			_meshCount = 0;

			//for(int i = 0; i < _componentInstanceData.Count; i++)
			Utils.ParallelForRange(0, _componentInstanceData.Count, 0, (start, end) =>
			{
				for(int i = start; i < end; i++)
				{
					var data = _componentInstanceData[i];

					int count = data.Component.Primitives[data.CurrentLod].Count;

					int meshIndexStart;
					meshIndexStart = Interlocked.Add(ref _meshCount, count) - count;
					//meshIndexStart = _meshCount;
					//_meshCount += count;

					Matrix4 transform = Utils.Math.GetWorldMatrix((Vector3)(data.Position - MainCamera.Position), data.Rotation,
						data.Scale);
					for(int lodMeshIndex = 0; lodMeshIndex < count; lodMeshIndex++)
					{
						var primitive = data.Component.Primitives[data.CurrentLod][lodMeshIndex];
						int index = meshIndexStart + lodMeshIndex;
						_meshPrimitives[index] = primitive;
						_meshData[index] = primitive.GetMesh(transform);
						var sphere = Utils.Math.BoundingSphereTransformed(_meshData[index].Mesh.BoundingSphere, _meshData[index].Transform4x4);
						_meshBoundingSpheres[index] = sphere;
					}
				}
			});

			_instancesAdded.Reset();
			//_instancesUpdated.Reset();
			_instancesRemoved.Reset();
		}

		void FrustumCullAndBucket(Matrix4 cullMatrix, out List<MeshInstanceData> staticMeshes, out List<MeshInstanceData> staticAlphaMeshes)
		{
			staticMeshes = new List<MeshInstanceData>();
			staticAlphaMeshes = new List<MeshInstanceData>();

			Vector4 plane0, plane1, plane2, plane3;
			Utils.Math.GetPlanes(cullMatrix, out plane0, out plane1, out plane2, out plane3);

			var vecX = new System.Numerics.Vector4(plane0.X, plane1.X, plane2.X, plane3.X);
			var vecY = new System.Numerics.Vector4(plane0.Y, plane1.Y, plane2.Y, plane3.Y);
			var vecZ = new System.Numerics.Vector4(plane0.Z, plane1.Z, plane2.Z, plane3.Z);
			var vecW = new System.Numerics.Vector4(plane0.W, plane1.W, plane2.W, plane3.W);

			for(int i = 0; i < _meshCount; i++)
			{
				var sphere = _meshBoundingSpheres[i];

				var result = vecW - new System.Numerics.Vector4(sphere.W);
				result += vecX * sphere.X;
				result += vecY * sphere.Y;
				result += vecZ * sphere.Z;

				if(((result.X < 0 && result.Y < 0 && result.Z < 0 && result.W < 0)) && _meshData[i].Mesh.IsLoaded)
				{
					if(_meshPrimitives[i].Material.IsAlphaTest)
					{
						staticAlphaMeshes.Add(_meshData[i]);
					}
					else
					{
						staticMeshes.Add(_meshData[i]);
					}
				}
			}
		}

		public void GetData(Matrix4 cullMatrix, out List<MeshInstanceData> meshStaticInstances, out List<MeshInstanceData> meshStaticAlphaInstances)
		{
			FrustumCullAndBucket(cullMatrix, out meshStaticInstances, out meshStaticAlphaInstances);
			//meshStaticInstances = new List<MeshInstanceData>(_meshCount);
			//for(int i = 0; i < _meshCount; i++)
			//{
			//	meshStaticInstances.Add(_meshData[i]);
			//}
		}

		public void GetDataCube(Vector3 cubeIncludeCenter, float cubeIncludeSize, Vector3 cubeExcludeCenter, float cubeExcludeSize, out List<MeshInstanceData> instances)
		{
			instances = new List<MeshInstanceData>();

			BoolArray surviving = new BoolArray(_meshCount, false);

			for(int i = 0; i < _meshCount; i++)
			{
				var sphere = _meshBoundingSpheres[i];

				Vector3 inBox = Utils.Math.Abs(sphere.Xyz - cubeIncludeCenter) - new Vector3(cubeIncludeSize + sphere.W);

				if(inBox.X > 0 || inBox.Y > 0 || inBox.Z > 0)
				{
					// Sphere is outside of the box -> cull
					continue;
				}

				inBox = Utils.Math.Abs(sphere.Xyz - cubeExcludeCenter) - new Vector3(cubeExcludeSize - sphere.W);

				if(inBox.X < 0 && inBox.Y < 0 && inBox.Z < 0)
				{
					// Sphere is entirely inside the exclude box
					continue;
				}

				surviving[i] = true;
			}

			for(int i = 0; i < _meshCount; i++)
			{
				if(surviving[i] &&_meshData[i].Mesh.IsLoaded)
				{
					instances.Add(_meshData[i]);
				}
			}
		}
	}
}
