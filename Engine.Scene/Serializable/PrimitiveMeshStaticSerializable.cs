﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Scene.Serializable
{
	[Serializable]
	public class PrimitiveMeshStaticSerializable
	{
		public string MeshFile;
		public string MeshName;
		public string MaterialFile;
		public Vector3 Position;
		public Quaternion Rotation;
		public float Scale;
		public MeshPrimitiveFlags Flags;
	}
}
