﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Scene.Serializable
{
	public class ComponentLodSerializable
	{
		public List<PrimitiveMeshStaticSerializable> Primitives;
	}
}
