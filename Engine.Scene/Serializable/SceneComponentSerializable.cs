﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Scene.Serializable
{
	[Serializable]
	public class SceneComponentSerializable
	{
		public ComponentLodSerializable[] Lods;
		public float[] LodDistances;
	}
}
