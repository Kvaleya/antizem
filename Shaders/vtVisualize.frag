#version 430

layout(location = 0) in vec2 screenPos;
layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D tex0;
layout(binding = 1) uniform sampler2D tex1;

void main() {
	vec2 scale = vec2(0.25);	
	
	vec2 tc = screenPos / scale;
	
	if(tc.x < 0.0 || tc.y < 0.0 || tc.x > 1.0 || tc.y > 1.0)
		discard;
	
	outColor = vec4(0.0);
    outColor = texture(tex0, tc);
}