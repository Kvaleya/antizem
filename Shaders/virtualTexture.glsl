struct VTIndirection {
	ivec2 page;
	ivec2 offset;
	int downsize;
};

float VTGetFloatScale(VTIndirection data)
{
	return 1.0 / float(1 << (data.downsize));
}

// By saving the downsize into the 4 most significant bits and ensuring that the greatest packed downsize value means that the most texels from the page are used, we can now decide which packed indirection is better with a simple max() function, since it prefers the one with more usable texels

VTIndirection UnpackIndirection(uint data)
{
	VTIndirection result;
	result.page = ivec2(
		int((data) & 0x7F),
		int((data >> 7) & 0x7F)
	);
	result.offset = ivec2(
		int((data >> 14) & 0x7F),
		int((data >> 21) & 0x7F)
	);
	result.downsize = int(0xF - (data >> 28));
	
	return result;
}

uint PackIndirection(VTIndirection data)
{
	uint result;
	result = result | uint((data.page.x & 0x7F));
	result = result | uint((data.page.y & 0x7F) << 7);
	result = result | uint((data.offset.x & 0x7F) << 14);
	result = result | uint((data.offset.y & 0x7F) << 21);
	result = result | uint((0xF - (data.downsize & 0xF)) << 28);
	return result;
}

bool IndirectionIsFullSize(uint data)
{
	return data >= 0xF0000000;
	return int(data >> 28) == 0xF;
}

// x y
// z w
uvec4 UpsamleIndirection(uint source, uvec4 target)
{
	uvec4 result = target;
	
	int downsize = int(0xF - (source >> 28));
	
	int halfSize = 64 >> downsize;
	
	uint p = source;
	p = (p & 0x0FFFFFFF) | uint((0xF - min(downsize + 1, 0xF)) << 28);
	
	result.x = p;
	result.y = p + (halfSize << 14);
	result.z = p + (halfSize << 21);
	result.w = p + (halfSize << 21) + (halfSize << 14);
	
	// Always use the upsampled tile, unless the original tile is full size
	if(IndirectionIsFullSize(target.x))
		result.x = target.x;
	if(IndirectionIsFullSize(target.y))
		result.y = target.y;
	if(IndirectionIsFullSize(target.z))
		result.z = target.z;
	if(IndirectionIsFullSize(target.w))
		result.w = target.w;
	
	return result;
}
























