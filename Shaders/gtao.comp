#version 430

layout (binding = 0, r8) uniform restrict writeonly image2D texOutput;

// Ground truth-based ambient occlusion
// Implementation based on:
// Practical Realtime Strategies for Accurate Indirect Occlusion, Siggraph 2016
// Jorge Jimenez, Xianchun Wu, Angelo Pesce, Adrian Jarabo

// Implementation by Jakub Pelc
// 2018-08-11
// Updated
// 2019-10-31
// 2020-01

#define PI 3.1415926535897932384626433832795
#define PI_HALF 1.5707963267948966192313216916398

#define GTAO_RADIUS 0.5 // World space radius
#define GTAO_MAX_SCREENSPACE_RADIUS 0.1 // Decreases AO radius on near surfaces to avoid too large AO screenspace radius
#define GTAO_VALIDITY_DISTANCE_SCALE 0.1 // Controls discarding of samples based on distance from original position
#define GTAO_SAMPLES 3 // Samples taken on one side of the plane, so actual sample count is double this number

// Depth buffer
layout(binding = 0) uniform sampler2D texGbufferDepthLinear;

// vec2(1.0 / sreenWidth, 1.0 / screenHeight)
uniform vec2 screenSizeRcp;

// Used to get vector from camera to pixel
// tan(horizontal fov)
uniform float hfovTan;

// These are offsets that change every frame, results are accumulated using temporal filtering in a separate shader
uniform float angleOffset;
uniform float spacialOffset;

uniform float minMip;

// [Eberly2014] GPGPU Programming for Games and Science
float GTAOFastAcos(float x)
{
	float res = -0.156583 * abs(x) + PI_HALF;
	res *= sqrt(1.0 - abs(x));
	return x >= 0 ? res : PI - res;
}

float IntegrateArc(float h1, float h2, float n)
{
	float cosN = cos(n);
	float sinN = sin(n);
	return 0.25 * (-cos(2.0 * h1 - n) + cosN + 2.0 * h1 * sinN - cos(2.0 * h2 - n) + cosN + 2.0 * h2 * sinN);
}

vec3 Visualize_0_3(float x)
{
	const vec3 color0 = vec3(1.0, 0.0, 0.0);
	const vec3 color1 = vec3(1.0, 1.0, 0.0);
	const vec3 color2 = vec3(0.0, 1.0, 0.0);
	const vec3 color3 = vec3(0.0, 1.0, 1.0);
	vec3 color = mix(color0, color1, clamp(x - 0.0, 0.0, 1.0));
	color = mix(color, color2, clamp(x - 1.0, 0.0, 1.0));
	color = mix(color, color3, clamp(x - 2.0, 0.0, 1.0));
	return color;
}

vec3 GetCameraVec(vec2 uv)
{	
	// Returns the vector from camera to the specified position on the camera plane (uv argument), located one unit away from the camera
	// This vector is not normalized.
	// The nice thing about this setup is that the returned vector from this function can be simply multiplied with the linear depth to get pixel's position relative to camera position.
	// This particular function does not account for camera rotation or position at all (since we don't need it for AO)
	float aspect = screenSizeRcp.x / screenSizeRcp.y;
	return vec3(vec2(uv.x * -2.0 + 1.0, uv.y * 2.0 * aspect - aspect) * hfovTan, 1.0);
}

// Inverse function to GetCameraVec
vec2 GetTcFromPosition(vec3 pos)
{
	float aspect = screenSizeRcp.x / screenSizeRcp.y;
	pos.xy = pos.xy / pos.z / hfovTan;
	return vec2((pos.x - 1.0) * -0.5, (pos.y + aspect) / aspect / 2.0);
}

void SliceSample(vec2 tc_original, vec3 pos, vec3 ray, vec3 v, inout float closest)
{
	vec2 tc = GetTcFromPosition(pos);
	
	if(tc.x < 0.0 || tc.x > 1.0 || tc.y < 0.0 || tc.y > 1.0)
		return;
	
	vec2 tcdiff = abs(tc - tc_original) / screenSizeRcp;
	
	const float maxMip = 7.0;
	const float mipScale = 0.5;
	float mip = (clamp(log2(max(tcdiff.x, tcdiff.y)) * mipScale, minMip, maxMip));

	float depth = textureLod(texGbufferDepthLinear, tc, mip).x;
	// Vector from current pixel to current slice sample
	vec3 toSample = (pos.xyz / pos.z) * depth - ray;
	// Cosine of the horizon angle of the current sample
	float current = dot(v, normalize(toSample));
	
	// Discard samples that are too far from the current position
	float validity = clamp((1.0 - length(toSample) / GTAO_RADIUS * GTAO_VALIDITY_DISTANCE_SCALE), 0.0, 1.0);
	
	if(current > closest)
	{
		closest = mix(closest, current, clamp(validity, 0.0, 1.0));
	}
}

layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main()
{	
	vec2 tc_original = (vec2(gl_GlobalInvocationID.xy) + vec2(0.5)) * screenSizeRcp;
	
	// Depth of the current pixel
	float dhere = textureLod(texGbufferDepthLinear, tc_original, minMip).x;
	// Vector from camera to the current pixel's position
	vec3 ray = GetCameraVec(tc_original) * dhere;
	
	const float normalSampleDist = 1.0;
	
	// Calculate normal from the 4 neighbourhood pixels
	vec2 uv = tc_original + vec2(screenSizeRcp.x * normalSampleDist, 0.0);
	vec3 p1 = ray - GetCameraVec(uv) * textureLod(texGbufferDepthLinear, uv, minMip).x;
	
	uv = tc_original + vec2(0.0, screenSizeRcp.y * normalSampleDist);
	vec3 p2 = ray - GetCameraVec(uv) * textureLod(texGbufferDepthLinear, uv, minMip).x;
	
	vec3 normal = normalize(cross(p1, p2));
	
	// Calculate the distance between samples (direction vector scale) so that the world space AO radius remains constant but also clamp to avoid cache trashing
	vec3 up = normalize(cross(ray, vec3(1.0, 0.0, 0.0)));
	vec3 right = normalize(cross(ray, up));
	
	// Calculate slice direction from pixel's position
	float dirAngle = (PI / 16.0) * (((int(gl_GlobalInvocationID.x) + int(gl_GlobalInvocationID.y) & 3) << 2) + (int(gl_GlobalInvocationID.x) & 3)) + angleOffset;
	vec3 aoDir = (up * sin(dirAngle) + right * cos(dirAngle)) * GTAO_RADIUS / GTAO_SAMPLES;
	
	vec2 tcDifference = abs(GetTcFromPosition(ray + aoDir) - tc_original);
	float maxDifference = max(tcDifference.x, tcDifference.y);
	
	if(maxDifference > GTAO_MAX_SCREENSPACE_RADIUS)
	{
		aoDir *= GTAO_MAX_SCREENSPACE_RADIUS / maxDifference;
	}
	
	// Get the view vector (normalized vector from pixel to camera)
	vec3 v = normalize(-ray);
	
	// Project world space normal to the slice plane
	vec3 toDir = ray + aoDir;
	vec3 planeNormal = normalize(cross(v, -toDir));
	vec3 projectedNormal = normal - planeNormal * dot(normal, planeNormal);
	
	// Calculate angle "n" between view vector and projected normal vector
	vec3 projectedDir = normalize(normalize(toDir) + v);
	float n = GTAOFastAcos(dot(-projectedDir, normalize(projectedNormal))) - PI_HALF;
	
	// Init variables
	float c1 = -1.0;
	float c2 = -1.0;
	
	float localOffset = (0.25 * ((int(gl_GlobalInvocationID.y) - int(gl_GlobalInvocationID.x)) & 3) - 0.375 + spacialOffset);	
	vec3 samples_base_pos = ray + localOffset * aoDir;
	
	// Find horizons of the slice
	for(int i = -GTAO_SAMPLES; i <= -1; i++)
	{
		SliceSample(tc_original, samples_base_pos + aoDir * i, ray, v, c1);
	}
	for(int i = 1; i <= GTAO_SAMPLES; i++)
	{
		SliceSample(tc_original, samples_base_pos + aoDir * i, ray, v, c2);
	}
	
	// Finalize
	float h1a = -GTAOFastAcos(c1);
	float h2a = GTAOFastAcos(c2);
	
	// Clamp horizons to the normal hemisphere
	float h1 = n + max(h1a - n, -PI_HALF);
	float h2 = n + min(h2a - n, PI_HALF);
	
	float visibility = mix(1.0, IntegrateArc(h1, h2, n), length(projectedNormal));
	
	imageStore(texOutput, ivec2(gl_GlobalInvocationID.xy), vec4(visibility));
}