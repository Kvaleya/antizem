// https://gist.github.com/patriciogonzalezvivo/670c22f3966e662d2f83

float mod289(float x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 mod289(vec4 x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 perm(vec4 x){return mod289(((x * 34.0) + 1.0) * x);}

float noise3D(vec3 p){
    vec3 a = floor(p);
    vec3 d = p - a;
    d = d * d * (3.0 - 2.0 * d);

    vec4 b = a.xxyy + vec4(0.0, 1.0, 0.0, 1.0);
    vec4 k1 = perm(b.xyxy);
    vec4 k2 = perm(k1.xyxy + b.zzww);

    vec4 c = k2 + a.zzzz;
    vec4 k3 = perm(c);
    vec4 k4 = perm(c + 1.0);

    vec4 o1 = fract(k3 * (1.0 / 41.0));
    vec4 o2 = fract(k4 * (1.0 / 41.0));

    vec4 o3 = o2 * d.z + o1 * (1.0 - d.z);
    vec2 o4 = o3.yw * d.x + o3.xz * (1.0 - d.x);

    return o4.y * d.y + o4.x * (1.0 - d.y);
}

float noise_cos(float x, float y, float z)
{
	float f = noise3D(vec3(x, y, z));
	return f / cos(f);
}

const vec4 noise_magic = vec4(1111.1111, 3141.5926, 2718.2818, 0);

vec3 GetRandomDir(vec3 seed)
{
	// Adapted from http://lukas-polok.cz/tutorial_sphere.htm
	seed.xy *= noise_magic.xy;
    vec3 skewed_seed = vec3(seed.z * noise_magic.z + seed.y - seed.x) + noise_magic.yzw;

    vec3 dir;
    dir.x = noise_cos(seed.x, seed.y, skewed_seed.x);
    dir.y = noise_cos(seed.y, skewed_seed.y, seed.x);
    dir.z = noise_cos(skewed_seed.z, seed.x, seed.y);

    dir = normalize(dir * 2.0 - 1.0);
	
	return dir;
}
