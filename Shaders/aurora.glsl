#version 430

#define AURORA_PLANET_RADIUS 6378
#define AURORA_LAYER_BOTTOM 100
#define AURORA_LAYER_TOP 300

struct CurtainParams {
	vec3 ColorBottom;
	float Brightness;

	vec3 ColorTop;
	float HeightBottom;

	float HeightCenter;
	float HeightTop;
	float PowerColor;
	float PowerData;

	float PowerHeightBottom;
	float PowerHeightTop;
	
	//float Padding0;
	//float Padding1;
};

// Adapted from https://gist.github.com/wwwtyro/beecc31d65d1004f5a9d
float RaySphereIntersect(vec3 r0, vec3 rd, float sr) {
    // - r0: ray origin
    // - rd: normalized ray direction
    // - s0: sphere center
    // - sr: sphere radius
    // - Returns distance from r0 to first intersecion with sphere,
    //   or -1.0 if no intersection.
    float a = dot(rd, rd);
    vec3 s0_r0 = r0;
    float b = 2.0 * dot(rd, s0_r0);
    float c = dot(s0_r0, s0_r0) - (sr * sr);
    if (b*b - 4.0*a*c < 0.0) {
        return -1.0;
    }
    return (-b + sqrt((b*b) - 4.0*a*c))/(2.0*a);
}

vec3 GetAuroraCurtain(float heightNormalized, float data, CurtainParams params)
{
	heightNormalized = clamp((heightNormalized - params.HeightBottom) / (params.HeightTop - params.HeightBottom), 0.0, 1.0);
	
	float dBottom = clamp(heightNormalized / params.HeightCenter, 0.0, 1.0);
	float dTop = clamp((1.0 - heightNormalized) / (1.0 - params.HeightCenter), 0.0, 1.0);
	
	float a = clamp(pow(dBottom, params.PowerHeightBottom), 0.0, 1.0) * clamp(pow(dTop, params.PowerHeightTop), 0.0, 1.0);
	
	return params.Brightness * a * pow(data, heightNormalized + params.PowerData) * mix(params.ColorBottom, params.ColorTop, clamp(pow(heightNormalized, params.PowerColor), 0.0, 1.0));
}

#define GET_AURORA(i) {\
	params.ColorBottom = PARAM_COLOR_BOTTOM_ ## i;\
	params.Brightness = PARAM_BRIGHTNESS_ ## i;\
	params.ColorTop = PARAM_COLOR_TOP_ ## i;\
	params.HeightBottom = PARAM_HEIGHT_BOTTOM_ ## i;\
	params.HeightCenter = PARAM_HEIGHT_CENTER_ ## i;\
	params.HeightTop = PARAM_HEIGHT_TOP_ ## i;\
	params.PowerColor = PARAM_POWER_COLOR_ ## i;\
	params.PowerData = PARAM_POWER_DATA_ ## i;\
	params.PowerHeightBottom = PARAM_POWER_HEIGHT_BOTTOM_ ## i;\
	params.PowerHeightTop = PARAM_POWER_HEIGHT_TOP_ ## i;\
	color += GetAuroraCurtain(hnorm, data[i], params);\
	}

vec3 AuroraColor(float hnorm, vec4 data)
{
	vec3 color = vec3(0.0);
	
	CurtainParams params;
	
	GET_AURORA(0);
	GET_AURORA(1);
	GET_AURORA(2);
	GET_AURORA(3);
	
	/*
	color += GetAuroraCurtain2(hnorm, data[0], curtains[0]);
	color += GetAuroraCurtain2(hnorm, data[1], curtains[1]);
	color += GetAuroraCurtain2(hnorm, data[2], curtains[2]);
	color += GetAuroraCurtain2(hnorm, data[3], curtains[3]);
	*/

	return color;
}

vec3 AuroraRayMarch(sampler2D auroraTex, vec3 origin, vec3 ray, float rayOffset, int numSamples)
{
	if(ray.y < 0.0)
		return vec3(0.0);

	vec3 originAltitude = origin + vec3(0, AURORA_PLANET_RADIUS, 0);
	float t_enter = RaySphereIntersect(originAltitude, ray, AURORA_PLANET_RADIUS + AURORA_LAYER_BOTTOM);
	float t_leave = RaySphereIntersect(originAltitude, ray, AURORA_PLANET_RADIUS + AURORA_LAYER_TOP);
	
	t_enter = max(t_enter, 0.0);
	t_leave = max(t_leave, 0.0);
	
	vec3 vec_enter = normalize(originAltitude + ray * t_enter);
	vec3 vec_leave = normalize(originAltitude + ray * t_leave);
	
	float t_delta = (t_leave - t_enter) / numSamples;
	
	vec3 accum = vec3(0.0);
	
	for(int i = 0; i < numSamples; i++)
	{
		vec3 planetaryPos = originAltitude + ray * (t_enter + t_delta * i + rayOffset * t_delta);
		float len = length(planetaryPos);
		float height = len - AURORA_PLANET_RADIUS;
		planetaryPos /= len; // normalize
		vec2 coords = planetaryPos.xz / planetaryPos.y;
		
		coords *= 1.2;
		
		coords.xy += 0.5;
		
		coords = fract(coords);
		
		accum += AuroraColor(clamp((height - AURORA_LAYER_BOTTOM) / (AURORA_LAYER_TOP - AURORA_LAYER_BOTTOM), 0.0, 1.0), texture(auroraTex, coords));
	}
	
	accum /= numSamples;
	
	float pixelFade = clamp(ray.y * 5, 0.0, 1.0);
	accum *= pixelFade;
	
	return accum;
}
