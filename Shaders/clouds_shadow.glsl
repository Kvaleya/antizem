float GetCloudShadow(vec3 pos, sampler2D map, mat4 m, float esmConst, float contrast, float sun_y)
{
	vec3 tc = (m * vec4(pos, 1.0)).xyz;
	tc.xyz = tc.xyz * 0.5 + 0.5;
	if(tc.x < 0.0 || tc.x > 1.0 || tc.y < 0.0 || tc.y > 1.0 || tc.z > 1.0)
		return 1.0;
	float s = texture(map, tc.xy).x;
	s = saturate((pow(exp(-tc.z * esmConst) * s, 1) - 0.2) * contrast + 0.2);
	return mix(1.0, s, saturate(sun_y * 100.0));
	//return Gradient(s, tc.z, tc.z + 0.01);
}