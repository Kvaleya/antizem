#version 430

layout(location = 0) in vec3 vPos;
layout(location = 1) flat in int vMaterial;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec2 vTexCoord;
layout(location = 4) in vec3 vTangent;
layout(location = 5) in vec3 vBiTangent;
#ifndef VOXELIZE
layout(location = 0) out vec4 outGbBaseColor;
layout(location = 1) out vec4 outGbNormal;
layout(location = 2) out vec4 outGbMaterial;

layout(early_fragment_tests) in;
#endif

#include materialUbo.glsl
#include gbuffer.glsl

// Virtual texture
#include virtualTextureSampling.glsl

#include common.glsl

#ifdef VOXELIZE
layout (binding = 0, r32ui) uniform uimage3D texBuildOpacity;
uniform vec4 cubePosScale;
uniform int voxelResolution;
uniform int maskShift;
uniform int majorAxis;
//uniform float numSamplesRcp;
layout (binding = 4) uniform sampler2D texAlphaAtlas;

float GetVoxelSize()
{
	return (cubePosScale.w * 2.0) / float(voxelResolution);
}

void WriteVoxel(vec3 pos, float alpha, vec3 color, vec3 normal)
{
	vec3 fVoxelCoords = ((pos - (cubePosScale.xyz - cubePosScale.w)) / cubePosScale.w * 0.5) * voxelResolution;
	ivec3 iVoxelCoords = ivec3(fVoxelCoords);
	
	int vcMin = min(min(iVoxelCoords.x, iVoxelCoords.y), iVoxelCoords.z);
	int vcMax = max(max(iVoxelCoords.x, iVoxelCoords.y), iVoxelCoords.z);
	//float msaa = bitCount(gl_SampleMaskIn[0]) * numSamplesRcp;
	//alpha *= sqrt(msaa);
	if(vcMin >= 0 && vcMax < voxelResolution)
		imageAtomicMax(texBuildOpacity, iVoxelCoords, clamp(uint(alpha * 255), 0u, 255u));
}

#endif

void main() {
	MaterialData material = materials[vMaterial];
	
	#ifdef VOXELIZE
	float alpha = SampleAlpha(material, vTexCoord, texAlphaAtlas, true);
	if(alpha < 0.1)
	{
		return;
	}
	#endif
	vec4 baseColor, normalMaterial;
	
	SampleVT(vTexCoord, material.vtParams, gl_FragCoord.xyz, baseColor, normalMaterial);	
	
	vec3 normal = normalMaterial.xyz;
	
	normal = MaterialGetNormal(normalMaterial, material);	
	normal = normalize(normal);	
	normal.xyz = normalize(mat3(vTangent, vBiTangent, vNormal) * normal.xyz);
	
	vec4 matparams = MaterialGetMaterial(vec4(normalMaterial.z * normalMaterial.z, max(normalMaterial.w * 2.0 - 1.0, 0.0), max(normalMaterial.w * -2.0 + 1.0, 0.0), 0.0), material);
	
	#ifndef VOXELIZE
	if(!gl_FrontFacing)
		normal.xyz *= -1;
	#endif
	
	baseColor = MaterialGetBaseColor(baseColor, material);
	
	#ifdef VOXELIZE
	WriteVoxel(vPos, alpha, baseColor.xyz, normal);
	//WriteVoxel(vPos - vNormal.xyz * GetVoxelSize() * 0.7, alpha, baseColor.xyz, normal);
	
	#else
	PackToGbuffer(baseColor.xyz, normal, matparams, vec4(0.0), outGbBaseColor, outGbNormal, outGbMaterial);
	#endif
}