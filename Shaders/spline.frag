#version 430

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 vPos;
layout(location = 1) in vec2 vSplinePos;

layout(binding = 0) uniform sampler2DArray texQuantity;

uniform vec4 primaryColor;
uniform vec4 secondaryColor;
uniform float arrayLayer;
uniform float texCoordScale;

void main() {
	vec2 pos = vSplinePos * 2.0 - 1.0;
	
	vec3 tc = vec3(0.0, 0.0, arrayLayer);
	
	tc.x = vSplinePos.y * texCoordScale;
	tc.y = vSplinePos.x;
	
	float fade = 1.0;
	fade *= pow(1.0 - abs(pos.x), 2);
	fade *= pow(1.0 - max(abs(pos.y) * 6 - 5, 0), 2);
	
	vec2 quantity = texture(texQuantity, tc).xy;

	vec4 color = quantity.x * primaryColor + quantity.y * secondaryColor;
	
    outColor = color * fade;
}