#version 430

layout(location = 0) out flat ivec3 voxel;

uniform int voxelResolutionPow;

out gl_PerVertex {
    vec4 gl_Position;
};


void main() {
	int mask = ~int(0xffffffffu << voxelResolutionPow);
	voxel = ivec3(0);
	
	int vertex = int(gl_VertexID);
	
	voxel.x = vertex & mask;
	voxel.y = (vertex >> voxelResolutionPow) & mask;
	voxel.z = (vertex >> (voxelResolutionPow * 2)) & mask;
	gl_Position = vec4(0.5);
}