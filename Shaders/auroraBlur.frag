#version 430

layout(location = 0) in vec2 screenPos;
layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D tex;

uniform vec2 texelSize;

//#define NOBLUR

void main() {
	vec3 color = texture(tex, screenPos).rgb;
	
	color += texture(tex, screenPos + vec2(texelSize.x, 0.0)).rgb;
	color += texture(tex, screenPos + vec2(-texelSize.x, 0.0)).rgb;
	color += texture(tex, screenPos + vec2(0.0, texelSize.y)).rgb;
	color += texture(tex, screenPos + vec2(0.0, -texelSize.y)).rgb;
	
    outColor = vec4(color / 5.0, 0.0);
}