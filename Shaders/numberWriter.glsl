
#define DIGITDATA_NULL 0x00000000;
#define DIGITDATA_MINUS 0x0000E000;
#define DIGITDATA_1 0xF4444564;
#define DIGITDATA_2 0xF124899F;
#define DIGITDATA_3 0xF888F88F;
#define DIGITDATA_4 0x8888F999;
#define DIGITDATA_5 0x7988F11F;
#define DIGITDATA_6 0xF999F11F;
#define DIGITDATA_7 0x2224488F;
#define DIGITDATA_8 0xF999F99F;
#define DIGITDATA_9 0xF988F99F;
#define DIGITDATA_0 0xF999999F;

bool WriteDigit(ivec2 coords, int digit)
{
	if(coords.x > 3 || coords.y > 7 || coords.x < 0 || coords.y < 0)
		return false;
	uint digitData = DIGITDATA_NULL;
	if(digit < 0)
		digitData = DIGITDATA_MINUS;
	if(digit == 1)
		digitData = DIGITDATA_1;
	if(digit == 2)
		digitData = DIGITDATA_2;
	if(digit == 3)
		digitData = DIGITDATA_3;
	if(digit == 4)
		digitData = DIGITDATA_4;
	if(digit == 5)
		digitData = DIGITDATA_5;
	if(digit == 6)
		digitData = DIGITDATA_6;
	if(digit == 7)
		digitData = DIGITDATA_7;
	if(digit == 8)
		digitData = DIGITDATA_8;
	if(digit == 9)
		digitData = DIGITDATA_9;
	if(digit == 0)
		digitData = DIGITDATA_0;
	
	int pixelIndex = (coords.x + coords.y * 4);	
	return ((digitData >> pixelIndex) & 1) > 0;
}

bool WriteNumber(ivec2 coords, int number)
{
	bool result = false;
	
	if(number < 0)
	{
		result = result || WriteDigit(coords, -1);
		coords.x -= 5;
		number = -number;
	}
	
	if(number > 99999)
		number = 99999;
	
	int numberDigit = number;
	
	if(number >= 10000)
	{
		int digit = numberDigit / 10000;
		result = result || WriteDigit(coords, digit);
		coords.x -= 5;
		numberDigit -= digit * 10000;
	}
	if(number >= 1000)
	{
		int digit = numberDigit / 1000;
		result = result || WriteDigit(coords, digit);
		coords.x -= 5;
		numberDigit -= digit * 1000;
	}
	if(number >= 100)
	{
		int digit = numberDigit / 100;
		result = result || WriteDigit(coords, digit);
		coords.x -= 5;
		numberDigit -= digit * 100;
	}
	if(number >= 10)
	{
		int digit = numberDigit / 10;
		result = result || WriteDigit(coords, digit);
		coords.x -= 5;
		numberDigit -= digit * 10;
	}
	int digit = numberDigit;
	result = result || WriteDigit(coords, digit);
	coords.x -= 5;
	numberDigit -= digit;
	
	return result;
}

vec4 WriteNumber(ivec2 coords, int number, vec4 colorBg, vec4 colorNum)
{
	return (WriteNumber(coords, number) ? colorNum : colorBg);
}