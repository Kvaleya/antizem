
#ifndef NUM_VOXEL_CLIPMAPS
	#define NUM_VOXEL_CLIPMAPS 6
#endif
#ifndef VOXEL_CLIPMAP_RES
	#define VOXEL_CLIPMAP_RES 64
#endif

// XYZ: cube center, W: cube half size
uniform vec4 voxelClipmapParams[NUM_VOXEL_CLIPMAPS];

//    3--------7
//   /|       /|
//  2--------6 |
//  | |      | |   Y      Z
//  | 1------|-5  /\      >
//  |/       |/   |      /
//  0--------4    --> X /
// 0,0,0
// Returns a directional voxel opacity value using values of 2x2x2 cube of smaller voxels
vec4 VoxelFilter(vec4 voxels[8])
{
	vec3 opacity = vec3(0.0);
	
	#define GETX(a) (a.x)
	#define GETY(a) (a.y)
	#define GETZ(a) (a.z)
	
	//#define GETX(a) (a)
	//#define GETY(a) (a)
	//#define GETZ(a) (a)
	
	// For each of (x,y,z), the directional occlusion value is how much rays are blocked when looking at the 2x2x2 cube from that direction
	opacity.x += max(GETX(voxels[0]), GETX(voxels[4])); // The indices 0 and 4 lie behind one another when looking from X, thus use max func to combine their opacities
	opacity.x += max(GETX(voxels[1]), GETX(voxels[5]));
	opacity.x += max(GETX(voxels[2]), GETX(voxels[6]));
	opacity.x += max(GETX(voxels[3]), GETX(voxels[7]));
	
	opacity.y += max(GETY(voxels[0]), GETY(voxels[2]));
	opacity.y += max(GETY(voxels[1]), GETY(voxels[3]));
	opacity.y += max(GETY(voxels[4]), GETY(voxels[6]));
	opacity.y += max(GETY(voxels[5]), GETY(voxels[7]));
	
	opacity.z += max(GETZ(voxels[2]), GETZ(voxels[3]));
	opacity.z += max(GETZ(voxels[0]), GETZ(voxels[1]));
	opacity.z += max(GETZ(voxels[4]), GETZ(voxels[5]));
	opacity.z += max(GETZ(voxels[6]), GETZ(voxels[7]));
	
	opacity *= 0.25;
	
	//opacity = clamp(opacity * 1.5, 0.0, 1.0);
	
	return vec4(opacity.xyz, 0.0);
}

// Get texture coordinates of a position relative to a certain clipmap level
// Returned W component is validity (sample outside of clipmap is not valid)
vec4 GetVoxelClipmapTexCoord(vec3 pos, int clipmap)
{
	const float clampMin = 0.5 / VOXEL_CLIPMAP_RES;
	const float clampMax = 1.0 - clampMin;

	const float edgeStart = 0.7;
	const float edgeSize = 0.2;
	
	if(clipmap < 0 || clipmap >= NUM_VOXEL_CLIPMAPS)
		return vec4(0.0);
	
	vec3 tc = (pos - voxelClipmapParams[clipmap].xyz) / voxelClipmapParams[clipmap].w;
	
	float far = length(tc);
	float fade = clamp((far - edgeStart) / edgeSize, 0.0, 1.0);
	
	return vec4(clamp(tc * 0.5 + 0.5, clampMin, clampMax), 1.0 - fade);
}

// Get texture coordinates of a certain world space position in voxel clipmaps
// Returns tc relative to the clipmap, clipmap index, mipmapping lod and validity
void GetVoxelTexCoord(vec3 pos, float minLod, int minClipmap, out vec3 tc, out int clipmap, out float lod, out float validity)
{
	tc = vec3(0.0);
	clipmap = NUM_VOXEL_CLIPMAPS - 1;
	validity = 0.0;
	
	for(int i = minClipmap; i < NUM_VOXEL_CLIPMAPS; i++)
	{		
		vec4 tc0 = GetVoxelClipmapTexCoord(pos, i);
		
		if(tc0.w > 0.0)
		{
			tc = tc0.xyz;
			validity = tc0.w;
			clipmap = i;
			break;
		}
	}
	
	// Fade to larger clipmap level at edges of smaller clipmap
	lod = max(minLod, clipmap + 1.0 - validity);
	
	// All samples that fall into the second-largest clipmap are valid, only samples that are near the edge of the largest clipmap can be invalid
	if(clipmap < NUM_VOXEL_CLIPMAPS - 1)
	{
		validity = 1.0;
	}
}

// Returns size of voxels at a specified world space position
// At transitions from smaller clipmap to larger, voxel sizes of either are returned based on pixelProbability, which should be a random value
float GetVoxelSize(vec3 pos, float minLod, float pixelProbability)
{
	vec3 tc;
	int clipmap;
	float lod, validity;
	
	GetVoxelTexCoord(pos, minLod, 0, tc, clipmap, lod, validity);
	
	float size = voxelClipmapParams[0].w * 2.0 / VOXEL_CLIPMAP_RES;
	size *= pow(2.0, floor(lod));
	
	if(pixelProbability < fract(lod))
		size *= 2.0;
	
	return size;
}

const float voxelDefaultPixelProb = 0.5;

float GetVoxelSize(vec3 pos, float minLod)
{
	return GetVoxelSize(pos, minLod, voxelDefaultPixelProb);
}

// Samples the voxel clipmap at a specified world space position
vec4 SampleVoxels(vec3 pos, float minLod, sampler3D voxelSampler, int minClipmap, out float lod, float pixelProbability)
{
	vec3 tc;
	int clipmap;
	float validity;
	
	GetVoxelTexCoord(pos, minLod, minClipmap, tc, clipmap, lod, validity);
	
	tc.x /= NUM_VOXEL_CLIPMAPS;
	tc.x += float(clipmap) / NUM_VOXEL_CLIPMAPS;
	
	vec3 dataInvalid = vec3(0.0);
	vec4 data = vec4(0.0);
	
	float mip = floor(lod - clipmap);
	
	if(pixelProbability < fract(lod))
		mip += 1.0;
	
	if(validity > 0.0)
		data += textureLod(voxelSampler, tc, mip);
	
	data.w = validity;
	data.xyz = mix(data.xyz, dataInvalid, 1.0 - validity);
	
	return data;
}

vec4 SampleVoxels(vec3 pos, float minLod, sampler3D voxelSampler, int minClipmap, out float lod)
{
	return SampleVoxels(pos, minLod, voxelSampler, minClipmap, lod, voxelDefaultPixelProb);
}
