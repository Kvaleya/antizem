#version 430

layout (points) in;
layout (triangle_strip, max_vertices = 14) out;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) flat in ivec3 voxelArray[];

#define voxel voxelArray[0]

layout(location = 1) out vec3 normal;
layout(location = 2) out vec4 color;
layout(location = 3) out vec3 outpos;

layout(binding = 0) uniform sampler3D texVoxelOpacity;

uniform vec4 voxelClipmapParams;
uniform int voxelResolutionPow;
uniform int voxelReadOffset;
uniform mat4 projection;

void main()
{
	vec4 voxelData = texelFetch(texVoxelOpacity, voxel + ivec3(voxelReadOffset, 0, 0), 0);
	
	int res = (1 << voxelResolutionPow);
	vec3 voxelCenter = ((voxel + vec3(0.5)) / res) * voxelClipmapParams.w * 2.0 + (voxelClipmapParams.xyz - voxelClipmapParams.w);
	float voxelHalfSize = voxelClipmapParams.w / res;
	
	if(length(voxelData) < 0.01)
		return;
	
	// Adapted from https://www.gamedev.net/forums/topic/674733-vertex-to-cube-using-geometry-shader/
	for(int i = 0; i < 14; i++)
	{
		int b = 1 << i;
		bvec3 vertex;
		vertex.x = (0x287a & b) != 0;
		vertex.y = (0x02af & b) != 0;
		vertex.z = (0x31e3 & b) != 0;
		
		vec4 pos = vec4(voxelCenter, 1.0);
		
		for(int j = 0; j < 3; j++)
			pos[j] += vertex[j] ? voxelHalfSize : -voxelHalfSize;
		
		color = voxelData;
		normal = vec3(0.0);
		outpos = pos.xyz;
		gl_Position = projection * pos;
		EmitVertex();
	}
	
	EndPrimitive();	
}
