struct ClusterData {
	int InstanceIndexDrawIndexPacked;
	int BaseVertex;
	int FirstIndex;
	int WriteDest;
	int ClusterIndex;
};

struct ClusterDataUnpacked {
	int InstanceIndex;
	int DrawIndex;
	int BaseVertex;
	int FirstIndex;
	int WriteDest;
	int ClusterIndex;
	int TriangleCount;
	int ClusterBucketIndex;
	bvec4 CullFlags; // Also used for cluster validity
};
// TODO: use this
ClusterData SetCullFlags(ClusterData data, bvec4 flags)
{
	ClusterData result = data;
	uint bits = 0;
	for(int i = 0; i < 4; i++)
	{
		if(flags[i])
			bits = bits | (1 << (28 + i));
	}
	result.BaseVertex = int((data.BaseVertex & 0x0fffffff) | bits);
	return result;
}

void UnpackClusterData(in ClusterData data, out ClusterDataUnpacked unpacked)
{
	unpacked.InstanceIndex = data.InstanceIndexDrawIndexPacked & 0xffff;
	unpacked.DrawIndex = (data.InstanceIndexDrawIndexPacked >> 16) & 0xffff;
	unpacked.BaseVertex = data.BaseVertex & 0x7fffffff;
	unpacked.FirstIndex = data.FirstIndex;
	unpacked.WriteDest = data.WriteDest & 0x00ffffff;
	unpacked.ClusterIndex = data.ClusterIndex & 0x00ffffff;
	unpacked.TriangleCount = ((data.WriteDest >> 24) & 0xff) + 1;
	unpacked.ClusterBucketIndex = (data.ClusterIndex >> 24) & 0xff;
	//unpacked.Valid = (uint(data.BaseVertex) & 0x80000000) == 0;
	unpacked.CullFlags.x = (uint(data.BaseVertex) & 0x10000000) > 0;
	unpacked.CullFlags.y = (uint(data.BaseVertex) & 0x20000000) > 0;
	unpacked.CullFlags.z = (uint(data.BaseVertex) & 0x40000000) > 0;
	unpacked.CullFlags.w = (uint(data.BaseVertex) & 0x80000000) > 0;
}

/*
For Renderdoc
{
    ushort InstanceIndex;
	ushort DrawIndex;
	int BaseVertex;
	int FirstIndex;
	xint TriangleCountWriteDest;
	xint ClusterBucketIndexClusterIndex;
}
*/