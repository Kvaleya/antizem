#version 430

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec4 vPosition;
layout(location = 1) in vec2 vTexCoord;
layout(location = 2) in vec4 vColor;

uniform sampler2D tex;

//#define MSAA4 1
//#define MSAA8 1
#define MSAA16 1

void main() {
	const float alphaTest = vPosition.w;
	
	float textSample = 0.0;
	
	vec2 texDerivs = vec2(dFdx(vTexCoord.x), dFdy(vTexCoord.y));
	#ifdef MSAA16
	const float bias = -1.0;
	vec2 td = texDerivs * 0.0625;
	
	textSample += (texture(tex, vTexCoord + vec2(1, 1) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-1, -3) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-3, 2) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(4, -1) * td, bias).r > alphaTest ? 1.0 : 0.0);
	
	textSample += (texture(tex, vTexCoord + vec2(-5, -2) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(2, 5) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(5, 3) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(3, -5) * td, bias).r > alphaTest ? 1.0 : 0.0);
	
	textSample += (texture(tex, vTexCoord + vec2(-2, 6) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(0, -7) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-4, -6) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-6, 4) * td, bias).r > alphaTest ? 1.0 : 0.0);
	
	textSample += (texture(tex, vTexCoord + vec2(-8, 0) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(7, -4) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(6, 7) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-7, -8) * td, bias).r > alphaTest ? 1.0 : 0.0);
	
	textSample *= 0.0625;
	#else
	#ifdef MSAA8
	const float bias = -1.0;
	vec2 td = texDerivs * 0.0625;
	
	textSample += (texture(tex, vTexCoord + vec2(1, -3) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-1, 3) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(5, 1) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-3, 5) * td, bias).r > alphaTest ? 1.0 : 0.0);
	
	textSample += (texture(tex, vTexCoord + vec2(-5, 5) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-7, -1) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(3, 7) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(7, -7) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample *= 0.125;
	#else
	#ifdef MSAA4
	const float bias = -1.0;
	vec2 td = texDerivs * 0.0625;
	
	textSample += (texture(tex, vTexCoord + vec2(-2, -6) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(6, -2) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(-6, 2) * td, bias).r > alphaTest ? 1.0 : 0.0);
	textSample += (texture(tex, vTexCoord + vec2(2, 6) * td, bias).r > alphaTest ? 1.0 : 0.0);
	
	textSample *= 0.25;
	#else
	textSample = (texture(tex, vTexCoord, 0.0).r > alphaTest ? 1.0 : 0.0);
	#endif
	#endif
	#endif
	
    outColor = vec4(vColor.rgb, vColor.a * textSample);
}