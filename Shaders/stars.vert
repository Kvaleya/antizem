#version 430

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexTexCoord;
layout(location = 2) in vec3 vertexColor;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(binding = 0) uniform sampler2D transmittanceLUT;

vec3 GetExtincion(vec3 v)
{
	vec3 transmittance = texture(transmittanceLUT, vec2(0.0, v.y * 0.5 + 0.5)).xyz;
	return exp(-transmittance);
}

uniform mat4 world;
uniform mat4 camera;
uniform mat4 projection;

layout(location = 0) out vec2 vTexCoord;
layout(location = 1) out vec3 vColor;

void main() {
	vTexCoord = vertexTexCoord;
	vec3 pos = (world * vec4(vertexPosition, 0.0)).xyz;
	vColor = vertexColor * GetExtincion(pos);
    gl_Position = projection * vec4((camera * vec4(pos, 0.0)).xyz, 1.0);
}