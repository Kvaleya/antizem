// Sources:
// Moving Frostbite to PBR
// Real shading in Unreal Engine 4
// https://learnopengl.com/#!PBR/Lighting
// http://simonstechblog.blogspot.cz/2011/12/microfacet-brdf.html

#include common.glsl

float pow5(float v)
{
	float v2 = v * v;
	return v2 * v2 * v;
}

vec3 FresnelSchlick(float u, vec3 f0)
{
	return f0 + (1.0 - f0) * pow5(1.0 - u);
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    return NdotV / (NdotV * (1.0 - k) + k);
}

float GeometrySmith(float NdotV, float NdotL, float roughness)
{
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
    return ggx1 * ggx2;
}

vec3 FresnelSchlickRoughness(float u, vec3 f0, float roughness)
{
    return f0 + (max(vec3(1.0 - roughness), f0) - f0) * pow(1.0 - u, 5.0);
}

float DistributionGGX(float NdotH, float roughness)
{
    float a2 = roughness * roughness;	
    float denom = (NdotH * NdotH * (a2 - 1.0) + 1.0);
    denom = KV_PI * denom * denom;
    return a2 / denom;
}

vec3 GetLightSubsurface(vec3 diffuse, vec3 n, float subsurface, const vec3 l)
{
	return vec3(0.0);
	float NdotL = clamp(dot(-n.xyz, l.xyz), 0.000001, 1.0);
	return exp(diffuse * -(1 / NdotL) * subsurface);
}

vec3 DiffuseSpecularBRDF(vec3 albedo, vec3 f0, float roughness, float metallic, float subsurface, vec3 n, vec3 v, vec3 l, vec3 illuminance, float NdotLDiffuse, float NdotLSpecular)
{
	vec3 h = normalize(l + v);
	float HdotV = max(dot(v, h), 0.0);
	float NdotH = max(dot(n.xyz, h), 0.0);
	float NdotV = max(dot(n, v), 0);
	float HdotL = max(dot(h, l), 0.0);
	vec3 f = FresnelSchlick(HdotL, f0);
	float ndf = DistributionGGX(NdotH, roughness);
	float g = GeometrySmith(NdotV, NdotLSpecular, roughness);
	vec3 specularLight = (g * ndf * f) / (4.0 * NdotV * NdotLSpecular + 0.001);
	vec3 kD = (vec3(1.0) - FresnelSchlick(NdotLDiffuse, f0)) * min(1.0 - metallic, 1.0);
	return (kD * (albedo + GetLightSubsurface(albedo, n, subsurface, l)) + specularLight) * illuminance;
}

const float SunAngularRadiusSin = 0.00475600428170141385448614912307;
const float SunAngularRadiusCos = 0.99998869014767983915730076679793;

// sunIlluminance - in lux
vec3 GetSunLight(vec3 albedo, vec3 f0, float roughness, float metallic, float subsurface, vec3 n, vec3 v, vec3 dir, vec3 sunIlluminance)
{
	vec3 r = reflect(-v, n.xyz);
	float DdotR = dot(dir, r);
	vec3 s = r - DdotR * dir;
	vec3 l = DdotR < SunAngularRadiusCos ? normalize(SunAngularRadiusCos * dir + normalize(s) * SunAngularRadiusSin) : r;
	float NdotLDiffuse = max(dot(n.xyz, dir), 0.0);
	vec3 illuminance = sunIlluminance * NdotLDiffuse;
	return DiffuseSpecularBRDF(albedo, f0, roughness, metallic, subsurface, n, v, l, illuminance, NdotLDiffuse, max(dot(n.xyz, l), 0.0));
}

#define POINTLIGHT_RADIUS 0.01
// luminuosIntensity - in candelas
vec3 GetPointLight(vec3 albedo, vec3 f0, float roughness, float metallic, float subsurface, vec3 n, vec3 v, vec3 lightPos, vec3 worldPos, vec3 luminuosIntensity)
{
	vec3 l = lightPos - worldPos;
	float dirLength = length(l.xyz);
	l.xyz /= dirLength;
	float NdotLDiffuse = max(dot(n.xyz, l.xyz), 0.0);
	vec3 illuminance = luminuosIntensity * NdotLDiffuse / max(dirLength * dirLength, POINTLIGHT_RADIUS * POINTLIGHT_RADIUS);
	return DiffuseSpecularBRDF(albedo, f0, roughness, metallic, subsurface, n, v, l, illuminance, NdotLDiffuse, NdotLDiffuse);
}

vec3 ImportanceSampleGGX(vec2 Xi, vec3 normal, float roughness)
{
	float a = roughness*roughness;

	float phi = 2.0 * KV_PI * Xi.x;
	float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
	float sinTheta = sqrt(1.0 - cosTheta*cosTheta);

	// from spherical coordinates to cartesian coordinates
	vec3 H;
	H.x = cos(phi) * sinTheta;
	H.y = sin(phi) * sinTheta;
	H.z = cosTheta;

	// from tangent-space vector to world-space sample vector
	vec3 up        = abs(normal.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
	vec3 tangent   = normalize(cross(up, normal));
	vec3 bitangent = cross(normal, tangent);

	vec3 sampleVec = tangent * H.x + bitangent * H.y + normal * H.z;
	return normalize(sampleVec);
}
