#version 430

#include common.glsl
#include cloud_noises.glsl

layout(binding = 0, rgba8) uniform restrict writeonly image2D texOut;
layout(binding = 0) uniform sampler2D texRandom;

uniform ivec2 offsetXY;
uniform vec2 sizercp;

const float num_pi = 3.141592653589793;
const float num_e = 2.718281828459045;
const float num_log2 = 0.693147180559;

vec4 fbm4(vec3 st, int scale, float randomnessOffset)
{
	return vec4(
		fbm(st, scale, randomnessOffset + 2.5411147),
		fbm(st, scale, randomnessOffset + 4.78114),
		fbm(st, scale, randomnessOffset + 7.987321),
		fbm(st, scale, randomnessOffset + 9.7863123)
	) * 2.0 - 1.0;
}

vec4 fn(vec2 st)
{
	const int scale = 8;
	return fbm4(vec3(st, 0.0), scale, 0.0);
}

layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main(void)
{
	ivec2 localCoords = ivec2(gl_GlobalInvocationID.xy + offsetXY);
	vec2 fpos = (localCoords + 0.5) * sizercp;
	vec4 color = vec4(0.0);
	/*
	color.x = fbm(fpos + vec2(2.87547, 3.14785), 16, 0.5, 2.0);
	color.y = fbm(fpos + vec2(2.87547, 3.14785) + 
		vec2(
		fbm(fpos + vec2(4.4587, 6.9887), 8, 0.5, 2.0),
		fbm(fpos + vec2(9.69862, 7.12387969), 8, 0.5, 2.0)
		) * 0.2, 32, 0.5, 3.0);
	color.x = color.x - color.y * 0.5;
	*/
	/*
	float n1 = fbm(fpos + vec2(num_pi, num_e), 32, 0.5, 2.0, ivec2(0, 3));
	float n2 = fbm(fpos + vec2(num_log2, num_e), 32, 0.5, 2.0, ivec2(256, 5));
	float n3 = fbm(fpos + vec2(num_pi, num_log2), 32, 0.5, 2.0, ivec2(512, 7));
	float n4 = fbm(fpos + vec2(num_e, num_pi), 32, 0.5, 2.0, ivec2(768, 300));
	float n5 = fbm(fpos + vec2(num_log2, num_pi), 32, 0.5, 2.0, ivec2(0, 312));
	float n6 = fbm(fpos + vec2(num_e, num_log2), 32, 0.5, 2.0, ivec2(256, 345));
	
	color.x = fbm(fpos + vec2(num_e, num_pi) + (vec2(n1, n2) * 2.0 - 1.0) * 0.02, 16, 0.5, 2.0, ivec2(12, 512));
	color.y = fbm(fpos + vec2(num_pi, num_log2) + (vec2(n3, n4) * 2.0 - 1.0) * 0.02, 16, 0.5, 2.0, ivec2(276, 512));
	color.z = fbm(fpos + vec2(num_log2, num_e) + (vec2(n5, n6) * 2.0 - 1.0) * 0.02, 16, 0.5, 2.0, ivec2(413, 512));
	*/
	
	const float of1 = 0.07;
	const float of2 = 0.2;
	
	color = fn(fpos + fn(fpos + fn(fpos).xy * of1).xy * of2) * 0.5 + 0.5;
	
	imageStore(texOut, localCoords, color);
}
