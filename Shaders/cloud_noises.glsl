// Adapted from The Book of Shaders
// https://thebookofshaders.com/13/
// Based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd

vec3 random3(vec3 st)
{
    vec3 s = vec3(
		dot(st, vec3(12.3456, 34.1415, 18.544)),
		dot(st, vec3(39.1144, 78.4563, 22.5366)),
        dot(st, vec3(42.2154, 15.2854, 32.178)));
    return fract(sin(s) * 45678.9) * 2.0 - 1.0;
}

float gradientNoise(in vec3 st, int tiling, float randomnessOffset)
{
    ivec3 cell = ivec3(floor(st));
    vec3 f = fract(st);
	
	#define RND2(a) random3(vec3((a) & (tiling - 1)) + vec3(randomnessOffset, 0.0, 0.0))
    
    vec3 s000 = RND2(cell);
    vec3 s001 = RND2(cell + ivec3(0, 0, 1));
    vec3 s010 = RND2(cell + ivec3(0, 1, 0));
    vec3 s011 = RND2(cell + ivec3(0, 1, 1));
	
	vec3 s100 = RND2(cell + ivec3(1, 0, 0));
	vec3 s101 = RND2(cell + ivec3(1, 0, 1));
    vec3 s110 = RND2(cell + ivec3(1, 1, 0));
    vec3 s111 = RND2(cell + ivec3(1, 1, 1));
    
    float d000 = dot(s000, f);
    float d001 = dot(s001, f - vec3(0, 0, 1));
    float d010 = dot(s010, f - vec3(0, 1, 0));
    float d011 = dot(s011, f - vec3(0, 1, 1));
	float d100 = dot(s100, f - vec3(1, 0, 0));
    float d101 = dot(s101, f - vec3(1, 0, 1));
    float d110 = dot(s110, f - vec3(1, 1, 0));
    float d111 = dot(s111, f - vec3(1, 1, 1));
    
    vec3 u = smoothstep(0.0, 1.0, f);
    
    float noise = mix(
		mix(mix(d000, d100, u.x), mix(d010, d110, u.x), u.y),
		mix(mix(d001, d101, u.x), mix(d011, d111, u.x), u.y),
		u.z);
    return clamp(noise * 0.7 + 0.5, 0.0, 1.0);
}

float gradientNoise(in vec3 st, int tiling)
{
	return gradientNoise(st, tiling, 0.0);
}

// Generates fbm that tiles range 0..1
float fbm(in vec3 st, int topLevelDetail, float randomnessOffset, int octaves)
{
	int texSize = topLevelDetail;
	
	// Initial values
	float value = 0.0;
	float amplitude = 0.5;
	float frequency = 0.;
	st *= texSize;
	// Loop of octaves
	for (int i = 0; i < octaves; i++) {
		value += amplitude * gradientNoise(st, texSize, randomnessOffset);
		texSize *= 2;
		st *= 2.;
		amplitude *= .5;
	}
	return value;
}

float fbm(in vec3 st, int topLevelDetail, float randomnessOffset)
{
	return fbm(st, topLevelDetail, randomnessOffset, 8);
}

float fbm(in vec3 st, int topLevelDetail)
{
	return fbm(st, topLevelDetail, 0.0);
}