#version 430

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D tex;

layout(location = 0) in vec2 screenPos;

void main()
{
	outColor = texture(tex, screenPos);
}
