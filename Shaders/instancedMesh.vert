#version 430

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoord;
layout(location = 3) in vec3 vertexTangent;
// Per instance
layout(location = 6) in vec4 WorldRight;
layout(location = 7) in vec4 WorldUp;
layout(location = 8) in vec4 WorldForward;
layout(location = 9) in ivec4 InstanceAttributes;

#include meshStruct.glsl
#include common.glsl

layout(binding = 0, std430) restrict readonly buffer bufferMeshData
{
    MeshData meshDataArray[];
};

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 3) out vec2 vTexCoord;
layout(location = 1) flat out int vMaterial;

#ifndef DEPTH_PREPASS
layout(location = 0) out vec3 vPos;
layout(location = 2) out vec3 vNormal;
layout(location = 4) out vec3 vTangent;
layout(location = 5) out vec3 vBiTangent;
#endif

uniform mat4 projection;

void main() {
	vMaterial = InstanceAttributes.y;
	
	#ifndef DEPTH_PREPASS
	
	vec3 n = safeNormalize(vec3(dot(vertexNormal, WorldRight.xyz), dot(vertexNormal, WorldUp.xyz), dot(vertexNormal, WorldForward.xyz)));
	vec3 t = safeNormalize(vec3(dot(vertexTangent, WorldRight.xyz), dot(vertexTangent, WorldUp.xyz), dot(vertexTangent, WorldForward.xyz)));
	t = safeNormalize(t - dot(t, n) * n);
	vTangent = t;
	vNormal = n;
	vBiTangent = safeNormalize(cross(t, n));
	#endif
	
	vec3 pos = vertexPosition;
	MeshData data = meshDataArray[InstanceAttributes.x];
	pos = pos * data.positionScale.xyz + data.positionOffset.xyz;	
	
	vec4 p4 = vec4(pos, 1.0);
	pos = vec3(dot(p4, WorldRight), dot(p4, WorldUp), dot(p4, WorldForward));
	
	vTexCoord = data.texCoordOffsetScale.xy + vertexTexCoord * data.texCoordOffsetScale.zw;
	vTexCoord.y = 1.0 - vTexCoord.y;
	
	#ifndef DEPTH_PREPASS
	vPos = pos;	
	#endif
	
    gl_Position = projection * vec4(pos, 1.0);
	
	#ifdef VOXELIZE
		gl_Position.z = 0.5;
	#endif
}