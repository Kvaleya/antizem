#version 430

#include common.glsl

uniform ivec2 cloud_coverage_size;
uniform vec2 cloud_camera_pos_coverage;
uniform vec3 cloud_camera_pos_noise3D;
uniform float cloud_camera_pos_y;

// PBR cloud stuff

// NUBIS slide 66, Siggraph 2017
float BeerMultiScattering(float dens)
{
	return max(
		exp(-dens),
		exp(-dens * 0.25) * 0.7
	);
}

float Powder(float dens)
{
	return saturate(1.0 - exp(-dens * 1.0));
}

float Beer(float dens)
{
	return exp(-dens);
}

float BeerPowder(float dens, float ldotv)
{
	float beer = BeerMultiScattering(dens);
	float powder = Powder(dens);
	return mix(beer, beer * powder, saturate((-ldotv)));
}

#define ONEDIV4PI 0.07957747154594766788444188168626

float HenyeyGreensteinPhaseFunction(float cosA, float g)
{
	float g2 = g * g;
	float temp = ONEDIV4PI * ( (1.0 - g2) / pow(1.0 + g2 - 2 * g * cosA, 1.5) );
	return clamp(temp, 0.0, 1.0) * 0.75 + 0.25;
}

float PhaseFuncComposite(float cosA)
{
	// Physically Based Sky, Atmosphere and Cloud Rendering in Frostbite
	// See slide 34
	const float g0 = 0.8;
	const float g1 = -0.5;
	const float alpha = 0.5;
	float f0 = HenyeyGreensteinPhaseFunction(cosA, g0);
	float f1 = HenyeyGreensteinPhaseFunction(cosA, g1);
	return mix(f0, f1, alpha);
}

float GetHeightCurved(vec3 pos)
{
	const float earthRadius = 6378000.0;
	vec3 fromEarthCenter = pos + vec3(0.0, earthRadius, 0.0);
	return length(fromEarthCenter) - earthRadius;
}

float FogDensity(vec3 pos, vec4 coverage)
{
	float height = GetHeightCurved(pos);
	float rainfog = coverage.y; // Rainfog causes some wavefront-shaped bugs on NV???
	return pow(Gradient(height, 2200.0, 0.0), 2) * (rainfog * 50.0 + 1.0) * 0.00002;
}

const float cloudLow = 1024.0;
const float cloudHigh = 6000.0;

vec4 cloudSampleCoverage(sampler2D tCoverage, vec2 uv)
{
	uv = cloud_camera_pos_coverage + uv / e_scale_coverage;
	
	return texture(tCoverage, uv);
}

// Camera is always at (0,0,0)
float CloudDensity(vec3 pos, float timeEllapsed, sampler3D tNoise3d, sampler2D tNoise2d, sampler2D tCoverage, out vec4 coverageData, out bool detailed)
{
	detailed = false;

	const float epsilon = 0.0000000001;

	float height = GetHeightCurved(pos);

	coverageData = cloudSampleCoverage(tCoverage, pos.xz);
	
	if(coverageData.x < epsilon)
		return 0.0;
	
	const float weather = 0.35;
	
	float lowerEdge = mix(1400.0, 1100.0, sqrt(saturate(coverageData.x * 8.0)));
	float upperEdge = mix(1200.0, 5900.0, sqrt(coverageData.x));
	
	float density = max(min(height - lowerEdge, upperEdge - height), 0.0);
	density = saturate(min(density * 0.001, coverageData.x * 2.0));
	
	if(density < epsilon)
		return 0.0;
	
	vec4 noise2d = texture(tNoise2d, fract(pos.xz * 0.001));
	
	vec3 offset3d = (noise2d.xyz * 2.0 - 1.0) * 0.05;
	
	vec2 noise3d = textureLod(tNoise3d, cloud_camera_pos_noise3D + pos / e_scale_noise3d + offset3d, 0.0).xy;

	float bumps = noise3d.y * noise3d.y * 0.4;
	float wisps = noise3d.x * 0.7;
	
	float shapeRemap = mix(bumps, wisps, coverageData.z);
	
	density = RemapSat(density, shapeRemap, 1.0, 0.0, 1.0);
	
	if(density > epsilon)
		detailed = true;
	
	return density * 1.0;
}


























