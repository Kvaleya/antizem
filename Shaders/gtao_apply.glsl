float GtaoEvalLine(float depth, vec2 line)
{
	float result = line.x * depth + line.y;
	if(isnan(result) || isinf(result))
		return 1.0;
	return clamp(result, 0.0, 1.0);
}
