#include common.glsl

void PackToGbuffer(vec3 baseColor, vec3 normal, vec4 material, vec4 glow, out vec4 gbBaseColor, out vec4 gbNormal, out vec4 gbMaterial)
{
	gbBaseColor = vec4(0.0);
	gbNormal = vec4(0.0);
	gbMaterial = vec4(0.0);
	
	gbBaseColor.rgb = baseColor;
	gbBaseColor.a = material.y * 0.5 + 0.5 - material.z * 0.5; // Metallic/subsurface
	gbNormal.rgb = normal * 0.5 + 0.5;	
	gbMaterial.x = material.x; // Roughness
}

void UnpackGbuffer(vec4 gbBaseColor, vec4 gbNormal, vec4 gbMaterial, out vec3 albedo, out vec3 normal, out vec3 f0, out float roughness, out float metallic, out float subsurface)
{
	const float roughnessEpsilon = 0.01;

	vec3 baseColor = pow3(gbBaseColor.rgb, 2.2);
	metallic = max(gbBaseColor.a * 2.0 - 1.0, 0.0);
	normal = normalize(gbNormal.xyz * 2.0 - 1.0);
	roughness = gbMaterial.x * gbMaterial.x * (1.0 - roughnessEpsilon) + roughnessEpsilon;
	f0 = max(mix(vec3(0.02), baseColor, metallic), 0.01);
	albedo = baseColor * (1.0 - metallic);
	subsurface = max(gbBaseColor.a * -2.0 + 1.0, 0.0);
}