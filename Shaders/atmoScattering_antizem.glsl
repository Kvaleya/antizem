#include common.glsl

uniform float atmo_planet_radius;
uniform float atmo_radius;

uniform vec3 atmo_coef_rayleigh;
uniform vec3 atmo_coef_ozone;
uniform float atmo_coef_mie;

uniform float atmo_g_mie;
uniform float atmo_a_mie;

uniform float atmo_h0_R;
uniform float atmo_h0_M;

uniform vec3 atmo_recover_mie;

#define ATMO_CENTER vec3(0, -atmo_planet_radius, 0)
#define ATMO_RADIUS (atmo_planet_radius + atmo_radius)

#ifndef NUMSAMPLES_PRIMARY
	#define NUMSAMPLES_PRIMARY 20
#endif
#ifndef NUMSAMPLES_SECONDARY
	#define NUMSAMPLES_SECONDARY 20
#endif
#ifndef NUMSAMPLES_MULTIPLE
	#define NUMSAMPLES_MULTIPLE 10
#endif

#define TC_CONST 1.02809128080726 // 1.0 / (1.0 - exp(-3.6))

// Sources:
// http://old.cescg.org/CESCG-2009/papers/PragueCUNI-Elek-Oskar09.pdf
// Physically Based Sky, Atmosphere and Cloud Rendering in Frostbite

// v.x = h (height above planet surface)
// v.y = view/zenith angle cosine
// v.z = sun/zenith angle cosine
vec3 ViewParamsToTc(vec3 v)
{
	return vec3(
		sqrt(v.x / atmo_radius),
		(1.0 + v.y) * 0.5,
		(1.0 - exp(-2.8 * v.z - 0.8)) * TC_CONST
		);
}

vec3 TcToViewParams(vec3 tc)
{
	return vec3(
		tc.x * tc.x * atmo_radius,
		2.0 * tc.y - 1.0,
		(log(tc.z / -TC_CONST + 1.0) + 0.8) / -2.8
		);
}

bool RaySphereIntersection(vec3 sphereOrigin, float sphereRadiusSquared, vec3 rayOrigin, vec3 rayDir, out vec2 r)
{
	// dot(rayDir, rayDir) is always one because rayDir is normalized
	rayOrigin -= sphereOrigin;
	float dotOriginRay = dot(rayOrigin, rayDir);
	float discriminant = dotOriginRay * dotOriginRay - dot(rayOrigin, rayOrigin) + sphereRadiusSquared;
	
	r = vec2(0.0);
	if(discriminant >= 0.0)
	{
		discriminant = sqrt(discriminant);
		r = vec2(
			(-dotOriginRay + discriminant),
			(-dotOriginRay - discriminant)
			);
		if(r.x > r.y)
			r.xy = r.yx;
		return true;
	}
	else
		return false;
}

float PhaseFuncRayleighAdhoc(float cosTheta)
{
	return 0.8 * (1.4 + 0.5 * cosTheta);
}

float PhaseFuncRayleigh(float cosTheta)
{
	return PhaseFuncRayleighAdhoc(cosTheta);
	//return 0.75 * (1.0 + cosTheta * cosTheta);
}


float PhaseFuncMie(float cosTheta, float g)
{
	float g2 = g * g;
	float nom = 3.0 * (1.0 - g2) * (1.0 + cosTheta * cosTheta);
	float denom = 2.0 * (2.0 + g2) * pow(1.0 + g2 - 2.0 * g * cosTheta, 1.5);
	return nom / denom;
}

float Density(float height, float distribution)
{
	return exp(-max(height, 0.0) / distribution);
}

vec2 Transmittance(vec3 pa, vec3 pb)
{
	vec3 dir = (pb - pa);
	float len = length(dir);
	dir /= NUMSAMPLES_SECONDARY;
	
	vec2 accum = vec2(0.0);
	
	for(int i = 0; i < NUMSAMPLES_SECONDARY; i++)
	{
		vec3 p = pa + dir * (i + 0.5);
		float h = length(p - ATMO_CENTER) - atmo_planet_radius;
		accum.x += Density(h, atmo_h0_R);
		accum.y += Density(h, atmo_h0_M);
		#ifdef HEIGHTFOG
			accum.y += Density(h, HEIGHTFOG) * HEIGHTFOGMULT;
		#endif
	}
	
	return accum * len / NUMSAMPLES_SECONDARY;
}

// Can be used for realtime atmo scattering directly without precomputation
vec3 SingleScattering(vec3 origin, vec3 dir, vec3 sunlight_dir)
{
	vec3 pa = origin;
	vec2 rb;
	RaySphereIntersection(ATMO_CENTER, ATMO_RADIUS * ATMO_RADIUS, origin, dir, rb);
	float len = rb.y / NUMSAMPLES_PRIMARY;
	
	vec3 accumR = vec3(0.0);
	vec3 accumM = vec3(0.0);
	
	for(int i = 0; i < NUMSAMPLES_PRIMARY; i++)
	{
		vec3 p = pa + dir * len * (i + 0.5);
		float h = length(p - ATMO_CENTER) - atmo_planet_radius;
		
		vec2 rc;
		RaySphereIntersection(ATMO_CENTER, ATMO_RADIUS * ATMO_RADIUS, p, sunlight_dir, rc);
		vec3 pc = p + sunlight_dir * rc.y;
		vec2 transmittanceA = Transmittance(p, pa);
		vec2 transmittanceC = Transmittance(p, pc);
		
		vec3 transmittanceSum = (-transmittanceA.x - transmittanceC.x) * atmo_coef_rayleigh + (-transmittanceA.x - transmittanceC.x) * atmo_coef_ozone + (-transmittanceA.y - transmittanceC.y) * atmo_coef_mie * atmo_a_mie;
		vec3 extincionFinal = exp(transmittanceSum);
		
		accumR += Density(h, atmo_h0_R) * extincionFinal;
		accumM += Density(h, atmo_h0_M) * extincionFinal;
	}
	
	accumR *= len;
	accumM *= len;
	
	accumR *= PhaseFuncRayleigh(dot(dir, sunlight_dir));
	accumM *= PhaseFuncMie(dot(dir, -sunlight_dir), atmo_g_mie);
	
	accumR *= atmo_coef_rayleigh / (4.0 * KV_PI);
	accumM *= atmo_coef_mie / (4.0 * KV_PI);
	
	return (accumR + accumM);
}

void SingleScatteringPrecomp(vec3 origin, vec3 dir, vec3 sunlight_dir, in sampler2D lutTransmittance, out vec3 accumR, out vec3 accumM)
{
	vec3 pa = origin;
	float ha = length(pa - ATMO_CENTER);
	vec3 zenithA = (pa - ATMO_CENTER) / ha;
	ha -= atmo_planet_radius;
	vec3 transmittanceOpposite = texture(lutTransmittance, vec2(ha, dot(-dir, zenithA) * 0.5 + 0.5)).xyz;
	vec2 rb;
	RaySphereIntersection(ATMO_CENTER, ATMO_RADIUS * ATMO_RADIUS, origin, dir, rb);
	float len = rb.y / NUMSAMPLES_PRIMARY;
	
	accumR = vec3(0.0);
	accumM = vec3(0.0);
	
	for(int i = 0; i < NUMSAMPLES_PRIMARY; i++)
	{
		vec3 p = pa + dir * len * (i + 0.5);
		float h = length(p - ATMO_CENTER) - atmo_planet_radius;
		
		float hparam = h / atmo_radius;
		vec3 zenith = normalize(p - ATMO_CENTER);
		
		vec3 transmittanceSum = texture(lutTransmittance, vec2(hparam, dot(sunlight_dir, zenith) * 0.5 + 0.5)).xyz;
		//transmittanceSum += texture(lutTransmittance, vec2(hparam, dot(-dir, zenith) * 0.5 + 0.5)).xyz;
		//transmittanceSum -= transmittanceOpposite;
		vec2 dens = Transmittance(p, pa);
		transmittanceSum += dens.x * atmo_coef_rayleigh + dens.x * atmo_coef_ozone + dens.y * atmo_coef_mie * atmo_a_mie;
		vec3 extincionFinal = exp(-transmittanceSum);
		
		accumR += Density(h, atmo_h0_R) * extincionFinal;
		accumM += Density(h, atmo_h0_M) * extincionFinal;
	}
	
	accumR *= len;
	accumM *= len;
}

void GetScatteringRM(float h, vec3 dir, vec3 sunlight_dir, in sampler3D scatteringLut, out vec3 accumR, out vec3 accumM)
{
	vec3 tc = ViewParamsToTc(vec3(h, dir.y, sunlight_dir.y));
	
	vec4 lut = texture(scatteringLut, tc);
	
	accumR = lut.rgb;
	float accumMScalar = lut.a;
	
	accumR *= PhaseFuncRayleigh(dot(dir, sunlight_dir));
	accumMScalar *= PhaseFuncMie(dot(dir, -sunlight_dir), atmo_g_mie);
	
	accumR *= atmo_coef_rayleigh / (4.0 * KV_PI);
	accumMScalar *= atmo_coef_mie / (4.0 * KV_PI);
	
	accumM = accumR.rgb * (accumMScalar / accumR.r) * atmo_recover_mie;
}

// for multiple scattering
void GatherScattering(float h, vec3 dir, vec3 sunlight_dir, in sampler3D scatteringLut, out vec3 accumR, out vec3 accumM)
{
	const float sampleDelta = 0.25;
	float numSamples = 0;
	
	accumR = vec3(0.0);
	accumM = vec3(0.0);
	
	for(float phi = 0.0; phi < 2.0 * KV_PI; phi += sampleDelta)
	{
		for(float theta = -0.5 * KV_PI; theta <= 0.5 * KV_PI; theta += sampleDelta)
		{
			vec3 localDir = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
			vec3 localR, localM;
			GetScatteringRM(h, localDir, sunlight_dir, scatteringLut, localR, localM);
			
			accumR += localR * PhaseFuncRayleigh(dot(dir, localDir));
			accumM += localM * PhaseFuncMie(dot(dir, localDir), atmo_g_mie);
			
			numSamples++;
		}
	}
	float scale = KV_PI / numSamples;
	accumR *= scale;
	accumM *= scale;
}

void MultipleScatteringPrecomp(vec3 origin, vec3 dir, vec3 sunlight_dir, in sampler3D scatteringLut, out vec3 accumR, out vec3 accumM)
{
	vec3 pa = origin;
	float ha = length(pa - ATMO_CENTER);
	vec3 zenithA = (pa - ATMO_CENTER) / ha;
	ha -= atmo_planet_radius;
	vec2 rb;
	RaySphereIntersection(ATMO_CENTER, ATMO_RADIUS * ATMO_RADIUS, origin, dir, rb);
	float len = rb.y / NUMSAMPLES_MULTIPLE;
	
	accumR = vec3(0.0);
	accumM = vec3(0.0);
	
	for(int i = 0; i < NUMSAMPLES_MULTIPLE; i++)
	{
		vec3 p = pa + dir * len * (i + 0.5);
		float h = length(p - ATMO_CENTER) - atmo_planet_radius;
		
		vec2 dens = Transmittance(p, pa);
		vec3 transmittanceSum = dens.x * atmo_coef_rayleigh + dens.x * atmo_coef_ozone + dens.y * atmo_coef_mie * atmo_a_mie;
		vec3 extincionFinal = exp(-transmittanceSum);
		
		vec3 localR, localM;
		GatherScattering(h, dir, sunlight_dir, scatteringLut, localR, localM);
		
		accumR += localR * Density(h, atmo_h0_R) * extincionFinal;
		accumM += localM * Density(h, atmo_h0_M) * extincionFinal;
	}
	
	accumR *= len;
	accumM *= len;
	
	// accumR *= atmo_coef_rayleigh / (4.0 * KV_PI);
	// accumM *= atmo_coef_mie / (4.0 * KV_PI);
}

vec3 GetScattering(float h, vec3 dir, vec3 sunlight_dir, vec3 starColor, in sampler3D scatteringLut, bool sundisc)
{
	vec3 accumR, accumM;
	GetScatteringRM(h, dir, sunlight_dir, scatteringLut, accumR, accumM);
	
	if(sundisc)
	{
		const float sunRadiusCos = 0.99998;//0.99999999710025711272917289599046;
		float sunDot = dot(dir, sunlight_dir);
		if(sunDot >= sunRadiusCos)
		{
			vec2 rb;
			vec3 origin = vec3(0, h, 0);
			RaySphereIntersection(ATMO_CENTER, ATMO_RADIUS * ATMO_RADIUS, origin, dir, rb);
			vec2 transmittanceA = Transmittance(origin, origin + dir * rb.y);
			
			vec3 transmittanceSum = transmittanceA.x * atmo_coef_rayleigh + transmittanceA.x * atmo_coef_ozone + transmittanceA.y * atmo_coef_mie * atmo_a_mie;
			vec3 extincionFinal = exp(-transmittanceSum);
			
			accumM += extincionFinal * 10.0;
		}
	}
	// Fake blue night sky
	vec3 accumR2, accumM2;
	GetScatteringRM(h, dir, vec3(0.0, 1.0, 0.0), scatteringLut, accumR2, accumM2);
	
	const float nightSkyBrightness = 0.0002;
	
	accumR += accumR2 * nightSkyBrightness;
	
	vec3 final = (accumR + accumM) * starColor;
	// In case of NaN
	if(!(final.x >= 0.0))
		final = vec3(0.0);
	return final;
}
