#version 430

#include virtualTexture.glsl

uniform vec4 screenSize;
uniform vec4 vtTileBorderOffsetScale;
uniform vec4 vtTileScaleTexelScale;

#define VT_INDIRECTION_RESOLUTION 1024
#define VT_INDIRECTION_MAX_LEVEL 10
#define VT_TILE_USABLE_TEXEL_SIZE 128

#define VT_FEEDBACK_W 128
#define VT_FEEDBACK_H 64

#ifndef VOXELIZE
layout (binding = 0, r32ui) uniform restrict uimage2D texVTFeedback;
#endif

uniform usampler2D texVTIndirection;
uniform sampler2D texVTPhysicalBaseColor;
uniform sampler2D texVTPhysicalNormalMap;

// https://www.opengl.org/discussion_boards/showthread.php/177520-Mipmap-level-calculation-using-dFdx-dFdy
float VTMipLevel2(vec2 uv, uvec4 vtParams)
{
	float texSize = (vtParams.y & 0xFF) * VT_TILE_USABLE_TEXEL_SIZE;

	uv = uv * texSize;

	// The OpenGL Graphics System: A Specification 4.2
	//  - chapter 3.9.11, equation 3.21
	vec2 dx_vtc = dFdx(uv);
	vec2 dy_vtc = dFdy(uv);
	float delta_max_sqr = min(dot(dx_vtc, dx_vtc), dot(dy_vtc, dy_vtc)); 

	//return max(0.0, 0.5 * log2(delta_max_sqr) - 1.0); // == log2(sqrt(delta_max_sqr));
	return max(0.0, 0.5 * log2(delta_max_sqr)); // == log2(sqrt(delta_max_sqr));
}

ivec2 GetMipOffset(int mip)
{
	ivec2 offset = ivec2(0);
	
	if(mip > 0)
	{
		offset.x = VT_INDIRECTION_RESOLUTION;
	}
	
	int modify = VT_INDIRECTION_RESOLUTION >> 1;
	for(int i = 2; i <= VT_INDIRECTION_MAX_LEVEL; i++)
	{
		if(mip >= i)
		{
			offset.y += modify;
		}
		modify = modify >> 1;
	}
	return offset;
}

void SampleVTCache(vec2 pageUv, ivec2 pageCoords, int mip, out vec4 color, out vec4 normal)
{
	// Load indirection
	VTIndirection indirection = UnpackIndirection(texelFetch(texVTIndirection, pageCoords, mip).x);
	
	// Adjust for border
	vec2 finalUv = vec2(0.0);
	finalUv += indirection.page * vtTileScaleTexelScale.xy;
	finalUv += vtTileBorderOffsetScale.xy;
	finalUv += indirection.offset * vtTileScaleTexelScale.zw;
	finalUv += fract(pageUv) * vtTileBorderOffsetScale.zw * VTGetFloatScale(indirection);
	
	color = texture(texVTPhysicalBaseColor, finalUv);
	normal = texture(texVTPhysicalNormalMap, finalUv);
}

void SampleVT(vec2 uv, uvec4 vtParams, vec3 fragCoords, out vec4 baseColor, out vec4 normalMaterial)
{
	// Adjust texture coordinates for non-power-of-two texture size
	vec2 uvAdjustFactor = vec2(float(vtParams.y >> 16) / 65535.0, float(vtParams.z & 0xFFFF) / 65535.0);
	
	float lod = VTMipLevel2(uv * uvAdjustFactor, vtParams);
	uv = fract(uv);	
	uv *= uvAdjustFactor;
		
	lod = clamp(lod + 0.1, 0.0, VT_INDIRECTION_MAX_LEVEL + 0.99);
	
	int mip = int(floor(lod));	
	// Virtual image size in pages
	int size = int(vtParams.y & 0xFF);
	int maxMip = int((vtParams.y >> 8) & 0xFF);
	
	mip = min(mip, maxMip - 1);
	float lodFract = clamp(lod - mip, 0.0, 1.0);
	#ifdef VOXELIZE
		mip = maxMip - 1;
		lodFract = 0.0;
	#endif
	
	ivec2 pageCoords = ivec2(vtParams.x & 0xFFFF, vtParams.x >> 16) >> mip;
	vec2 pageUv = (uv * size) / (1 << mip);
	pageCoords += ivec2(floor(pageUv));
	ivec2 usageCoords = pageCoords + GetMipOffset(mip);
	
	#ifndef VOXELIZE
	// Mark the used page
	ivec2 fc = ivec2(fragCoords.xy);
	ivec2 fbCoords = ivec2(floor(fragCoords.xy * screenSize.zw * vec2(VT_FEEDBACK_W, VT_FEEDBACK_H)));
	uint feedback = usageCoords.x | (usageCoords.y << 11) | (int(clamp(fragCoords.z, 0.0, 1.0) * 2047) << 21);
	if((fc.x & 3) == 0 && (fc.y & 3) == 0)
		imageAtomicMax(texVTFeedback, fbCoords, (feedback));
	#endif
	
	#ifdef VOXELIZE
	SampleVTCache(pageUv, pageCoords, mip, baseColor, normalMaterial);
	#else
	vec4 sc0, sc1, sn0, sn1;
	
	SampleVTCache(pageUv, pageCoords, mip, sc0, sn0);
	SampleVTCache(pageUv * 0.5, pageCoords >> 1, mip + 1, sc1, sn1);	
	
	baseColor = mix(sc0, sc1, lodFract);
	normalMaterial = mix(sn0, sn1, lodFract);
	#endif
	
	ivec2 numCoords = ivec2(fract(pageUv) * 32) - 1;
	/*
	color = WriteNumber(numCoords - ivec2(0, 0), indirection.downsize, color, vec4(0.0, 0.0, 1.0, 1.0));
	
	color = WriteNumber(numCoords - ivec2(0, 9), indirection.offset.x, color, vec4(1.0, 1.0, 0.0, 1.0));
	color = WriteNumber(numCoords - ivec2(0, 18), indirection.offset.y, color, vec4(1.0, 1.0, 0.0, 1.0));
	
	ivec2 borderCoords = ivec2(fract(pageUv) * 128);
	
	if(borderCoords.x == 0 || borderCoords.y == 0 || borderCoords.x == 127 || borderCoords.y == 127)
		color = vec4(1.0);
	*/
}