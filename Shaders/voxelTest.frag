#version 430

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec2 screenPos;

layout(binding = 0) uniform sampler3D texVoxelOpacity;

#include voxels.glsl

uniform vec3 cameraPos;

uniform vec4 ray00;
uniform vec4 ray01;
uniform vec4 ray10;
uniform vec4 ray11;

vec3 GetRay(vec2 tc)
{
	return mix(
		mix(ray01.xyz, ray11.xyz, tc.x),
		mix(ray00.xyz, ray10.xyz, tc.x),
		tc.y);
}

void main()
{
	return;
	vec4 accum = vec4(0.0);
	
	vec3 ray = normalize(GetRay(screenPos));
	vec3 rayAbs = abs(ray);
	ray /= max(max(rayAbs.x, rayAbs.y), rayAbs.z);
	ray *= 1.1;
	
	const int numSamples = 128;
	float rayStep = 1.0 / 32.0;
	float len = rayStep;
	const float rise = 1.02;
	
	float opacity = 0.0;

	for(int i = 0; i < numSamples; i++)
	{
		vec3 pos = cameraPos + ray * len;
		len += rayStep;
		rayStep *= rise;
		
		vec4 s = SampleVoxels(pos, 0, texVoxelOpacity);
		
		float localOpacity = dot(rayAbs, s.xyz);
		localOpacity = mix(0.0, localOpacity, s.w);
		
		accum.xyz += s.xyz * localOpacity * (1.0 - opacity) * 4.0;
		
		opacity += localOpacity;
		
		if(opacity > 1.0)
		{
			opacity = 1.0;
			break;
		}
	}
	
	//accum.xyz = ray * 0.5 + 0.5;
	outColor.w = opacity * 0.5;
	outColor.xyz = accum.xyz / len;
}