#version 430

#include common.glsl

layout(binding = 0, rg8) uniform restrict readonly image3D inTex;
layout(binding = 1, rg8) uniform restrict writeonly image3D outMip1;
layout(binding = 2, rg8) uniform restrict writeonly image3D outMip2;
layout(binding = 3, rg8) uniform restrict writeonly image3D outMip3;

shared vec2 sampleCache[4][4][4];

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main()
{
	ivec3 local = ivec3(gl_LocalInvocationID.xyz);
	int localMask = local.x | local.y | local.z;
	
	ivec3 ipos = ivec3(gl_GlobalInvocationID.xyz);
	vec2 sum = vec2(0.0);
	sum += imageLoad(inTex, ipos * 2 + ivec3(0, 0, 0)).xy;
	sum += imageLoad(inTex, ipos * 2 + ivec3(1, 0, 0)).xy;
	sum += imageLoad(inTex, ipos * 2 + ivec3(0, 1, 0)).xy;
	sum += imageLoad(inTex, ipos * 2 + ivec3(1, 1, 0)).xy;
	sum += imageLoad(inTex, ipos * 2 + ivec3(0, 0, 1)).xy;
	sum += imageLoad(inTex, ipos * 2 + ivec3(1, 0, 1)).xy;
	sum += imageLoad(inTex, ipos * 2 + ivec3(0, 1, 1)).xy;
	sum += imageLoad(inTex, ipos * 2 + ivec3(1, 1, 1)).xy;
	sum *= 0.125;
	imageStore(outMip1, ipos, vec4(sum, 0.0, 0.0));
	
	sampleCache[local.x][local.y][local.z] = sum;
	
	MEMORY_BARRIER_GROUPSHARED;
	
	if((localMask & 1) > 0)
		return;
	
	sum = vec2(0.0);
	sum += sampleCache[local.x + 0][local.y + 0][local.z + 0];
	sum += sampleCache[local.x + 1][local.y + 0][local.z + 0];
	sum += sampleCache[local.x + 0][local.y + 1][local.z + 0];
	sum += sampleCache[local.x + 1][local.y + 1][local.z + 0];
	sum += sampleCache[local.x + 0][local.y + 0][local.z + 1];
	sum += sampleCache[local.x + 1][local.y + 0][local.z + 1];
	sum += sampleCache[local.x + 0][local.y + 1][local.z + 1];
	sum += sampleCache[local.x + 1][local.y + 1][local.z + 1];
	sum *= 0.125;
	imageStore(outMip2, ipos >> 1, vec4(sum, 0.0, 0.0));
	
	MEMORY_BARRIER_GROUPSHARED;
	
	sampleCache[local.x][local.y][local.z] = sum;
	
	MEMORY_BARRIER_GROUPSHARED;
	
	if((localMask & 2) > 0)
		return;

	sum = vec2(0.0);
	sum += sampleCache[local.x + 0][local.y + 0][local.z + 0];
	sum += sampleCache[local.x + 2][local.y + 0][local.z + 0];
	sum += sampleCache[local.x + 0][local.y + 2][local.z + 0];
	sum += sampleCache[local.x + 2][local.y + 2][local.z + 0];
	sum += sampleCache[local.x + 0][local.y + 0][local.z + 2];
	sum += sampleCache[local.x + 2][local.y + 0][local.z + 2];
	sum += sampleCache[local.x + 0][local.y + 2][local.z + 2];
	sum += sampleCache[local.x + 2][local.y + 2][local.z + 2];
	sum *= 0.125;
	imageStore(outMip3, ipos >> 2, vec4(sum, 0.0, 0.0));
}
