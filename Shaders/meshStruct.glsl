struct MeshData {
	vec4 positionOffset; // w unused
	vec4 positionScale; // w unused
	vec4 texCoordOffsetScale;
	vec4 boundingSphere;
};

struct TriangleCluster {
	vec4 boundingSphere;
	vec4 normalCone;
};

struct Instance {
	vec4 right;
	vec4 up;
	vec4 forward;
	ivec4 attributes;
};