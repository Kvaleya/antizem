#version 430

#include common.glsl
#include meshStruct.glsl
#include clusterStruct.glsl

// Contains cluster bounding volume
layout(binding = 0, std430) restrict readonly buffer bufferClusters
{
    TriangleCluster clusters[];
};

layout(binding = 1, std430) restrict readonly buffer bufferClusterBucketOffsets
{
    int clusterBucketOffsets[];
};

layout(binding = 2, std430) restrict buffer bufferIndirectDispatches
{
    ivec4 indirectDispatches[];
};

// Data required to process triangles in a cluster
layout(binding = 3, std430) restrict readonly buffer bufferClusterData
{
    ClusterData clusterDataArray[];
};

layout(binding = 4, std430) restrict readonly buffer bufferInstances
{
    Instance instances[];
};

layout(binding = 5, std430) restrict writeonly buffer bufferClustersCulled
{
    ClusterData clusterDataCulled[];
};

layout(binding = 0) uniform sampler2D texDepthPyramid;

uniform int clusterDataCount;
uniform int clusterDataOffset;

uniform vec4 frustumPlane0;
uniform vec4 frustumPlane1;
uniform vec4 frustumPlane2;
uniform vec4 frustumPlane3;

uniform vec3 cameraOffset;
uniform vec3 cameraForward;
uniform vec3 cameraUp;
uniform float cameraNear;

uniform vec3 includeBoxCenter;
uniform vec3 includeBoxSize; // Half of the box size (radius-like)
uniform vec3 excludeBoxCenter;
uniform vec3 excludeBoxSize;

uniform mat4 projection;

uniform int maxDepthLevel;

bool CullBoxBs(vec4 bs)
{
	vec3 inBox = abs(bs.xyz - includeBoxCenter) - includeBoxSize - bs.w;
	
	if(inBox.x > 0 || inBox.y > 0 || inBox.z > 0)
		return true; // Sphere is outside of the box -> cull
	
	inBox = abs(bs.xyz - excludeBoxCenter) - excludeBoxSize + bs.w;
	
	if(inBox.x < 0 && inBox.y < 0 && inBox.z < 0)
		return true; // Sphere is entirely inside the exclude box
	
	return false;
}

bool CullPlaneBs(vec4 bs, vec4 plane)
{
	return dot(bs.xyz, plane.xyz) + plane.w - bs.w > 0;
}

vec4 ProjectPoint(vec3 p)
{
	vec4 result = projection * vec4(p, 1.0);
	result.xyz /= result.w;
	return result;
}

vec4 GetSphereProjectedAABB(vec4 bs, out float sphereZ)
{
	vec3 cameraRight = cross(cameraForward, cameraUp);
	vec3 view = normalize(cameraOffset.xyz - bs.xyz);
	
	vec3 sphereUp = cross(cameraRight, cameraForward);
	vec3 sphereRight = cross(-view, cameraUp);
	
	vec3 left = bs.xyz - sphereRight * bs.w;
	vec3 right = bs.xyz + sphereRight * bs.w;
	vec3 down = bs.xyz - sphereUp * bs.w;
	vec3 up = bs.xyz + sphereUp * bs.w;
	
	left = ProjectPoint(left).xyz;
	right = ProjectPoint(right).xyz;
	down = ProjectPoint(down).xyz;
	up = ProjectPoint(up).xyz;
	
	sphereZ = ProjectPoint(bs.xyz - cameraForward * bs.w).z;
	sphereZ = max(sphereZ, left.z);
	sphereZ = max(sphereZ, right.z);
	sphereZ = max(sphereZ, down.z);
	sphereZ = max(sphereZ, up.z);
	
	// minX, minY, maxX, maxY
	vec4 aabb;
	aabb.x = min(min(left.x, right.x), min(down.x, up.x));
	aabb.y = min(min(left.y, right.y), min(down.y, up.y));
	aabb.z = max(max(left.x, right.x), max(down.x, up.x));
	aabb.w = max(max(left.y, right.y), max(down.y, up.y));
	
	return aabb;
}

shared int survivingCount;
shared int groupClusterWriteOffset;

/*
Possible cull options:
#define CULL_FRUSTUM
#define CULL_BACKFACE
#define CULL_OCCLUSION
#define KEEP_NEAR_CLUSTERS
#define CULL_CUBE
*/

layout (local_size_x = 64, local_size_y = 1, local_size_z = 1) in;
void main()
{
	int index = int(gl_GlobalInvocationID.x);
	if(index >= clusterDataCount)
		return;
	index += clusterDataOffset;
	
	if(gl_LocalInvocationID.x == 0)
		survivingCount = 0;
	
	MEMORY_BARRIER_GROUPSHARED;
	
	ClusterData cdata = clusterDataArray[index];
	
	ClusterDataUnpacked clusterUnpacked;
	UnpackClusterData(cdata, clusterUnpacked);
	
	if(clusterUnpacked.CullFlags.z)
		return;
	
	TriangleCluster cluster = clusters[clusterUnpacked.ClusterIndex];
	Instance instance = instances[clusterUnpacked.InstanceIndex];
	
	vec4 transformedBs = vec4(
		dot(vec4(cluster.boundingSphere.xyz, 1.0), instance.right),
		dot(vec4(cluster.boundingSphere.xyz, 1.0), instance.up),
		dot(vec4(cluster.boundingSphere.xyz, 1.0), instance.forward),
		cluster.boundingSphere.w * length(instance.right.xyz)
	);
	
	bool culled = false;
	
	#ifdef CULL_CUBE
	culled = culled || CullBoxBs(transformedBs);
	#endif
	
	// Frustum cull
	#ifdef CULL_FRUSTUM
	bool frustumCull = false;
	frustumCull = frustumCull || CullPlaneBs(transformedBs, frustumPlane0);
	frustumCull = frustumCull || CullPlaneBs(transformedBs, frustumPlane1);
	frustumCull = frustumCull || CullPlaneBs(transformedBs, frustumPlane2);
	frustumCull = frustumCull || CullPlaneBs(transformedBs, frustumPlane3);
	
	culled = culled || frustumCull;
	#endif
	
	// Normal cone cull
	// Broken!
	#ifdef CULL_BACKFACE
	vec4 transformedCone = vec4(
		dot(cluster.normalCone.xyz, instance.right.xyz),
		dot(cluster.normalCone.xyz, instance.up.xyz),
		dot(cluster.normalCone.xyz, instance.forward.xyz),
		cluster.normalCone.w
	);
	
	culled = culled || (dot(transformedCone.xyz,  normalize(cameraOffset - (transformedBs.xyz - transformedCone.xyz * transformedBs.w * 1.5))) < transformedCone.w);
	#endif
	
	// Occlusion cull
	
	#ifdef CULL_OCCLUSION
	float sphereZ;
	vec4 aabb = GetSphereProjectedAABB(transformedBs, sphereZ) * 0.5 + 0.5;
	
	vec2 aabbMin = floor(clamp(aabb.xy, 0.0, 1.0) * DEPTHTEXRES - 0.5);
	vec2 aabbMax = ceil(clamp(aabb.zw, 0.0, 1.0) * DEPTHTEXRES + 0.5);
	
	int depthMipLevel = max(int(ceil(log2(max(max(aabbMax.x - aabbMin.x, aabbMax.y - aabbMin.y), 1.0)))), 0);
	
	#define DEPTHMIPBIAS 0
	
	if(!culled && depthMipLevel < maxDepthLevel - DEPTHMIPBIAS)
	{
		// Depth cull
		float sceneMaxDepth = DEPTH_NEAR;
		ivec2 coords = clamp(ivec2(aabbMin.xy) >> (depthMipLevel + DEPTHMIPBIAS), ivec2(0, 0), DEPTHTEXRES >> (depthMipLevel + DEPTHMIPBIAS));
		sceneMaxDepth = min(texelFetch(texDepthPyramid, coords + ivec2(0, 0), depthMipLevel + DEPTHMIPBIAS).x, sceneMaxDepth);
		sceneMaxDepth = min(texelFetch(texDepthPyramid, coords + ivec2(0, 1), depthMipLevel + DEPTHMIPBIAS).x, sceneMaxDepth);
		sceneMaxDepth = min(texelFetch(texDepthPyramid, coords + ivec2(1, 0), depthMipLevel + DEPTHMIPBIAS).x, sceneMaxDepth);
		sceneMaxDepth = min(texelFetch(texDepthPyramid, coords + ivec2(1, 1), depthMipLevel + DEPTHMIPBIAS).x, sceneMaxDepth);
		
		culled = culled || (sphereZ * 1.01 < sceneMaxDepth);
	}
	#endif
	
	// Do not cull if camera is inside cluster
	#ifdef KEEP_NEAR_CLUSTERS
	if(length(cameraOffset - transformedBs.xyz) < transformedBs.w)
		culled = false;
	#endif
	
	//culled = false;
	
	// Write surviving clusters
	
	int localIndex = 0;
	
	if(!culled)
	{
		localIndex = atomicAdd(survivingCount, 1);
	}
	
	MEMORY_BARRIER_GROUPSHARED;
	
	if(gl_LocalInvocationID.x == 0)
	{
		int offset = 0;
		// Clustery v jedné grupě mohou chtít psát jinam...
		offset += atomicAdd(indirectDispatches[clusterUnpacked.ClusterBucketIndex][0], survivingCount);
		offset += clusterBucketOffsets[clusterUnpacked.ClusterBucketIndex];
		groupClusterWriteOffset = offset;
	}
	
	MEMORY_BARRIER_GROUPSHARED;
	
	if(!culled)
		clusterDataCulled[localIndex + groupClusterWriteOffset] = cdata;
}

































