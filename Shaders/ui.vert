#version 430

layout(location = 0) in vec4 vertexPosition;
layout(location = 1) in vec2 vertexTexCoord;
layout(location = 2) in vec4 vertexColor;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec4 vPosition;
layout(location = 1) out vec2 vTexCoord;
layout(location = 2) out vec4 vColor;

void main() {
	vec4 vertexPosUnpacked = vertexPosition * 2.0 - 0.5;
	vPosition = vertexPosUnpacked;
	vTexCoord = vertexTexCoord;
	vColor = vertexColor;
    gl_Position = vec4(vertexPosUnpacked.xy * vec2(2.0, -2.0) + vec2(-1.0, 1.0), 0.0, 1.0);
}