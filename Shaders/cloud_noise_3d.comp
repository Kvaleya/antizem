#version 430

#include common.glsl
#include cloud_noises.glsl

layout(binding = 0, rg8) uniform restrict writeonly image3D texOut;
layout(binding = 0) uniform sampler2D texRandom;

#define RNDSIZESHIFT 10

uniform int offsetZ;
uniform vec3 sizercp;

const int tex3d_size = TEX3DSIZE;

//
// Worley noise
//

vec3 GenPointInCell(ivec3 cell, int pointId, int cellCount)
{
	ivec3 cellMod = (cell + ivec3(cellCount)) & (cellCount - 1);
	int cellId = cellMod.x + cellMod.y * cellCount + cellMod.z * cellCount * cellCount;
	vec3 p = texelFetch(texRandom, ivec2(cellId, pointId), 0).xyz;
	return (cell + p) / cellCount;
}

vec3 GenPointFromIndex(ivec3 cellOffset, int i, int cellCount, int pointsInCell, int pointsInCellShift, int rndFetchOffset)
{
	int j = i >> pointsInCellShift;
	ivec3 cell = cellOffset + ivec3(j % 3, (j / 3) % 3, (j / 9) % 3) - ivec3(1);
	return GenPointInCell(cell, (i & (pointsInCell - 1)) + rndFetchOffset, cellCount);
}

shared vec3 points[1536];

vec3 GetWorley(vec3 p, int pointCount)
{
	float globalClosestDist = 1000.0;
	vec3 toClosest;
	
	for(int i = 0; i < pointCount; i++)
	{
		vec3 toLocal = points[i] - p;
		float localClosestDist = length(toLocal);
		if(localClosestDist < globalClosestDist)
		{
			globalClosestDist = localClosestDist;
			toClosest = toLocal;
		}
	}
	
	return toClosest;
}

float GenerateWorleyNoise(ivec3 localCoords, int texSize, int cellsShift, int pointsInCellShift, float contrast, int rndFetchOffset)
{
	int cellCount = 1 << cellsShift;
	int pointsInCell = 1 << pointsInCellShift;
	int pointCount = pointsInCell * 27;
	vec3 fpos = (localCoords + 0.5) / texSize;
	ivec3 localCell = localCoords / (texSize / cellCount);
	
	int localIndex = int(gl_LocalInvocationID.x + gl_LocalInvocationID.y * 8);
	
	for(int i = 0; i < 16; i++)
	{
		if(localIndex < pointCount)
			points[localIndex] = GenPointFromIndex(localCell, localIndex, cellCount, pointsInCell, pointsInCellShift, rndFetchOffset);
		localIndex += 64;
	}
	
	MEMORY_BARRIER_GROUPSHARED;
	
	vec3 w = GetWorley(fpos, pointCount);
	
	MEMORY_BARRIER_GROUPSHARED;
	
	return clamp(1.0 - length(w) * contrast, 0.0, 1.0);
}

float contrast(float v, float center, float contr)
{
	return clamp((v - center) * contr + center, 0.0, 1.0);
}

layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main(void)
{
	ivec3 localCoords = ivec3(gl_GlobalInvocationID.xy, offsetZ);
	vec3 fpos = (localCoords + 0.5) * sizercp;
	
	vec4 color = vec4(0.0);
	/*
	color.x += GenerateWorleyNoise(localCoords, tex3d_size, 3, 4, 32.0, 0) * 0.4;
	color.x += GenerateWorleyNoise(localCoords, tex3d_size, 3, 4, 24.0, 32) * 0.4;
	color.x += GenerateWorleyNoise(localCoords, tex3d_size, 4, 3, 16.0, 64) * 0.1;
	
	const float worley_importance = 0.6;
	
	color.x = 1.0 - color.x;
	
	color.x = contrast(fbm(fpos + vec3(2.7845, 3.1415784, 1.2458), 16), 0.3, 2.5) * (color.x * worley_importance + (1.0 - worley_importance));
	*/
	/*
	color.x += 1.0 - fbm(fpos + vec3(2.7845, 3.1415784, 1.2458), 4);
	color.x *= color.x;
	float worley = GenerateWorleyNoise(localCoords, tex3d_size, 3, 5, 24.0, 0);
	color.x -= worley * saturate(0.5 - color.x) * 0.35;
	
	color.y += GenerateWorleyNoise(localCoords, tex3d_size, 2, 5, 16.0, 64) * 0.65;
	color.y += GenerateWorleyNoise(localCoords, tex3d_size, 3, 5, 16.0, 96) * 0.25;
	color.y += GenerateWorleyNoise(localCoords, tex3d_size, 3, 4, 16.0, 128) * 0.10;
	
	color.y = 1.0 - color.y;
	
	float coloryFbm = 0.2;
	
	color.y = color.y * (1.0 - coloryFbm) + fbm(fpos + vec3(2.7845, 3.1415784, 1.2458), 32) * coloryFbm;
	*/
	color.x = fbm(fpos, 4);
	
	color.y += GenerateWorleyNoise(localCoords, tex3d_size, 3, 3, 12.0, 0) * 0.5;
	color.y += GenerateWorleyNoise(localCoords, tex3d_size, 4, 3, 16.0, 128) * 0.2;
	color.y += GenerateWorleyNoise(localCoords, tex3d_size, 3, 2, 16.0, 256) * 0.2;
	
	color.y *= 0.5 + 0.5 * fbm(fpos, 4);
	
	vec3 uv = fpos * 2.0 - 1.0;
	uv *= uv;
	uv *= uv;
	//color.x = max(max(abs(uv.x), abs(uv.y)), abs(uv.z));
	
	//color.z += GenerateWorleyNoise(localCoords, tex3d_size, 3, 2, 12.0, 64) * 0.75;
	//color.z += GenerateWorleyNoise(localCoords, tex3d_size, 3, 3, 16.0, 72) * 0.25;
	
	imageStore(texOut, localCoords, color);
}
