struct MaterialData {
	uvec4 baseColorParams;
	uvec4 materialParams;
	uvec4 vtParams;
	uvec4 alphaParams;
};

layout (std140, binding = 0) uniform materialUniforms
{
    MaterialData materials[256];
};

bool MaterialGetFlag(int flag, MaterialData data)
{
	return ((data.vtParams.w >> 24) & flag) > 0;
}

vec4 MaterialUnpackVec4(uvec2 u)
{
	const float ushortRcp = 1.5259021896696421759365224689097e-5;
	vec4 v;
	v.x = ((u.x & 0x0000FFFF) * ushortRcp) * 16.0 - 8.0;
	v.y = ((u.x >> 16) * ushortRcp) * 16.0 - 8.0;
	v.z = ((u.y & 0x0000FFFF) * ushortRcp) * 16.0 - 8.0;
	v.w = ((u.y >> 16) * ushortRcp) * 16.0 - 8.0;
	return v;
}

float SampleAlpha(MaterialData data, vec2 tc, sampler2D s, bool useSmallestMip)
{
	if(!MaterialGetFlag(4, data))
		return 1.0;
	
	tc = fract(tc);
	
	vec4 tcParams;
	tcParams.x = float((data.alphaParams.x >> 0) & 0xFFFF) / 65535.0;
	tcParams.y = float((data.alphaParams.x >> 16) & 0xFFFF) / 65535.0;
	tcParams.z = float((data.alphaParams.y >> 0) & 0xFFFF) / 65535.0;
	tcParams.w = float((data.alphaParams.y >> 16) & 0xFFFF) / 65535.0;
	int minMip = int((data.alphaParams.z >> 0) & 0xFF);
	int maxMip = int((data.alphaParams.z >> 8) & 0xFF);
	tc = tcParams.xy + tc * tcParams.zw;
	
	float lod = maxMip;
	if(!useSmallestMip)
	{
		vec2 lodVec = textureQueryLod(s, tc);
		lod = min(max(max(lodVec.x, lodVec.y), minMip), maxMip);
	}
	
	return textureLod(s, tc.xy, lod).x;
}

vec4 MaterialGetBaseColor(vec4 baseColor, MaterialData data)
{
	return baseColor * MaterialUnpackVec4(data.baseColorParams.zw) + MaterialUnpackVec4(data.baseColorParams.xy);
}

vec4 MaterialGetMaterial(vec4 material, MaterialData data)
{
	return material * MaterialUnpackVec4(data.materialParams.zw) + MaterialUnpackVec4(data.materialParams.xy);
}

vec3 MaterialGetNormal(vec4 normalMaterial, MaterialData data)
{
	if(MaterialGetFlag(2, data))
	{
		vec3 normal;
		normal.xy = normalMaterial.xy * 2.0 - 1.0;
		float lensq = dot(normal.xy, normal.xy);
		normal.z = (lensq < 1.0 ? sqrt(1.0 - lensq) : 0.0);
		return normal;
	}
	else
	{
		return vec3(0.0, 0.0, 1.0);
	}
}