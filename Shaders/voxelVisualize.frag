#version 430

layout(location = 0) out vec4 outColor;

layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 inColor;
layout(location = 3) in vec3 inPos;

void main() {
	vec4 color = inColor;
	color.w = .0;
	color.rgb *= 1.0 / pow(dot(inPos, inPos), 0.1);
    outColor = color;
}