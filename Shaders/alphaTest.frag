#version 430

layout(location = 1) flat in int vMaterial;
layout(location = 3) in vec2 vTexCoord;

#include materialUbo.glsl

uniform sampler2D texAlphaAtlas;

void main() {
	MaterialData material = materials[vMaterial];
	
	float alpha = SampleAlpha(material, vTexCoord, texAlphaAtlas, false);
	
	if(alpha < 0.5)
		discard;
}