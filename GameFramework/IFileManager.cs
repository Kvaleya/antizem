﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameFramework
{
	/// <summary>
	/// Assumes single threaded access
	/// </summary>
	public interface IFileManager
	{
		void MountArchive(string archive);

		bool FileExists(string file);

		Stream GetStream(string file);
	}
}
