﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameFramework
{
	public class FileManagerZip : IFileManager
	{
		Dictionary<string, ZipArchiveEntry> _zipEntries = new Dictionary<string, ZipArchiveEntry>();

		string _baseRelativePath;

		readonly TextOutput _textOutput;

		public FileManagerZip(TextOutput textOutput, string baseRelativePath)
		{
			_textOutput = textOutput;
			_baseRelativePath = baseRelativePath;
		}

		string GetPathString(string path)
		{
			path = path.ToLowerInvariant();
			path = path.Replace('\\', '/');
			path = path.Replace("//", "/");
			path.Trim();
			return path;
		}

		public void MountArchive(string archive)
		{
			var stream = GetStream(archive);

			if(stream == null)
			{
				_textOutput.Print(OutputType.Error, "Error opening archive: " + archive + ": file not found");
				return;
			}

			ZipArchive zip;

			try
			{
				zip = new ZipArchive(stream);
			}
			catch(Exception e)
			{
				stream.Dispose();
				_textOutput.PrintException(OutputType.Error, "Error opening archive: " + archive, e);
				return;
			}

			foreach(ZipArchiveEntry entry in zip.Entries)
			{
				string path = GetPathString(entry.FullName);

				if(_zipEntries.ContainsKey(path))
					_zipEntries[path] = entry;
				else
					_zipEntries.Add(path, entry);
			}
		}

		public bool FileExists(string file)
		{
			string path = GetPathString(file);
			string relativePath = Path.Combine(_baseRelativePath, path);
			return _zipEntries.ContainsKey(path) || File.Exists(relativePath);
		}

		public Stream GetStream(string file)
		{
			string path = GetPathString(file);

			if(_zipEntries.ContainsKey(path))
			{
				return _zipEntries[path].Open();
			}

			string relativePath = Path.Combine(_baseRelativePath, path);

			if(File.Exists(relativePath))
			{
				try
				{
					return new FileStream(relativePath, FileMode.Open, FileAccess.Read);
				}
				catch(Exception e)
				{
					_textOutput.PrintException(OutputType.Error, "Error opening file: " + file, e);
					return null;
				}
			}

			_textOutput.Print(OutputType.Warning, "File not found: " + file);

			return null;
		}
	}
}
