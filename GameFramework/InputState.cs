﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Input;

namespace GameFramework
{
	public enum MouseKey
	{
		Left = 0,
		Right = 1,
		Middle = 2,
		Extra1 = 3,
		Extra2 = 4,
	}

	public class MouseInputState
	{
		const int NumMouseKeys = 5;

		IViewport _viewport;

		// Mouse
		MouseState _mouseState = new MouseState();
		MouseState _oldMouseState = new MouseState();

		/// <summary>
		/// Keys that have started being pressed down
		/// </summary>
		bool[] _mouseKeysPressed = new bool[NumMouseKeys];

		/// <summary>
		/// Keys that are pressed down
		/// </summary>
		bool[] _mouseKeysDown = new bool[NumMouseKeys];

		/// <summary>
		/// Keys that have stopped being pressed down
		/// </summary>
		bool[] _mouseKeysReleased = new bool[NumMouseKeys];
		
		public Vector2 MouseDelta { get; private set; }

		public int MouseX { get; private set; }
		public int MouseY { get; private set; }

		public int MouseOldX { get; private set; }
		public int MouseOldY { get; private set; }

		public int MouseWindowX { get; private set; }
		public int MouseWindowY { get; private set; }

		public float MouseScrollDelta { get; private set; }

		float _mouseWheelPreciseLast = 0;

		private MouseInputState()
		{
			MouseDelta = new Vector2();
		}
		
		public bool IsMouseKeyDown(int k)
		{
			if(k >= _mouseKeysDown.Length || k < 0)
				return false;
			return _mouseKeysDown[k];
		}

		public bool IsMouseKeyPressed(int k)
		{
			if(k >= _mouseKeysPressed.Length || k < 0)
				return false;
			return _mouseKeysPressed[k];
		}

		public bool IsMouseKeyReleased(int k)
		{
			if(k >= _mouseKeysReleased.Length || k < 0)
				return false;
			return _mouseKeysReleased[k];
		}

		public bool IsMouseKeyDown(MouseKey k)
		{
			return IsMouseKeyDown((int)k);
		}

		public bool IsMouseKeyPressed(MouseKey k)
		{
			return IsMouseKeyPressed((int)k);
		}
		public bool IsMouseKeyReleased(MouseKey k)
		{
			return IsMouseKeyReleased((int)k);
		}

		public MouseInputState(IViewport viewport)
		{
			_viewport = viewport;
		}

		public void Update()
		{
			MouseState mouse = Mouse.GetState();
			_oldMouseState = _mouseState;
			_mouseState = mouse;

			for(int i = 0; i < NumMouseKeys; i++)
			{
				MouseKey k = (MouseKey)i;
				bool down = false;

				switch(k)
				{
					case MouseKey.Left:
						{
							down = mouse.LeftButton == ButtonState.Pressed;
							break;
						}
					case MouseKey.Right:
						{
							down = mouse.RightButton == ButtonState.Pressed;
							break;
						}
					case MouseKey.Middle:
						{
							down = mouse.MiddleButton == ButtonState.Pressed;
							break;
						}
					case MouseKey.Extra1:
						{
							down = mouse.XButton1 == ButtonState.Pressed;
							break;
						}
					case MouseKey.Extra2:
						{
							down = mouse.XButton2 == ButtonState.Pressed;
							break;
						}
				}

				_mouseKeysPressed[i] = !_mouseKeysDown[i] && down;
				_mouseKeysReleased[i] = _mouseKeysDown[i] && !down;

				_mouseKeysDown[i] = down;
			}

			MouseOldX = MouseX;
			MouseOldY = MouseY;

			MouseX = mouse.X;
			MouseY = mouse.Y;

			int wx, wy;
			GetMouseWindowPos(out wx, out wy);

			MouseWindowX = wx;
			MouseWindowY = wy;

			MouseDelta = new Vector2(MouseX - MouseOldX, MouseY - MouseOldY);

			MouseScrollDelta = mouse.WheelPrecise - _mouseWheelPreciseLast;
			_mouseWheelPreciseLast = mouse.WheelPrecise;
		}

		/// <summary>
		/// Returns a new MouseInputState object in up-to-date state without updating the parent state. Intended for late-latching
		/// </summary>
		public MouseInputState GetUpdated()
		{
			MouseInputState updated = new MouseInputState();
			updated._mouseState = _mouseState;
			updated.MouseX = MouseX;
			updated.MouseY = MouseY;

			for(int i = 0; i < NumMouseKeys; i++)
			{
				updated._mouseKeysDown[i] = _mouseKeysDown[i];
			}

			updated.Update();

			return updated;
		}

		public void ClearState()
		{
			MouseState mouse = Mouse.GetState();
			_oldMouseState = mouse;
			_mouseState = mouse;

			for(int i = 0; i < NumMouseKeys; i++)
			{
				MouseKey k = (MouseKey)i;

				_mouseKeysPressed[i] = false;
				_mouseKeysReleased[i] = false;
				_mouseKeysDown[i] = false;
			}

			MouseX = mouse.X;
			MouseY = mouse.Y;
			MouseOldX = mouse.X;
			MouseOldY = mouse.Y;

			int wx, wy;
			GetMouseWindowPos(out wx, out wy);

			MouseWindowX = wx;
			MouseWindowY = wy;

			MouseDelta = Vector2.Zero;
			MouseScrollDelta = 0;
		}

		void GetMouseWindowPos(out int x, out int y)
		{
			var p = _viewport.TopLeftPoint;
			x = Mouse.GetCursorState().X - p.Item1;
			y = Mouse.GetCursorState().Y - p.Item2;
		}
	}

	public class KeyboardInputState
	{
		const int NumKeys = 132;

		KeyboardState _keyboardState = new KeyboardState();
		KeyboardState _oldKeyboardState = new KeyboardState();

		/// <summary>
		/// Keys that have started being pressed down
		/// </summary>
		bool[] _keysPressed = new bool[NumKeys];

		/// <summary>
		/// Keys that are pressed down
		/// </summary>
		bool[] _keysDown = new bool[NumKeys];

		/// <summary>
		/// Keys that have stopped being pressed down
		/// </summary>
		bool[] _keysReleased = new bool[NumKeys];

		public KeyboardInputState()
		{
			
		}

		public bool IsKeyDown(int k)
		{
			if(k >= _keysDown.Length || k < 0)
				return false;
			return _keysDown[k];
		}

		public bool IsKeyPressed(int k)
		{
			if(k >= _keysPressed.Length || k < 0)
				return false;
			return _keysPressed[k];
		}

		public bool IsKeyReleased(int k)
		{
			if(k >= _keysReleased.Length || k < 0)
				return false;
			return _keysReleased[k];
		}

		public bool IsKeyDown(Key k)
		{
			return IsKeyDown((int)k);
		}

		public bool IsKeyPressed(Key k)
		{
			return IsKeyPressed((int)k);
		}

		public bool IsKeyReleased(Key k)
		{
			return IsKeyReleased((int)k);
		}

		// TODO: since only one keyboard is expected to be present at all times, maybe it makes sence to only ever have one KeyboardInput object that is updated from a centralized location
		public void Update()
		{
			KeyboardState keyboard = Keyboard.GetState();
			_oldKeyboardState = _keyboardState;
			_keyboardState = keyboard;

			for(int i = 0; i < NumKeys; i++)
			{
				bool down = keyboard[(Key)i];

				_keysPressed[i] = !_keysDown[i] && down;
				_keysReleased[i] = _keysDown[i] && !down;

				_keysDown[i] = down;
			}
		}

		public void ClearState()
		{
			KeyboardState keyboard = Keyboard.GetState();
			_oldKeyboardState = keyboard;
			_keyboardState = keyboard;

			for(int i = 0; i < NumKeys; i++)
			{
				_keysPressed[i] = false;
				_keysReleased[i] = false;
				_keysDown[i] = false;
			}
		}
	}
}
