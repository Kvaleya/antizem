﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameFramework
{
	public interface IViewport
	{
		/// <summary>
		/// The top left point of the client area. Used to get cursor position within window.
		/// </summary>
		Tuple<int, int> TopLeftPoint { get; }
	}
}
