﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FontGen
{
	class Program
	{
		static void Main(string[] args)
		{
			var regular = "Carlito-Regular.ttf";
			var bold = "Carlito-Bold.ttf";
			var italic = "Carlito-Italic.ttf";
			var boldItalic = "Carlito-BoldItalic.ttf";

			FontBankGen.CreateFromFonts(new string[]
			{
				regular,
				bold,
				italic,
				boldItalic
			}, 32, 16384, "testFont.zip");
		}
	}
}
