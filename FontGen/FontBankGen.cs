﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Engine.UI;
using OpenTK;
using SharpFont;

namespace FontGen
{
	class FontBankGen
	{
		public static void CreateFromFonts(string[] fontFiles, int charRowsColumns, int resolution, string fileOut)
		{
			int pixelsPerCharacterStore = resolution / charRowsColumns / 2;

			var characters = new Dictionary<int, Character>[]
			{
				new Dictionary<int, Character>(),
				new Dictionary<int, Character>(),
				new Dictionary<int, Character>(),
				new Dictionary<int, Character>(),
			};

			byte[] imageBytes = new byte[resolution * resolution];

			for(int fontStyle = 0; fontStyle < 4; fontStyle++)
			{
				string filename = fontFiles[fontStyle];

				Library lib = new Library();
				Face face = new Face(lib, filename, 0);
				face.SetPixelSizes(0, (uint)pixelsPerCharacterStore);

				for(int charIndex = 0; charIndex < charRowsColumns * charRowsColumns; charIndex++)
				{
					int x = (charIndex % charRowsColumns);
					int y = (charIndex / charRowsColumns);

					// Offset for different font styles
					if(fontStyle % 2 == 1)
						x += charRowsColumns;
					if(fontStyle / 2 == 1)
						y += charRowsColumns;

					Character c = new Character();
					c.Char = charIndex;
					face.LoadChar((uint)charIndex, LoadFlags.Render, LoadTarget.Normal);
					c.Size = new Vector2(face.Glyph.Bitmap.Width, face.Glyph.Bitmap.Rows) / pixelsPerCharacterStore;
					c.Bearing = new Vector2(face.Glyph.BitmapLeft, face.Glyph.BitmapTop) / pixelsPerCharacterStore;
					c.Advance = face.Glyph.Advance.X / (float)pixelsPerCharacterStore / 64;

					x = x * pixelsPerCharacterStore;
					y = y * pixelsPerCharacterStore;

					c.TexCoordMin = new Vector2(x, y) / resolution;
					c.TexCoordMax = new Vector2(x + face.Glyph.Bitmap.Width, y + face.Glyph.Bitmap.Rows) / resolution;

					characters[fontStyle].Add(charIndex, c);
					
					int offset = x + y * resolution;
					int glyphOffset = 0;
					for(int row = 0; row < face.Glyph.Bitmap.Rows; row++)
					{
						var data = face.Glyph.Bitmap.BufferData.ToList().ToArray();
						for(int column = 0; column < face.Glyph.Bitmap.Width; column++)
						{
							imageBytes[offset + column] = data[glyphOffset + column];
						}
						offset += resolution;
						glyphOffset += face.Glyph.Bitmap.Width;
					}
				}
			}

			using(FileStream fsOut = File.Create(fileOut))
			using(ZipArchive zip = new ZipArchive(fsOut, ZipArchiveMode.Create))
			{
				int res = resolution;
				int level = 0;
				while(true)
				{
					string name = string.Format(FontBank.FileMipName, level);
					var entry = zip.CreateEntry(name, CompressionLevel.Optimal);
					using(BinaryWriter bw = new BinaryWriter(entry.Open()))
					{
						bw.Write(imageBytes);
					}
					level++;
					res = res / 2;
					if(res == 0)
						break;

					byte[] mip = new byte[res * res];
					for(int i = 0; i < mip.Length; i++)
					{
						int x = (i % res) * 2;
						int y = (i / res) * 2;
						int value = 0;
						value += imageBytes[y * res * 2 + x];
						value += imageBytes[y * res * 2 + (x + 1)];
						value += imageBytes[(y + 1) * res * 2 + x];
						value += imageBytes[(y + 1) * res * 2 + (x + 1)];
						mip[i] = (byte)(value / 4);
					}
					imageBytes = mip;
				}

				BitmapFontDataSerializable data = new BitmapFontDataSerializable();
				data.BitmapResolution = resolution;
				data.CharactersRegular = characters[0].Values.ToList();
				data.CharactersBold = characters[1].Values.ToList();
				data.CharactersItalic = characters[2].Values.ToList();
				data.CharactersBoldItalic = characters[3].Values.ToList();

				var xmlEntry = zip.CreateEntry(FontBank.FileXmlName, CompressionLevel.Optimal);
				using(var stream = xmlEntry.Open())
				{
					SerializeData(data, stream);
				}
			}
		}

		static void SerializeData(BitmapFontDataSerializable data, Stream stream)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(BitmapFontDataSerializable));
			TextWriter writer = new StreamWriter(stream);
			serializer.Serialize(writer, data);
		}

	}
}
