﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Main;
using Engine.Renderer;
using GameFramework;

namespace Antizem
{
	class Program
	{
		static void Main(string[] args)
		{
			StartEngine();
		}

		static void StartEngine()
		{
			const bool debug = true;

			var textOutput = TextOutput.CreateCommon();
			var fileManager = new FileManagerZip(textOutput, "");
			var viewport = new CommonWindow(1280, 720, debug);
			var renderer = new Renderer(textOutput, fileManager, debug);
			Engine.Main.Engine e = new Engine.Main.Engine(viewport, textOutput, fileManager, renderer);
			e.Inicialize();
			const bool debugFrameLimit = false;
			e.Run(debugFrameLimit);
		}
	}
}
