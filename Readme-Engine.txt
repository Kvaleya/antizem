Antizem Engine by Jakub Pelc (jakub.pelc@email.cz)

Antizem is a C# and OpenGL based game engine. It is not intended to be a generic game engine such as Unreal, Unity or Godot, but rather a purpose build but extensible platform for first person games with large, detailed worlds that require data streaming. 

Major goals for the engine:
	- have fun and learn stuff by writing it :)
	- realtime lighting and world editing
	- support very large worlds
		- memory management
		- streaming
		- avoid floating-point errors
	- use the most efficient rendering possible under OpenGL

The current intent is to support two defining features:
	- planet-sized worlds?
	- portals?

	Project Structure


The engine is separated into many small libraries to keep the dependencies sensible. The libraries should follow a layered structure, where each layer is more specialized that the one below:

Main layer:
	The game itself - custom entity systems and such
	Engine.Main - connects everything together
Engine systems layer:
	Engine.Meshes...
	Engine.Renderer - interacts with OpenGL, uses Glob
Engine common layer:
	Engine.Common - provides objects that the upper layers can use to communicate with each other, for example scene objects that can be passed from engine systems to the renderer without a direct reference to the renderer
Base layer:
	GameFramework - functionality useful for making any game, such as a virtual file system or a logging system
	Glob - wraps around some OpenGL objects and provides useful functionality, such as shader runtime recompiling


	The engine and the renderer


The engine is started by creating an instance of Engine object and then calling the Inicialize and Run methods. Engine instance requires a reference to several object: IFileManager (virtual filesystem), TextOutput (logging), IEngineViewport (the window into which the video output will be rendered - also needed to get mouse position relative to the window and to detect whether the engine is focused or not) and IRenderer (takes care of creating the video output).
The Engine class will also create a Context object that will be passed to other engine systems. This object contains everything a system might need: IFileManager, Input states, frame times...
The Incialize method must be called from the thread that own the OpenGL context. It creates another thread in which the engine will run and incializes the renderer. The engine thread is simply an infinite while loop that calls the ProcessFrame method which calls all of the other systems and takes care of synchronization with the OpenGL thread.
The Run method must be called from the thread that own the OpenGL context. It simply starts the Engine thread and the rendering loop in the OpenGL thread. The rendering loop must be implemented in the IEngineViewport object. The IEngineViewport.StartMainLoop roughly corresponds to the OpenTK gamewindow Run() method, but it also takes the IRenderer and two ManualResetEvent objects as parameters.

The rendering loop and synchronization

The OpenGL graphics API has very limited support for multithreading, so in order to utilize multiple CPU cores the renderer (OpenGL) runs in a separate thread that the rest of the engine. While the renderer is submitting OpenGL commands to render frame 1, the engine is already processing frame 2. When both the renderer and the engine are done, the prepared frame 2 is passed to the renderer and the engine begins work on frame 3 while the renderer renders frame 2.
There are two ManualResetEvents: framePrepared and frameDone (AutoResetEvents could be used too). They are used this way:

Engine thread loop: update-systems wait-for-frameDone reset-frameDone update-renderer set-framePrepared
OpenGL thread loop: wait-for-framePrepared reset-framePrepared render-frame set-frameDone swapBuffers

DataInjector: passing data to the renderer

The renderer is kept separate from the rest of the engine systems (the systems do not reference the renderer), but various systems must be able to pass data to it (mostly scene information such as object transforms, particles, lights...). The classes that are passed to the renderer are defined in Engine.Common, a library that is referenced by both the engine systems and the renderer.

Let us imagine a Sun object that is passed from the engine to the renderer. It describes the position of the sun in the sky. In the renderer this data is needed for several things such as for sky rendering and for generating sun shadows. The sky class and shadows class need to have access to this data. This is achieved using a property with a special attribute in both classes:

[InjectProperty]
public Sun SunData { get; set; }

At the beginning of each frame, the up-to-date Sun object will be injected into all such properties in the renderer. This is achieved using the DataInjector. When a rendering system (a class instance) is created during the renderer incialization, it is first registered into the DataInjector, which will find all properties with the InjectProperty attribute in the system. When the frame is being prepared and the engine generates a new Sun object, it is passed to the DataInjector. When the frame is done preparing and when the renderer is waiting for a new frame (thus not processing any frame at the moment), the DataInjector will inject all the objects that were passed to it from the engine into the correct properties in the rendering systems.
The DataInjector object is part of the engine Context object and is created along with the Engine instance. It is passed to the renderer during engine incialization.


	Resource management


The ResourceManager is an abstract class that serves as the base of classes that manage resource loading and unloading based on memory budget and resource importance. The resources themselves should only be objects that have large data associated with them such as meshes and textures.

Resources

Every resource type is wrapped in a Resource class. For example a Mesh resource would be an instance of Resource<Mesh> class. The Mesh object itself may or may not actually exist (be loaded) within the Resource class.
An instance of a Resource is obtained using the ResourceManager.GetResource method that takes a single string as parameter. This string should be enough to describe any resource of that type (for examle a filename). The same Resource object is retured for every same string
All objects that requested a Resource are presumably located somewhere in the game world and have some level of importance. This importance should be mainly based on the distance from the player or camera. So an object that is close to the camera should generally be more important that an object that is far away. An object that is far away but is also very large should also be more important than a small object that is a medium distance away, because the large but distant object covers more screen area.
This importance value is represented by a float number that is usually called "distance" in the code. The GREATER the importance, the LOWER the distance is. An important object has low distance. This distance is used to determine which resources are to be loaded.
Every object that requested a Resource should also pass its distance to the Resource using SetDistance method (this method is also used for updating the distance). The Resource will internally keep a min-heap of these distances and the smallest of the distances can be queried. When an object is done using a Resource, it should call the RemoveDistance method to remove the distance.

ResourceManager

TODO


	Virtual texturing


This engine uses virtual texturing to pack most of the required textures into a single set of OpenGL objects. All textures are added into virtual texture space from which they are sampled. Every texture will always be available in at least 128x128 resolution to avoid generating composite tiles for smaller mip levels.

Alpha textures are stored in a separate (non-virtual) texture atlas. This atlas is used to render alpha-tested shadows, but also to render a depth-prepass of all alpha-tested geoemtry to avoid using the "discard" call in the expensive pixel shader and utilize early depth tests. This atlas uses a quad-tree like subdivision to allocate more space to textures that are more important. Same subdivision is (planned to be) used by the shadow map atlas.

Alpha textures are stored as a signed distance fields for increased quality.

Major advantage of using virtual texturing and shared vertex buffers is that all objects that use standart mesh format and standart material type can be rendered at once using a single MultiDraw call. For example, rendering the Crytek Sponza scene only requires three draw calls: one for all opaque geometry, one for a depth-prepass of alpha tested geometry and one to render the alpha-tested geometry.

	Culling

The AABB of every mesh is precomputed during mesh processing (to efficiently quantize vertex positions). This AABB is used to generate and approximate bounding sphere for the mesh. During rendering setup, every mesh instance is frustum culled on the CPU using this bounding sphere. Surviving instances are submitted to the renderer.

Surviving mesh instances are further culled on the GPU using compute shaders. Culling is first done on clusters of 256 triangles, and then the individual triangles of surviving clusters are culled. Cluster and triangle culling involves several test:
	- frustum - using cluster bounding spheres
	- occlusion - using mipmapped reprojected last frame's depth buffer
	- orientation - eliminates backfacing triangles and clusters (using cluster normal cone)
	- screen size (individual triangles only) - eliminates triangles that do not cover any pixels after rasterizaion

GPU culling is managed by the "MeshProcessor.cs" class.

To cull a set of mesh instances, they first need to be separated into "batches". A batch is nothing more that a collection of instances that use the same mesh format and the same shader. These instances may use different meshes, textures and materials, as long as the shader that is used to render them remains the same.

// TODO: Action<Batch> onBatchRender

A (ordered) list of batches is then passed to MeshProcessor, which then prepares it for culling and rendering. For each cluster of each instance, a cluster description structure is generated and later uploaded to a GPU buffer. It structure maps to a single thread of the cluster culling shader and to a single work group of the triangle culling shader.

The resulting clusters are also divided into several "MeshProcessBatch" items. One MeshProcessBatch corresponds to a segment of the input ordered batch list which uses the same vertex and index buffers. During triangle culling, single MeshProcessBatch maps to single compute dispatch, and binding the correct buffers and shader beforehand. Similarly, it maps to a single MultiDrawIndirect during rendering of the surviving triangles.

But since there are about 256 times less clusters than triangles, MeshProcessBatches do *not* map to different compute dispatches during cluster culling. Instead, all clusters for a set of input batches are processed with a single dispatch. In order to not mix clusters which belong to different MeshProcessBatches, each MeshProcessBatch has its own cluster "bucket", into which surviving clusters are written.

For each processed instance, enough space for all of its triangles is allocated in a shared culled index buffer. The pointer to this space is passed to the shaders in the cluster data structure. When allocating indices for instances, the buffer is assumed to be infinite. In practise, the buffer can only hold finite amount of indices, so culling is done in several passes. Each pass only processes a portion of all clusters, the size of wich is determined so that the shared index buffer does not overflow even in the worst case. The culled indices are then rendered, and another pass starts.

Surviving triangles are drawn using MultiDrawIndirect. Each instance has its own draw structure (instances of the same mesh may consist of different surviving triangles, so they cannot share a single instanced draw). The draw structure's index count starts out as zero, and is atomically increased as the triangle culling shader writes surviving indices for this draw.

The cluster structure consists of several items:
	- index of the instance this cluster belongs to - needed to transform its bounding sphere from object space to world space using the instance's transform
	- triangle count of this cluster - used when culling the cluster's triangles
	- flags - such as disabling orientation culling for this cluster
	- base vertex - used to fetch the correct vertex data during triangle processing
	- first index - used to fetch vertex indices during triangle processing
	- write destination offset - pointer to where to write suriving triangle's indices
	- cluster index - used to fetch cluster's bounding sphere and normal cone
	- cluster bucket index - pointer to where to write this cluster if it survives culling

Glob docs TODO:
PSOs
Shaders
VertexBufferFormats and Sources - dont use VAOs!






