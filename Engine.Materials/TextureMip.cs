﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using GameFramework;

namespace Engine.Materials
{
	public class TextureMip : IDisposable
	{
		byte[] _data;

		public byte[] Data { get { return _data; } }
		public int BlockSize { get; private set; }
		public int Width { get; private set; }
		public int Height { get; private set; }

		public int BlocksX { get { return (Width + 3) / 4; } }
		public int BlocksY { get { return (Height + 3) / 4; } }

		public TextureMip(IFileManager fileManager, IBcSurface surface, int mipLevel)
		{
			BlockSize = surface.BlockSize;
			Width = surface.Width;
			Height = surface.Height;

			for(int i = 0; i < mipLevel; i++)
			{
				Width = Math.Max(1, Width / 2);
				Height = Math.Max(1, Height / 2);
			}

			_data = surface.GetMipData(fileManager, mipLevel);
		}

		// Coordinates are in BC blocks, not texels! (BC block is 4x4 texels)
		public byte[] GetBlocksBytes(int offsetX, int offsetY, int width, int height)
		{
			byte[] blocks = new byte[width * height * BlockSize];

			int writeOffset = 0;

			for(int y = 0; y < height; y++)
			{
				for(int x = 0; x < width; x++)
				{
					CopyBlock(offsetX + x, offsetY + y, ref writeOffset, blocks);
				}
			}

			return blocks;
		}

		// Wraps around texture borders
		void CopyBlock(int x, int y, ref int offset, byte[] target)
		{
			x = Mod(x, BlocksX);
			y = Mod(y, BlocksY);

			int readStart = (BlocksX * y + x) * BlockSize;

			for(int i = 0; i < BlockSize; i++)
			{
				target[offset] = _data[readStart + i];
				offset++;
			}
		}

		int Mod(int x, int m)
		{
			int r = x % m;
			return r < 0 ? r + m : r;
		}

		public void Dispose()
		{
			_data = null;
		}
	}
}
