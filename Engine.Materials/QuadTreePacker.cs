﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;

namespace Engine.Materials
{
	public interface IQuadTreeItem
	{
		float AreaImportance { get; }
		int MaxSize { get; }

		void SetNode(int x, int y, int size);
		void SetNodeNull();
	}

	// TODO: make a 3D variant
	public class QuadTreePacker<T>
		where T : class, IQuadTreeItem
	{
		class Node
		{
			public int X;
			public int Y;
			public int Size;
			public int UsedNodes { get; private set; } = 0;

			Node[] _children = null;
			
			public Node[] Children { get { return _children; } }

			public Node(int size)
			{
				X = 0;
				Y = 0;
				Size = size;
			}

			public Node(Node parent, int index)
			{
				Size = parent.Size / 2;
				X = parent.X;
				Y = parent.Y;

				if((index & 0x1) > 0)
				{
					X += Size;
				}
				if((index & 0x2) > 0)
				{
					Y += Size;
				}
			}

			public void Subdivide()
			{
				_children = new Node[4];
				for(int i = 0; i < _children.Length; i++)
				{
					_children[i] = new Node(this, i);
				}
			}

			/// <summary>
			/// Removes redundant nodes and returns the number of used nodes including the children
			/// </summary>
			/// <param name="nodesKeepAlive">Hashset containing the nodes to keep alive. Any nodes whose children are in this set will also survive. If the hashset is null, all nodes will be collapsed.</param>
			/// <returns>How many subnodes are used</returns>
			public int CollapseRecursive(HashSet<Node> nodesKeepAlive)
			{
				UsedNodes = 0;

				if(_children != null)
				{
					for(int i = 0; i < _children.Length; i++)
					{
						UsedNodes += _children[i].CollapseRecursive(nodesKeepAlive);
					}

					if(UsedNodes == 0)
					{
						_children = null;
					}
				}

				if(nodesKeepAlive != null && nodesKeepAlive.Contains(this))
				{
					UsedNodes += 1;
				}

				return UsedNodes;
			}

			public Node FindNode(int x, int y)
			{
				if(x < X || x >= X + Size || y < Y || y >= Y + Size)
				{
					return null;
				}

				if(_children == null)
				{
					return this;
				}

				int index = 0;

				if(x >= X + Size / 2)
					index += 1;
				if(y >= Y + Size / 2)
					index += 2;

				return _children[index].FindNode(x, y);
			}
		}

		public int MaxItems { get; private set; }

		public int QuadTreeLevels { get; private set; }

		public int CellsX
		{
			get
			{
				if(_rootB == null)
				{
					return CellsY;
				}
				else
				{
					return CellsY * 2;
				}
			}
		}

		public int CellsY
		{
			get { return 1 << (QuadTreeLevels - 1); }
		}

		public int TotalCells
		{
			get { return CellsX * CellsY; }
		}

		public int SmallestSizeArea
		{
			get { return 1; }
		}

		Dictionary<T, Node> _usedNodes;
		Dictionary<Node, T> _nodeUsage;
		Dictionary<int, HashSet<T>> _itemsBySize;

		// Two roots, _rootB is used for rectangular 2:1 atlases
		// _rootB is null for square atlases
		Node _rootA, _rootB;

		// Setting rectangular to true will double the cell capacity for the same number of levels
		public QuadTreePacker(int levels, bool rectangular, int maxItems)
		{
			MaxItems = maxItems;
			QuadTreeLevels = levels;

			_rootA = new Node(CellsY);
			if(rectangular)
			{
				_rootB = new Node(CellsY);
				_rootB.X = CellsY;
			}

			_usedNodes = new Dictionary<T, Node>();
			_nodeUsage = new Dictionary<Node, T>();
			_itemsBySize = new Dictionary<int, HashSet<T>>();
			for(int i = 0; i < levels; i++)
			{
				_itemsBySize.Add(1 << i, new HashSet<T>());
			}
		}

		public T FindUsage(int x, int y)
		{
			Node a = _rootA.FindNode(x, y);
			Node b = _rootA.FindNode(x, y);

			Node found = a;

			if(a == null)
			{
				found = b;
			}

			if(found == null)
			{
				return null;
			}

			if(!_nodeUsage.ContainsKey(found))
			{
				return null;
			}

			return _nodeUsage[found];
		}

		public void Update(List<T> inputItems)
		{
			/*
			- clamp item number to a user-defined maximum
			- sort all items by area (radius / distance^2 for lights)
			- sum total area, normalize per-item area to a fraction of total area
			- assign cell size to each item
				- keep vars "remaining relative area" and "remaining physical area" (both range 0..1)
				- foreach item
					- find ideal physical area
					- subtract physical area from "remaining physical area"
					- subtract relative item area from "remaining relative area"
			*/

			// Order items by area from most important to least important
			var ordered = inputItems.OrderByDescending(x => x.AreaImportance).ToList();

			// Clamp item count to max allowed items
			int itemCount = Math.Min(MaxItems, ordered.Count);

			// Get the sum of the area of all items that survived the clamp
			float areaSum = 0.0f;
			for(int i = 0; i < itemCount; i++)
			{
				var item = ordered[i];
				areaSum += item.AreaImportance;
			}



			HashSet<Node> nodesKeepAlive = new HashSet<Node>();

			// Clear the dictionary
			foreach(var hashSet in _itemsBySize.Values)
			{
				hashSet.Clear();
			}

			float remainingItemArea = 1.0f;
			int currentSize = CellsY / 2; // Never use the root nodes
			int remainingCells = TotalCells;
			// Assign a size to each item
			for(int i = 0; i < itemCount; i++)
			{
				var item = ordered[i];

				if(item.MaxSize == 0)
					continue;

				// Normalize the item area to be a relative fraction of the total area of all items
				float itemArea = item.AreaImportance / areaSum;

				int useSize = Math.Min(currentSize, item.MaxSize);

				// Keep adding items with size set to currentSize, unless:
				// 1st condition:
				// Make sure that the remaining relative area after subtracting this item is not greater than the actual remaining area in the quadree
				// 2nd condition:
				// Make sure that all remaining items can fit with at least the smallest size
				while((remainingItemArea - itemArea) * TotalCells > remainingCells - useSize * useSize
					  || remainingCells - useSize * useSize < (itemCount - i - 1) * SmallestSizeArea)
				{
					// Use smaller size for all subsequent items
					currentSize = currentSize / 2;
					useSize = Math.Min(currentSize, item.MaxSize);
				}

				if(_usedNodes.ContainsKey(item))
				{
					var node = _usedNodes[item];
					if(node.Size == currentSize)
					{
						nodesKeepAlive.Add(node);
					}
					else
					{
						_nodeUsage.Remove(node);
						_usedNodes.Remove(item);
					}
				}

				remainingItemArea -= itemArea;
				remainingCells -= useSize * useSize;

				_itemsBySize[useSize].Add(item);
			}

			// All the items that didn't make it
			for(int i = itemCount; i < ordered.Count; i++)
			{
				var item = ordered[i];
				item.SetNodeNull();

				if(!_usedNodes.ContainsKey(item))
					continue;

				var node = _usedNodes[item];
				_nodeUsage.Remove(node);
				_usedNodes.Remove(item);
			}

			// Remove all nodes that will not be reused from last update
			_rootA.CollapseRecursive(nodesKeepAlive);
			_rootB?.CollapseRecursive(nodesKeepAlive);

			// Process the quadtree level by level
			// Assign a node to each item
			// Create more nodes per level if needed

			// Contains only nodes with zero usage, the smallest nodes are at the top of the heap
			var freeNodes = new Heap<int, Node>(-1);
			var nodesToProcess = new List<Node>();
			nodesToProcess.Add(_rootA);
			if(_rootB != null)
			{
				nodesToProcess.Add(_rootB);
			}

			int levelSize = _rootA.Size;
			while(levelSize > 0)
			{
				var items = _itemsBySize[levelSize];

				List<Node> usableNodes = new List<Node>();
				List<Node> collapseNodes = new List<Node>();

				// Find all nodes that can be used for items or subdivided into smaller nodes
				foreach(var node in nodesToProcess)
				{
					// If the node already has an item from last update, no action is done with the node
					if(nodesKeepAlive.Contains(node))
					{
						items.Remove(_nodeUsage[node]);
					}
					else
					{
						if(node.UsedNodes == 0)
						{
							usableNodes.Add(node);
						}
						else
						{
							collapseNodes.Add(node);
						}
					}
				}

				// First try to subdivide larger nodes
				while(usableNodes.Count < items.Count && freeNodes.Count > 0)
				{
					var freeNode = freeNodes.Top.Value;
					freeNodes.RemoveTop();

					if(freeNode.Size == levelSize)
					{
						usableNodes.Add(freeNode);
					}
					else
					{
						freeNode.Subdivide();
						for(int i = 0; i < freeNode.Children.Length; i++)
						{
							var child = freeNode.Children[i];
							freeNodes.Add(child.Size, child);
						}
					}
				}

				// If we still don't have enough usable nodes, collapse smaller nodes
				if(usableNodes.Count < items.Count)
				{
					// Prefer nodes that contain the lowest number of reusable subnodes
					// (do not cause items to change their nodes)
					collapseNodes = collapseNodes.OrderBy(x => x.UsedNodes).ToList();

					while(usableNodes.Count < items.Count && collapseNodes.Count > 0)
					{
						var collapse = collapseNodes[collapseNodes.Count - 1];
						collapseNodes.RemoveAt(collapseNodes.Count - 1);

						collapse.CollapseRecursive(null);

						usableNodes.Add(collapse);
					}
				}

				// Assign usable nodes to items
				while(usableNodes.Count > 0 && items.Count > 0)
				{
					var item = items.First();
					items.Remove(item);
					var node = usableNodes[usableNodes.Count - 1];
					usableNodes.RemoveAt(usableNodes.Count - 1);

					_nodeUsage[node] = item;
					_usedNodes[item] = node;
				}

				// All nodes are used but there are still items left - should never happen as long as there is a sensible limit on max item number
				if(items.Count > 0)
					throw new Exception("Cannot allocate enough nodes!");

				// Get all nodes left
				usableNodes.AddRange(collapseNodes);

				nodesToProcess.Clear();

				foreach(var node in usableNodes)
				{
					if(node.Children != null)
					{
						// Add all children nodes to the next level for processing
						nodesToProcess.AddRange(node.Children);
					}
					else
					{
						// Add nodes without children to the heap for subdivision
						freeNodes.Add(node.Size, node);
					}
				}

				levelSize = levelSize / 2;
			}

			// Assign nodes to the items
			foreach(var pair in _nodeUsage)
			{
				var node = pair.Key;

				pair.Value.SetNode(node.X , node.Y, node.Size);
			}
		}
	}
}
