﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using GameFramework;

namespace Engine.Materials
{
	public class TextureMipResourceManager : ResourceManager<TextureMip>
	{
		IFileManager _fileManager;
		TextOutput _textOutput;
		LazyCache<string, IBcSurface> _surfaceCache;
		WorkerThread _ioThread;

		public TextureMipResourceManager(long memoryBudgetBytes, IFileManager fileManager, TextOutput textOutput, WorkerThread ioThread) : base(memoryBudgetBytes)
		{
			_fileManager = fileManager;
			_textOutput = textOutput;
			_ioThread = ioThread;
			_surfaceCache = new LazyCache<string, IBcSurface>(s => BcSurfaceLoader.Load(s, _fileManager));
		}

		protected override void LoadResource(Resource<TextureMip> resource)
		{
			_ioThread.Enqueue(() => LoadMip(resource));
		}

		protected override void GetResourceSize(Resource<TextureMip> resource)
		{
			_ioThread.Enqueue(() =>
			{
				string filename;
				int mip;
				UnpackFilenameAndMip(resource.Name, out filename, out mip);

				var surface = _surfaceCache.GetItem(filename);
				OnResourceSizeKnown(resource, surface.GetMipSize(mip));
			});
		}

		public Resource<TextureMip> GetResource(string filename, int mip)
		{
			return GetResource(PackFilenameAndMip(filename, mip));
		}

		public IBcSurface GetSurface(string filename)
		{
			return _surfaceCache.GetItem(filename);
		}

		void LoadMip(Resource<TextureMip> resource)
		{
			string filename;
			int mip;
			UnpackFilenameAndMip(resource.Name, out filename, out mip);

			var surface = _surfaceCache.GetItem(filename);

			if(surface != null)
			{
				try
				{
					var texmip = new TextureMip(_fileManager, surface, mip);
					resource.Update(texmip);
				}
				catch(IndexOutOfRangeException e)
				{
					_textOutput.Print(OutputType.Warning, "Error on mip load: " + e.ToString());
					resource.Update(null);
				}
			}
		}

		static string PackFilenameAndMip(string filename, int mip)
		{
			return filename + " " + mip.ToString();
		}

		static void UnpackFilenameAndMip(string packed, out string filename, out int mip)
		{
			var split = packed.Split(' ');
			filename = split[0];
			mip = int.Parse(split[1]);
		}
	}
}
