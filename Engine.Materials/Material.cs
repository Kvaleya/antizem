﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Engine.Common;
using Engine.Common.Rendering;
using GameFramework;
using OpenTK;

namespace Engine.Materials
{
	public class MaterialInject
	{
		public Vector4 BaseColorOffset = Vector4.Zero;
		public Vector4 BaseColorScale = Vector4.One;
		public Vector4 MaterialOffset = Vector4.Zero;
		public Vector4 MaterialScale = Vector4.One;

		public string TextureBaseColor = String.Empty;
		public string TextureNormalRoughnessMetallic = String.Empty;
		public string TextureHeightAlpha = String.Empty;

		public int ImageWidth = 0;
		public int ImageHeight = 0;

		public float Distance = 0;
	}

	public class MaterialSerializable
	{
		public Vector4 BaseColorOffset = Vector4.Zero;
		public Vector4 BaseColorScale = Vector4.One;

		public float RoughnessOffset = 0f;
		public float RoughnessScale = 1f;

		public float MetallicOffset = 0f;
		public float MetallicScale = 1f;

		public string TextureBaseColor = String.Empty;
		public string TextureNormalRoughnessMetallic = String.Empty;
		public string TextureHeightAlpha = String.Empty;
	}

	class MaterialToken : IDisposable
	{
		MaterialManager _manager;
		string _name;

		public MaterialToken(MaterialManager manager, string name)
		{
			_manager = manager;
			_name = name;
		}

		public void Dispose()
		{
			_manager.FreeMaterial(_name);
		}
	}

	public class Material
	{
		const int NumSurfaces = 3;

		int _loaded = 0;
		Resource<MaterialToken> _token;
		bool _fromFile;

		public string FileString
		{
			get
			{
				if(_fromFile)
					return _token.Name;
				return "";
			}
		}

		public bool IsAlphaTest { get { return !string.IsNullOrEmpty(_textureHeightAlpha); } }

		public Vector4 BaseColorOffset = Vector4.Zero;
		public Vector4 BaseColorScale = Vector4.One;
		public Vector4 MaterialOffset = Vector4.Zero;
		public Vector4 MaterialScale = Vector4.One;

		string _textureBaseColor = String.Empty;
		string _textureNormalRoughnessMetallic = String.Empty;
		string _textureHeightAlpha = String.Empty;

		MaterialManager _manager;

		IBcSurface[] _surfaces = new IBcSurface[NumSurfaces];
		Resource<TextureMip>[][] _texturesMipsSlots = new Resource<TextureMip>[NumSurfaces][];

		public bool Loaded { get { return _loaded > 0; } }

		internal int Handle { get; set; } = -1;

		public int ID
		{
			get
			{
				if(!Loaded || Handle < 0)
					return 0;
				return Handle;
			}
		}

		public string TextureBaseColor
		{
			get { return _textureBaseColor; }
			set
			{
				_textureBaseColor = value;
				ChangeTexture(0, value);
			}
		}

		public string TextureNormalRoughnessMetallic
		{
			get { return _textureNormalRoughnessMetallic; }
			set
			{
				_textureNormalRoughnessMetallic = value;
				ChangeTexture(1, value);
			}
		}

		public string TextureHeightAlpha
		{
			get { return _textureHeightAlpha; }
			set
			{
				_textureHeightAlpha = value;
				ChangeTexture(2, value);
			}
		}

		public MaterialInject InjectMaterial
		{
			get
			{
				var inject = new MaterialInject()
				{
					BaseColorOffset = BaseColorOffset,
					BaseColorScale = BaseColorScale,
					MaterialOffset = MaterialOffset,
					MaterialScale = MaterialScale,
					TextureBaseColor = _textureBaseColor,
					TextureNormalRoughnessMetallic = _textureNormalRoughnessMetallic,
					TextureHeightAlpha = _textureHeightAlpha,
					Distance = this._token.TopDistance,
				};

				int w = 0, h = 0;

				for(int i = 0; i < _surfaces.Length; i++)
				{
					var s = _surfaces[i];
					if(s != null)
					{
						if(w == 0 && h == 0)
						{
							w = s.Width;
							h = s.Height;
						}
						else
						{
							if(w != s.Width || h != s.Height)
							{
								w = 0;
								h = 0;
								break;
							}
						}
					}
				}

				inject.ImageWidth = w;
				inject.ImageHeight = h;

				return inject;
			}
		}

		internal Material(MaterialManager manager, Resource<MaterialToken> token, bool loaded, bool fromFile)
		{
			_manager = manager;
			_token = token;
			_fromFile = fromFile;
			if(loaded)
				_loaded = 1;
		}

		internal void Load(IFileManager fileManager, string file)
		{
			MaterialSerializable data = null;

			try
			{
				if(fileManager.FileExists(file))
				{
					using(var stream = fileManager.GetStream(file))
					{
						XmlSerializer xmlSerializer = new XmlSerializer(typeof(MaterialSerializable));
						data = (MaterialSerializable)xmlSerializer.Deserialize(stream);
					}
				}
				else
				{
					throw new FileNotFoundException("Cannot load material file " + file);
				}
			}
			catch(Exception e)
			{
				this._manager.TextOutput.PrintException(OutputType.Warning, "Error loading material", e);
				Interlocked.Increment(ref _loaded);
				return;
			}

			this.BaseColorOffset = data.BaseColorOffset;
			this.BaseColorScale = data.BaseColorScale;
			this.MaterialOffset = new Vector4(data.RoughnessOffset, data.MetallicOffset, 0, 0);
			this.MaterialScale = new Vector4(data.RoughnessScale, data.MetallicScale, 1, 1);
			this.TextureBaseColor = data.TextureBaseColor;
			this.TextureNormalRoughnessMetallic = data.TextureNormalRoughnessMetallic;
			this.TextureHeightAlpha = data.TextureHeightAlpha;

			Interlocked.Increment(ref _loaded);
		}

		public void SaveToFile(string filename)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(MaterialSerializable));

			using(var stream = new FileStream(filename, FileMode.Create))
			{
				MaterialSerializable data = new MaterialSerializable();

				data.BaseColorOffset = BaseColorOffset;
				data.BaseColorScale = BaseColorScale;
				data.RoughnessOffset = MaterialOffset.X;
				data.MetallicOffset = MaterialOffset.Y;
				data.RoughnessScale = MaterialScale.X;
				data.MetallicScale = MaterialScale.Y;
				data.TextureBaseColor = TextureBaseColor;
				data.TextureNormalRoughnessMetallic = TextureNormalRoughnessMetallic;
				data.TextureHeightAlpha = TextureHeightAlpha;

				xmlSerializer.Serialize(stream, data);
			}
		}

		public void SetDistance(Object caller, float d)
		{
			_token.SetDistance(caller, d);
		}

		public void RemoveDistance(Object caller)
		{
			_token.RemoveDistance(caller);
		}

		internal void UpdateTextureDistances()
		{
			float dist = _token.TopDistance;

			if(!Loaded)
			{
				dist = float.PositiveInfinity;
			}

			for(int i = 0; i < _texturesMipsSlots.Length; i++)
			{
				var mips = _texturesMipsSlots[i];
				if(mips == null)
					continue;

				for(int j = 0; j < mips.Length; j++)
				{
					var mip = mips[j];

					if(j < 8)
					{
						mip.SetDistance(this, 0);
					}
					else
					{
						mip.SetDistance(this, dist / (1 << j));
					}
				}
			}
		}

		void ChangeTexture(int slot, string newTexture)
		{
			// Clear old texture resources
			var mips = _texturesMipsSlots[slot];
			if(mips != null)
			{
				foreach(var mip in mips)
				{
					mip.RemoveDistance(this);
				}
			}

			if(string.IsNullOrEmpty(newTexture))
			{
				_texturesMipsSlots[slot] = null;
				_surfaces[slot] = null;
				return;
			}

			var surface = _manager.MipManager.GetSurface(newTexture);
			_surfaces[slot] = surface;
			// TODO: ošetřit otevírání neexistujícího souboru
			Resource<TextureMip>[] newMips;
			if(surface != null)
			{
				newMips = new Resource<TextureMip>[surface.NumMips];
			}
			else
			{
				newMips = new Resource<TextureMip>[0];
			}

			for(int i = 0; i < newMips.Length; i++)
			{
				newMips[i] = _manager.MipManager.GetResource(newTexture, i);
			}

			_texturesMipsSlots[slot] = newMips;

			UpdateTextureDistances();
		}
	}

	class MaterialResourceManager : ResourceManager<MaterialToken>
	{
		MaterialManager _manager;

		public MaterialResourceManager(long maxMaterials, MaterialManager manager)
			: base(maxMaterials)
		{
			_manager = manager;
		}

		protected override void LoadResource(Resource<MaterialToken> resource)
		{
			resource.Update(new MaterialToken(_manager, resource.Name));
			_manager.AllocMaterial(resource.Name);
			OnResourceLoadFinished(resource);
		}

		protected override void GetResourceSize(Resource<MaterialToken> resource)
		{
			OnResourceSizeKnown(resource, 1);
		}
	}

	public class MaterialManager
	{
		LazyCache<string, Material> _cache;
		IFileManager _fileManager;
		MaterialResourceManager _resourceMananger;

		WorkerThread _ioThread;

		internal TextureMipResourceManager MipManager;
		internal TextOutput TextOutput;

		Stack<int> _freeIDs = new Stack<int>();
		readonly Material[] _materials;
		ConcurrentDictionary<string, Material> _nameMap = new ConcurrentDictionary<string, Material>();

		public int MaxMaterials { get { return _materials.Length; } }

		FileSystemWatcher _watcher;
		Object _fileWatcherSync;
		List<string> _changedFiles;

		public MaterialManager(IFileManager fileManager, TextOutput textOutput, WorkerThread ioThread, int maxTextureBytes, int maxMaterials = 256)
		{
			MipManager = new TextureMipResourceManager(maxTextureBytes, fileManager, textOutput, ioThread);
			TextOutput = textOutput;
			_ioThread = ioThread;

			_resourceMananger = new MaterialResourceManager(maxMaterials, this);
			_fileManager = fileManager;
			_cache = new LazyCache<string, Material>(name => CreateMaterial(name, false));

			for(int i = maxMaterials - 1; i > 0; i--)
			{
				_freeIDs.Push(i);
			}

			_materials = new Material[maxMaterials];
			var defaultMaterial = this.AddOrGetMaterial("DefaultMaterial");
			defaultMaterial.SetDistance(this, -1);
			defaultMaterial.BaseColorOffset = new Vector4(0.5f, 0.5f, 0.5f, 0.0f);
			defaultMaterial.Handle = 0;
			_materials[0] = defaultMaterial;
			_resourceMananger.Update();

			StartFileWatcher("Materials");
		}

		///// <summary>
		///// Loads a material from file, material may not be available for some time
		///// </summary>
		///// <param name="name"></param>
		///// <returns></returns>
		//public Material GetMaterial(string name)
		//{
		//	return _cache.GetItem(name);
		//}

		/// <summary>
		/// Returns a blank material that can be modified right away
		/// </summary>
		/// <returns></returns>
		public Material AddOrGetMaterial(string name, bool fromFile = false)
		{
			return _cache.AddOrGet(name, () => CreateMaterial(name, fromFile));
		}

		public Resource<TextureMip> GetMip(string file, int level)
		{
			if(string.IsNullOrEmpty(file))
				return null;

			return MipManager.GetResource(file, level);
		}

		public IBcSurface GetSurface(string file)
		{
			return MipManager.GetSurface(file);
		}

		Material CreateMaterial(string name, bool fromFile)
		{
			if(fromFile)
			{
				name = CleanupPath(name);
			}

			var token = _resourceMananger.GetResource(name);
			var material = new Material(this, token, !fromFile, fromFile);

			if(fromFile)
			{
				_ioThread.Enqueue(() => material.Load(_fileManager, name));
			}

			_nameMap.TryAdd(name, material);

			return material;
		}

		internal void AllocMaterial(string name)
		{
			var material = _nameMap[name];
			if(material.Handle < 0)
			{
				material.Handle = _freeIDs.Pop();
				_materials[material.Handle] = material;
			}
		}

		internal void FreeMaterial(string name)
		{
			var material = _nameMap[name];
			_freeIDs.Push(material.Handle);
			_materials[material.Handle] = null;
			material.Handle = -1;
			material.UpdateTextureDistances();
		}

		public void Update(DataInjector rendererInjector)
		{
			// Update changed material files
			// TODO: is this thread safe?
			if(_changedFiles.Count > 0)
			{
				lock(_fileWatcherSync)
				{
					foreach(var changedFile in _changedFiles)
					{
						var path = CleanupPath(changedFile);
						Material m;
						if(_nameMap.TryGetValue(path, out m))
						{
							m.Load(_fileManager, path);
						}
						else
						{
							TextOutput.Print(OutputType.MinorWarning, "Failed to update material from file: " + path);
						}
					}

					_changedFiles.Clear();
				}
			}

			foreach(var material in _materials)
			{
				material?.UpdateTextureDistances();
			}

			_resourceMananger.Update();
			MipManager.Update();

			MaterialData inject = new MaterialData();
			var injectMaterials = new MaterialInject[_materials.Length];
			inject.InjectMaterials = injectMaterials;

			for(int i = 0; i < _materials.Length; i++)
			{
				var material = _materials[i];

				if(material == null)
					continue;

				injectMaterials[i] = material.InjectMaterial;
			}

			rendererInjector.AddData(inject);
		}

		void StartFileWatcher(string path)
		{
			_fileWatcherSync = new Object();
			_changedFiles = new List<string>();

			_watcher = new FileSystemWatcher(path, "*");
			_watcher.IncludeSubdirectories = true;
			_watcher.Changed += (sender, args) =>
			{
				OnFileChanged(args.FullPath);
			};
			_watcher.Renamed += (sender, args) =>
			{
				OnFileChanged(args.FullPath);
				OnFileChanged(args.OldFullPath);
			};

			_watcher.EnableRaisingEvents = true;
		}

		void OnFileChanged(string fullpath)
		{
			Thread.Sleep(10);
			lock(_fileWatcherSync)
			{
				_changedFiles.Add(fullpath);
			}
		}

		string CleanupPath(string p)
		{
			return p.ToLowerInvariant().Replace('\\', '/').Replace("//", "/");
		}
	}
}
