﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Meshes;
using GameFramework;

namespace MeshConverter
{
	class Program
	{
		static void Main(string[] args)
		{
			var output = TextOutput.CreateCommon("logMeshConverter");

			output.Print(OutputType.Notify, "KvEngine/Antizem mesh converter");

			// Resource manager test
			/*
			Object meshOwner = new Object();

			int id = 0;

			Dictionary<int, Resource<MeshBinary>> resources = new Dictionary<int, Resource<MeshBinary>>();

			var fileManager = new FileManagerZip(output, "");
			MeshBinaryResourceManager resourceManager = new MeshBinaryResourceManager(fileManager, output, 400000);

			output.Print("Usage: load [filename]; kill [id]");

			while(true)
			{
				double percent = Math.Round((double)resourceManager.MemoryBytesUsed / resourceManager.MemoryBytesBudget, 4) * 100;
				output.Print(percent + "% " + resourceManager.MemoryBytesUsed + " / " + resourceManager.MemoryBytesBudget);

				foreach(var resource in resources)
				{
					output.Print("Resource: " + resource.Key + ": " + (resource.Value.Item?.FileName ?? "not loaded"));
				}

				var command = Console.ReadLine().Split(' ');

				if(command.Length > 1)
				{
					if(command[0] == "load")
					{
						var mesh = resourceManager.GetResource(Utils.Meshes.MeshFileAndNameCombine(command[1], ""));
						mesh.SetDistance(meshOwner, 0);
						resources.Add(id++, mesh);
					}
					if(command[0] == "kill")
					{
						var mesh = resources[int.Parse(command[1])];
						mesh.RemoveDistance(meshOwner);
					}
				}

				resourceManager.Update();
			}
			
			return;
			*/

			// Default meshes
			//ProcessInput("cube.obj cube.zip true 90", output);
			//ProcessInput("teapot.obj teapot.zip true 45", output);
			//ProcessInput("buddha_lod4.obj buddha_lod4.zip true 45", output);
			//ProcessInput("buddha_lod10.obj buddha_lod10.zip true 45", output);
			//ProcessInput("sponza.obj sponza.zip false 0", output);

			while(true)
			{
				output.Print(OutputType.Notify, "Usage: sourceFilePath [dest=destinationFilePath] [keepnormals] [smoothangle=angle(0 .. 90)] [indices32]");
				output.Print(OutputType.Notify, "Example:");
				output.Print(OutputType.Notify, "teapot.obj dest=teapot_processed.zip keepnormals");

				ProcessInput(Console.ReadLine(), output);
			}
		}

		static void ProcessInput(string line, TextOutput output)
		{
			var input = line.Split(' ');

			string fileIn, fileOut;
			bool genNormals = true;
			float smoothAngle = 45;
			bool forceIndices32 = false;

			if(input.Length < 1)
			{
				InvalidInputMessage(output);
				return;
			}
			fileIn = input[0];

			var ext = Path.GetExtension(fileIn);
			fileOut = fileIn.Substring(0, fileIn.Length - ext.Length) + ".zip";

			if(!File.Exists(fileIn))
			{
				output.Print(OutputType.Warning, "Input file does not exist!");
				return;
			}

			bool quit = false;

			Action<string> error = (s) =>
			{
				InvalidInputMessage(output, s);
				quit = true;
			};

			for(int i = 1; i < input.Length; i++)
			{
				var text = input[i].ToLowerInvariant();

				if(text.Length == 0)
					continue;

				var split = text.Split('=');

				if(split.Length == 2 && split[0] == "smoothangle")
				{
					float angle = 0;
					if(float.TryParse(split[1], NumberStyles.Float, CultureInfo.InvariantCulture, out angle))
					{
						smoothAngle = angle;
					}
					else
					{
						error(text);
					}
					continue;
				}

				if(split.Length == 2 && split[0] == "dest")
				{
					fileOut = split[1];
					continue;
				}

				if(text == "keepnormals")
				{
					genNormals = false;
					continue;
				}

				if(text == "indices32")
				{
					forceIndices32 = true;
					continue;
				}

				error(text);
			}

			if(quit)
			{
				output.Print(OutputType.Notify, "Aborted due to invalid arguments");
				return;
			}

			smoothAngle = (float)Math.PI * (smoothAngle / 180);

			string report;

			var meshes = ObjLoader.LoadMeshes(new StreamReader(fileIn), fileIn, out report);

			if(!string.IsNullOrEmpty(report))
			{
				output.Print(OutputType.Warning, report);
				return;
			}

			var meshesBinary = MeshBinary.FromMeshGroup(meshes, genNormals, smoothAngle, forceIndices32);

			MeshBinary.Save(meshesBinary, new FileStream(fileOut, FileMode.Create));

			output.Print(OutputType.Notify, "Done!");
		}

		static void InvalidInputMessage(TextOutput t)
		{
			t.Print(OutputType.Warning, "Invalid input");
		}

		static void InvalidInputMessage(TextOutput t, string message)
		{
			t.Print(OutputType.Warning, "Invalid input: " + message);
		}
	}
}
