﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;

namespace Engine.Renderer.FrameGraph
{
	[Flags]
	public enum RenderPassFlags
	{
		Compute = 1,
		SideEffects = 2,
	}

	public class FrameGraphBuilder<TContext>
		where TContext : class, IFrameGraphContext
	{
		TContext _context;
		ObjectPool<RenderPass> _renderPassPool = new ObjectPool<RenderPass>();
		RenderPass _currentPass;

		public void AddRenderPass<TData>(string passName, Action<TData, FrameGraphBuilder<TContext>> setup, Action<TData, TContext> execute)
			where TData : class, new()
		{
			TData data = new TData();

			var pass = _renderPassPool.Take();
			pass.Name = passName;

			// Start pass dependency build
			BeginBuild(pass);
			setup.Invoke(data, this);
			EndBuild(pass);
			// End build

			
			pass.ActionExecute = () =>
			{
				execute.Invoke(data, _context);
			};
		}

		void BeginBuild(RenderPass pass)
		{
			_currentPass = pass;
		}

		void EndBuild(RenderPass pass)
		{
			_currentPass = null;
		}

		public FrameGraphResource ReadResource(FrameGraphResource resource)
		{
			_currentPass.ResourcesRead.Add(resource);
			return resource;
		}

		public FrameGraphResource WriteResource(FrameGraphResource resource)
		{
			_currentPass.ResourcesWrite.Add(resource);
			return new FrameGraphResource()
			{
				Name = resource.Name,
				Version = resource.Version + 1,
				Handle = resource.Handle,
			};
		}
	}
}
