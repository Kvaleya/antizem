﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;

namespace Engine.Renderer.FrameGraph
{
	class RenderPass : IClearable
	{
		public string Name;
		public Action ActionExecute;

		public List<FrameGraphResource> ResourcesRead = new List<FrameGraphResource>();
		public List<FrameGraphResource> ResourcesWrite = new List<FrameGraphResource>();

		public void Clear()
		{
			Name = null;
			ActionExecute = null;
			ResourcesRead.Clear();
			ResourcesWrite.Clear();
		}
	}
}
