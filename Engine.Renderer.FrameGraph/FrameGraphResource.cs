﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Renderer.FrameGraph
{
	public class FrameGraphResource
	{
		public string Name { get; internal set; }
		public int Version { get; internal set; }
		public int Handle;
	}
}
