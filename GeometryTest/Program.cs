﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Csg;
using Engine.Csg.Brushes;
using GameFramework;
using OpenTK;

namespace GeometryTest
{
	class Program
	{
		static void Main(string[] args)
		{
			TextOutput output = new TextOutput("log", 8);
			output.PrintEvent += GameFramework.TextOutput.OnPrintConsole;

			// TODO: move matrix utils form Glob to EngineCommon
			var cube = new CubeBrush(new Mat4x3(Glob.Utils.GetWorldMatrix(new Vector3(0.5f, 1.0f, 0.2f), Quaternion.FromAxisAngle(Vector3.UnitX, 0.5f), Vector3.One)));
			var cube2 = new CubeBrush(new Mat4x3(Matrix4.Identity));

			var basic = cube.Triangulate();
			basic.AddRange(cube2.Triangulate());

			WriteObj("csgBase.obj", basic);

			var finalBrush = new UnionBrush(cube, cube2);

			List<TriangleFrac> finalMesh = new List<TriangleFrac>();

			int tests = 100;

			Stopwatch sw = new Stopwatch();
			sw.Start();
			for(int i = 0; i < tests; i++)
			{
				finalMesh = finalBrush.Triangulate();
			}
			sw.Stop();

			Console.WriteLine(sw.ElapsedMilliseconds / (double)tests);

			WriteObj("csgResult.obj", finalMesh);

			//while(true)
			//{
			//	Console.WriteLine("Enter 6 vertices");
			//	Vec3[] verts = new Vec3[6];
			//	for(int i = 0; i < 6; i++)
			//	{
			//		var line = Console.ReadLine().Split(' ');
			//		verts[i] = new Vec3(int.Parse(line[0]), int.Parse(line[1]), int.Parse(line[2]));
			//	}

			//	TriangleFrac tri0 = new TriangleFrac(verts[0], verts[1], verts[2]);
			//	TriangleFrac tri1 = new TriangleFrac(verts[3], verts[4], verts[5]);

			//	Console.WriteLine(GeometryUtils.TrianglesIntersect(tri0, tri1));
			//}

			Console.WriteLine("Done!");

			Console.ReadLine();
		}

		static void WriteObj(string filename, List<TriangleFrac> mesh)
		{
			List<Vec3> v = new List<Vec3>();
			List<int> indices = new List<int>();
			Dictionary<Vec3, int> vertsLookup = new Dictionary<Vec3, int>();

			foreach(TriangleFrac triangle in mesh)
			{
				if(vertsLookup.ContainsKey(triangle.A))
				{
					indices.Add(vertsLookup[triangle.A]);
				}
				else
				{
					indices.Add(v.Count);
					vertsLookup.Add(triangle.A, v.Count);
					v.Add(triangle.A);
				}

				if(vertsLookup.ContainsKey(triangle.B))
				{
					indices.Add(vertsLookup[triangle.B]);
				}
				else
				{
					indices.Add(v.Count);
					vertsLookup.Add(triangle.B, v.Count);
					v.Add(triangle.B);
				}

				if(vertsLookup.ContainsKey(triangle.C))
				{
					indices.Add(vertsLookup[triangle.C]);
				}
				else
				{
					indices.Add(v.Count);
					vertsLookup.Add(triangle.C, v.Count);
					v.Add(triangle.C);
				}
			}

			for(int i = 0; i < indices.Count; i++)
			{
				indices[i]++;
			}

			using(StreamWriter sw = new StreamWriter(filename))
			{
				foreach(Vec3 vert in v)
				{
					sw.WriteLine("v " + vert.X.ToDouble().ToString(CultureInfo.InvariantCulture) + " " + vert.Y.ToDouble().ToString(CultureInfo.InvariantCulture) + " " + vert.Z.ToDouble().ToString(CultureInfo.InvariantCulture));
				}

				for(int i = 0; i < indices.Count; i += 3)
				{
					sw.WriteLine("f " + indices[i] + " " + indices[i + 1] + " " + indices[i + 2]);
				}
			}
		}
	}
}
