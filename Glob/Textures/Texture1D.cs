﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Glob
{
	public class Texture1D : Texture
	{
		public int Width { get { return base.StorageWidth; } }

		/// <summary>
		/// Creates a Texture1D using immutable storage.
		/// </summary>
		/// <param name="device">Glob device</param>
		/// <param name="name">Debug label</param>
		/// <param name="format">Texture internal format</param>
		/// <param name="width">Width in texels</param>
		/// <param name="levels">Number of mip level. Special values: 1 for the base level only, 0 to compute number of mip levels automatically from texture dimensions.</param>
		public Texture1D(Device device, string name, SizedInternalFormatGlob format, int width, int levels = 0)
			: base(TextureTarget.Texture1D, name)
		{
			device.BindTexture(Target, Handle);
			TexStorage1D(format, width, levels);
		}

		public Texture1D(Device device, string name, SizedInternalFormatGlob format, Texture origTexture, int minLevel, int numLevels, int minLayer = 0)
			: base(TextureTarget.Texture2D, name)
		{
			// Texture view - no need to bind
			TextureView(origTexture, format, minLevel, numLevels, minLayer, 1);
		}
	}
}
