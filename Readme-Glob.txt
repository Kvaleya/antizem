﻿		Glob

Glob, in previous KvEngine iterations named KvGlob (Kv's OpenGL Objects), is a helper library for C# projects using OpenGL and OpenTK. Its primary purpose is to make writing OpenGL code easier and faster. It is NOT a complete abstraction of OpenGL, but it rather provides objects that wrap around certain OpenGL functionality and provide extra features. An example is the Glob's shader system that enables shader caching and runtime recompilation.

Required OpenGL extensions:
	ARB_separate_shader_objects
	ARB_buffer_storage
	ARB_texture_storage


		GlobContext

This is the main class of the Glob library. It serves as an interface for other Glob object to communicate with instances of other systems, currently the only usage is to get shaders.
The Update method should be called at the beginning of every frame. It is used to update various Glob systems that are not visible to outside code such as the shader system.


		Shader system

		
Objects:
	ResolvedShader
	Shader
	ShaderPipeline
	ShaderRepository
	ShaderSource
	UniformValue

The Glob shader system is used to load and compile shaders, to recompile shaders at runtime (if changes in the shader source code are detected) and to cache the resulting shader objects to avoid duplication.

Each shader is compiled individually. Shaders are then linked into program pipelines (using the extension GL_ARB_separate_shader_objects). Each shader object also remembers the values of all uniform variables and all buffer bindings. This information is used to avoid redundant glUniform calls and to rebind all the uniforms if the underlying OpenGL shader object is recreated (due to shader runtime recompilation).

The shader system also adds support for #include macro in glsl code. This is done purely by the library and does not depend upon any OpenGL extension.

ShaderRepository

This is the central class of the shader system. It keeps track of all other shader related objects. Shader source updates are also handled primarily here.

A shader can be acquired by calling GetShader() method. Parameters include the shader filename and a list of macros to define (usage example: generating ubershader permutations).

ShaderSource

This class represent a single file of GLSL source code. It contains methods for parsing the #include statements in the source and for resolving the dependency graph (of included files). First, all #include and #version statements are found and commented out. When resolving the shader source, all required source code is inserted to the beginning of the file in the correct order. A ResolvedShader object is generated.

The resolved source code is written out by the ShaderRepository for shader debugging purposes.

This shader system was originally designed for Vulkan. The resolved source would be written to the filesystem and then compiled using Glslang. The idea was to NOT use a custom Glslang based library for shader compiling, as this solution would be hard to maintain since the engine language is C# and using Glslang programatically would require a wrapper library. Instead the resolved source files would be compiled using the Glslang command line tool (started from C#) and when a new version of Glslang is released, all that would need to be done is to replace the Glslang exe with the new one. (instead of recompiling some sort of wrapper library)

ResolvedShader

This object only contains the GLSL source code with resolved dependencies and the information at which line does each included file start in the resolved source. This information is used when a GLSL error is generated when compiling the shader to translate the line number in the resolved source to the correct line number in the file from where the line originated.

Shader

This is the final shader object. It is a single shader stage that can be linked into a program pipeline with other shader stages.




