﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Ecs
{
	class EntityTypeKey
	{
		readonly int _hash;
		readonly bool[] _data;

		// https://stackoverflow.com/questions/11527876/using-a-boolean-array-as-custom-dictionary-key
		public EntityTypeKey(bool[] source)
		{
			_data = new bool[source.Length];
			Array.Copy(source, _data, source.Length);
			_hash = ComputeHash();
		}

		int ComputeHash()
		{
			int result = 29;
			for(int i = 0; i < _data.Length; i++)
			{
				if(_data[i])
				{
					result++;
				}
				result *= 23;
			}
			return result;
		}

		protected bool Equals(EntityTypeKey other)
		{
			return _data.SequenceEqual(other._data);
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			if(ReferenceEquals(this, obj)) return true;
			if(obj.GetType() != this.GetType()) return false;
			return Equals((EntityTypeKey) obj);
		}

		public override int GetHashCode()
		{
			return _hash;
		}

		public static bool IsEntityCompatibleWithSystem(EntityTypeKey entity, EntityTypeKey system)
		{
			if(entity._data.Length != system._data.Length)
				throw new Exception("This should never happen.");
			for(int i = 0; i < system._data.Length; i++)
			{
				// Entity must have all components the system needs
				if(system._data[i] && !entity._data[i])
					return false;
			}
			return true;
		}
	}
}
