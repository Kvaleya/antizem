﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;

namespace Engine.Ecs
{
	public abstract class EntitySystem
	{
		internal SystemData Data;
		public EntityManager Manager { get; internal set; }

		public EntitySystem()
		{
			Data = new SystemData();
		}

		protected internal virtual void OnEntityAdded(Entity entity)
		{
			
		}

		protected internal virtual void OnEntityRemoved(Entity entity)
		{

		}
	}
}
