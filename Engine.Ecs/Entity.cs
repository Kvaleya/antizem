﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Ecs
{
	public abstract class Entity
	{
		internal readonly Component[] Components;
		internal readonly EntityTypeKey Key;
		protected readonly EntityManager _manager;

		public Entity(EntityManager manager, params Component[] components)
		{
			_manager = manager;
			Components = new Component[_manager.NumComponents];
			bool[] hasComponent = new bool[Components.Length];
			foreach(Component component in components)
			{
				if(component == null)
					continue;
				Components[_manager.GetComponentTypeID(component.GetType())] = component;
			}
			for(int i = 0; i < Components.Length; i++)
			{
				hasComponent[i] = Components[i] != null;
			}
			Key = new EntityTypeKey(hasComponent);
			//_manager.RegisterEntity(this); // Entities must be explicitly added
		}

		public T GetComponent<T>()
			where T : Component
		{
			return (T)Components[_manager.GetComponentTypeID(typeof(T))];
		}
	}
}
