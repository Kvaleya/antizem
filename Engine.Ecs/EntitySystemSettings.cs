﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Ecs
{
	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
	public class EntitySystemSettings : Attribute
	{
		internal ExecutionStyle Execution { get; private set; }
		internal readonly int MinInstancesForParallel;

		/// <summary>
		/// Describes secondary system properties
		/// </summary>
		/// <param name="executionStyle">Controls whether entities can be processed in parallel by this system. ParallelBlanaced uses Parallel.For, which is useful for long or varying execution time per entity, while ParallelRanged is more suited for short execution time per entity.</param>
		/// <param name="minInstancesForParallel">Minimal number of entities required to use parallel processing. If the number of entities is lower, sequential processing will be used instead.</param>
		public EntitySystemSettings(ExecutionStyle executionStyle, int minInstancesForParallel = 1)
		{
			Execution = executionStyle;
			MinInstancesForParallel = minInstancesForParallel;
		}
	}

	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
	public class EntitySystemExecuteAfter : Attribute
	{
		internal readonly Type[] Systems;

		public EntitySystemExecuteAfter(params Type[] systems)
		{
			Systems = systems;
		}
	}

	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
	public class EntitySystemExecuteBefore : Attribute
	{
		internal readonly Type[] Systems;

		public EntitySystemExecuteBefore(params Type[] systems)
		{
			Systems = systems;
		}
	}
}
