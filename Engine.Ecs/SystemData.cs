﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;

namespace Engine.Ecs
{
	class SystemData
	{
		internal EntityTypeKey Key;
		internal Action<object, Entity> ProcessMethod;
		internal HashedOrderlessList<Entity> Entities;
		internal EntitySystemSettings Settings;
		internal List<Type> Dependencies; // Systems to execute before this one - contains systems from this system's executeAfter and all systems that contain this system in their executeBefore
		internal Type[] ExecuteBefore;
		internal Type[] ExecuteAfter;
		internal int Order;
	}
}
