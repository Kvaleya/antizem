﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Ecs
{
	public enum ExecutionStyle
	{
		/// <summary>
		/// All entities are executed by a single thread in sequence
		/// </summary>
		Sequential,
		/// <summary>
		/// Process every entity with a separate task, moderate per entity overhead.
		/// Use for expensive entities.
		/// </summary>
		ParallelBalanced,
		/// <summary>
		/// Use several tasks (one per hardware thread) that loop over a segment of all entities
		/// Use for many cheap entities
		/// </summary>
		ParallelRanged,
	}
}
