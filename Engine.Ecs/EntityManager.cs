﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;

namespace Engine.Ecs
{
	public class EntityManager
	{
		LazyCache<EntityTypeKey, List<EntitySystem>> _entityTypeToSystemsMap;
		List<Assembly> _assemblies = new List<Assembly>();
		Lazy<int> _numComponents;
		Dictionary<Type, int> _componentTypeIDs = new Dictionary<Type, int>();
		List<EntitySystem> _systems = new List<EntitySystem>();
		ProducerConsumerBag<Entity> _entitiesToAdd = new ProducerConsumerBag<Entity>();
		ProducerConsumerBag<Entity> _entitiesToRemove = new ProducerConsumerBag<Entity>();
		HashSet<Entity> _allEntities = new HashSet<Entity>();

		/// <summary>
		/// Controls the number of threads to be used for parallel entity processing. Set to zero to use system thread count.
		/// </summary>
		int ThreadCount = 0;

		internal int NumComponents
		{
			get { return _numComponents.Value; }
		}

		public EntityManager()
		{
			_numComponents = new Lazy<int>(() =>
			{
				int count = 0;
				foreach(Assembly assembly in _assemblies)
				{
					count +=
						assembly.GetTypes()
							.Count(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(Component)));
				}
				return count;
			});

			_entityTypeToSystemsMap = new LazyCache<EntityTypeKey, List<EntitySystem>>(type =>
			{
				List<EntitySystem> systems = new List<EntitySystem>();

				foreach(EntitySystem system in _systems)
				{
					if(EntityTypeKey.IsEntityCompatibleWithSystem(type, system.Data.Key))
					{
						systems.Add(system);
					}
				}

				return systems;
			});
		}

		internal int GetComponentTypeID(Type type)
		{
			return _componentTypeIDs[type];
		}

		/// <summary>
		/// Not thread safe
		/// </summary>
		/// <param name="system"></param>
		public void AddSystem(EntitySystem system)
		{
			_systems.Add(system);
		}

		public void RegisterAssembly(Type type)
		{
			RegisterAssembly(Assembly.GetAssembly(type));
		}

		public void RegisterAssembly(Assembly assembly)
		{
			_assemblies.Add(assembly);
		}

		private void RegisterEntity(Entity entity)
		{
			var systems = _entityTypeToSystemsMap.GetItem(entity.Key);

			_allEntities.Add(entity);

			foreach(EntitySystem system in systems)
			{
				system.Data.Entities.Add(entity);
				system.OnEntityAdded(entity);
			}
		}

		private void DestoryEntity(Entity entity)
		{
			var systems = _entityTypeToSystemsMap.GetItem(entity.Key);

			_allEntities.Remove(entity);

			foreach(EntitySystem system in systems)
			{
				system.Data.Entities.Remove(entity);
				system.OnEntityRemoved(entity);
			}
		}

		public void AddEntity(Entity entity)
		{
			_entitiesToAdd.Add(entity);
		}

		public void RemoveEntity(Entity entity)
		{
			_entitiesToRemove.Add(entity);
		}

		public void Inicialize(Type genericArgumentsType)
		{
			InitComponents();
			InitSystems(genericArgumentsType);
		}

		void InitComponents()
		{
			List<Type> types = new List<Type>();

			foreach(Assembly assembly in _assemblies)
			{
				types.AddRange(assembly.GetTypes().Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(Component))));
			}

			for(int i = 0; i < types.Count; i++)
			{
				_componentTypeIDs.Add(types[i], i);
			}
		}

		void InitSystems(Type genericArgumentsType)
		{
			Dictionary<Type, EntitySystem> typeToInstanceMap = new Dictionary<Type, EntitySystem>();
			//_systems = new List<EntitySystem>();

			//List<Type> types = new List<Type>();

			//foreach(Assembly assembly in _assemblies)
			//{
			//	types.AddRange(assembly.GetTypes()
			//		.Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(EntitySystem))));
			//}

			//// Find all systems
			//foreach(Type type in types)
			//	EntitySystem system = (EntitySystem)Activator.CreateInstance(type);
			//	_systems.Add(system);
			foreach(var system in _systems)
			{
				var type = system.GetType();
				system.Manager = this;
				typeToInstanceMap.Add(type, system);

				system.Data.Entities = new HashedOrderlessList<Entity>();

				// Get settings attribute
				var attributes = type.GetCustomAttributes();
				foreach(var attribute in attributes)
				{
					var settings = attribute as EntitySystemSettings;
					var after = attribute as EntitySystemExecuteAfter;
					var before = attribute as EntitySystemExecuteBefore;

					if(settings != null)
					{
						system.Data.Settings = settings;
					}

					if(after != null)
					{
						system.Data.ExecuteAfter = after.Systems;
					}

					if(before != null)
					{
						system.Data.ExecuteBefore = before.Systems;
					}
				}

				if(system.Data.Settings == null)
				{
					system.Data.Settings = new EntitySystemSettings(ExecutionStyle.Sequential);
				}
				if(system.Data.ExecuteBefore == null)
				{
					system.Data.ExecuteBefore = new Type[0];
				}
				if(system.Data.ExecuteAfter == null)
				{
					system.Data.ExecuteAfter = new Type[0];
				}

				system.Data.Dependencies = new List<Type>(system.Data.ExecuteAfter);

				// Parse Process() method and precompile invoke call
				bool[] key = new bool[NumComponents];
				List<Tuple<int, Type>> components = new List<Tuple<int, Type>>();
				var method = type.GetMethod("Process");
				var parameters = method.GetParameters();
				foreach(ParameterInfo info in parameters)
				{
					if(!info.ParameterType.IsSubclassOf(typeof(Component)))
						continue;
					key[GetComponentTypeID(info.ParameterType)] = true;
					components.Add(new Tuple<int, Type>(GetComponentTypeID(info.ParameterType), info.ParameterType));
				}
				system.Data.Key = new EntityTypeKey(key);
				system.Data.ProcessMethod = GetProcessMethod(components, method, system, genericArgumentsType);
			}

			foreach(var system in _systems)
			{
				foreach(var before in system.Data.ExecuteBefore)
				{
					var beforeSystem = typeToInstanceMap[before];
					beforeSystem.Data.Dependencies.Add(system.GetType());
				}
			}

			// Solve system dependency graph
			Dictionary<EntitySystem, List<EntitySystem>> graph = new Dictionary<EntitySystem, List<EntitySystem>>();
			List<EntitySystem> next = new List<EntitySystem>();
			List<EntitySystem> current = new List<EntitySystem>();
			foreach(EntitySystem system in _systems)
			{
				graph.Add(system, new List<EntitySystem>());
				if(system.Data.Dependencies.Count == 0)
					next.Add(system);
			}

			foreach(EntitySystem system in _systems)
			{
				foreach(var dependency in system.Data.Dependencies)
				{
					graph[typeToInstanceMap[dependency]].Add(system);
				}
			}

			if(next.Count == 0)
				throw new Exception("System dependency graph cannot be solved");

			int wave = 0;

			while(next.Count > 0)
			{
				var temp = current;
				current = next;
				next = temp;
				next.Clear();

				foreach(EntitySystem system in current)
				{
					system.Data.Order = Math.Max(system.Data.Order, wave);
					foreach(EntitySystem child in graph[system])
					{
						next.Add(child);
					}
				}

				wave++;
			}

			_systems = _systems.OrderBy(x => x.Data.Order).ToList();
		}

		public void UpdateAllSystems(object genericArguments)
		{
			List<Entity> added = new List<Entity>();
			List<Entity> removed = new List<Entity>();

			_entitiesToAdd.TryCollectAndClear(added);
			_entitiesToRemove.TryCollectAndClear(removed);

			foreach(Entity e in added)
			{
				RegisterEntity(e);
			}
			foreach(Entity e in removed)
			{
				DestoryEntity(e);
			}

			foreach(EntitySystem system in _systems)
			{
				ExecutionStyle style = system.Data.Settings.Execution;

				int count = system.Data.Entities.List.Count;

				if(count == 0)
					continue;

				if(count < system.Data.Settings.MinInstancesForParallel || ThreadCount == 1)
					style = ExecutionStyle.Sequential;

				switch(style)
				{
					case ExecutionStyle.ParallelBalanced:
						{
							Parallel.For(0, count, new ParallelOptions()
							{
								MaxDegreeOfParallelism = (ThreadCount < 1) ? Environment.ProcessorCount : ThreadCount
							}, i =>
							{
								system.Data.ProcessMethod.Invoke(genericArguments, system.Data.Entities.List[i]);
							});
							break;
						}
					case ExecutionStyle.ParallelRanged:
						{
							Utils.ParallelForRange(0, count, ThreadCount, (start, end) =>
							{
								for(int j = start; j < end; j++)
								{
									system.Data.ProcessMethod.Invoke(genericArguments, system.Data.Entities.List[j]);
								}
							});
							break;
						}
					default:
						{
							foreach(Entity entity in system.Data.Entities.List)
							{
								system.Data.ProcessMethod.Invoke(genericArguments, entity);
							}
							break;
						}
				}
			}
		}

		static Action<object, Entity> GetProcessMethod(List<Tuple<int, Type>> componentKeys, MethodInfo processMethod, EntitySystem system, Type genericArgumentsType)
		{
			// A label expression of the void type that is the target for Expression.Return().
			//LabelTarget returnTarget = Expression.Label();

			var entityParameter = Expression.Parameter(typeof(Entity), "entity");
			var argsParameter = Expression.Parameter(typeof(object), "args");

			List<Expression> arguments = new List<Expression>();
			arguments.Add(Expression.TypeAs(argsParameter, genericArgumentsType));
			arguments.Add(entityParameter);
			for(int i = 0; i < componentKeys.Count; i++)
			{
				arguments.Add(Expression.TypeAs(Expression.ArrayAccess(Expression.Field(entityParameter, "Components"), Expression.Constant(componentKeys[i].Item1)), componentKeys[i].Item2));
			}

			// This block contains a GotoExpression that represents a return statement with no value.
			// It transfers execution to a label expression that is initialized with the same LabelTarget as the GotoExpression.
			// The types of the GotoExpression, label expression, and LabelTarget must match.
			BlockExpression blockExpr =
				Expression.Block(
					Expression.Call(Expression.Constant(system), processMethod, arguments)
				);

			// The following statement first creates an expression tree,
			// then compiles it, and then runs it.
			return Expression.Lambda<Action<object, Entity>>(blockExpr, argsParameter, entityParameter).Compile();
		}
	}
}
