﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using Engine.UI;
using OpenTK;

namespace Engine.Main
{
	class TimerVisualizer
	{
		Dictionary<string, Measurement> _historyMeasurements = new Dictionary<string, Measurement>();

		public void VisualizePerfCounters(FrameOutput output, UIManager ui)
		{
			Vector2 scale = new Vector2(0.03f);
			Vector4 color = new Vector4(0.2f, 1.0f, 0.2f, 1.0f);

			Dictionary<string, double> values = new Dictionary<string, double>();
			HashSet<string> counters = new HashSet<string>();

			// Combine timers with the same name
			foreach(var timer in output.Timers)
			{
				string name = QueryNameToString(timer);

				if(timer is CounterQuery)
				{
					counters.Add(name);
				}

				if(values.ContainsKey(name))
				{
					values[name] += timer.DisplayEllapsed;
				}
				else
				{
					values.Add(name, timer.DisplayEllapsed);
				}
			}

			// Remove history timers that no longer exist

			//var keys = _historyMeasurements.Keys.ToList();
			//foreach(var key in keys)
			//{
			//	if(!values.ContainsKey(key))
			//	{
			//		_historyMeasurements.Remove(key);
			//	}
			//}

			foreach(Measurement measurement in _historyMeasurements.Values)
			{
				measurement.Updated = false;
			}

			// Add new values to history timers
			foreach(var pair in values)
			{
				Measurement measurement;
				if(_historyMeasurements.ContainsKey(pair.Key))
				{
					measurement = _historyMeasurements[pair.Key];
				}
				else
				{
					measurement = new Measurement(pair.Key);
					_historyMeasurements.Add(pair.Key, measurement);
				}
				measurement.AddValue(pair.Value);
				measurement.Updated = true;
			}

			var sorted = _historyMeasurements.Values.OrderBy(x => x.Name);

			StringBuilder sbDescriptions = new StringBuilder();
			StringBuilder sbAvg = new StringBuilder();
			StringBuilder sbMax = new StringBuilder();
			StringBuilder sbMin = new StringBuilder();
			
			Func<double, string> getTimeString = d =>
			{
				return string.Format("{0:00.00}", Math.Round(d * 1000, 2));
			};

			float width = ui.GetTextWidth(getTimeString(0.0), scale.X) * 0.8f;

			sbDescriptions.AppendLine("Name");
			sbAvg.AppendLine("Avg");
			sbMax.AppendLine("Max");
			sbMin.AppendLine("Min");

			foreach(var measurement in sorted)
			{
				string prefix = "";

				if(!measurement.Updated)
				{
					prefix = "~";
				}

				if(counters.Contains(measurement.Name))
				{
					sbDescriptions.AppendLine(measurement.Name + ":");
					sbMax.AppendLine(prefix + NumberToString(measurement.Max));
					sbMin.AppendLine(prefix + NumberToString(measurement.Min));
					sbAvg.AppendLine(prefix + NumberToString(measurement.Average));
				}
				else
				{
					sbDescriptions.AppendLine(measurement.Name + ":");
					sbAvg.AppendLine(prefix + getTimeString(measurement.Average));
					sbMax.AppendLine(prefix + getTimeString(measurement.Max));
					sbMin.AppendLine(prefix + getTimeString(measurement.Min));
				}
			}

			ui.AddText(sbDescriptions.ToString(), scale, new Vector2(1f - width * 3.0f, 0f), color, TextAlignmentHorizontal.Right, TextAlignmentVertical.Top);
			ui.AddText(sbAvg.ToString(), scale, new Vector2(1f - width * 2, 0f), color, TextAlignmentHorizontal.Right, TextAlignmentVertical.Top);
			ui.AddText(sbMax.ToString(), scale, new Vector2(1f - width, 0f), color, TextAlignmentHorizontal.Right, TextAlignmentVertical.Top);
			ui.AddText(sbMin.ToString(), scale, new Vector2(1f, 0f), color, TextAlignmentHorizontal.Right, TextAlignmentVertical.Top);
		}

		string NumberToString(double number)
		{
			number = Math.Ceiling(number);

			Func<long, char, string> toNiceNumber = (e, k) =>
			{
				int large = (int)Math.Floor(number / e);
				string s = large.ToString();

				int allowedDecimals = 3 - s.Length;

				if(e > 1)
				{
					string format = "0";
					if(allowedDecimals > 0)
					{
						format += ".";
						for(int i = 0; i < allowedDecimals; i++)
						{
							format += "0";
						}
					}
					s = (number / e).ToString(format, CultureInfo.InvariantCulture);
					s += k;
				}

				return s;
			};

			if(number < 10000)
			{
				return toNiceNumber(1, ' ');
			}
			if(number < 1000000)
			{
				return toNiceNumber(1000, 'K');
			}
			if(number < 1000000000)
			{
				return toNiceNumber(1000000, 'M');
			}
			if(number < 1000000000000)
			{
				return toNiceNumber(1000000000, 'G');
			}

			return toNiceNumber(1000000000000, 'T');
		}

		string QueryNameToString(ITimerQuery query)
		{
			return query.Kind.ToString() + ": " + query.Name;
		}

		class Measurement
		{
			const int MaxSamples = 30;

			public double[] Values;
			int _current = 0;
			int _validSamples = 0;

			public bool Updated = false;

			public string Name { get; private set; }

			public double Average
			{
				get
				{
					double d = 0;
					foreach(var value in Values)
					{
						d += value;
					}
					d /= _validSamples;
					return d;
				}
			}

			public double Max
			{
				get
				{
					double max = 0;
					foreach(var value in Values)
					{
						max = Math.Max(max, value);
					}
					return max;
				}
			}

			public double Min
			{
				get
				{
					double min = double.PositiveInfinity;
					foreach(var value in Values)
					{
						if(value > 0)
							min = Math.Min(min, value);
					}
					return min;
				}
			}

			public Measurement(string name)
			{
				Name = name;
				Values = new double[MaxSamples];
				for(int i = 0; i < MaxSamples; i++)
				{
					Values[i] = 0;
				}
			}

			public void AddValue(double value)
			{
				Values[_current] = value;
				_current++;
				if(_validSamples < MaxSamples)
					_validSamples++;
				if(_current >= Values.Length)
					_current = 0;
			}
		}
	}
}
