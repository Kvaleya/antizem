﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using OpenTK;

namespace Engine.Main.Entities.Components
{
	public struct TransformComponent
	{
		public bool Changed { get; set; }

		public Vector3d Position
		{
			get
			{
				return _position;
			}
			set
			{
				_position = value;
				Changed = true;
			}
		}

		public Quaternion Rotation
		{
			get { return _rotation; }
			set
			{
				_rotation = value;
				Changed = true;
			}
		}

		public float Scale
		{
			get { return _scale; }
			set
			{
				_scale = value;
				Changed = true;
			}
		}

		Vector3d _position;
		Quaternion _rotation;
		float _scale;

		public TransformComponent(Vector3d position, Quaternion rotation, float scale)
		{
			_position = position;
			_rotation = rotation;
			_scale = scale;
			Changed = false;
		}
	}
}
