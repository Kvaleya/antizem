﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Main.Entities.Components
{
	struct RotateComponent
	{
		public float Speed;

		public RotateComponent(float speed)
		{
			Speed = speed;
		}
	}
}
