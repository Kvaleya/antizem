﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Scene;
using Engine.Scene.Descriptions;

namespace Engine.Main.Entities.Components
{
	struct SceneObjectComponent
	{
		public SceneComponentInstance Instance;
		public SceneComponent SceneComponent;

		public SceneObjectComponent(SceneComponent component)
		{
			SceneComponent = component;
			Instance = null;
		}
	}
}
