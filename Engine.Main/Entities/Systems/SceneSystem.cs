﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcsLib;
using Engine.Main.Entities.Components;
using Engine.Scene;
using Engine.Scene.Descriptions;

namespace Engine.Main.Entities.Systems
{
	[ProcessorExecution(ProcessorExecutionType.PerThreadLoop, 500)]
	class SceneSystem : EntityProcessor
	{
		[SingletonComponent]
		SceneMeshManager SceneManager { get; set; }

		public SceneSystem()
		{
		}

		protected override void OnEntityAdded(Entity e)
		{
			base.OnEntityAdded(e);

			var transform = GetExternalComponent<TransformComponent>(e);
			var sceneObject = GetExternalComponent<SceneObjectComponent>(e);

			sceneObject.Instance = new SceneComponentInstance(SceneManager, sceneObject.SceneComponent)
			{
				Position = transform.Position,
				Rotation = transform.Rotation,
				Scale = transform.Scale,
			};

			SetExternalComponent(e, sceneObject);
		}

		protected override void OnEntityRemoved(Entity e)
		{
			var sceneObject = GetExternalComponent<SceneObjectComponent>(e);
			sceneObject.Instance.Dispose();
			sceneObject.Instance = null;
			SetExternalComponent(e, sceneObject);
		}
		
		public void Process(Entity e, ref TransformComponent transform, ref SceneObjectComponent sceneObject)
		{
			if(transform.Changed)
			{
				transform.Changed = false;
				sceneObject.Instance.Position = transform.Position;
				sceneObject.Instance.Rotation = transform.Rotation;
				sceneObject.Instance.Scale = transform.Scale;
			}
		}
	}
}
