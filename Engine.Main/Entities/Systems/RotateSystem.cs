﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcsLib;
using Engine.Main.Entities.Components;
using OpenTK;

namespace Engine.Main.Entities.Systems
{
	[ProcessorExecuteBefore(typeof(SceneSystem))]
	[ProcessorExecution(ProcessorExecutionType.PerThreadLoop, 500)]
	class RotateSystem : EntityProcessor
	{
		[SingletonComponent]
		Engine.Context EngineContext { get; set; }

		public RotateSystem()
		{
		}

		public void Process(Entity e, ref TransformComponent transform, [ComponentReadOnly] ref RotateComponent rotateComponent)
		{
			transform.Rotation = transform.Rotation *
			                     Quaternion.FromAxisAngle(Vector3.UnitY, (float)EngineContext.TimeDelta * rotateComponent.Speed);
		}
	}
}
