﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EcsLib;
using Engine.Common;
using Engine.Common.Rendering;
using Engine.Materials;
using Engine.Meshes;
using Engine.Scene;
using Engine.Scene.Descriptions;
using Engine.UI;
using GameFramework;
using OpenTK;

namespace Engine.Main
{
	public class Engine
	{
		IEngineViewport _viewport;
		TextOutput _textOutput;
		IFileManager _fileManager;
		IRenderer _renderer;

		Stopwatch _stopwatch;
		long _frameNumber = 0;

		Thread _engineThread;

		ManualResetEvent _syncframePrepared; // TODO: change these to AutoResetEvent?
		ManualResetEvent _syncframeDone;

		Context _context;

		// TODO: remove test objects
		TestScene _scene;

		public Engine(IEngineViewport viewport, TextOutput textOutput, IFileManager fileManager, IRenderer renderer)
		{
			_viewport = viewport;
			_textOutput = textOutput;
			_fileManager = fileManager;
			_renderer = renderer;

			var ioThread = new WorkerThread("IO Thread");

			_context = new Context()
			{
				TextOutput = textOutput,
				FileManager = fileManager,
				KeyboardInput = new KeyboardInputState(),
				MouseInput = new MouseInputState(viewport),
				RendererInjector = new DataInjector(),
				TimeDelta = 0d,
				TimeEllapsed = 0d,
				ResourceManagerMeshBinary = new MeshBinaryResourceManager(fileManager, textOutput, ioThread, 1024 * 1024 * 192),
				MaterialManager = new MaterialManager(fileManager, _textOutput, ioThread, (1 << 20) * 512), // 512mb cpu-side texture cache
				Viewport = _viewport,
				IOThread = ioThread,
			};

			_context.UIManager = new UIManager(new GdxFont("carlito32.fnt", textOutput), _context.RendererInjector, _viewport);

			_stopwatch = new Stopwatch();

			_syncframePrepared = new ManualResetEvent(false);
			_syncframeDone = new ManualResetEvent(true);
		}

		public void Inicialize()
		{
			_engineThread = new Thread(() =>
			{
				while(true)
				{
					this.ProcessFrame();
				}
			});
			_engineThread.Name = "Engine.Main";

			_renderer.Inicialize(_context.RendererInjector, this._context.ResourceManagerMeshBinary, this._context.MaterialManager, out this._context.ResourceManagerMesh);
			_context.RendererInjector.CompileInjector();

			_scene = new TestScene(_context.ResourceManagerMesh, _context.MaterialManager, _context.FileManager, _context);
		}

		/// <summary>
		/// Call this from OpenGL thread
		/// </summary>
		public void Run(bool debugFrameLimit)
		{
			//try
			//{
				_stopwatch.Start();
				_engineThread.Start();
				_viewport.StartMainLoop(_syncframePrepared, _syncframeDone, _renderer, debugFrameLimit);
			//}
			//catch(Exception e)
			//{
			//	this._context.TextOutput.PrintException(OutputType.Error, "Fatal exception", e);
			//	throw e;
			//}
		}

		void ProcessFrame()
		{
			// Update context
			double ellapsedNow = _stopwatch.ElapsedTicks / (double)Stopwatch.Frequency;
			_context.TimeDelta = ellapsedNow - _context.TimeEllapsed;
			_context.TimeEllapsed = ellapsedNow;
			if(_viewport.IsFocused)
			{
				_context.KeyboardInput.Update();
				_context.MouseInput.Update();
			}
			else
			{
				_context.KeyboardInput.ClearState();
				_context.MouseInput.ClearState();
			}

			_context.RendererInjector.AddData(new BasicFrameData()
			{
				FrameNumber = _frameNumber,
			});

			_context.TimerQueryCpuManager = new TimerQueryCPUManager();
			_context.RendererInjector.AddData(_context.TimerQueryCpuManager);

			var cpuFrameQuery = new TimerQueryCPU(_context.TimerQueryCpuManager, "Frame");
			cpuFrameQuery.StartQuery();

			// Update systems
			_scene.Update(_context);
			_context.TimerVisualizer.VisualizePerfCounters(_context.FrameOutput, _context.UIManager);

			// Update resource managers
			_context.ResourceManagerMeshBinary.Update();
			_context.ResourceManagerMesh.Update();
			_context.UIManager.Swap();
			_context.MaterialManager.Update(_context.RendererInjector);

			cpuFrameQuery.EndQuery();

			// Sync with rendering
			_syncframeDone.WaitOne();
			_syncframeDone.Reset();
			// Update rendering
			_context.RendererInjector.Swap();
			// Get frame output
			_context.FrameOutput = _viewport.FrameOutput;
			if(_context.FrameOutput == null)
			{
				_context.FrameOutput = new FrameOutput();
			}
			// Start rendering
			_syncframePrepared.Set();
			// TODO: synchronised renderer update?

			_frameNumber++;
		}

		public class Context
		{
			public double TimeEllapsed;
			public double TimeDelta;
			public DataInjector RendererInjector;
			public IFileManager FileManager;
			public TextOutput TextOutput;
			public MouseInputState MouseInput;
			public KeyboardInputState KeyboardInput;

			public MaterialManager MaterialManager;
			public MeshBinaryResourceManager ResourceManagerMeshBinary;
			public IMeshManager ResourceManagerMesh;

			public EcsManager EntityManager; // TODO
			public SceneMeshManager SceneManager;
			public SceneComponentRepository SceneComponentRepository;

			public UIManager UIManager;

			public FrameOutput FrameOutput = new FrameOutput();

			public TimerQueryCPUManager TimerQueryCpuManager;

			internal TimerVisualizer TimerVisualizer = new TimerVisualizer();

			public IEngineViewport Viewport;

			public WorkerThread IOThread;
		}
	}
}
