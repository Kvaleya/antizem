﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using Engine.Scene;
using OpenTK;

namespace Engine.Main
{
	class VoxelManager
	{
		SceneMeshManager _sceneManager;

		const int SmallestVoxelCubeSize = 8;
		const int SmallestVoxelCubeSnapDiv = 1;
		const int ClipmapLevels = 6;

		Vec3i[] _currentPositions = new Vec3i[6];

		public VoxelManager(SceneMeshManager sceneMeshManager)
		{
			_sceneManager = sceneMeshManager;
		}

		public List<MeshInstanceData> Update(Engine.Context context, Vector3d cameraPos, int debugClipmap)
		{
			List<MeshInstanceData> instances = new List<MeshInstanceData>();

			TimerQueryCPU timer = new TimerQueryCPU(context.TimerQueryCpuManager, "Voxels");

			timer.StartQuery();

			for(int i = ClipmapLevels - 1; i >= 0; i--)
			{
				int div = SmallestVoxelCubeSnapDiv << i;
				int size = SmallestVoxelCubeSize << i;

				Vec3i cameraBase = new Vec3i((int)cameraPos.X, (int)cameraPos.Y, (int)cameraPos.Z);

				Vec3i newPos = new Vec3i(
					(cameraBase.X / div) * div,
					(cameraBase.Y / div) * div,
					(cameraBase.Z / div) * div);
				
				if(newPos.X != _currentPositions[i].X || newPos.Y != _currentPositions[i].Y || newPos.Z != _currentPositions[i].Z)
				{
					// Update voxels
					Vec3i voxelCenter = new Vec3i(
						newPos.X - cameraBase.X,
						newPos.Y - cameraBase.Y,
						newPos.Z - cameraBase.Z
						);

					var center = new Vector3(voxelCenter.X, voxelCenter.Y, voxelCenter.Z);
					_sceneManager.GetDataCube(center, size, center, 0f, out instances);
					var batchesVoxels = MeshBatch.GetBatchedInstances(instances);
					context.RendererInjector.AddData(new VoxelData()
					{
						Batches = batchesVoxels,
						CubeIncludeCenter = newPos,
						CubeIncludeSize = size,
						CubeExcludeCenter = _currentPositions[i],
						CubeExcludeSize = size,
						ClipmapLevel = i,
						DebugClipmapLevel = debugClipmap % ClipmapLevels,
					});

					_currentPositions[i] = newPos;

					return instances;
				}
			}

			timer.EndQuery();

			return instances;
		}
	}
}
