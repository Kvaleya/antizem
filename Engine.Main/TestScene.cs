﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Game;
using Engine.Common.Rendering;
using EcsLib;
using Engine.Main.Entities;
using Engine.Main.Entities.Components;
using Engine.Main.Entities.Systems;
using Engine.Materials;
using Engine.Meshes;
using Engine.Scene;
using Engine.Scene.Descriptions;
using Engine.UI;
using GameFramework;
using OpenTK;
using OpenTK.Input;

namespace Engine.Main
{
	class TestScene
	{
		//List<MeshInstance> _instances;
		//List<MeshInstance> _instancesRotate;
		FirstPersonInput _fpsInput;
		
		float _moveSpeed = 20f;

		SceneMeshManager _sceneManager;
		EcsManager _ecsManager;
		SceneComponentRepository _sceneComponentRepository;

		VoxelManager _voxelManager;

		CameraPerspective _camera = new CameraPerspective(Vector3d.Zero, Quaternion.Identity, 90);
		CameraPerspective _cameraPrevious;
		CameraPerspective _cameraCull = new CameraPerspective(Vector3d.Zero, Quaternion.Identity, 90);
		bool _visualizeCulling = false;
		bool _visualizeVt = false;
		bool _computeCull = true;
		bool _enableTonemap = true;
		bool _enableUI = true;
		bool _enableVoxelUpdate = true;
		bool _enableAA = true;
		int _renderScale = 0;
		int _debugVoxelClipmapLevel = 0;

		int _localExposure = 0;

		Vector3 _sunDir = Vector3.Normalize(new Vector3(0, 0.5f, -1.0f));

		public TestScene(IMeshManager meshManager, MaterialManager materialManager, IFileManager fileManager, Engine.Context context)
		{
			//Scene.ArchAZDOScene.ArchAZDOConverter.Convert("sponza_azdo.xml", "Scenes/sponza_textured.xml", "archazdo", fileManager);

			// Init systems
			_sceneManager = new SceneMeshManager(_camera);
			_voxelManager = new VoxelManager(_sceneManager);
			_ecsManager = new EcsManager();
			_ecsManager.AddProcessors(new SceneSystem(), new RotateSystem());

			_ecsManager.SetSingletonProperty(context);
			_ecsManager.SetSingletonProperty(_sceneManager);

			_sceneComponentRepository = new SceneComponentRepository(fileManager, context.TextOutput, meshManager, materialManager);

			_fpsInput = new FirstPersonInput();
			_fpsInput.MouseSensitivity = 0.25f;
			//_instances = new List<MeshInstance>();
			//_instancesRotate = new List<MeshInstance>();
			
			LazyComponent(_sceneComponentRepository.GetComponent("Scenes/sphere_mirror.xml"), new Vector3(0, 0, 20), Quaternion.Identity, 4f);
			LazyComponent(_sceneComponentRepository.GetComponent("Scenes/cube.xml"), new Vector3(0, 0, -20), Quaternion.Identity, 4f);
			LazyComponent(_sceneComponentRepository.GetComponent("Scenes/sponza_azdo.xml"), new Vector3(0, 0, 0), Quaternion.Identity, 0.01f);
			LazyComponent(_sceneComponentRepository.GetComponent("Scenes/birch.xml"), new Vector3(30, 0, 0), Quaternion.Identity, 1f);
			//LazyMesh(meshManager, materialManager, fileManager, "meshes/pulserifle.zip", "materials/testMaterial.xml", new Vector3(0, 10, 0), Quaternion.Identity, 1f);
			
			LazyMesh(meshManager, materialManager, fileManager, "meshes/cphills.zip", "materials/neutral.xml", new Vector3(0, -200, 0), Quaternion.Identity, 0.1f);

			LazyMesh(meshManager, materialManager, fileManager, "meshes/cube.zip", "materials/ksphad.xml", new Vector3(0, 0, -80), Quaternion.FromAxisAngle(Vector3.UnitX, (float)Math.PI * -0.5f), 16f);
			LazyMesh(meshManager, materialManager, fileManager, "meshes/cube.zip", "materials/ksphad.xml", new Vector3(0, 0, -80), Quaternion.FromAxisAngle(Vector3.UnitX, (float)Math.PI * -0.5f), -16f);
			
			//for(int i = 0; i < 11; i++)
			//{
			//	for(int j = 0; j < 8; j++)
			//	{
			//		LazyComponent(_sceneComponentRepository.GetComponent("Scenes/cube.xml"), new Vector3(i * 20, j * 0.0625f, -60 - j * 20),
			//			Quaternion.FromAxisAngle(Vector3.UnitZ, i * (float)(Math.PI * 0.5 * 0.1)), 8f);
			//	}
			//}

			//LazyMesh(meshManager, materialManager, fileManager, "meshes/cube.zip", "materials/neutral.xml", new Vector3(0, 0, -4000), Quaternion.Identity, 400f);

			//GenForest(new Vector3(0, -20, 0), 500, 100);
			//GenBuddhas(meshManager, materialManager, 10000);

			GenCubes(new Vector3d(0, -200, 0), 100, 100);
			/*
			for(int i = 0; i < 100; i++)
			{
				LazyComponent(_sceneComponentRepository.GetComponent("Scenes/cubeDiffuse.xml"), new Position(0, i * 30, 0), Quaternion.Identity, 30);
			}
			*/
			//GenGrid(80, 1, 1, 40, (p) =>
			//{
			//	LazyComponent(_sceneComponentRepository.GetComponent("Scenes/sponza_azdo.xml"), p, Quaternion.Identity, 0.01f);
			//	//LazyMesh(meshManager, materialManager, fileManager, "meshes/cphills.zip", "materials/neutral.xml", p, Quaternion.Identity, 0.1f);
			//});

			//LazyMesh(meshManager, materialManager, fileManager, "meshes/buddha_lod4.zip", "materials/mirror.xml", new Vector3(0, 20, 0), Quaternion.Identity, 1f);

			//LazyMesh(meshManager, materialManager, fileManager, "meshes/cube.zip", "Textures/ori.DDS", "", new Vector3(0, 20, 0), Quaternion.Identity, 1f);

			// Sponza
			string sponzaFile = "meshes/sponza.zip";

			for(int i = 0; i < 5; i++)
			{
				string normal = "";
				string tex = "";
				if(i == 0)
				{
					tex = "Textures/stairs_lf.DDS";
				}
				if(i == 1)
				{
					tex = "Textures/greasy-metal-pan1-albedo.DDS";
					normal = "Textures/greasy-metal-pan1-normal_nrm.DDS";
				}
				if(i == 2)
				{
					tex = "Textures/sponza_bricks_a_albedo.DDS";
				}
				if(i == 3)
				{
					tex = "Textures/rustediron-streaks_basecolor.DDS";
				}
				if(i >= 4)
				{
					tex = "Textures/slate2-tiled-albedo2.DDS";
					normal = "Textures/slate2-tiled-normal3-UE4_nrm.DDS";
				}
				//LazyMesh(meshManager, materialManager, fileManager, sponzaFile, tex, normal, new Vector3(i * 40, 0, 0), Quaternion.Identity, 0.01f);
			}
		}

		void LazyMesh(IMeshManager meshManager, MaterialManager materialManager, IFileManager fileManager, string meshFile, string materialFile, Vector3 pos, Quaternion rot, float scale)
		{
			LazyMesh(meshManager, materialManager, fileManager, meshFile, materialFile, (Vector3d)pos, rot, scale);
		}

		void LazyMesh(IMeshManager meshManager, MaterialManager materialManager, IFileManager fileManager, string meshFile, string materialFile, Vector3d pos, Quaternion rot, float scale)
		{
			var material = materialManager.AddOrGetMaterial(materialFile, true);

			LazyMesh(meshManager, materialManager, fileManager, meshFile, material, pos, rot, scale);
		}

		void LazyMesh(IMeshManager meshManager, MaterialManager materialManager, IFileManager fileManager, string meshFile, string textureFile, string normalFile, Vector3d pos, Quaternion rot, float scale)
		{
			var material = materialManager.AddOrGetMaterial(meshFile + " " + textureFile, false);
			material.TextureBaseColor = textureFile;
			material.TextureNormalRoughnessMetallic = normalFile;

			LazyMesh(meshManager, materialManager, fileManager, meshFile, material, pos, rot, scale);
		}

		void LazyMesh(IMeshManager meshManager, MaterialManager materialManager, IFileManager fileManager, string meshFile, Material material, Vector3d pos, Quaternion rot, float scale)
		{
			var meshes = MeshBinary.GetPackageContents(fileManager.GetStream(meshFile));

			var primitives = new List<PrimitiveMeshStatic>();

			foreach(var name in meshes)
			{
				var mesh = meshManager.GetMesh(Utils.Meshes.MeshFileAndNameCombine(meshFile, name));

				primitives.Add(new PrimitiveMeshStatic()
				{
					Mesh = mesh,
					Material = material,
					//Flags = MeshPrimitiveFlags.TwoSided,
				});
			}

			var component = new SceneComponent(new List<PrimitiveMeshStatic>[]
			{
			primitives,
			}, new float[]
			{
			float.PositiveInfinity,
			});

			//component.SaveToFile("lolz");

			LazyComponent(component, pos, rot, scale);
		}

		void LazyComponent(SceneComponent component, Vector3 pos, Quaternion rot, float scale)
		{
			LazyComponent(component, (Vector3d)pos, rot, scale);
		}

		void LazyComponent(SceneComponent component, Vector3d pos, Quaternion rot, float scale)
		{
			var ent = _ecsManager.AddEntity(new SceneObjectComponent(component), new TransformComponent(pos, rot, scale));
		}

		void GenGrid(int w, int h, int d, int spacing, Action<Vector3d> gen)
		{
			Vector3d corner = new Vector3d(w * spacing, h * spacing, d * spacing) * -0.5;

			for(int z = 0; z < d; z++)
			{
				for(int y = 0; y < h; y++)
				{
					for(int x = 0; x < w; x++)
					{
						gen(corner + new Vector3d(x * spacing, y * spacing, z * spacing));
					}
				}
			}
		}

		void GenCubes(Vector3d offset, int dimCount, int cellSize)
		{
			Random r = new Random(1);

			int range = 5;
			int rangeY = 5;

			for(int y = 0; y < dimCount; y++)
			{
				for(int x = 0; x < dimCount; x++)
				{
					Vector3d pos = offset + new Vector3d(r.Next(-range, range), r.Next(-rangeY, rangeY), r.Next(-range, range));
					pos += new Vector3d(x - dimCount / 2, 0, y - dimCount / 2) * cellSize;

					if(pos.LengthFast < 1000)
						continue;

					Quaternion rot = Quaternion.FromEulerAngles((float)(r.NextDouble() * Math.PI), (float)(r.NextDouble() * Math.PI), (float)(r.NextDouble() * Math.PI));
					float rndScale = (float)r.NextDouble();
					float scale = 0.5f * cellSize + rndScale * cellSize * 2;
					LazyComponent(_sceneComponentRepository.GetComponent("Scenes/cubeDiffuse.xml"), pos, rot, scale);
				}
			}
		}

		void GenForest(Vector3d offset, int count, int range = 30, int rangeY = 1)
		{
			Random r = new Random(1);

			float rangeS = 0.2f;

			for(int i = 0; i < count; i++)
			{
				Vector3d pos = offset + new Vector3d(r.Next(-range, range), r.Next(-rangeY, rangeY), r.Next(-range, range));
				//Quaternion rot = Quaternion.FromEulerAngles((float)(r.NextDouble() * 2 * Math.PI), (float)(r.NextDouble() * 2 * Math.PI), (float)(r.NextDouble() * 2 * Math.PI));
				Quaternion rot = Quaternion.FromAxisAngle(Vector3.UnitY, (float)(r.NextDouble() * Math.PI * 2));
				float scale = 1 + ((float)r.NextDouble() * 2f - 1f) * rangeS;

				LazyComponent(_sceneComponentRepository.GetComponent("Scenes/birch.xml"), pos, rot, scale);
			}
		}

		void GenBuddhas(IMeshManager meshManager, MaterialManager materialManager, int count)
		{
			Random r = new Random(2);

			int range = 100;

			List<Material> materials = new List<Material>();

			for(int i = 0; i < 50; i++)
			{
				var material = materialManager.AddOrGetMaterial("buddhaTestMaterial_" + i.ToString());
				material.BaseColorOffset.X = (float)r.NextDouble();
				material.BaseColorOffset.Y = (float)r.NextDouble();
				material.BaseColorOffset.Z = (float)r.NextDouble();

				float max = Math.Max(Math.Max(material.BaseColorOffset.X, material.BaseColorOffset.Y), material.BaseColorOffset.Z);

				material.BaseColorOffset.X /= max;
				material.BaseColorOffset.Y /= max;
				material.BaseColorOffset.Z /= max;

				//material.TextureBaseColor = "Textures/ori.DDS";

				materials.Add(material);
			}

			List<SceneComponent> components = new List<SceneComponent>();

			foreach(var material in materials)
			{
				SceneComponent testComponent = new SceneComponent(new List<PrimitiveMeshStatic>[]
				{
					new List<PrimitiveMeshStatic>()
					{
						new PrimitiveMeshStatic()
						{
							Mesh = meshManager.GetMesh(Utils.Meshes.MeshFileAndNameCombine("meshes/buddha_lod2.zip", "defaultObject defaultGroup defaultMaterial")),
							Material = material,
						},
					},
					new List<PrimitiveMeshStatic>()
					{
						new PrimitiveMeshStatic()
						{
							Mesh = meshManager.GetMesh(Utils.Meshes.MeshFileAndNameCombine("meshes/buddha_lod4.zip", "defaultObject defaultGroup defaultMaterial")),
							Material = material,
						},
					},
					new List<PrimitiveMeshStatic>()
					{
						new PrimitiveMeshStatic()
						{
							Mesh = meshManager.GetMesh(Utils.Meshes.MeshFileAndNameCombine("meshes/buddha_lod10.zip", "defaultObject defaultGroup defaultMaterial")),
							Material = material,
						},
					},
					new List<PrimitiveMeshStatic>()
					{
						new PrimitiveMeshStatic()
						{
							Mesh = meshManager.GetMesh(Utils.Meshes.MeshFileAndNameCombine("meshes/buddha_lod12.zip", "defaultObject defaultGroup defaultMaterial")),
							Material = material,
						},
					},
				}, new float[]
				{
					50f,
					400f,
					1600f,
					float.PositiveInfinity,
				});

				components.Add(testComponent);
			}

			for(int i = 0; i < count; i++)
			{
				Vector3d pos = new Vector3d(r.Next(-range, range), r.Next(-range, range), r.Next(-range, range));
				//Quaternion rot = Quaternion.FromEulerAngles((float)(r.NextDouble() * 2 * Math.PI), (float)(r.NextDouble() * 2 * Math.PI), (float)(r.NextDouble() * 2 * Math.PI));
				Quaternion rot = Quaternion.Identity;
				float scale = 1.5f;

				Entity e = _ecsManager.AddEntity(new SceneObjectComponent(components[r.Next(components.Count)]),
					new TransformComponent(pos, rot, scale), new RotateComponent(0.5f));
			}
		}

		HashSet<MeshInstanceData> _instancesHashset = new HashSet<MeshInstanceData>();

		public List<MeshInstanceData> InstanceUnion(params List<MeshInstanceData>[] instances)
		{
			_instancesHashset.Clear();

			foreach(var list in instances)
			{
				foreach(var instance in list)
				{
					_instancesHashset.Add(instance);
				}
			}

			var l = _instancesHashset.ToList();

			return l;
		}

		public void Update(Engine.Context context)
		{
			_cameraPrevious = _camera.Clone();

			if(context.KeyboardInput.IsKeyPressed(Key.Space))
			{
				context.Viewport.MouseLocked = !context.Viewport.MouseLocked;
			}

			if(context.Viewport.MouseLocked)
				_fpsInput.UpdateCameraAngles(context.MouseInput);

			// Basic fps movement
			Vector2 angles = _fpsInput.CameraAngles / 180 * (float)Math.PI;

			var move = _fpsInput.GetMove(context.KeyboardInput);
			//move.Y += 2;

			Quaternion rotation = Quaternion.FromAxisAngle(Vector3.UnitX, angles.Y) * Quaternion.FromAxisAngle(Vector3.UnitY, angles.X);
			var matrix = Matrix3.CreateFromQuaternion(rotation);

			Vector3d moveDelta = -(Vector3d)matrix.Column2 * move.Y;
			moveDelta += (Vector3d)matrix.Column0 * move.X;

			if((context.KeyboardInput.IsKeyDown(Key.LAlt) || context.KeyboardInput.IsKeyDown(Key.RAlt)) && context.KeyboardInput.IsKeyPressed(Key.Enter))
			{
				context.Viewport.SwitchFullscreenWindowed();
			}

			if(context.KeyboardInput.IsKeyDown(Key.LAlt))
			{
				moveDelta *= 0.1f;
			}
			if(context.KeyboardInput.IsKeyDown(Key.LShift))
			{
				moveDelta *= 10f;
			}

			if(context.KeyboardInput.IsKeyDown(Key.X))
			{
				moveDelta *= 100f;
			}

			moveDelta *= _moveSpeed * (float)context.TimeDelta;

			_camera.Move(moveDelta, rotation);

			if(context.KeyboardInput.IsKeyPressed(Key.Number1))
			{
				_visualizeCulling = !_visualizeCulling;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.Number2))
			{
				_visualizeVt = !_visualizeVt;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.Number3))
			{
				context.Viewport.ResShift++;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.Number4))
			{
				context.Viewport.ResShift--;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.Number5))
			{
				_debugVoxelClipmapLevel++;
			}


			if(context.KeyboardInput.IsKeyDown(Key.E) && !context.KeyboardInput.IsKeyDown(Key.LShift))
			{
				_sunDir = -matrix.Column2;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.R))
			{
				_computeCull = !_computeCull;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.T))
			{
				_enableTonemap = !_enableTonemap;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.U))
			{
				_enableUI = !_enableUI;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.V))
			{
				_enableAA = !_enableAA;
			}

			if(context.KeyboardInput.IsKeyPressed(Key.KeypadPlus))
			{
				_localExposure++;
			}
			if(context.KeyboardInput.IsKeyPressed(Key.KeypadMinus))
			{
				_localExposure--;
			}
			

			TimerQueryCPU queryEcs = new TimerQueryCPU(context.TimerQueryCpuManager, "Ecs");
			TimerQueryCPU queryUpdateScene = new TimerQueryCPU(context.TimerQueryCpuManager, "UpdateScene");
			TimerQueryCPU queryGatherScene = new TimerQueryCPU(context.TimerQueryCpuManager, "GatherScene");

			//Stopwatch sw = new Stopwatch();

			// System updates
			queryEcs.StartQuery();
			_ecsManager.Tick();
			queryEcs.EndQuery();
			//sw.Start();
			queryUpdateScene.StartQuery();
			_sceneManager.Update(!_visualizeCulling);
			queryUpdateScene.EndQuery();
			//sw.Stop();

			//context.TextOutput.Print("Scene update:" + (sw.ElapsedTicks / (double)Stopwatch.Frequency * 1000) + " ms", OutputType.LogOnly);

			if(!_visualizeCulling)
			{
				_cameraCull = _camera.Clone();
			}


			queryGatherScene.StartQuery();
			List<MeshInstanceData> renderMeshesStatic;
			List<MeshInstanceData> renderMeshesStaticAlpha;
			List<MeshInstanceData> voxelMeshes = new List<MeshInstanceData>();

			if(_enableVoxelUpdate)
				voxelMeshes = _voxelManager.Update(context, _camera.Position, _debugVoxelClipmapLevel);
			//var voxelMeshes = new List<MeshInstanceData>();

			_sceneManager.GetData(_cameraCull.ToProjectionCamera(context.Viewport.DisplayWidth, context.Viewport.DisplayHeight, 1, 128), out renderMeshesStatic, out renderMeshesStaticAlpha);
			queryGatherScene.EndQuery();

			//foreach(var instance in _instancesRotate)
			//{
			//	render.Add(new MeshInstance(instance.Mesh, Utils.Math.GetWorldMatrix(Vector3.Zero, Quaternion.FromAxisAngle(Vector3.UnitZ, (float)(context.TimeEllapsed * 0.5)), new Vector3(10))));
			//}

			var batches = MeshBatch.GetBatchedInstances(renderMeshesStatic);
			var batchesAlpha = MeshBatch.GetBatchedInstances(renderMeshesStaticAlpha);

			//for(int i = 0; i < 10; i++)
			//{
			//	context.UIManager.AddText("The quick brown fox jumps over the lazy dog", new Vector2(0.1f) * (float)Math.Pow(0.8f, i), new Vector2(0f, 0.1f * i), new Vector4(1f), TextAlignmentHorizontal.Left, TextAlignmentVertical.Top);
			//}

			var sceneData = new TestSceneData()
			{
				Camera = new CameraData(_camera, _cameraPrevious),
				Batches = batches,
				BatchesAlphaTest = batchesAlpha,
				VisualizeCulling = _visualizeCulling,
				VisualizeVirtualTexture = _visualizeVt,
				ComputeCulling = _computeCull,
				EnableTonemap = _enableTonemap,
				EnableUI = _enableUI,
				EnableAA = _enableAA,
				//AllInstances = InstanceUnion(renderMeshesStatic, renderMeshesStaticAlpha, voxelMeshes),
				SunDir = _sunDir,
				//Exposure = ExposureFromSunAngle(_sunDir.Y),
				Ellapsed = context.TimeEllapsed,
				TimeDelta = context.TimeDelta,
			};

			context.UIManager.AddText($"Exposure: {_localExposure} SunAngle: {Math.Asin(sceneData.SunDir.Y) / Math.PI * 180} Pos: {_camera.Position.ToString()}", new Vector2(0.03f), new Vector2(1f, 1f), new Vector4(0.2f, 1.0f, 0.2f, 1.0f), TextAlignmentHorizontal.Right, TextAlignmentVertical.Bottom);

			string bottomLeftMsg = "";

			if(_visualizeCulling)
				bottomLeftMsg += "Culling PAUSED; ";
			if(!_enableTonemap)
				bottomLeftMsg += "Tonemapping OFF; ";
			if(!_enableAA)
				bottomLeftMsg += "AA OFF; ";
			if(context.Viewport.ResShift != 0)
				bottomLeftMsg += "ResShift = " + context.Viewport.ResShift.ToString() + "; ";

			context.UIManager.AddText(bottomLeftMsg, new Vector2(0.03f), new Vector2(0f, 1f), new Vector4(0.2f, 1.0f, 0.2f, 1.0f), TextAlignmentHorizontal.Left, TextAlignmentVertical.Bottom);

			context.RendererInjector.AddData(sceneData);
		}

		float ExposureFromSunAngle(float sunVecY)
		{
			double x = Math.Asin(sunVecY) / Math.PI * 180;
			double x2 = x * x;
			double x3 = x2 * x;
			double x4 = x2 * x2;
			double x5 = x4 * x;
			double x6 = x3 * x3;
			double x7 = x6 * x;
			double x8 = x4 * x4;
			double x9 = x8 * x;

			double y = 7.521352297e-8 * x9 - 2.308003053e-7 * x8 - 2.048970167e-5 * x7 - 7.043234916e-6 * x6 +
			           1.574503287e-3 * x5 + 3.876325109e-3 * x4 - 2.67785374e-2 * x3 - 6.889985385e-2 * x2 - 5.238297596e-1 *
			           x - 5.999999942;

			y = Math.Min(Math.Max(y, -9), 1);

			return (float)Math.Pow(2, y);
		}
	}
}
