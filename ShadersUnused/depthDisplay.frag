#version 430

#include includeTest.glsl

layout(location = 0) uniform sampler2D texDepth;

layout(location = 0) in vec2 screenPos;

layout(location = 0) out vec4 outColor;

void main() {
	float depth = texture(texDepth, screenPos.xy).x;
	depth *= depth;
	depth *= depth;
	depth *= depth;
	depth *= depth;
    outColor = vec4(depth, depth, depth, 1.0);
    //outColor = vec4(screenPos.xy, 0.0, 1.0);
}