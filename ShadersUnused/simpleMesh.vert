#version 430

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec3 vPos;
layout(location = 1) flat out int vIndex;
layout(location = 2) out vec3 vNormal;

uniform mat4 projection;

uniform vec3 posOffset;
uniform vec3 posScale;

void main() {
	vec3 pos = vertexPosition * posScale + posOffset;
	vIndex = gl_VertexID;
	vPos = pos;
	vNormal = vertexNormal;
    gl_Position = projection * vec4(pos, 1.0);
}