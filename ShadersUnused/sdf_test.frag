#version 430

#include common.glsl

layout(location = 0) in vec2 screenPos;
layout(location = 0) out vec4 fragcolor;

uniform vec3 lightDir;
uniform vec3 cameraPosition;

uniform vec4 ray00;
uniform vec4 ray01;
uniform vec4 ray11;
uniform vec4 ray10;

uniform vec2 resolution;
uniform vec2 resolution_rcp;

// Sources:
// http://jamie-wong.com/2016/07/15/ray-marching-signed-distance-functions/
// http://iquilezles.org/www/articles/distfunctions/distfunctions.htm
// http://www.iquilezles.org/www/articles/smin/smin.htm

float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

float IntersectSDF(float distA, float distB)
{
    return max(distA, distB);
}

float UnionSDF(float distA, float distB)
{
    return min(distA, distB);
}

float DifferenceSDF(float distA, float distB)
{
    return max(distA, -distB);
}

float SphereSDF(vec3 p, vec3 pos, float radius)
{
    return length(pos - p) - radius;
}

float CubeSDF(vec3 p, vec3 pos, vec3 size)
{
	return length(max(abs(p - pos) - size, 0.0));
	/*
	vec3 localp = (p - pos) / size;
	vec3 closest = clamp(localp, -1.0, 1.0);
	float greatest = maxcomp(abs(closest));
	
	if(abs(closest.x) == greatest)
		closest.x = sign(closest.x);
	if(abs(closest.y) == greatest)
		closest.y = sign(closest.y);
	if(abs(closest.z) == greatest)
		closest.z = sign(closest.z);
	if(greatest == 0.0)	
		closest = vec3(1.0, 0.0, 0.0);	
	
	closest *= size;
	float dist = length(p - closest - pos);
	if(maxcomp(abs(localp)) < 1.0)
		dist = -dist;
	return dist;
	*/
}

float CubeRoundSDF(vec3 p, vec3 pos, vec3 size, float radius)
{
	return CubeSDF(p, pos, size) - radius;
}

float GlobalSDF(vec3 p)
{
	float sphere = SphereSDF(p, vec3(0.0, 0.0, 0.0), 1.0);
	float dist = sphere;
	dist = UnionSDF(dist, SphereSDF(p, vec3(0.0, 1.0, 0.0), 1.0));
	dist = IntersectSDF(dist, CubeRoundSDF(p, vec3(1.0, 0.0, 0.0), vec3(0.6, 0.6, 0.6), 0.1));
	return max(dist, 0.0);
}

vec3 EstimateNormal(vec3 p)
{
	const float EPSILON = 0.01;
    return normalize(vec3(
        GlobalSDF(vec3(p.x + EPSILON, p.y, p.z)) - GlobalSDF(vec3(p.x - EPSILON, p.y, p.z)),
        GlobalSDF(vec3(p.x, p.y + EPSILON, p.z)) - GlobalSDF(vec3(p.x, p.y - EPSILON, p.z)),
        GlobalSDF(vec3(p.x, p.y, p.z  + EPSILON)) - GlobalSDF(vec3(p.x, p.y, p.z - EPSILON))
    ));
}

const float totalDist = 10.0;

void TraceRay(vec3 origin, vec3 dir, out vec3 p, out float dist, out float minDist)
{
	// Raymarch
	dist = 0.0;	
		
	minDist = totalDist * 10;
	
	int maxIter = 100;
	int i = 0;
	p;
	
	while(dist < totalDist && i < maxIter)
	{
		p = origin + dir * dist;
		float d = GlobalSDF(p);
		
		dist += d;
		minDist = min(minDist, d);
		
		if(d <= 0.0005)
			break;
		i++;
	}
}

void main() {
	vec3 ray = mix(mix(ray00, ray10, screenPos.x), mix(ray01, ray11, screenPos.x), screenPos.y).xyz;
	ray = normalize(ray);
	
	vec3 p;
	float dist, minDist;
	
	TraceRay(cameraPosition, ray, p, dist, minDist);
	
	if(dist > totalDist * 0.99)
	{
		fragcolor = vec4(0.0);
		return;
	}
	
	vec3 normal = EstimateNormal(p);
	
	//vec3 lightDir = normalize((ray00.xyz + ray01.xyz + ray11.xyz + ray10.xyz)).yzx;
	//vec3 lightDir = normalize(vec3(1.0, 0.5, 2.0));
	
	
	float lightDist, lightMinDist;
	vec3 lightP;
	TraceRay(p + lightDir * 0.05, lightDir, lightP, lightDist, lightMinDist);
	
	float light = (max(dot(normal, lightDir), 0.0) * clamp(lightMinDist * 100, 0.0, 1.0) * 0.75 + 0.25);
	
    fragcolor = vec4((normal.xyz * 0.5 + 0.5) * light, 1.0);
	
	
	//fragcolor = vec4((normal.xyz * 0.5 + 0.5), 1.0);
}