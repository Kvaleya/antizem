#version 430

uvec2 PackVertex(vec3 vert)
{
	uvec2 vpacked;
	vpacked.x = uint(clamp(vert.x, 0.0, 1.0) * 65535);
	vpacked.x |= uint(clamp(vert.y, 0.0, 1.0) * 65535) << 16;
	vpacked.y = floatBitsToUint(vert.z);
	return vpacked;
}

vec3 UnpackVertex(uvec2 vpacked)
{
	vec3 vert;
	
	const float ushort_rcp = 1.0 / 65535;
	
	vert.x = float(vpacked.x & 0x0000FFFF) * ushort_rcp;
	vert.y = float((vpacked.x >> 16) & 0x0000FFFF) * ushort_rcp;
	vert.z = uintBitsToFloat(vpacked.y);
	
	return vert;
}