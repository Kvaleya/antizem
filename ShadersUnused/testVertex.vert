#version 430

#include includeTest.glsl

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec3 vertColor;
layout(location = 1) out vec2 screenPos;

vec2 positions[3] = vec2[3](
    vec2(0.0, -0.5),
    vec2(0.5, 0.5),
    vec2(-0.5, 0.5)
);

vec3 colors[3] = vec3[3](
    vec3(1.0, CLR, 0.0),
    vec3(1.0, 1.0, 0.0),
    vec3(0.0, 0.0, 1.0)
);

void main() {
    gl_Position = vec4(positions[gl_VertexID], 0.0, 1.0);
    vertColor = colors[gl_VertexID];
	screenPos = gl_Position.xy;
}