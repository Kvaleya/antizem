#version 430

#include common.glsl

layout(location = 0) in vec2 screenPos;
layout(location = 0) out vec4 fragcolor;

uniform vec3 cameraPosition;

uniform vec4 ray00;
uniform vec4 ray01;
uniform vec4 ray11;
uniform vec4 ray10;

uniform vec2 resolution;
uniform vec2 resolution_rcp;

void main() {
	vec3 ray = mix(mix(ray00, ray10, screenPos.x), mix(ray01, ray11, screenPos.x), screenPos.y).xyz;
	ray = normalize(ray);
	
	vec3 p;
	float dist, minDist;
	
	TraceRay(cameraPosition, ray, p, dist, minDist);
	
	if(dist > totalDist * 0.99)
	{
		fragcolor = vec4(0.0);
		return;
	}
	
	vec3 normal = EstimateNormal(p);
	
	//vec3 lightDir = normalize((ray00.xyz + ray01.xyz + ray11.xyz + ray10.xyz)).yzx;
	//vec3 lightDir = normalize(vec3(1.0, 0.5, 2.0));
	
	
	float lightDist, lightMinDist;
	vec3 lightP;
	TraceRay(p + lightDir * 0.05, lightDir, lightP, lightDist, lightMinDist);
	
	float light = (max(dot(normal, lightDir), 0.0) * clamp(lightMinDist * 100, 0.0, 1.0) * 0.75 + 0.25);
	
    fragcolor = vec4((normal.xyz * 0.5 + 0.5) * light, 1.0);
	
	
	//fragcolor = vec4((normal.xyz * 0.5 + 0.5), 1.0);
}