#version 430

#include includeTest.glsl

layout(location = 0) in vec3 vertColor;
layout(location = 1) in vec2 screenPos;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = vec4(vertColor + vec3(CLR), 1.0);
	//outColor = vec4(1.0);
	
	float scale = 1.0;
	vec2 center = vec2(0.0, 0.0);
	
	vec2 tc = screenPos;
	
	vec2 z, c;
	
	int iter = 100;
	
	c.x = 1.3333 * (tc.x - 0.5) * scale - center.x;
    c.y = (tc.y - 0.5) * scale - center.y;

    int i;
    z = c;
    for(i=0; i<iter; i++) {
        float x = (z.x * z.x - z.y * z.y) + c.x;
        float y = (z.y * z.x + z.x * z.y) + c.y;

        if((x * x + y * y) > 4.0) break;
        z.x = x;
        z.y = y;
    }
	
	outColor = vec4(float(i) / 100.0);
}