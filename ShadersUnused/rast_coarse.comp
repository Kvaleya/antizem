#version 430

#include vertexPack.glsl

layout (binding = 0, r32ui) uniform restrict uimage2D depthBuffer;

layout(binding = 1, std430) readonly buffer bufferIndices
{
    int indices[];
};

layout(binding = 2, std430) readonly buffer bufferPositionsTransformed
{
    vec4 positionsTransformed[];
};

#include rasterTiles.glsl

uniform vec2 resolution;
uniform vec2 resolution_rcp;

uniform int firstElement;
uniform int elementCount;

float Orient2D(vec2 a, vec2 b, vec2 c)
{
	return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

layout (local_size_x = 256, local_size_y = 1, local_size_z = 1) in;
void main(void)
{
	if(gl_GlobalInvocationID.x * 3 >= elementCount)
		return;
	
	// Load triangle
	int triangle = firstElement + int(gl_GlobalInvocationID.x) * 3;
	
	ivec3 vindex;
	
	vindex.x = indices[triangle];
	vindex.y = indices[triangle + 1];
	vindex.z = indices[triangle + 2];
		
	vec4 pos0 = positionsTransformed[vindex.x];
	vec4 pos1 = positionsTransformed[vindex.y];
	vec4 pos2 = positionsTransformed[vindex.z];
			
	// compute bounding box
	
	vec2 aabbMin = vec2(min(pos0.x, min(pos1.x, pos2.x)), min(pos0.y, min(pos1.y, pos2.y)));
	vec2 aabbMax = vec2(max(pos0.x, max(pos1.x, pos2.x)), max(pos0.y, max(pos1.y, pos2.y)));
	
	// clip AABB to screen
	
	aabbMin = max(aabbMin, vec2(0.0));
	aabbMax = min(aabbMax, vec2(1.0));
	
	if(aabbMin.x < aabbMax.x && aabbMin.y < aabbMax.y)
	{
		const float prec = 65536.0;
		const float prec_rcp = 1.0 / prec;
		
		vec2 clampedMin = (floor(aabbMin * resolution) - vec2(1.5)) * prec_rcp;
		vec2 clampedMax = (ceil(aabbMax * resolution) + vec2(1.5)) * prec_rcp;
		
		// Triangle area check
		float area = Orient2D(pos0.xy * resolution, pos1.xy * resolution, pos2.xy * resolution);
		if(area <= 0.0)
			return;
			
		int smallTriangle = 0;
		
		//if(area < 16.0)
		//	smallTriangle = numTilesTotal;
		
		pos0.xy = pos0.xy * resolution * prec_rcp;
		pos1.xy = pos1.xy * resolution * prec_rcp;
		pos2.xy = pos2.xy * resolution * prec_rcp;
		
		vec3 w_row = vec3(Orient2D(pos1.xy, pos2.xy, clampedMin), Orient2D(pos2.xy, pos0.xy, clampedMin), Orient2D(pos0.xy, pos1.xy, clampedMin));
			
		vec3 a = vec3(pos0.y - pos1.y, pos1.y - pos2.y, pos2.y - pos0.y) * prec_rcp;
		vec3 b = vec3(pos1.x - pos0.x, pos2.x - pos1.x, pos0.x - pos2.x) * prec_rcp;		
		
		// For conservative rasterization
		vec3 bias = vec3(
			Orient2D(pos1.xy, pos2.xy, pos1.xy + normalize(vec2(pos2.y - pos1.y, pos1.x - pos2.x)) * prec_rcp),
			Orient2D(pos2.xy, pos0.xy, pos2.xy + normalize(vec2(pos0.y - pos2.y, pos2.x - pos0.x)) * prec_rcp),
			Orient2D(pos0.xy, pos1.xy, pos0.xy + normalize(vec2(pos1.y - pos0.y, pos0.x - pos1.x)) * prec_rcp)
		);
		
		w_row -= bias;
		
		vec2 p;
		for(p.y = clampedMin.y; p.y < clampedMax.y; p.y += prec_rcp)
		{
			vec3 w = w_row;
			for(p.x = clampedMin.x; p.x < clampedMax.x; p.x += prec_rcp)
			{
				if((floatBitsToInt(w.x) | floatBitsToInt(w.y) | floatBitsToInt(w.z)) >= 0)
				{
					ivec2 pixel = ivec2(p.xy * prec);
					pixel.x += smallTriangle;
					
					if(AddTriangleTile(pixel, triangle))
						return;
				}
				
				w += a.yzx;
			}
			
			w_row += b.yzx;
		}
	}
}