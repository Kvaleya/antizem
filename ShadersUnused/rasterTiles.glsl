#version 430

#define LOOSE_TRIS_GROUP_SIZE 256
#define LOOSE_TRIS_GROUP_SIZE_POW 8

uniform int numTilesX;
uniform int numTilesTotal;
uniform int looseTrianglesStart;

layout(binding = 3, std430) buffer bufferDispatchIndirect
{
	int numGroupsLooseTris;
    int numGroupsY_02;
    int numGroupsZ_02;
	int numLooseTriangles;		
};

layout(binding = 4, std430) buffer bufferTileTriangles
{
    int tileTriangles[];
};

int AllocLooseTriangle()
{
	// Alloc triangle
	int index = atomicAdd(numLooseTriangles, 1);
	// Number of groups needed to process current amount of triangles
	int groups = (index + LOOSE_TRIS_GROUP_SIZE) >> LOOSE_TRIS_GROUP_SIZE_POW;
	// If the current group count is lower, increase it
	atomicMax(numGroupsLooseTris, groups);
	//atomicCompSwap(numGroupsLooseTris, groups - 1, groups);	
	
	return index;
}

bool AddTriangleTile(ivec2 tile, int triangle)
{
	int tileIndex = tile.x + tile.y * numTilesX;
	int triangleIndex;
	
	bool loose = true;
	
	if(tileIndex < numTilesTotal)
	{
		int tileStart = tileIndex * TILE_TRIANGLE_COUNT;
		int inTile = atomicAdd(tileTriangles[tileStart], 1);
		
		if(inTile < TILE_TRIANGLE_COUNT - 1)
		{
			triangleIndex = tileStart + inTile + 1;
			loose = false;
		}
	}
	
	if(loose)
	{
		triangleIndex = looseTrianglesStart + AllocLooseTriangle();
	}
	
	tileTriangles[triangleIndex] = triangle;
	
	return loose;
}