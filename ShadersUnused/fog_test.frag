#version 430

#include common.glsl

layout(location = 0) in vec2 screenPos;
layout(location = 0) out vec4 fragcolor;

uniform vec3 lightDir;
uniform vec3 cameraPosition;

uniform vec4 ray00;
uniform vec4 ray01;
uniform vec4 ray11;
uniform vec4 ray10;

uniform vec2 resolution;
uniform vec2 resolution_rcp;

// Author @patriciogv - 2015
// http://patriciogonzalezvivo.com
float random (in vec3 st) {
    return fract(sin(dot(st.xyz,
                         vec3(12.9898,78.233, 163.248)))*
        43758.5453123);
}

// Based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise (in vec3 st) {
    vec3 i = floor(st);
    vec3 f = fract(st);

	// Corners in a cube
    float a0 = random(i + vec3(0.0, 0.0, 0.0));
    float b0 = random(i + vec3(1.0, 0.0, 0.0));
    float c0 = random(i + vec3(0.0, 1.0, 0.0));
    float d0 = random(i + vec3(1.0, 1.0, 0.0));
	
	float a1 = random(i + vec3(0.0, 0.0, 1.0));
    float b1 = random(i + vec3(1.0, 0.0, 1.0));
    float c1 = random(i + vec3(0.0, 1.0, 1.0));
    float d1 = random(i + vec3(1.0, 1.0, 1.0));

    vec3 u = f * f * (3.0 - 2.0 * f);

	float m0 = mix(a0, b0, u.x) +
            (c0 - a0)* u.y * (1.0 - u.x) +
            (d0 - b0) * u.x * u.y;
			
	float m1 = mix(a1, b1, u.x) +
            (c1 - a1)* u.y * (1.0 - u.x) +
            (d1 - b1) * u.x * u.y;
	
    return mix(m0, m1, u.z);
}

#define OCTAVES 4
float fbm (in vec3 st) {
    // Initial values
    float value = 0.0;
    float amplitude = .5;
    float frequency = 0.;
    //
    // Loop of octaves
    for (int i = 0; i < OCTAVES; i++) {
        value += amplitude * noise(st);
        st *= 2.;
        amplitude *= .5;
    }
    return value;
}

float Powder(float dens)
{
	return clamp(1.0 - exp(-dens * 2.0), 0.0, 1.0);
}

float Beer(float dens)
{
	return exp(-dens);
}

float BeerPowder(float dens)
{
	return Beer(dens) * Powder(dens);
}

#define CLOUD_VISFUNC_ALPHA(dens) Beer(dens)
#define CLOUD_VISFUNC_LIGHT(dens) BeerPowder(dens)

vec4 AccumulateScattering(vec4 front, vec4 back)
{
	vec3 light = front.rgb + clamp(CLOUD_VISFUNC_ALPHA(front.a), 0.0, 1.0) * back.rgb;
	return vec4(light, front.a + back.a);
}

float CloudDens(vec3 p)
{
	float dens = fbm(p.xyz);
	float toCenter = length(p - vec3(0.0, 0.0, 0.0));
	
	const float sradius = 1.5;
	const float sfalloff = 0.2;
	
	float insphere = clamp(1.0 - max(toCenter - sradius, 0.0) / sfalloff, 0.0, 1.0);
	
	dens *= insphere;
	
	return dens * 0.9;
}

vec4 GetCloud(vec3 p)
{
	float shadow = 0.0;
	
	vec3 l = normalize(vec3(1.0, 0.5, 0.2));
	
	shadow += CloudDens(p + l);
	shadow += CloudDens(p + l + vec3(0.2, 0.1, 0.3));
	shadow += CloudDens(p + l + vec3(0.3, -0.2, -0.1));
	shadow += CloudDens(p + l + vec3(-0.2, -0.3, 0.1));
	shadow += CloudDens(p + l + vec3(-0.1, 0.1, -0.2));
	
	return vec4(CLOUD_VISFUNC_ALPHA(shadow).xxx, CloudDens(p));
}

void main() {
	vec3 ray = mix(mix(ray00, ray10, screenPos.x), mix(ray01, ray11, screenPos.x), screenPos.y).xyz;
	ray = normalize(ray);
	
	vec3 p;
	
	float dist = 0.0;
	float maxdist = 8.0;
	
	const int iters = 16;
	
	float stepLength = maxdist / iters;
	
	vec4 accum = vec4(0.0);
	
	for(int i = 0; i < iters; i++)
	{		
		dist = i * stepLength;
		p = cameraPosition + ray * dist;
		
		vec4 cloud = GetCloud(p);
		
		cloud.a *= stepLength;
		cloud.rgb *= max(Powder(cloud.a), 0.0);
		
		accum = AccumulateScattering(accum, cloud);
	}	
	
	vec4 result = vec4(accum.rgb, clamp(CLOUD_VISFUNC_ALPHA(accum.a), 0.0, 1.0));
	
	fragcolor.rgb = result.rgb * (1.0 - result.a);
	
	//fragcolor = vec4((normal.xyz * 0.5 + 0.5), 1.0);
}






























