#version 430

#include common.glsl

layout(location = 0) in vec2 screenPos;
layout(location = 0) out vec4 fragcolor;

uniform vec3 lightDir;
uniform vec3 cameraPosition;

uniform vec4 ray00;
uniform vec4 ray01;
uniform vec4 ray11;
uniform vec4 ray10;

uniform vec2 resolution;
uniform vec2 resolution_rcp;

// Author @patriciogv - 2015
// http://patriciogonzalezvivo.com
float random (in vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}

// Based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    vec2 u = f * f * (3.0 - 2.0 * f);

    return mix(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}

#define OCTAVES 6
float fbm (in vec2 st) {
    // Initial values
    float value = 0.0;
    float amplitude = .5;
    float frequency = 0.;
    //
    // Loop of octaves
    for (int i = 0; i < OCTAVES; i++) {
        value += amplitude * noise(st);
        st *= 2.;
        amplitude *= .5;
    }
    return value;
}

// Sources:
// http://jamie-wong.com/2016/07/15/ray-marching-signed-distance-functions/
// http://iquilezles.org/www/articles/distfunctions/distfunctions.htm
// http://www.iquilezles.org/www/articles/smin/smin.htm

float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

float IntersectSDF(float distA, float distB)
{
    return max(distA, distB);
}

float UnionSDF(float distA, float distB)
{
    return min(distA, distB);
}

float DifferenceSDF(float distA, float distB)
{
    return max(distA, -distB);
}

float SphereSDF(vec3 p, vec3 pos, float radius)
{
    return length(pos - p) - radius;
}

float CubeSDF(vec3 p, vec3 pos, vec3 size)
{
	return length(max(abs(p - pos) - size, 0.0));
	/*
	vec3 localp = (p - pos) / size;
	vec3 closest = clamp(localp, -1.0, 1.0);
	float greatest = maxcomp(abs(closest));
	
	if(abs(closest.x) == greatest)
		closest.x = sign(closest.x);
	if(abs(closest.y) == greatest)
		closest.y = sign(closest.y);
	if(abs(closest.z) == greatest)
		closest.z = sign(closest.z);
	if(greatest == 0.0)	
		closest = vec3(1.0, 0.0, 0.0);	
	
	closest *= size;
	float dist = length(p - closest - pos);
	if(maxcomp(abs(localp)) < 1.0)
		dist = -dist;
	return dist;
	*/
}

float CubeRoundSDF(vec3 p, vec3 pos, vec3 size, float radius)
{
	return CubeSDF(p, pos, size) - radius;
}

float GlobalSDF(vec3 p)
{
	float sphere = SphereSDF(p, vec3(0.0, 0.0, 0.0), 1.0);
	float dist = sphere;
	dist = UnionSDF(dist, SphereSDF(p, vec3(0.0, 1.0, -1.0), 1.0));
	dist = IntersectSDF(dist, CubeRoundSDF(p, vec3(1.0, 0.0, -1.0), vec3(0.6, 0.6, 0.6), 0.1));
	dist = UnionSDF(dist, CubeRoundSDF(p, vec3(0.0, 0.0, 1.2), vec3(0.1, 0.6, 1.8), 0.1));
	return max(dist, 0.0);
}

vec3 EstimateNormal(vec3 p)
{
	const float EPSILON = 0.01;
    return normalize(vec3(
        GlobalSDF(vec3(p.x + EPSILON, p.y, p.z)) - GlobalSDF(vec3(p.x - EPSILON, p.y, p.z)),
        GlobalSDF(vec3(p.x, p.y + EPSILON, p.z)) - GlobalSDF(vec3(p.x, p.y - EPSILON, p.z)),
        GlobalSDF(vec3(p.x, p.y, p.z  + EPSILON)) - GlobalSDF(vec3(p.x, p.y, p.z - EPSILON))
    ));
}

const float totalDist = 10.0;

void TraceRay(vec3 origin, vec3 dir, out vec3 p, out float dist, out float minDist)
{
	// Raymarch
	dist = 0.0;	
		
	minDist = totalDist * 10;
	
	int maxIter = 100;
	int i = 0;
	p;
	
	while(dist < totalDist && i < maxIter)
	{
		p = origin + dir * dist;
		float d = GlobalSDF(p);
		
		dist += d;
		minDist = min(minDist, d);
		
		if(d <= 0.0005)
			break;
		i++;
	}
}

void main() {
	vec3 ray = mix(mix(ray00, ray10, screenPos.x), mix(ray01, ray11, screenPos.x), screenPos.y).xyz;
	ray = normalize(ray);
	
	vec3 p;
	
	float dist = 0.0;
	float maxdist = 16.0;
	
	const int iters = 100;
	
	for(int i = 0; i < iters; i++)
	{
		
		dist = (i / float(iters)) * maxdist;
		p = cameraPosition + ray * dist;
		float h = fbm(p.xy) * 2 - 1.7;
		if(h >= p.z)
		{
			break;
		}
	}
	
	
	fragcolor = vec4(dist / maxdist, 0.0, 0.0, 1.0);
	
	//fragcolor = vec4((normal.xyz * 0.5 + 0.5), 1.0);
}






























