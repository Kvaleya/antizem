﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Meshes
{
	public class MeshData
	{
		public readonly string Filename;
		public readonly string MeshName;
		public readonly List<Vertex> Vertices;

		public MeshData(string filename, string meshName)
		{
			Filename = filename;
			MeshName = meshName;
			Vertices = new List<Vertex>();
		}
	}
}
