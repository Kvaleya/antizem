﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Meshes
{
	public class MeshGroup
	{
		public readonly string FileName;
		public List<MeshData> Meshes;

		internal MeshGroup(string fileName, List<MeshData> meshes)
		{
			FileName = fileName;
			Meshes = meshes;
		}
	}
}
