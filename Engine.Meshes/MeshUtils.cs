﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using MeshOptimizerLib;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Meshes
{
	internal static class MeshUtils
	{
		/// <summary>
		/// Generates normals and smooth tangents
		/// </summary>
		/// <param name="mesh"></param>
		/// <param name="smoothAngleThreshold"></param>
		public static void GenerateNormals(MeshData mesh, float smoothAngleThreshold = 0)
		{
			float smoothAngleDot = (float)Math.Cos(smoothAngleThreshold);

			// List of all vertices with the same position
			Dictionary<Vector3, List<int>> sharedPos = new Dictionary<Vector3, List<int>>();

			for(int i = 0; i < mesh.Vertices.Count; i++)
			{
				var p = mesh.Vertices[i].Position;

				if(!sharedPos.ContainsKey(p))
				{
					sharedPos.Add(p, new List<int>());
				}

				sharedPos[p].Add(i);
			}

			// Clear current normals to flat normals
			for(int i = 0; i < mesh.Vertices.Count; i += 3)
			{
				Vector3 a = mesh.Vertices[i + 0].Position;
				Vector3 b = mesh.Vertices[i + 1].Position;
				Vector3 c = mesh.Vertices[i + 2].Position;

				Vector3 n = Vector3.Cross(b - a, c - a).Normalized();

				mesh.Vertices[i + 0].Normal = n;
				mesh.Vertices[i + 1].Normal = n;
				mesh.Vertices[i + 2].Normal = n;
			}

			Vector3[] newNormals = new Vector3[mesh.Vertices.Count];
			Vector3[] newTangents = new Vector3[mesh.Vertices.Count];

			HashSet<Vector3> normals = new HashSet<Vector3>();
			HashSet<Vector3> tangents = new HashSet<Vector3>();

			for(int i = 0; i < mesh.Vertices.Count; i++)
			{
				normals.Clear();
				tangents.Clear();

				normals.Add(mesh.Vertices[i].Normal);
				tangents.Add(mesh.Vertices[i].Tangent);

				foreach(int v in sharedPos[mesh.Vertices[i].Position])
				{
					if(v == i)
						continue;

					if(Vector3.Dot(mesh.Vertices[v].Normal, mesh.Vertices[i].Normal) > smoothAngleDot && !normals.Contains(mesh.Vertices[v].Normal))
					{
						normals.Add(mesh.Vertices[v].Normal);
						tangents.Add(mesh.Vertices[v].Tangent);
					}
				}

				Vector3 n = new Vector3();
				Vector3 t = new Vector3();

				foreach(Vector3 v in normals)
				{
					n += v;
				}
				foreach(Vector3 v in tangents)
				{
					t += v;
				}

				newNormals[i] = (n / normals.Count).Normalized();
				newTangents[i] = (t / tangents.Count).Normalized();
			}

			for(int i = 0; i < mesh.Vertices.Count; i++)
			{
				mesh.Vertices[i].Normal = newNormals[i];
				mesh.Vertices[i].Tangent = newTangents[i];
			}
		}

		public static void GenerateTangents(MeshData data)
		{
			Vertex v0, v1, v2;

			for(int i = 2; i < data.Vertices.Count; i += 3)
			{
				v0 = data.Vertices[i - 2];
				v1 = data.Vertices[i - 1];
				v2 = data.Vertices[i - 0];

				Vector3 edge1 = v1.Position - v0.Position;
				Vector3 edge2 = v2.Position - v0.Position;

				float deltaU1 = v1.TexCoord.X - v0.TexCoord.X;
				float deltaV1 = v1.TexCoord.Y - v0.TexCoord.Y;
				float deltaU2 = v2.TexCoord.X - v0.TexCoord.X;
				float deltaV2 = v2.TexCoord.Y - v0.TexCoord.Y;

				float f = 1.0f / (deltaU1 * deltaV2 - deltaU2 * deltaV1);

				Vector3 tangent;

				tangent.X = f * (deltaV2 * edge1.X - deltaV1 * edge2.X);
				tangent.Y = f * (deltaV2 * edge1.Y - deltaV1 * edge2.Y);
				tangent.Z = f * (deltaV2 * edge1.Z - deltaV1 * edge2.Z);

				if(float.IsNaN(tangent.X) || float.IsNaN(tangent.Y) || float.IsNaN(tangent.Z))
				{
					tangent = new Vector3(1, 0, 0);
				}

				tangent.Normalize();

				v0.Tangent = tangent;
				v1.Tangent = tangent;
				v2.Tangent = tangent;

				data.Vertices[i - 2] = v0;
				data.Vertices[i - 1] = v1;
				data.Vertices[i - 0] = v2;
			}
		}

		public static void ReduceVertexDataPrecision(MeshData mesh)
		{
			const double maxTexCoordPrecision = 1.0 / 16384;

			for(int i = 0; i < mesh.Vertices.Count; i++)
			{
				var vertex = mesh.Vertices[i];
				vertex.Normal = Utils.Pack.UnpackNormal(Utils.Pack.PackNormal(vertex.Normal));
				vertex.Tangent = Utils.Pack.UnpackNormal(Utils.Pack.PackNormal(vertex.Tangent));
				float u = (float)((long)((double)vertex.TexCoord.X / maxTexCoordPrecision) * maxTexCoordPrecision);
				float v = (float)((long)((double)vertex.TexCoord.Y / maxTexCoordPrecision) * maxTexCoordPrecision);
				vertex.TexCoord = new Vector2(u, v);
			}
		}

		public static void GenerateIndexedMesh(MeshData mesh, out List<Vertex> vertices, out List<int> indices)
		{
			Dictionary<Vertex, int> vertexIndices = new Dictionary<Vertex, int>();
			vertices = new List<Vertex>();
			indices = new List<int>();

			int lastIndex = 0;

			foreach(Vertex v in mesh.Vertices)
			{
				int index = lastIndex;

				if(vertexIndices.ContainsKey(v))
				{
					index = vertexIndices[v];
				}
				else
				{
					vertexIndices.Add(v, lastIndex);
					lastIndex++;
					vertices.Add(v);
				}

				indices.Add(index);
			}
		}

		public static int[] GenerateShadowIndices(List<Vertex> vertices, int[] indices)
		{
			int[] shadowIndices = new int[indices.Length];
			Dictionary<Vector3, int> vertexIndices = new Dictionary<Vector3, int>();

			int lastIndex = 0;

			foreach(Vertex v in vertices)
			{
				if(!vertexIndices.ContainsKey(v.Position))
				{
					vertexIndices.Add(v.Position, lastIndex);
					lastIndex++;
				}
			}

			for(int i = 0; i < indices.Length; i++)
			{
				shadowIndices[i] = vertexIndices[vertices[indices[i]].Position];
			}

			return shadowIndices;
		}

		public static void ReorderVertices(List<Vertex> vertices, int[] indices)
		{
			int[] remap = new int[vertices.Count];

			for(int i = 0; i < remap.Length; i++)
			{
				remap[i] = -1;
			}

			int pos = 0;

			var verts = vertices.ToList();

			for(int i = 0; i < indices.Length; i++)
			{
				if(remap[indices[i]] < 0)
				{
					vertices[pos] = verts[indices[i]];
					remap[indices[i]] = pos++;
				}
				indices[i] = remap[indices[i]];
			}
		}

		public static unsafe int[] OptimizeVertexCache(int[] indices, int vertexCount)
		{
			int[] result = new int[indices.Length];
			fixed(int* i = &indices[0])
			fixed(int* r = &result[0])
			{
				MeshOptimizer.OptimizeVertexCache(r, i, indices.Length, vertexCount);
			}
			return result;
		}

		public static byte[] GenClusterData(List<Vertex> vertices, int[] indices)
		{
			int clusterCount = (indices.Length + MeshBinary.ClusterSize * 3 - 1) / (MeshBinary.ClusterSize * 3);

			byte[] clusterData = new byte[clusterCount * MeshBinary.ClusterDataSize];

			Parallel.For(0, clusterCount, clusterIndex =>
			{
				Vector3 positionAabbMin = new Vector3(float.PositiveInfinity);
				Vector3 positionAabbMax = new Vector3(float.NegativeInfinity);

				Vector3 normalAabbMin = new Vector3(float.PositiveInfinity);
				Vector3 normalAabbMax = new Vector3(float.NegativeInfinity);

				int triangleCount = Math.Min(MeshBinary.ClusterSize, indices.Length / 3 - clusterIndex * MeshBinary.ClusterSize);

				Vector3[] normals = new Vector3[triangleCount];

				for(int i = 0; i < triangleCount; i++)
				{
					int firstIndex = i * 3 + clusterIndex * MeshBinary.ClusterSize * 3;

					Vector3[] positions = new Vector3[]
					{
						vertices[indices[firstIndex + 0]].Position,
						vertices[indices[firstIndex + 1]].Position,
						vertices[indices[firstIndex + 2]].Position,
					};

					Vector3 normal = Vector3.Cross(positions[1] - positions[0], positions[2] - positions[0]).Normalized();
					normals[i] = normal;

					// Positions aabb
					for(int j = 0; j < 3; j++)
					{
						for(int k = 0; k < 3; k++)
						{
							if(positions[j][k] < positionAabbMin[k])
								positionAabbMin[k] = positions[j][k];
							if(positions[j][k] > positionAabbMax[k])
								positionAabbMax[k] = positions[j][k];
						}
					}

					// Normals aabb
					for(int j = 0; j < 3; j++)
					{
						if(normal[j] < normalAabbMin[j])
							normalAabbMin[j] = normal[j];
						if(normal[j] > normalAabbMax[j])
							normalAabbMax[j] = normal[j];
					}
				}

				Vector4 boundingSphere = Vector4.Zero;
				boundingSphere.Xyz = (positionAabbMax + positionAabbMin) / 2;
				boundingSphere.W = (positionAabbMax - boundingSphere.Xyz).Length;

				// Guess the best bounding cone of triangle normals
				Vector4 bestCone = new Vector4(0, 0, 0, -1);
				bestCone.Xyz = ((normalAabbMax + normalAabbMin) / 2).Normalized();

				Vector3 newCone = bestCone.Xyz;

				Random random = new Random(clusterIndex);

				const int coneIterations = 1024;

				double angleStep = 0.1;
				double angleStepMultiply = 0.99;

				for(int randomIter = 0; randomIter < coneIterations; randomIter++)
				{
					float worstCos = 1;

					for(int i = 0; i < normals.Length; i++)
					{
						float cos = Vector3.Dot(newCone, normals[i]);

						if(cos < worstCos)
							worstCos = cos;
					}

					if(worstCos > bestCone.W)
					{
						bestCone.Xyz = newCone;
						bestCone.W = worstCos;
					}

					Vector3d eulerAngles = new Vector3d(random.NextDouble(), random.NextDouble(), random.NextDouble());
					eulerAngles = (eulerAngles * 2.0 - Vector3d.One) * Math.PI * angleStep;
					newCone = Vector3.Transform(bestCone.Xyz, Quaternion.FromEulerAngles(new Vector3((float)eulerAngles.X, (float)eulerAngles.Y, (float)eulerAngles.Z)));
					angleStep *= angleStepMultiply;
				}

				// Finalize
				bestCone.W = (float)(-Math.Sin(Math.Min(Math.Max(Math.Acos(bestCone.W), 0) + Math.PI / 32, Math.PI * 0.5)));

				int offset = clusterIndex * MeshBinary.ClusterDataSize;

				AddBytes(BitConverter.GetBytes(boundingSphere.X), clusterData, ref offset);
				AddBytes(BitConverter.GetBytes(boundingSphere.Y), clusterData, ref offset);
				AddBytes(BitConverter.GetBytes(boundingSphere.Z), clusterData, ref offset);
				AddBytes(BitConverter.GetBytes(boundingSphere.W), clusterData, ref offset);

				AddBytes(BitConverter.GetBytes(bestCone.X), clusterData, ref offset);
				AddBytes(BitConverter.GetBytes(bestCone.Y), clusterData, ref offset);
				AddBytes(BitConverter.GetBytes(bestCone.Z), clusterData, ref offset);
				AddBytes(BitConverter.GetBytes(bestCone.W), clusterData, ref offset);
			});

			return clusterData;
		}

		static void AddBytes(byte[] source, byte[] target, ref int offset)
		{
			for(int i = 0; i < source.Length; i++)
			{
				target[offset] = source[i];
				offset++;
			}
		}
	}
}
