﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Meshes
{
	public class MeshDescription
	{
		public const int Size = 12;

		public int VertexCount;
		public int IndexCount;
		public int IndexCountShadow;
	}
}
