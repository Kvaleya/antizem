﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Engine.Common;
using GameFramework;

namespace Engine.Meshes
{
	public class MeshBinaryResourceManager : ResourceManager<MeshBinary>
	{
		//ReaderWriterLockSlim _objCacheLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
		TextOutput _output;
		IFileManager _fileManager;

		Dictionary<string, MeshBinary> _objCache = new Dictionary<string, MeshBinary>();
		HashSet<string> _objCacheFiles = new HashSet<string>();
		WorkerThread _ioThread;

		public MeshBinaryResourceManager(IFileManager fileManager, TextOutput output, WorkerThread ioThread, long memoryBytesBudget)
			: base(memoryBytesBudget)
		{
			_fileManager = fileManager;
			_output = output;
			_ioThread = ioThread;
		}

		protected override void LoadResource(Resource<MeshBinary> resource)
		{
			_ioThread.Enqueue(() => LoadMeshResource(resource));
		}

		protected override void GetResourceSize(Resource<MeshBinary> resource)
		{
			_ioThread.Enqueue(() =>
			{
				string fileName, meshName;
				Utils.Meshes.NameToPathAndMeshName(resource.Name, out fileName, out meshName);
				var ext = Path.GetExtension(fileName);
				if(ext == ".obj")
				{
					// Try find loaded obj mesh in the cache
					lock (_objCache)
					{
						if(_objCache.ContainsKey(resource.Name))
						{
							OnResourceSizeKnown(resource, _objCache[resource.Name].BytesUsed);
							return;
						}
					}
					return;
				}

				using(var stream = _fileManager.GetStream(fileName))
				{
					long size = MeshBinary.GetMeshSize(stream, meshName);
					OnResourceSizeKnown(resource, size);
				}
			});
		}

		private void LoadMeshResource(Resource<MeshBinary> resource)
		{
			string fileName, meshName;
			Utils.Meshes.NameToPathAndMeshName(resource.Name, out fileName, out meshName);

			long resourceSize = 0;

			try
			{
				var ext = Path.GetExtension(fileName);

				// Special handling for .obj
				if(ext == ".obj")
				{
					Stream stream;

					// Try find loaded obj mesh in the cache
					lock (_objCache)
					{
						if(_objCache.ContainsKey(resource.Name))
						{
							resource.Update(_objCache[resource.Name]);
							resourceSize = _objCache[resource.Name].BytesUsed;
							return;
						}

						// If the file was already loaded, it contains no mesh of such meshname
						if(_objCacheFiles.Contains(fileName))
						{
							resource.Update(null);
							return;
						}

						// Try to load the .obj

						_objCacheFiles.Add(fileName);

						stream = _fileManager.GetStream(fileName);

						if(stream != null)
						{
							MeshGroup group;
							using(var sr = new StreamReader(stream))
							{
								string report;
								group = ObjLoader.LoadMeshes(sr, fileName, out report);

								if(!string.IsNullOrEmpty(report))
								{
									_output.Print(OutputType.Error, "Error loading .obj mesh " + fileName + ": " + meshName);
									_output.Print(OutputType.Error, report);
								}
							}

							var binaries = MeshBinary.FromMeshGroup(group, true);

							// Null meshName should map to the first mesh in the file
							if(binaries.Count > 0)
								_objCache.Add(Utils.Meshes.MeshFileAndNameCombine(fileName, ""), binaries[0]);

							foreach(MeshBinary binary in binaries)
							{
								_objCache.Add(Utils.Meshes.MeshFileAndNameCombine(fileName, binary.MeshName), binary);
							}
						}

						if(_objCache.ContainsKey(resource.Name))
						{
							resource.Update(_objCache[resource.Name]);
							resourceSize = _objCache[resource.Name].BytesUsed;
						}
						else
						{
							resource.Update(null);
						}
						return;
					}
				}

				// Load non-obj mesh

				using(var stream = _fileManager.GetStream(fileName))
				{
					if(stream != null)
					{
						var meshBinary = MeshBinary.Load(stream, meshName);

						if(meshBinary != null)
						{
							meshBinary.FileName = fileName;
							meshBinary.MeshName = meshName;
							resourceSize = meshBinary.BytesUsed;
						}
						else
						{
							_output.Print(OutputType.Warning, "Error loading mesh: file: " + fileName + " name: " + meshName);
						}							

						resource.Update(meshBinary); // meshBinary can be null
					}
				}
			}
			catch(Exception e)
			{
				_output.PrintException(OutputType.Error, "Error loading mesh: " + fileName + " | " + meshName, e);
				resource.Update(null);
			}
			finally
			{
				if(resourceSize != 0)
					OnResourceSizeKnown(resource, resourceSize);
				OnResourceLoadFinished(resource);
			}
		}
	}
}
