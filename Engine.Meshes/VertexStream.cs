﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Meshes
{
	// Matches OpenGL
	public enum VertexStreamType
	{
		Byte = 5120,
		UnsignedByte = 5121,
		Short = 5122,
		UnsignedShort = 5123,
		Int = 5124,
		UnsignedInt = 5125,
		Float = 5126,
		Double = 5130,
		HalfFloat = 5131,
		Fixed = 5132,
		UnsignedInt2101010Rev = 33640,
		UnsignedInt10F11F11FRev = 35899,
		Int2101010Rev = 36255,
	}

	// Matches Glob
	public enum VertexStreamClass
	{
		Float = 0,
		Integer = 1,
		Double = 2,
	}

	public enum VertexStreamUsage
	{
		Normals = 1,
		TexCoords = 2,
		Tangents = 3,
		Clusters = -13,
		Positions = -14,
		Indices = -16,
		IndicesShadow = -15,
	}

	public enum VertexStreamComponentType
	{
		Scalar = 1,
		Vector2 = 2,
		Vector3 = 3,
		Vector4 = 4,
		Other = -1,
		GL_BGRA = 0x80E1,
	}

	public class VertexStream
	{
		const int FormatVersion = 1;

		public VertexStreamType Type;
		public VertexStreamClass DataClass;
		public VertexStreamUsage Usage;
		public int Stride;
		public VertexStreamComponentType ComponentType;
		public bool Normalized;
		public bool Quantized;
		public Vector4 ValueOffset;
		public Vector4 ValueScale;
		public Vector4 ValueMin;
		public Vector4 ValueMax;

		/// <summary>
		/// Number of bytes this stream's data uses
		/// </summary>
		public int ByteLength { get { return Data.Length; } }

		/// <summary>
		/// The number of vertices in this stream
		/// </summary>
		public int AttributeCount { get { return ByteLength / Stride; } }
		
		public byte[] Data;

		public string FormatDescription
		{
			get
			{
				StringBuilder sb = new StringBuilder();
				sb.Append(Type);
				sb.Append(' ');
				sb.Append(DataClass);
				sb.Append(' ');
				sb.Append(Usage);
				sb.Append(' ');
				sb.Append(Stride);
				sb.Append(' ');
				sb.Append(ComponentType);
				sb.Append(' ');
				sb.Append(Normalized);
				sb.Append(' ');
				sb.Append(Quantized);

				return sb.ToString();
			}
		}

		public Vector4 BoundingSphere
		{
			get
			{
				Vector3 min = ValueMin.Xyz;
				Vector3 max = ValueMax.Xyz;

				float dist = (max - min).Length;

				return new Vector4((min + max) / 2, dist / 2);
			}
		}

		internal VertexStream()
		{
			
		}

		internal static VertexStream FromBinary(BinaryReader br)
		{
			int version = br.ReadInt32();

			switch(version)
			{
				case 1:
				{
					return FromBinaryVersion01(br);
				}
				default:
				{
					return null;
				}
			}
		}

		internal void SaveToBinary(BinaryWriter bw)
		{
			bw.Write(FormatVersion);

			bw.Write((int)Type);
			bw.Write((int)DataClass);
			bw.Write((int)Usage);

			bw.Write(Stride);
			bw.Write((int)ComponentType);

			bw.Write(Normalized);
			bw.Write(Quantized);

			bw.Write(ValueOffset.X);
			bw.Write(ValueOffset.Y);
			bw.Write(ValueOffset.Z);
			bw.Write(ValueOffset.W);

			bw.Write(ValueScale.X);
			bw.Write(ValueScale.Y);
			bw.Write(ValueScale.Z);
			bw.Write(ValueScale.W);

			bw.Write(ValueMin.X);
			bw.Write(ValueMin.Y);
			bw.Write(ValueMin.Z);
			bw.Write(ValueMin.W);

			bw.Write(ValueMax.X);
			bw.Write(ValueMax.Y);
			bw.Write(ValueMax.Z);
			bw.Write(ValueMax.W);

			bw.Write(Data.Length);

			bw.Write(Data);
		}

		static VertexStream FromBinaryVersion01(BinaryReader br)
		{
			VertexStream vs = new VertexStream();

			vs.Type = (VertexStreamType)br.ReadInt32();
			vs.DataClass = (VertexStreamClass)br.ReadInt32();
			vs.Usage = (VertexStreamUsage)br.ReadInt32();

			vs.Stride = br.ReadInt32();
			vs.ComponentType = (VertexStreamComponentType)br.ReadInt32();

			vs.Normalized = br.ReadBoolean();
			vs.Quantized = br.ReadBoolean();

			float x, y, z, w;

			x = br.ReadSingle();
			y = br.ReadSingle();
			z = br.ReadSingle();
			w = br.ReadSingle();

			vs.ValueOffset = new Vector4(x, y, z, w);

			x = br.ReadSingle();
			y = br.ReadSingle();
			z = br.ReadSingle();
			w = br.ReadSingle();

			vs.ValueScale = new Vector4(x, y, z, w);

			x = br.ReadSingle();
			y = br.ReadSingle();
			z = br.ReadSingle();
			w = br.ReadSingle();

			vs.ValueMin = new Vector4(x, y, z, w);

			x = br.ReadSingle();
			y = br.ReadSingle();
			z = br.ReadSingle();
			w = br.ReadSingle();

			vs.ValueMax = new Vector4(x, y, z, w);

			int length = br.ReadInt32();

			vs.Data = br.ReadBytes(length);

			return vs;
		}
	}
}
