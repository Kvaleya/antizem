﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using GameFramework;
using OpenTK;

namespace Engine.Meshes
{
	public class MeshBinary : IDisposable
	{
		public const string FormatMagic = "Mesh";
		public const int FormatVersionNumber = 2;

		public const int ClusterSize = 256;
		public const int ClusterDataSize = 32;

		public string FileName, MeshName;

		public MeshDescription Description;

		// All vertex data streams including positions and indices
		public List<VertexStream> VertexStreams;
		VertexStream _streamIndicesShadow;

		public long BytesUsed
		{
			get
			{
				long bytes = MeshDescription.Size;

				foreach(var stream in VertexStreams)
				{
					bytes += stream.ByteLength;
				}

				return bytes;
			}
		}

		/// <summary>
		/// Generate a bounding sphere from the positions stream's offset and scale value
		/// </summary>
		/// <returns>Vector4 where xyz is the sphere center and w is the sphere radius</returns>
		public Vector4 ComputeBoundingSphere()
		{
			var positions = GetStream(VertexStreamUsage.Positions);
			return positions.BoundingSphere;
		}

		// Destroying the MeshBinary on Dispose() would cause problems with the mesh loading system
		// Let the GC collect it along with its VertexStreams instead
		public void Dispose()
		{
			
		}

		public VertexStream GetStream(VertexStreamUsage usage)
		{
			foreach(VertexStream stream in VertexStreams)
			{
				if(stream.Usage == usage)
					return stream;
			}
			return null;
		}

		public static void Save(List<MeshBinary> meshes, Stream stream)
		{
			using(ZipArchive zip = new ZipArchive(stream, ZipArchiveMode.Create))
			{
				foreach(var mesh in meshes)
				{
					var entry = zip.CreateEntry(mesh.MeshName, CompressionLevel.Optimal);
					using(BinaryWriter bw = new BinaryWriter(entry.Open()))
					{
						Save(bw, mesh);
					}
				}
			}
		}

		static void Save(BinaryWriter bw, MeshBinary mesh)
		{
			// Magic number
			bw.Write(FormatMagic[0]);
			bw.Write(FormatMagic[1]);
			bw.Write(FormatMagic[2]);
			bw.Write(FormatMagic[3]);
			// Version number
			bw.Write(FormatVersionNumber);

			// Stream count
			bw.Write(mesh.VertexStreams.Count);

			for(int i = 0; i < mesh.VertexStreams.Count; i++)
			{
				mesh.VertexStreams[i].SaveToBinary(bw);
			}
		}

		public static List<string> GetPackageContents(Stream stream)
		{
			if(stream == null)
				return new List<string>();

			using(ZipArchive zip = new ZipArchive(stream, ZipArchiveMode.Read))
			{
				List<string> contents = new List<string>();

				foreach(var entry in zip.Entries)
				{
					contents.Add(entry.FullName);
				}

				return contents;
			}
		}

		/// <summary>
		/// Returns null if the mesh is not present in the file or is otherwise invalid. Does not set the FileName and MeshName fields of the resulting MeshBinary instance.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="meshName"></param>
		/// <returns></returns>
		public static MeshBinary Load(Stream stream, string meshName)
		{
			using(ZipArchive zip = new ZipArchive(stream, ZipArchiveMode.Read))
			{
				ZipArchiveEntry entry;

				if(String.IsNullOrEmpty(meshName))
				{
					entry = zip.Entries[0];
				}
				else
				{
					entry = zip.GetEntry(meshName);
				}

				if(entry == null)
					return null;

				MeshBinary result = null;

				using(BinaryReader br = new BinaryReader(entry.Open()))
				{
					string magic = "";
					// Magic number
					magic += br.ReadChar();
					magic += br.ReadChar();
					magic += br.ReadChar();
					magic += br.ReadChar();

					if(magic != FormatMagic)
						return null;

					int version = br.ReadInt32();

					switch(version)
					{
						case 1:
							{
								result = LoadVersion01(br);
								break;
							}
						case 2:
							{
								result = LoadVersion02(br);
								break;
							}
					}
				}

				result?.FinalizeCreation();

				return result;
			}
		}

		public static long GetMeshSize(Stream stream, string meshName)
		{
			if(stream == null)
				return 0;
			using(ZipArchive zip = new ZipArchive(stream, ZipArchiveMode.Read))
			{
				ZipArchiveEntry entry;

				if(String.IsNullOrEmpty(meshName))
				{
					entry = zip.Entries[0];
				}
				else
				{
					entry = zip.GetEntry(meshName);
				}

				if(entry == null)
					return 0;

				return entry.Length;
			}
		}

		static MeshBinary LoadVersion01(BinaryReader br)
		{
			MeshBinary mesh = new MeshBinary();
			mesh.Description = new MeshDescription();
			mesh.VertexStreams = new List<VertexStream>();

			float x, y, z;

			VertexStream positions, indices, indicesShadow, normals, tangents, texCoords;

			positions = new VertexStream()
			{
				Type = VertexStreamType.UnsignedShort,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.Positions,
				Normalized = true,
				Quantized = true,
				Stride = 8,
				ComponentType = VertexStreamComponentType.Vector4,
			};

			normals = new VertexStream()
			{
				Type = VertexStreamType.Int2101010Rev,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.Normals,
				Normalized = true,
				Quantized = false,
				Stride = 4,
				ComponentType = VertexStreamComponentType.GL_BGRA,
			};

			tangents = new VertexStream()
			{
				Type = VertexStreamType.Int2101010Rev,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.Normals,
				Normalized = true,
				Quantized = false,
				Stride = 4,
				ComponentType = VertexStreamComponentType.GL_BGRA,
			};

			texCoords = new VertexStream()
			{
				Type = VertexStreamType.UnsignedShort,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.TexCoords,
				Normalized = true,
				Quantized = true,
				Stride = 4,
				ComponentType = VertexStreamComponentType.Vector2,
			};

			// Position offset
			x = br.ReadSingle();
			y = br.ReadSingle();
			z = br.ReadSingle();

			positions.ValueOffset = new Vector4(x, y, z, 0);

			// Position scale
			x = br.ReadSingle();
			y = br.ReadSingle();
			z = br.ReadSingle();

			positions.ValueScale = new Vector4(x, y, z, 1);

			// Texcoord offset
			x = br.ReadSingle();
			y = br.ReadSingle();

			texCoords.ValueOffset = new Vector4(x, y, 0, 0);

			// Texcoord scale
			x = br.ReadSingle();
			y = br.ReadSingle();

			texCoords.ValueScale = new Vector4(x, y, 1, 1);

			// Positions count
			int countPositions = br.ReadInt32();
			// Normals count
			int countNormals = br.ReadInt32();
			// Tangents count
			int countTangents = br.ReadInt32();
			// TexCoords count
			int countTexCoords = br.ReadInt32();
			// IndicesUshort count
			int countIndicesUshort = br.ReadInt32();
			// IndicesShadowUshort count
			int countIndicesShadowUshort = br.ReadInt32();
			// IndicesUint count
			int countIndicesUint = br.ReadInt32();
			// IndicesShadowUint count
			int countIndicesShadowUint = br.ReadInt32();

			mesh.Description.VertexCount = countPositions;
			mesh.Description.IndexCount = countIndicesUint + countIndicesUshort;
			mesh.Description.IndexCountShadow = countIndicesShadowUint + countIndicesShadowUshort;

			if(countIndicesUshort > 0)
			{
				indices = new VertexStream()
				{
					Type = VertexStreamType.UnsignedShort,
					DataClass = VertexStreamClass.Integer,
					Usage = VertexStreamUsage.Indices,
					Normalized = false,
					Quantized = false,
					Stride = 2,
					ComponentType = VertexStreamComponentType.Scalar,
				};
			}
			else
			{
				indices = new VertexStream()
				{
					Type = VertexStreamType.UnsignedInt,
					DataClass = VertexStreamClass.Integer,
					Usage = VertexStreamUsage.Indices,
					Normalized = false,
					Quantized = false,
					Stride = 2,
					ComponentType = VertexStreamComponentType.Scalar,
				};
			}

			if(countIndicesShadowUshort > 0)
			{
				indicesShadow = new VertexStream()
				{
					Type = VertexStreamType.UnsignedShort,
					DataClass = VertexStreamClass.Integer,
					Usage = VertexStreamUsage.IndicesShadow,
					Normalized = false,
					Quantized = false,
					Stride = 2,
					ComponentType = VertexStreamComponentType.Scalar,
				};
			}
			else
			{
				indicesShadow = new VertexStream()
				{
					Type = VertexStreamType.UnsignedInt,
					DataClass = VertexStreamClass.Integer,
					Usage = VertexStreamUsage.IndicesShadow,
					Normalized = false,
					Quantized = false,
					Stride = 2,
					ComponentType = VertexStreamComponentType.Scalar,
				};
			}
			
			// Positions
			if(countPositions > 0)
			{
				positions.Data = br.ReadBytes(countPositions * 8);
				mesh.VertexStreams.Add(positions);
			}

			// Normals
			if(countNormals > 0)
			{
				normals.Data = br.ReadBytes(countNormals * 4);
				mesh.VertexStreams.Add(normals);
			}

			// Tangents
			if(countTangents > 0)
			{
				tangents.Data = br.ReadBytes(countTangents * 4);
				mesh.VertexStreams.Add(tangents);
			}

			// TexCoords
			if(countTexCoords > 0)
			{
				texCoords.Data = br.ReadBytes(countTexCoords * 4);
				mesh.VertexStreams.Add(texCoords);
			}

			// IndicesUshort
			if(countIndicesUshort > 0)
			{
				indices.Data = br.ReadBytes(countIndicesUshort * 2);
				mesh.VertexStreams.Add(indices);
			}

			// IndicesShadowUshort
			if(countIndicesShadowUshort > 0)
			{
				indicesShadow.Data = br.ReadBytes(countIndicesShadowUshort * 2);
				mesh.VertexStreams.Add(indicesShadow);
			}

			// IndicesUint
			if(countIndicesUint > 0)
			{
				indices.Data = br.ReadBytes(countIndicesUint * 4);
				mesh.VertexStreams.Add(indices);
			}

			// IndicesShadowUint
			if(countIndicesShadowUint > 0)
			{
				indicesShadow.Data = br.ReadBytes(countIndicesShadowUint * 4);
				mesh.VertexStreams.Add(indicesShadow);
			}

			return mesh;
		}

		static MeshBinary LoadVersion02(BinaryReader br)
		{
			int streamCount = br.ReadInt32();

			MeshBinary mesh = new MeshBinary();
			mesh.VertexStreams = new List<VertexStream>();

			mesh.Description = new MeshDescription();

			for(int i = 0; i < streamCount; i++)
			{
				var stream = VertexStream.FromBinary(br);
				switch(stream.Usage)
				{
					case VertexStreamUsage.Indices:
					{
						mesh.Description.IndexCount = stream.ByteLength / stream.Stride;
						break;
					}
					case VertexStreamUsage.IndicesShadow:
					{
						mesh._streamIndicesShadow = stream;
						mesh.Description.IndexCountShadow = stream.ByteLength / stream.Stride;
						break;
					}
					case VertexStreamUsage.Positions:
					{
						mesh.Description.VertexCount = stream.ByteLength / stream.Stride;
						break;
					}
				}
				mesh.VertexStreams.Add(stream);
			}

			return mesh;
		}

		public static List<MeshBinary> FromMeshGroup(MeshGroup group, bool generateNormals = false, float smoothingAngle = 0f, bool forceIndices32 = false)
		{
			Vector3 posMin = new Vector3(Single.PositiveInfinity, Single.PositiveInfinity, Single.PositiveInfinity);
			Vector3 posMax = new Vector3(Single.NegativeInfinity, Single.NegativeInfinity, Single.NegativeInfinity);

			Vector2 tcMin = new Vector2(Single.PositiveInfinity, Single.PositiveInfinity);
			Vector2 tcMax = new Vector2(Single.NegativeInfinity, Single.NegativeInfinity);

			foreach(MeshData data in @group.Meshes)
			{
				for(int i = 0; i < data.Vertices.Count; i++)
				{
					posMin.X = Math.Min(posMin.X, data.Vertices[i].Position.X);
					posMin.Y = Math.Min(posMin.Y, data.Vertices[i].Position.Y);
					posMin.Z = Math.Min(posMin.Z, data.Vertices[i].Position.Z);

					posMax.X = Math.Max(posMax.X, data.Vertices[i].Position.X);
					posMax.Y = Math.Max(posMax.Y, data.Vertices[i].Position.Y);
					posMax.Z = Math.Max(posMax.Z, data.Vertices[i].Position.Z);

					tcMin.X = Math.Min(tcMin.X, data.Vertices[i].TexCoord.X);
					tcMin.Y = Math.Min(tcMin.Y, data.Vertices[i].TexCoord.Y);

					tcMax.X = Math.Max(tcMax.X, data.Vertices[i].TexCoord.X);
					tcMax.Y = Math.Max(tcMax.Y, data.Vertices[i].TexCoord.Y);
				}
			}

			Vector3 posOffset = posMin;
			Vector3 posScale = posMax - posMin;

			Vector2 tcOffset = tcMin;
			Vector2 tcScale = tcMax - tcMin;

			List<MeshBinary> meshes = new List<MeshBinary>();

			foreach(MeshData data in @group.Meshes)
			{
				meshes.Add(FromMeshData(data, posOffset, posScale, tcOffset, tcScale, generateNormals, smoothingAngle, forceIndices32));
			}

			return meshes;
		}

		public static MeshBinary FromMeshData(MeshData data, Vector3 posOffset, Vector3 posScale, Vector2 tcOffset, Vector2 tcScale, bool generateNormals = false, float smoothingAngle = 0f, bool forceIndices32 = false)
		{
			MeshBinary binary = new MeshBinary();
			binary.FileName = data.Filename;
			binary.MeshName = data.MeshName;
			binary.Description = new MeshDescription();

			if(generateNormals)
			{
				MeshUtils.GenerateTangents(data);
				MeshUtils.GenerateNormals(data, smoothingAngle);
			}

			// Reduce precision of normals, tangents and texcoords - helps with generating index buffers
			MeshUtils.ReduceVertexDataPrecision(data);

			// Generate index buffers
			List<Vertex> vertices;
			List<int> indicesList;
			MeshUtils.GenerateIndexedMesh(data, out vertices, out indicesList);
			//var indices = indicesList.ToArray();
			int[] indices = MeshUtils.OptimizeVertexCache(indicesList.ToArray(), vertices.Count);
			MeshUtils.ReorderVertices(vertices, indices);
			var indicesShadow = MeshUtils.GenerateShadowIndices(vertices, indices);

			// Write index buffers
			int maxIndex = 0;

			for(int i = 0; i < indices.Length; i++)
			{
				maxIndex = Math.Max(maxIndex, indices[i]);
			}

			binary.Description.IndexCount = indices.Length;
			binary.Description.IndexCountShadow = indicesShadow.Length;
			binary.Description.VertexCount = vertices.Count;

			VertexStream positions = new VertexStream()
			{
				Type = VertexStreamType.UnsignedShort,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.Positions,
				Normalized = true,
				Quantized = true,
				Stride = 8,
				ComponentType = VertexStreamComponentType.Vector4,
			};

			VertexStream normals = new VertexStream()
			{
				Type = VertexStreamType.Int2101010Rev,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.Normals,
				Normalized = true,
				Quantized = false,
				Stride = 4,
				ComponentType = VertexStreamComponentType.GL_BGRA,
			};

			VertexStream tangents = new VertexStream()
			{
				Type = VertexStreamType.Int2101010Rev,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.Tangents,
				Normalized = true,
				Quantized = false,
				Stride = 4,
				ComponentType = VertexStreamComponentType.GL_BGRA,
			};

			VertexStream texCoords = new VertexStream()
			{
				Type = VertexStreamType.UnsignedShort,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.TexCoords,
				Normalized = true,
				Quantized = true,
				Stride = 4,
				ComponentType = VertexStreamComponentType.Vector2,
			};

			VertexStream clusters = new VertexStream()
			{
				Type = VertexStreamType.Float,
				DataClass = VertexStreamClass.Float,
				Usage = VertexStreamUsage.Clusters,
				Normalized = false,
				Quantized = false,
				Stride = 32,
				ComponentType = VertexStreamComponentType.Other,
			};

			VertexStream streamIndices, streamIndicesShadow;

			// Use 32bit or 16bit indices
			if(maxIndex <= UInt16.MaxValue && !forceIndices32)
			{
				streamIndices = new VertexStream()
				{
					Type = VertexStreamType.UnsignedShort,
					DataClass = VertexStreamClass.Integer,
					Usage = VertexStreamUsage.Indices,
					Normalized = false,
					Quantized = false,
					Stride = 2,
					ComponentType = VertexStreamComponentType.Scalar,
				};
				streamIndicesShadow = new VertexStream()
				{
					Type = VertexStreamType.UnsignedShort,
					DataClass = VertexStreamClass.Integer,
					Usage = VertexStreamUsage.IndicesShadow,
					Normalized = false,
					Quantized = false,
					Stride = 2,
					ComponentType = VertexStreamComponentType.Scalar,
				};

				streamIndices.Data = new byte[indices.Length * 2];
				streamIndicesShadow.Data = new byte[indicesShadow.Length * 2];

				for(int i = 0; i < indices.Length; i++)
				{
					var bytes = BitConverter.GetBytes((ushort)indices[i]);
					streamIndices.Data[i * 2 + 0] = bytes[0];
					streamIndices.Data[i * 2 + 1] = bytes[1];
				}
				for(int i = 0; i < indicesShadow.Length; i++)
				{
					var bytes = BitConverter.GetBytes((ushort)indicesShadow[i]);
					streamIndicesShadow.Data[i * 2 + 0] = bytes[0];
					streamIndicesShadow.Data[i * 2 + 1] = bytes[1];
				}
			}
			else
			{
				streamIndices = new VertexStream()
				{
					Type = VertexStreamType.UnsignedInt,
					DataClass = VertexStreamClass.Integer,
					Usage = VertexStreamUsage.Indices,
					Normalized = false,
					Quantized = false,
					Stride = 4,
					ComponentType = VertexStreamComponentType.Scalar,
				};
				streamIndicesShadow = new VertexStream()
				{
					Type = VertexStreamType.UnsignedInt,
					DataClass = VertexStreamClass.Integer,
					Usage = VertexStreamUsage.IndicesShadow,
					Normalized = false,
					Quantized = false,
					Stride = 4,
					ComponentType = VertexStreamComponentType.Scalar,
				};

				streamIndices.Data = new byte[indices.Length * 4];
				streamIndicesShadow.Data = new byte[indicesShadow.Length * 4];

				for(int i = 0; i < indices.Length; i++)
				{
					var bytes = BitConverter.GetBytes(indices[i]);
					streamIndices.Data[i * 4 + 0] = bytes[0];
					streamIndices.Data[i * 4 + 1] = bytes[1];
					streamIndices.Data[i * 4 + 2] = bytes[2];
					streamIndices.Data[i * 4 + 3] = bytes[3];
				}
				for(int i = 0; i < indicesShadow.Length; i++)
				{
					var bytes = BitConverter.GetBytes(indicesShadow[i]);
					streamIndicesShadow.Data[i * 4 + 0] = bytes[0];
					streamIndicesShadow.Data[i * 4 + 1] = bytes[1];
					streamIndicesShadow.Data[i * 4 + 2] = bytes[2];
					streamIndicesShadow.Data[i * 4 + 3] = bytes[3];
				}
			}

			positions.ValueOffset = new Vector4(posOffset, 0);
			positions.ValueScale = new Vector4(posScale, 1);

			texCoords.ValueOffset = new Vector4(tcOffset.X, tcOffset.Y, 0, 0);
			texCoords.ValueScale = new Vector4(tcScale.X, tcScale.Y, 1, 1);

			positions.Data = new byte[vertices.Count * 8];
			normals.Data = new byte[vertices.Count * 4];

			clusters.Data = MeshUtils.GenClusterData(vertices, indices);

			binary.VertexStreams = new List<VertexStream>();

			binary.VertexStreams.Add(positions);
			binary.VertexStreams.Add(normals);
			binary.VertexStreams.Add(streamIndices);
			binary.VertexStreams.Add(streamIndicesShadow);
			binary.VertexStreams.Add(clusters);

			Vector4 min = new Vector4(Single.PositiveInfinity);
			Vector4 max = new Vector4(Single.NegativeInfinity);

			// TODO: no positionsShadow?
			// TODO: when encoding positions into float32, fill in pos offset and scale anyway, it is used for bounding sphere

			for(int i = 0; i < vertices.Count; i++)
			{
				var bytesPos =
					BitConverter.GetBytes(Utils.Pack.PackVec3(vertices[i].Position, posOffset, posOffset + posScale, vertices[i].Extra));
				var bytesNormal = BitConverter.GetBytes(Utils.Pack.PackNormal(vertices[i].Normal));

				for(int j = 0; j < 3; j++)
				{
					min[j] = Math.Min(min[j], vertices[i].Position[j]);
					max[j] = Math.Max(max[j], vertices[i].Position[j]);
				}

				positions.Data[i * 8 + 0] = bytesPos[0];
				positions.Data[i * 8 + 1] = bytesPos[1];
				positions.Data[i * 8 + 2] = bytesPos[2];
				positions.Data[i * 8 + 3] = bytesPos[3];
				positions.Data[i * 8 + 4] = bytesPos[4];
				positions.Data[i * 8 + 5] = bytesPos[5];
				positions.Data[i * 8 + 6] = bytesPos[6];
				positions.Data[i * 8 + 7] = bytesPos[7];

				normals.Data[i * 4 + 0] = bytesNormal[0];
				normals.Data[i * 4 + 1] = bytesNormal[1];
				normals.Data[i * 4 + 2] = bytesNormal[2];
				normals.Data[i * 4 + 3] = bytesNormal[3];
			}

			for(int i = 0; i < 3; i++)
			{
				if(Single.IsInfinity(min[i]))
					min[i] = 0;
				if(Single.IsInfinity(max[i]))
					max[i] = 0;
			}

			positions.ValueMin = min;
			positions.ValueMax = max;

			if(tcScale.LengthSquared > 0)
			{
				texCoords.Data = new byte[vertices.Count * 4];
				tangents.Data = new byte[vertices.Count * 4];

				binary.VertexStreams.Add(texCoords);
				binary.VertexStreams.Add(tangents);

				for(int i = 0; i < vertices.Count; i++)
				{
					var bytesTc = BitConverter.GetBytes(Utils.Pack.PackVec2(vertices[i].TexCoord, tcOffset, tcOffset + tcScale));
					var bytesTan = BitConverter.GetBytes(Utils.Pack.PackNormal(vertices[i].Tangent));

					texCoords.Data[i * 4 + 0] = bytesTc[0];
					texCoords.Data[i * 4 + 1] = bytesTc[1];
					texCoords.Data[i * 4 + 2] = bytesTc[2];
					texCoords.Data[i * 4 + 3] = bytesTc[3];

					tangents.Data[i * 4 + 0] = bytesTan[0];
					tangents.Data[i * 4 + 1] = bytesTan[1];
					tangents.Data[i * 4 + 2] = bytesTan[2];
					tangents.Data[i * 4 + 3] = bytesTan[3];
				}
			}

			binary.FinalizeCreation();

			return binary;
		}

		//public static MeshBinary FromMeshData(MeshData data)
		//{
		//	Vector3 posMin = new Vector3(Single.PositiveInfinity, Single.PositiveInfinity, Single.PositiveInfinity);
		//	Vector3 posMax = new Vector3(Single.NegativeInfinity, Single.NegativeInfinity, Single.NegativeInfinity);

		//	Vector2 tcMin = new Vector2(Single.PositiveInfinity, Single.PositiveInfinity);
		//	Vector2 tcMax = new Vector2(Single.NegativeInfinity, Single.NegativeInfinity);

		//	for(int i = 0; i < data.Vertices.Count; i++)
		//	{
		//		posMin.X = Math.Min(posMin.X, data.Vertices[i].Position.X);
		//		posMin.Y = Math.Min(posMin.Y, data.Vertices[i].Position.Y);
		//		posMin.Z = Math.Min(posMin.Z, data.Vertices[i].Position.Z);

		//		posMax.X = Math.Max(posMax.X, data.Vertices[i].Position.X);
		//		posMax.Y = Math.Max(posMax.Y, data.Vertices[i].Position.Y);
		//		posMax.Z = Math.Max(posMax.Z, data.Vertices[i].Position.Z);

		//		tcMin.X = Math.Min(tcMin.X, data.Vertices[i].TexCoord.X);
		//		tcMin.Y = Math.Min(tcMin.Y, data.Vertices[i].TexCoord.Y);

		//		tcMax.X = Math.Max(tcMax.X, data.Vertices[i].TexCoord.X);
		//		tcMax.Y = Math.Max(tcMax.Y, data.Vertices[i].TexCoord.Y);
		//	}

		//	return FromMeshData(data, posMin, posMax - posMin, tcMin, tcMax - tcMin);
		//}

		void FinalizeCreation()
		{
			VertexStreams = VertexStreams.OrderBy(x => (int)x.Usage).ToList();
		}
	}
}
