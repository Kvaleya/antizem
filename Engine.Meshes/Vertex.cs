﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Meshes
{
	public class Vertex : IEquatable<Vertex>
	{
		public Vector3 Position;
		public Vector3 Normal;
		public Vector2 TexCoord;
		public Vector3 Tangent;
		public ushort Extra;

		public bool Equals(Vertex other)
		{
			if(ReferenceEquals(null, other)) return false;
			if(ReferenceEquals(this, other)) return true;
			return Position.Equals(other.Position) && Normal.Equals(other.Normal) && TexCoord.Equals(other.TexCoord) && Tangent.Equals(other.Tangent) && Extra == other.Extra;
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			if(ReferenceEquals(this, obj)) return true;
			if(obj.GetType() != this.GetType()) return false;
			return Equals((Vertex)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = Position.GetHashCode();
				hashCode = (hashCode * 397) ^ Normal.GetHashCode();
				hashCode = (hashCode * 397) ^ TexCoord.GetHashCode();
				hashCode = (hashCode * 397) ^ Tangent.GetHashCode();
				hashCode = (hashCode * 397) ^ Extra.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(Vertex left, Vertex right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(Vertex left, Vertex right)
		{
			return !Equals(left, right);
		}
	}
}
