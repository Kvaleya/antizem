﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cyotek.Drawing.BitmapFont;
using Engine.Common;
using Engine.Common.Rendering;
using OpenTK;
using System.IO;
using GameFramework;

namespace Engine.UI
{
	public class GdxFont
	{
		const char DefaultChar = (char)0;

		BitmapFont _font;

		public int AtlasWidthHeight;
		public byte[][] AtlasMips;

		public int LineHeight
		{
			get
			{
				if(_font == null)
					return 0;
				return _font.LineHeight;
			}
		}

		// Only supports single page fonts
		// Only supports square or horizontal-rectangle-shaped pages
		public GdxFont(string file, TextOutput output)
		{
			_font = null;
			DirectBitmap dbm = null;

			try
			{
				_font = BitmapFontLoader.LoadFontFromFile(file);
				string bitmapFile = _font.Pages[0].FileName;
				dbm = new DirectBitmap(Image.FromFile(bitmapFile));
			}
			catch(FileNotFoundException e)
			{
				output.Print(OutputType.Error, "Cannot load font files");
				AtlasWidthHeight = 1;
				AtlasMips = new byte[0][];
				return;
			}
			
			AtlasWidthHeight = dbm.Width;
			AtlasMips = new byte[6][];
			AtlasMips[0] = new byte[AtlasWidthHeight * AtlasWidthHeight];
			for(int i = 0; i < dbm.Bits.Length / 4; i++)
			{
				AtlasMips[0][i] = dbm.Bits[i * 4 + 3];
			}

			for(int mip = 1; mip < AtlasMips.Length; mip++)
			{
				int prevWh = AtlasWidthHeight >> (mip - 1);
				int wh = AtlasWidthHeight >> mip;

				AtlasMips[mip] = new byte[AtlasMips[mip - 1].Length / 4];

				int i = 0;
				for(int y = 0; y < wh; y++)
				{
					for(int x = 0; x < wh; x++)
					{
						int value = 0;
						value += AtlasMips[mip - 1][y * 2 * prevWh + x * 2];
						value += AtlasMips[mip - 1][y * 2 * prevWh + x * 2 + 1];
						value += AtlasMips[mip - 1][(y * 2 + 1) * prevWh + x * 2];
						value += AtlasMips[mip - 1][(y * 2 + 1) * prevWh + x * 2 + 1];
						AtlasMips[mip][i] = (byte)(value / 4);
						i++;
					}
				}
			}
		}

		internal float GetTextLength(string text, float scaleX)
		{
			if(_font == null)
				return 0;

			float scaleForInts = scaleX / _font.LineHeight; // Normalized scale so that scale of one covers the entire screen

			float textWidth = 0f;

			for(int i = 0; i < text.Length; i++)
			{
				char c = text[i];
				if(!_font.Characters.ContainsKey(c))
				{
					c = DefaultChar;
				}
				textWidth += _font.Characters[c].XAdvance;
			}
			textWidth *= scaleForInts;

			return textWidth;
		}

		internal List<VertexUI> GetStringVerts(string text, Vector2 scale, Vector2 offset, Vector4 color, TextAlignmentHorizontal alignmentHorizontal = TextAlignmentHorizontal.Left, float alphaTest = 0.5f)
		{
			List<VertexUI> verts = new List<VertexUI>();

			if(_font == null)
				return verts;

			Vector2 scaleForInts = scale / _font.LineHeight; // Normalized scale so that scale of one covers the entire screen

			float textWidth = GetTextLength(text, scale.X);

			switch(alignmentHorizontal)
			{
				case TextAlignmentHorizontal.Right:
					{
						offset.X -= textWidth;
						offset.X -= scale.X * 0.0625f;
						break;
					}
				case TextAlignmentHorizontal.Center:
					{
						offset.X -= textWidth * 0.5f;
						break;
					}
				default:
					{
						offset.X += scale.X * 0.0625f;
						break;
					}
			}

			int posX = 0;

			for(int i = 0; i < text.Length; i++)
			{
				Cyotek.Drawing.BitmapFont.Character data;

				if(_font.Characters.ContainsKey(text[i]))
				{
					data = _font.Characters[text[i]];
				}
				else
				{
					data = _font.Characters[DefaultChar];
				}

				int posMinX = posX + data.Offset.X;
				int posMinY = data.Offset.Y;
				int posMaxX = posMinX + data.Bounds.Width;
				int posMaxY = posMinY + data.Bounds.Height;

				Vector4 pos00 = new Vector4(offset.X + posMinX * scaleForInts.X, offset.Y + posMinY * scaleForInts.Y, 0, alphaTest);
				Vector4 pos10 = new Vector4(offset.X + posMaxX * scaleForInts.X, offset.Y + posMinY * scaleForInts.Y, 0, alphaTest);
				Vector4 pos01 = new Vector4(offset.X + posMinX * scaleForInts.X, offset.Y + posMaxY * scaleForInts.Y, 0, alphaTest);
				Vector4 pos11 = new Vector4(offset.X + posMaxX * scaleForInts.X, offset.Y + posMaxY * scaleForInts.Y, 0, alphaTest);

				float tcMinX = data.Bounds.Left / (float)AtlasWidthHeight;
				float tcMinY = data.Bounds.Top / (float)AtlasWidthHeight;
				float tcMaxX = data.Bounds.Right / (float)AtlasWidthHeight;
				float tcMaxY = data.Bounds.Bottom / (float)AtlasWidthHeight;

				//Vector4 pos00 = new Vector4(position.X + data.Bearing.X * scale.X, position.Y - data.Bearing.Y * scale.Y, 0, 1);
				//Vector4 pos10 = new Vector4(position.X + data.Bearing.X * scale.X + data.Size.X * scale.X, position.Y - data.Bearing.Y * scale.Y, 0, 1);
				//Vector4 pos01 = new Vector4(position.X + data.Bearing.X * scale.X, position.Y - data.Bearing.Y * scale.Y + data.Size.Y * scale.Y, 0, 1);
				//Vector4 pos11 = new Vector4(position.X + data.Bearing.X * scale.X + data.Size.X * scale.X, position.Y - data.Bearing.Y * scale.Y + data.Size.Y * scale.Y, 0, 1);

				verts.Add(new VertexUI(pos00, new Vector2(tcMinX, tcMinY), color));
				verts.Add(new VertexUI(pos10, new Vector2(tcMaxX, tcMinY), color));
				verts.Add(new VertexUI(pos01, new Vector2(tcMinX, tcMaxY), color));

				verts.Add(new VertexUI(pos01, new Vector2(tcMinX, tcMaxY), color));
				verts.Add(new VertexUI(pos10, new Vector2(tcMaxX, tcMinY), color));
				verts.Add(new VertexUI(pos11, new Vector2(tcMaxX, tcMaxY), color));

				//position.X += data.Advance * scale.X;

				posX += data.XAdvance;

				if(i < text.Length - 1)
				{
					posX += _font.GetKerning(text[i], text[i + 1]);
				}
			}

			return verts;
		}
	}

	class GdxChar
	{
		public int ID;
		public int X;
		public int Y;
		public int Width;
		public int Height;
		public int OffsetX;
		public int OffsetY;
		public int AdvanceX;
	}
}
