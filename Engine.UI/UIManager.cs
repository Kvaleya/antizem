﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using OpenTK;

namespace Engine.UI
{
	public class UIManager
	{
		GdxFont _font;
		// There should be no parallel access
		ParallelBuffer<VertexUI> _vertices = new ParallelBuffer<VertexUI>(VertexUI.MaxVertices);
		ParallelBuffer<VertexUI> _verticesBack = new ParallelBuffer<VertexUI>(VertexUI.MaxVertices);
		DataInjector _rendererInjector;
		IEngineViewport _viewport;

		public UIManager(GdxFont font, DataInjector rendererInjector, IEngineViewport viewport)
		{
			_font = font;
			_viewport = viewport;

			rendererInjector.AddData(new FontTextureData()
			{
				Mips = _font.AtlasMips,
				WidthHeight = _font.AtlasWidthHeight,
			});
			_rendererInjector = rendererInjector;
		}

		public float GetTextWidth(string text, float scaleX)
		{
			return _font.GetTextLength(text, scaleX);
		}

		public void AddText(string text, Vector2 scale, Vector2 offset, Vector4 color, TextAlignmentHorizontal alignmentHorizontal = TextAlignmentHorizontal.Left, TextAlignmentVertical alignmentVertical = TextAlignmentVertical.Top)
		{
			// Adjust X scale to match monitor aspect ratio
			scale.X = scale.X * (_viewport.DisplayHeight / (float)_viewport.DisplayWidth);

			// Adjust alphatest to make font readable when using small sizes
			const float alphaTestLarge = 0.5f; // For text that is larger or equal in size to the source bitmap
			const float alphaTestSmall = 0.4f; // For rendering smallest text
			float pixelSize = Math.Min(scale.X * _viewport.DisplayWidth, scale.Y * _viewport.DisplayHeight);
			float mix = pixelSize / _font.LineHeight;
			float alphaTest = Utils.Math.LerpClamped(alphaTestSmall, alphaTestLarge, (float)Math.Sqrt(mix));

			float height = 0f;
			float lineHeight = scale.Y;

			var lines = text.Split(new[] { '\n' });
			height = lineHeight * lines.Length;

			switch(alignmentVertical)
			{
				case TextAlignmentVertical.Top:
				{
					break;
				}
				case TextAlignmentVertical.Center:
				{
					offset.Y -= height * 0.5f;
					offset.Y += lineHeight * 0.5f;
					break;
				}
				default:
				{
					offset.Y -= height;
					//offset.Y += lineHeight;
					break;
				}
			}

			foreach(var line in lines)
			{
				_vertices.AddRange(_font.GetStringVerts(line, scale, offset, color, alignmentHorizontal, alphaTest).ToArray());
				offset.Y += lineHeight;
			}
		}

		public void Swap()
		{
			var render = _vertices;
			_vertices = _verticesBack;
			_verticesBack = render;

			UIFrameData data = new UIFrameData();

			data.Vertices = render.Array;
			data.VertexCount = render.Count;

			render.Reset();

			_rendererInjector.AddData(data);
		}
	}
}
