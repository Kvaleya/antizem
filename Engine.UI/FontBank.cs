﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using OpenTK;
using System.Xml.Serialization;
using Engine.Common.Rendering;
using GameFramework;
using OpenTK.Graphics.OpenGL;

namespace Engine.UI
{
	public enum TextAlignmentHorizontal
	{
		Left,
		Center,
		Right,
	}
	public enum TextAlignmentVertical
	{
		Top,
		Center,
		Bottom,
	}

	[Flags]
	public enum FontStyle
	{
		Regular = 0,
		Bold = 1,
		Italic = 2,
		Underline = 4,
		Strikeout = 8,
	}

	public class FontBank
	{
		public const string FileMipName = "mip{0}.r8";
		public const string FileXmlName = "fontData.xml";

		public int AtlasWidthHeight;
		public byte[][] AtlasMips;

		public Dictionary<int, Character>[] _characters;

		public FontBank(string filename, IFileManager fileManager)
		{
			int i;
			for (i = 0; i < 10; i++)
			{
				if (Load(filename, fileManager))
					break;
			}
			if (i == 10)
			{
				throw new ArgumentException("filename");
			}
		}

		/// <summary>
		/// Generate vertices for a single line of text
		/// </summary>
		/// <param name="text"></param>
		/// <param name="scale"></param>
		/// <param name="offset"></param>
		/// <param name="color"></param>
		/// <param name="style"></param>
		/// <param name="alignmentHorizontal"></param>
		/// <returns></returns>
		internal List<VertexUI> GetStringVerts(string text, Vector2 scale, Vector2 offset, Vector4 color, FontStyle style = FontStyle.Regular, TextAlignmentHorizontal alignmentHorizontal = TextAlignmentHorizontal.Left)
		{
			Vector2 position = offset;

			var charTable = _characters[((int)style & 0x00000003)];

			List<VertexUI> verts = new List<VertexUI>();

			float textWidth = 0f;

			for(int i = 0; i < text.Length; i++)
			{
				int charKey = text[i];
				if(!charTable.ContainsKey(charKey))
					charKey = 0;
				var data = charTable[charKey];
				textWidth += data.Advance * scale.X;
			}

			switch(alignmentHorizontal)
			{
				case TextAlignmentHorizontal.Right:
				{
					position.X -= textWidth;
					position.X -= scale.X * 0.0625f;
					break;
				}
				case TextAlignmentHorizontal.Center:
				{
					position.X -= textWidth * 0.5f;
					break;
				}
				default:
				{
					position.X += scale.X * 0.0625f;
					break;
				}
			}

			for (int i = 0; i < text.Length; i++)
			{
				int charKey = text[i];
				if (!charTable.ContainsKey(charKey))
					charKey = 0;
				var data = charTable[charKey];

				Vector4 pos00 = new Vector4(position.X + data.Bearing.X * scale.X, position.Y - data.Bearing.Y * scale.Y, 0, 1);
				Vector4 pos10 = new Vector4(position.X + data.Bearing.X * scale.X + data.Size.X * scale.X, position.Y - data.Bearing.Y * scale.Y, 0, 1);
				Vector4 pos01 = new Vector4(position.X + data.Bearing.X * scale.X, position.Y - data.Bearing.Y * scale.Y + data.Size.Y * scale.Y, 0, 1);
				Vector4 pos11 = new Vector4(position.X + data.Bearing.X * scale.X + data.Size.X * scale.X, position.Y - data.Bearing.Y * scale.Y + data.Size.Y * scale.Y, 0, 1);

				verts.Add(new VertexUI(pos00, new Vector2(data.TexCoordMin.X, data.TexCoordMin.Y), color));
				verts.Add(new VertexUI(pos10, new Vector2(data.TexCoordMax.X, data.TexCoordMin.Y), color));
				verts.Add(new VertexUI(pos01, new Vector2(data.TexCoordMin.X, data.TexCoordMax.Y), color));

				verts.Add(new VertexUI(pos01, new Vector2(data.TexCoordMin.X, data.TexCoordMax.Y), color));
				verts.Add(new VertexUI(pos10, new Vector2(data.TexCoordMax.X, data.TexCoordMin.Y), color));
				verts.Add(new VertexUI(pos11, new Vector2(data.TexCoordMax.X, data.TexCoordMax.Y), color));

				position.X += data.Advance * scale.X;
			}

			return verts;
		}

		bool Load(string filename, IFileManager fileManager)
		{
			ZipArchive zip = null;
			using(FileStream fs = (FileStream)fileManager.GetStream(filename))
			{
				if(fs == null)
				{
					FontLoadFailed();
					return false;
				}
				zip = new ZipArchive(fs);
				_characters = new Dictionary<int, Character>[]
				{
					new Dictionary<int, Character>(), 
					new Dictionary<int, Character>(), 
					new Dictionary<int, Character>(), 
					new Dictionary<int, Character>(), 
				};

				var zipEntry = zip.GetEntry(FileXmlName);

				var data = DeserializeData(zipEntry.Open());
				var res = data.BitmapResolution;

				AtlasWidthHeight = res;

				var sources = new List<List<Character>>()
				{
					data.CharactersRegular,
					data.CharactersBold,
					data.CharactersItalic,
					data.CharactersBoldItalic,
				};

				for (int i = 0; i < 4; i++)
				{
					var source = sources[i];
					var dest = _characters[i];
					foreach (Character character in source)
					{
						dest.Add(character.Char, character);
					}
				}

				List<byte[]> mips = new List<byte[]>();

				for (int i = 0; (1 << i) <= res; i++)
				{
					int texSize = res >> i;
					string mipName = string.Format(FileMipName, i);
					zipEntry = zip.GetEntry(mipName);

					Stream zipStream = zipEntry.Open();
					using (BinaryReader br = new BinaryReader(zipStream))
					{
						var bytes = br.ReadBytes(texSize * texSize);
						mips.Add(bytes);
					}
				}

				AtlasMips = mips.ToArray();
			}
			return true;
		}

		void FontLoadFailed()
		{
			_characters = new Dictionary<int, Character>[]
			{
				new Dictionary<int, Character>(),
				new Dictionary<int, Character>(),
				new Dictionary<int, Character>(),
				new Dictionary<int, Character>(),
			};
			_characters[0].Add(0, new Character());
			_characters[1].Add(0, new Character());
			_characters[2].Add(0, new Character());
			_characters[3].Add(0, new Character());
		}

		static BitmapFontDataSerializable DeserializeData(Stream stream)
		{
			XmlSerializer deserializer = new XmlSerializer(typeof(BitmapFontDataSerializable));
			TextReader reader = new StreamReader(stream);

			BitmapFontDataSerializable data = null;
			try
			{
				data = (BitmapFontDataSerializable)deserializer.Deserialize(reader);
			}
			catch(InvalidOperationException)
			{
				return null;
			}
			reader.Close();
			return data;
		}
	}

	public class Character
	{
		public int Char;
		// Normalized texture coorinates of the glyph in the texture atlas
		public Vector2 TexCoordMin;
		public Vector2 TexCoordMax;
		// Size relative to font height (in pixels / font height)
		public Vector2 Size;
		// Bearing relative to font height (in pixels / font height)
		public Vector2 Bearing;
		// Advance relative to font height (in pixels / font height)
		public float Advance;
	}

	public class BitmapFontDataSerializable
	{
		public int BitmapResolution;
		public List<Character> CharactersRegular;
		public List<Character> CharactersBold;
		public List<Character> CharactersItalic;
		public List<Character> CharactersBoldItalic;
	}
}
