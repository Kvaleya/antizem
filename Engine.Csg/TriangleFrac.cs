﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Csg
{
	public class TriangleFrac
	{
		Vec3 _a, _b, _c;
		Vec3 _normal;
		Scalar _planeOffset;

		Vec3 _boundingBoxMin;
		Vec3 _boundingBoxMax;

		public Vec3 A
		{
			get { return _a; }
			set
			{
				_a = value;
				UpdatePlaneEquation();
			}
		}

		public Vec3 B
		{
			get { return _b; }
			set
			{
				_b = value;
				UpdatePlaneEquation();
			}
		}

		public Vec3 C
		{
			get { return _c; }
			set
			{
				_c = value;
				UpdatePlaneEquation();
			}
		}

		public Scalar PlaneOffset { get { return _planeOffset; } }
		public Vec3 Normal { get { return _normal; } }

		public TriangleFrac(Vec3 a, Vec3 b, Vec3 c)
		{
			_a = a;
			_b = b;
			_c = c;
			UpdatePlaneEquation();
		}

		public Scalar SignedDistanceUnnormalized(Vec3 p)
		{
			return SignedDistanceUnnormalized(ref p);
		}

		public Scalar SignedDistanceUnnormalized(ref Vec3 p)
		{
			return Vec3.Dot(ref _normal, ref p) + _planeOffset;
		}

		public void Transform(Mat4x3 m)
		{
			_a = Vec3.Transform(ref _a, ref m);
			_b = Vec3.Transform(ref _b, ref m);
			_c = Vec3.Transform(ref _c, ref m);
			UpdatePlaneEquation();
		}

		/// <summary>
		/// Adds new triangles to newTriangles. Returns whether any new triangles were generated or not.
		/// </summary>
		/// <param name="splitter"></param>
		/// <param name="newTriangles"></param>
		/// <returns></returns>
		public bool SplitBy(Vec3 planeNormal, Scalar planeOffset, List<TriangleFrac> newTriangles)
		{
			Scalar d0 = Vec3.Dot(planeNormal, _a) + planeOffset;
			Scalar d1 = Vec3.Dot(planeNormal, _b) + planeOffset;
			Scalar d2 = Vec3.Dot(planeNormal, _c) + planeOffset;

			bool zero0 = d0.IsZero;
			bool zero1 = d1.IsZero;
			bool zero2 = d2.IsZero;

			Vec3 edge01 = _b - _a;
			Vec3 edge12 = _c - _b;
			Vec3 edge20 = _a - _c;

			Scalar t01 = -(Vec3.Dot(ref planeNormal, ref _a) + planeOffset) / Vec3.Dot(ref planeNormal, ref edge01);
			Scalar t12 = -(Vec3.Dot(ref planeNormal, ref _b) + planeOffset) / Vec3.Dot(ref planeNormal, ref edge12);
			Scalar t20 = -(Vec3.Dot(ref planeNormal, ref _c) + planeOffset) / Vec3.Dot(ref planeNormal, ref edge20);

			bool intersects01 = t01.IsValid && t01.IsNotNegative && t01.IsLessOrEqualThanOne;
			bool intersects12 = t12.IsValid && t12.IsNotNegative && t12.IsLessOrEqualThanOne;
			bool intersects20 = t20.IsValid && t20.IsNotNegative && t20.IsLessOrEqualThanOne;

			Vec3 i01 = _a + edge01 * t01;
			Vec3 i12 = _b + edge12 * t12;
			Vec3 i20 = _c + edge20 * t20;

			// Planar special case
			if(zero0 && zero1 && zero2)
			{
				// No need to do special handling for this case
				// The triangle will get split by all the other triangles that share an edge with the co-planar one
				return false;
			}

			// Do not generate new triangles if the plane only touches an edge
			if((zero0 && zero1 && !zero2) || (zero0 && !zero1 && zero2) || (!zero0 && zero1 && zero2))
				return false;

			// Plane touches one vertex and intersect the opposite edge
			if(zero0 && intersects12)
			{
				newTriangles.Add(new TriangleFrac(_c, _a, i12));
				newTriangles.Add(new TriangleFrac(i12, _a, _b));
				return true;
			}

			if(zero1 && intersects20)
			{
				newTriangles.Add(new TriangleFrac(_c, i20, _b));
				newTriangles.Add(new TriangleFrac(i20, _a, _b));
				return true;
			}

			if(zero2 && intersects01)
			{
				newTriangles.Add(new TriangleFrac(_c, _a, i01));
				newTriangles.Add(new TriangleFrac(_c, i01, _b));
				return true;
			}

			// If the plane only touches one vertex but does not intersect an edge, do not split the triangle
			if(zero0 || zero1 || zero2)
			{
				return false;
			}
			
			// The plane intersects two edges

			if(intersects01 && intersects20)
			{
				newTriangles.Add(new TriangleFrac(_a, i01, i20));
				newTriangles.Add(new TriangleFrac(i01, _b, _c));
				newTriangles.Add(new TriangleFrac(i01, _c, i20));
				return true;
			}

			if(intersects12 && intersects20)
			{
				newTriangles.Add(new TriangleFrac(_a, i12, i20));
				newTriangles.Add(new TriangleFrac(_a, _b, i12));
				newTriangles.Add(new TriangleFrac(i12, _c, i20));
				return true;
			}

			if(intersects01 && intersects12)
			{
				newTriangles.Add(new TriangleFrac(_a, i01, i12));
				newTriangles.Add(new TriangleFrac(_a, i12, _c));
				newTriangles.Add(new TriangleFrac(i01, _b, i12));
				return true;
			}

			return false;
		}

		public bool BoundingBoxIntersects(TriangleFrac triangle)
		{
			if(triangle._boundingBoxMax.X < this._boundingBoxMin.X || triangle._boundingBoxMin.X > this._boundingBoxMax.X)
				return false;
			if(triangle._boundingBoxMax.Y < this._boundingBoxMin.Y || triangle._boundingBoxMin.Y > this._boundingBoxMax.Y)
				return false;
			if(triangle._boundingBoxMax.Z < this._boundingBoxMin.Z || triangle._boundingBoxMin.Z > this._boundingBoxMax.Z)
				return false;
			return true;
		}

		void UpdatePlaneEquation()
		{
			Vec3 u = _b - _a;
			Vec3 v = _c - _a;
			_normal = Vec3.Cross(ref u, ref v);
			_planeOffset = -Vec3.Dot(ref _normal, ref _a);

			_boundingBoxMin.X = (_a.X < _b.X ? _a.X : _b.X);
			_boundingBoxMin.X = (_c.X < _boundingBoxMin.X ? _c.X : _boundingBoxMin.X);

			_boundingBoxMin.Y = (_a.Y < _b.Y ? _a.Y : _b.Y);
			_boundingBoxMin.Y = (_c.Y < _boundingBoxMin.Y ? _c.Y : _boundingBoxMin.Y);

			_boundingBoxMin.Z = (_a.Z < _b.Z ? _a.Z : _b.Z);
			_boundingBoxMin.Z = (_c.Z < _boundingBoxMin.Z ? _c.Z : _boundingBoxMin.Z);

			_boundingBoxMax.X = (_a.X > _b.X ? _a.X : _b.X);
			_boundingBoxMax.X = (_c.X > _boundingBoxMax.X ? _c.X : _boundingBoxMax.X);

			_boundingBoxMax.Y = (_a.Y > _b.Y ? _a.Y : _b.Y);
			_boundingBoxMax.Y = (_c.Y > _boundingBoxMax.Y ? _c.Y : _boundingBoxMax.Y);

			_boundingBoxMax.Z = (_a.Z > _b.Z ? _a.Z : _b.Z);
			_boundingBoxMax.Z = (_c.Z > _boundingBoxMax.Z ? _c.Z : _boundingBoxMax.Z);
		}
	}
}
