﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Csg
{
	static class GeometryUtils
	{
		static Scalar[] SolveMatrix(Scalar[,] matrix)
		{
			int lenghtX = matrix.GetLength(0);
			int lenghtY = matrix.GetLength(1);

			Scalar[] reseni = new Scalar[lenghtX - 1];

			// Upravi matici do trojuhelnikoveho tvaru
			for(int x = 0; x < lenghtX - 2; x++)
			{
				for(int y = x + 1; y < lenghtY; y++)
				{
					if(matrix[x, y] == 0)
						continue;

					Scalar s = matrix[x, y] / matrix[x, x];
					for(int cX = 0; cX < lenghtX; cX++)
					{
						matrix[cX, y] = matrix[cX, y] - matrix[cX, x] * s;
					}
				}
			}

			// Spocita hodnoty neznamych a upravi matici soustavy do jednotkoveho tvaru
			for(int x = lenghtX - 2; x >= 0; x--)
			{
				Scalar r = matrix[lenghtX - 1, x] / matrix[x, x];
				reseni[x] = r;
				matrix[x, x] = 1;
				matrix[lenghtX - 1, x] = r;
				for(int y = x - 1; y >= 0; y--)
				{
					Scalar s = matrix[x, y] / matrix[x, x];
					for(int cX = 0; cX < lenghtX; cX++)
					{
						matrix[cX, y] = matrix[cX, y] - matrix[cX, x] * s;
					}
				}
			}

			return reseni;
		}

		/*
		// http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
		public static Scalar LinePointDistanceSquared(Vec3 line1, Vec3 line2, Vec3 point)
		{
			Vec3 dir = line2 - line1;
			Vec3 upper = Vec3.Cross(dir, line1 - point);
			Scalar num = Vec3.Dot(upper, upper);
			Scalar den = Vec3.Dot(dir, dir);

			return num / den;
		}

		// https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
		public static bool LineSegmentsIntersect(Vec2 p, Vec2 r, Vec2 q, Vec2 s)
		{
			Scalar crossRS = Vec2.Cross(r, s);
			Vec2 diff = q - p;
			Scalar crossDS = Vec2.Cross(diff, s);
			Scalar crossDR = Vec2.Cross(diff, r);

			if(crossRS.Numerator == 0 && crossDR.Numerator == 0)
			{
				Scalar drr = Vec2.Dot(r, r);
				Scalar t0 = Vec2.Dot(diff, r) / drr;
				Scalar t1 = t0 + Vec2.Dot(s, r) / drr;

				if(t0 > t1)
				{
					Scalar temp = t0;
					t0 = t1;
					t1 = temp;
				}

				if(t1.Numerator < 0 || t0.Numerator > t0.Denominator)
					return false;
				return true;
			}

			if(crossRS.Numerator == 0 && crossDR.Numerator != 0)
				return false;

			Scalar t = crossDS / crossRS;
			Scalar u = crossDR / crossRS;

			if(crossRS.Numerator != 0 && t.Numerator >= 0 && t.Numerator <= t.Denominator && u.Numerator >= 0 &&
			   u.Numerator <= u.Denominator)
				return true;
			return false;
		}

		public static bool PointInTriangle(TriangleFrac triangle, Vec3 p)
		{
			Vec2 c;
			return PointInTriangle(triangle, p, out c);
		}
		*/

		public static bool PointInTriangle(TriangleFrac triangle, Vec3 p, out Vec2 pointCoords)
		{
			pointCoords = new Vec2(float.NaN, float.NaN);
			Scalar dist = Vec3.Dot(triangle.Normal, p) + triangle.PlaneOffset;
			if(!dist.IsZero)
				return false;
			p -= triangle.A;
			Vec3 edge01 = triangle.B - triangle.A;
			Vec3 edge02 = triangle.C - triangle.A;

			Vec3 eq0 = new Vec3(edge01.X, edge02.X, p.X);
			Vec3 eq1 = new Vec3(edge01.Y, edge02.Y, p.Y);
			Vec3 eq2 = new Vec3(edge01.Z, edge02.Z, p.Z);

			eq0 += eq2;
			eq1 += eq2;

			if(eq0.X == 0 || eq1.Y == 0)
			{
				var temp = eq0;
				eq0 = eq1;
				eq1 = temp;
			}

			var result = SolveMatrix(new Scalar[3, 2]
			{
				{eq0.X, eq1.X},
				{eq0.Y, eq1.Y},
				{eq0.Z, eq1.Z},
			});

			pointCoords.X = result[0];
			pointCoords.Y = result[1];

			Scalar xplusy = pointCoords.X + pointCoords.Y;

			// x >= 0 && y >= 0 && (x + y) <= 1
			if(pointCoords.X.IsNotNegative && pointCoords.Y.IsNotNegative && xplusy.IsLessOrEqualThanOne && pointCoords.X.IsValid && pointCoords.Y.IsValid)
				return true;
			return false;
		}

		/*
		public static bool TrianglesIntersect(TriangleFrac first, TriangleFrac second)
		{
			Scalar dist0 = second.SignedDistanceUnnormalized(first.A);
			Scalar dist1 = second.SignedDistanceUnnormalized(first.B);
			Scalar dist2 = second.SignedDistanceUnnormalized(first.C);

			bool zero0 = dist0.Numerator == 0;
			bool zero1 = dist1.Numerator == 0;
			bool zero2 = dist2.Numerator == 0;

			Vec2 planarUv0, planarUv1, planarUv2;

			bool inTriangle0 = PointInTriangle(second, first.A, out planarUv0);
			bool inTriangle1 = PointInTriangle(second, first.B, out planarUv1);
			bool inTriangle2 = PointInTriangle(second, first.C, out planarUv2);

			// If at least one vertex of one triangle lies inside the other triangle, return true
			if(inTriangle0 || inTriangle1 || inTriangle2)
				return true;
			if(PointInTriangle(first, second.A) || PointInTriangle(first, second.B) || PointInTriangle(first, second.C))
				return true;

			if(zero0 && zero1 && zero2)
			{
				// Triangles are co-planar and no vertex lies inside the other triangle
				Vec2 edge01 = planarUv1 - planarUv0;
				Vec2 edge12 = planarUv2 - planarUv1;
				Vec2 edge20 = planarUv0 - planarUv2;

				// Check all edges of first agains all edges of second
				if(LineSegmentsIntersect(planarUv0, edge01, new Vec2(0, 0), new Vec2(0, 1)))
					return true;
				if(LineSegmentsIntersect(planarUv1, edge12, new Vec2(0, 0), new Vec2(0, 1)))
					return true;
				if(LineSegmentsIntersect(planarUv2, edge20, new Vec2(0, 0), new Vec2(0, 1)))
					return true;

				if(LineSegmentsIntersect(planarUv0, edge01, new Vec2(0, 0), new Vec2(1, 0)))
					return true;
				if(LineSegmentsIntersect(planarUv1, edge12, new Vec2(0, 0), new Vec2(1, 0)))
					return true;
				if(LineSegmentsIntersect(planarUv2, edge20, new Vec2(0, 0), new Vec2(1, 0)))
					return true;

				if(LineSegmentsIntersect(planarUv0, edge01, new Vec2(1, 0), new Vec2(-1, 1)))
					return true;
				if(LineSegmentsIntersect(planarUv1, edge12, new Vec2(1, 0), new Vec2(-1, 1)))
					return true;
				if(LineSegmentsIntersect(planarUv2, edge20, new Vec2(1, 0), new Vec2(-1, 1)))
					return true;

				return false;
			}

			// If two vertices lie on the plane, check whether the line they form intersects the triangle
			Vec2 lineUv0 = new Vec2();
			Vec2 lineUv1 = new Vec2();
			bool line = false;
			if(zero0 && zero1 && !zero2)
			{
				line = true;
				lineUv0 = planarUv0;
				lineUv1 = planarUv1;
			}
			if(zero0 && !zero1 && zero2)
			{
				line = true;
				lineUv0 = planarUv2;
				lineUv1 = planarUv0;
			}
			if(!zero0 && zero1 && zero2)
			{
				line = true;
				lineUv0 = planarUv1;
				lineUv1 = planarUv2;
			}

			if(line)
			{
				Vec2 edge = lineUv1 - lineUv0;

				// No need to check if the line lies inside, because that was taken care of by the vertex-in-triangle test
				if(LineSegmentsIntersect(lineUv0, edge, new Vec2(0, 0), new Vec2(0, 1)))
					return true;
				if(LineSegmentsIntersect(lineUv0, edge, new Vec2(0, 0), new Vec2(1, 0)))
					return true;
				if(LineSegmentsIntersect(lineUv0, edge, new Vec2(1, 0), new Vec2(-1, 1)))
					return true;

				// The third vertex does not lie on the triangle plane, thus there is no way the triangle could intersect
				return false;
			}

			bool side0 = dist0.Numerator >= 0;
			bool side1 = dist1.Numerator >= 0;
			bool side2 = dist2.Numerator >= 0;

			if((side0 && side1 && side2) || (!side0 && !side1 && !side2))
			{
				// Triangles do not intersect
				return false;
			}

			Vec3 v0 = new Vec3();
			Vec3 v1 = new Vec3();
			Vec3 v2 = new Vec3();

			Scalar d0 = new Scalar();
			Scalar d1 = new Scalar();
			Scalar d2 = new Scalar();

			// Reorder vertices so that v1 lies on the other side of the plane than v0 and v2
			if((side0 && !side1 && !side2) || (!side0 && side1 && side2))
			{
				v0 = first.C;
				v1 = first.A;
				v2 = first.B;

				d0 = dist2;
				d1 = dist0;
				d2 = dist1;
			}
			if((!side0 && side1 && !side2) || (side0 && !side1 && side2))
			{
				v0 = first.A;
				v1 = first.B;
				v2 = first.C;

				d0 = dist0;
				d1 = dist1;
				d2 = dist2;
			}
			if((!side0 && !side1 && side2) || (side0 && side1 && !side2))
			{
				v0 = first.B;
				v1 = first.C;
				v2 = first.A;

				d0 = dist1;
				d1 = dist2;
				d2 = dist0;
			}

			
			Vec3 d = Vec3.Cross(first.Normal, second.Normal);
			Scalar t0, t1, t2, t3;
			Scalar p0, p1, p2;

			p0 = Vec3.Dot(v0, d);
			p1 = Vec3.Dot(v1, d);
			p2 = Vec3.Dot(v2, d);

			t0 = p0 + (p1 - p0) * d0 / (d0 - d1);
			t1 = p2 + (p1 - p2) * d2 / (d2 - d1);

			if(t0 > t1)
			{
				Scalar temp = t0;
				t0 = t1;
				t1 = temp;
			}

			// Same for second triangle
			dist0 = first.SignedDistanceUnnormalized(second.A);
			dist1 = first.SignedDistanceUnnormalized(second.B);
			dist2 = first.SignedDistanceUnnormalized(second.C);

			side0 = dist0.Numerator >= 0;
			side1 = dist1.Numerator >= 0;
			side2 = dist2.Numerator >= 0;

			if((side0 && !side1 && !side2) || (!side0 && side1 && side2))
			{
				v0 = second.C;
				v1 = second.A;
				v2 = second.B;

				d0 = dist2;
				d1 = dist0;
				d2 = dist1;
			}
			if((!side0 && side1 && !side2) || (side0 && !side1 && side2))
			{
				v0 = second.A;
				v1 = second.B;
				v2 = second.C;

				d0 = dist0;
				d1 = dist1;
				d2 = dist2;
			}
			if((!side0 && !side1 && side2) || (side0 && side1 && !side2))
			{
				v0 = second.B;
				v1 = second.C;
				v2 = second.A;

				d0 = dist1;
				d1 = dist2;
				d2 = dist0;
			}

			p0 = Vec3.Dot(v0, d);
			p1 = Vec3.Dot(v1, d);
			p2 = Vec3.Dot(v2, d);

			t2 = p0 + (p1 - p0) * d0 / (d0 - d1);
			t3 = p2 + (p1 - p2) * d2 / (d2 - d1);

			if(t2 > t3)
			{
				Scalar temp = t2;
				t2 = t3;
				t3 = temp;
			}

			// Final evaluation
			if(t2 > t1 || t3 < t0)
				return false;
			return true;
		}
		*/
	}
}
