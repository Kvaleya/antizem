﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Csg
{
	public struct Mat4x3
	{
		public Scalar[,] M;

		public Scalar this[int row, int column] 
		{
			get { return M[row, column]; }
			set { M[row, column] = value; }
		}

		public Mat4x3(Matrix4 m)
		{
			M = new Scalar[4,3];

			M[0, 0] = new Scalar(m.M11);
			M[0, 1] = new Scalar(m.M12);
			M[0, 2] = new Scalar(m.M13);
			//M[0, 3] = new Fraction(m.M14);

			M[1, 0] = new Scalar(m.M21);
			M[1, 1] = new Scalar(m.M22);
			M[1, 2] = new Scalar(m.M23);
			//M[1, 3] = new Fraction(m.M24);

			M[2, 0] = new Scalar(m.M31);
			M[2, 1] = new Scalar(m.M32);
			M[2, 2] = new Scalar(m.M33);
			//M[2, 3] = new Fraction(m.M34);

			M[3, 0] = new Scalar(m.M41);
			M[3, 1] = new Scalar(m.M42);
			M[3, 2] = new Scalar(m.M43);
			//M[3, 3] = new Fraction(m.M44);
		}
	}
}
