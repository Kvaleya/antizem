﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Csg
{
	static class CsgOps
	{
		public static List<TriangleFrac> Combine(List<TriangleFrac> meshA, List<TriangleFrac> meshB, bool discardAinsideB, bool discardBinsideA, bool flipA, bool flipB)
		{
			List<TriangleFrac> meshSplitA = SplitMeshByMesh(meshA, meshB);
			List<TriangleFrac> meshSplitB = SplitMeshByMesh(meshB, meshA);

			List<TriangleFrac> finalMesh = new List<TriangleFrac>();

			FilterTriangles(meshSplitA, meshB, finalMesh, discardAinsideB, flipA, false, true);
			FilterTriangles(meshSplitB, meshA, finalMesh, discardBinsideA, flipB, false, false);

			//finalMesh.AddRange(meshSplitA);
			//finalMesh.AddRange(meshSplitB);

			return finalMesh;
		}

		/// <summary>
		/// Performs a check for each triangle of mesh whether it is inside container. If discardModeInside is set to true, then all triangles inside the container are discarted, otherwise all outside triangles are discarted. If flipSurviving is set to true, then all surviving triangles have their vertex order flipped.
		/// </summary>
		/// <param name="mesh">The mesh of triangles that are to be filtered</param>
		/// <param name="container">The mesh used to perform the inside test</param>
		/// <param name="result"></param>
		/// <param name="discardModeInside"></param>
		/// <param name="flipSurviving"></param>
		/// <param name="includeZero">Whether to also count intersections that happen directly on the triangle that is being tested</param>
		public static void FilterTriangles(List<TriangleFrac> mesh, List<TriangleFrac> container, List<TriangleFrac> result, bool discardModeInside, bool flipSurviving, bool includeZero, bool keepIdenticalFaces)
		{
			foreach(TriangleFrac triangle in mesh)
			{
				Vec3 origin = (triangle.A + triangle.B + triangle.C) / 3;
				
				HashSet<Scalar> intersections = new HashSet<Scalar>();

				foreach(TriangleFrac blocker in container)
				{
					Scalar dot = Vec3.Dot(blocker.Normal, triangle.Normal);

					if(dot.IsZero)
						continue;

					Scalar t = -(Vec3.Dot(blocker.Normal, origin) + blocker.PlaneOffset) / Vec3.Dot(blocker.Normal, triangle.Normal);
					
					if(t.IsNegative || !t.IsValid)
						continue;
					if((t.IsZero && !includeZero) && !(t.IsZero && dot.IsPositive && !keepIdenticalFaces))
						continue;

					Vec3 intersection = origin + triangle.Normal * t;

					Vec2 uv;

					if(GeometryUtils.PointInTriangle(blocker, intersection, out uv))
					{
						if(!intersections.Contains(t))
							intersections.Add(t);
					}
				}

				bool inside = intersections.Count % 2 == 1;

				if((inside && !discardModeInside) || (!inside && discardModeInside))
				{
					if(flipSurviving)
					{
						result.Add(new TriangleFrac(triangle.A, triangle.C, triangle.B));
					}
					else
					{
						result.Add(triangle);
					}
				}
				else
				{
					var erere = 0;
				}
			}
		}

		public static List<TriangleFrac> SplitMeshByMesh(List<TriangleFrac> mesh, List<TriangleFrac> splitterMesh)
		{
			List<TriangleFrac> splitted = new List<TriangleFrac>();
			var stack = mesh.ToList();

			while(stack.Count > 0)
			{
				TriangleFrac triangle = stack[stack.Count - 1];

				stack.RemoveAt(stack.Count - 1);

				bool split = false;

				foreach(TriangleFrac splitter in splitterMesh)
				{
					if(triangle.BoundingBoxIntersects(splitter))
					{
						if(triangle.SplitBy(splitter.Normal, splitter.PlaneOffset, stack))
						{
							split = true;
							break;
						}
					}
				}

				if(!split)
				{
					splitted.Add(triangle);
				}
			}

			return splitted;
		}
	}
}
