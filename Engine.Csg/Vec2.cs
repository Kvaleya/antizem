﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Csg
{
	public struct Vec2 : IEquatable<Vec2>
	{
		public Scalar X, Y;

		public Vec2(Vector2 v)
		{
			X = new Scalar(v.X);
			Y = new Scalar(v.Y);
		}

		public Vec2(Scalar x, Scalar y)
		{
			X = x;
			Y = y;
		}

		public static Scalar Dot(Vec2 a, Vec2 b)
		{
			return Dot(ref a, ref b.X, ref b.Y);
		}

		public static Scalar Dot(ref Vec2 a, ref Vec2 b)
		{
			return Dot(ref a, ref b.X, ref b.Y);
		}

		public static Scalar Dot(ref Vec2 v, ref Scalar x, ref Scalar y)
		{
			var a = v.X * x + v.Y * y;
			return a;
		}

		public static Scalar Cross(Vec2 a, Vec2 b)
		{
			return Cross(ref a, ref b.X, ref b.Y);
		}

		public static Scalar Cross(ref Vec2 a, ref Vec2 b)
		{
			return Cross(ref a, ref b.X, ref b.Y);
		}

		public static Scalar Cross(ref Vec2 v, ref Scalar x, ref Scalar y)
		{
			return v.X * y - v.Y * x;
		}

		public static Vec2 operator +(Vec2 a, Vec2 b)
		{
			return new Vec2(a.X + b.X, a.Y + b.Y);
		}

		public static Vec2 operator -(Vec2 a, Vec2 b)
		{
			return new Vec2(a.X - b.X, a.Y - b.Y);
		}

		public static Vec2 operator *(Vec2 a, Vec2 b)
		{
			return new Vec2(a.X * b.X, a.Y * b.Y);
		}

		public static Vec2 operator *(Vec2 a, Scalar b)
		{
			return new Vec2(a.X * b, a.Y * b);
		}

		public static Vec2 operator *(Scalar a, Vec2 b)
		{
			return new Vec2(a * b.X, a * b.Y);
		}

		public static Vec2 operator /(Vec2 a, Vec2 b)
		{
			return new Vec2(a.X / b.X, a.Y / b.Y);
		}

		public static Vec2 operator /(Vec2 a, Scalar b)
		{
			return new Vec2(a.X / b, a.Y / b);
		}

		public static Vec2 operator /(Scalar a, Vec2 b)
		{
			return new Vec2(a / b.X, a / b.Y);
		}

		public override string ToString()
		{
			return "(" + X.ToDouble().ToString(CultureInfo.InvariantCulture) + "; " + Y.ToDouble().ToString(CultureInfo.InvariantCulture) + ")";
		}

		public bool Equals(Vec2 other)
		{
			return X.Equals(other.X) && Y.Equals(other.Y);
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			return obj is Vec2 && Equals((Vec2)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (X.GetHashCode() * 397) ^ Y.GetHashCode();
			}
		}

		public static bool operator ==(Vec2 left, Vec2 right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Vec2 left, Vec2 right)
		{
			return !left.Equals(right);
		}
	}
}
