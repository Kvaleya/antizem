﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Csg.Brushes
{
	public class CubeBrush : ICsgBrush
	{
		Mat4x3 _transform;

		public CubeBrush(Mat4x3 transform)
		{
			_transform = transform;
		}

		public List<TriangleFrac> Triangulate()
		{
			Scalar scale = 1;

			Vec3 a = new Vec3(-scale, -scale, -scale);
			Vec3 b = new Vec3(scale, -scale, -scale);
			Vec3 c = new Vec3(scale, scale, -scale);
			Vec3 d = new Vec3(-scale, scale, -scale);
			Vec3 e = new Vec3(-scale, -scale, scale);
			Vec3 f = new Vec3(scale, -scale, scale);
			Vec3 g = new Vec3(scale, scale, scale);
			Vec3 h = new Vec3(-scale, scale, scale);

			a = Vec3.Transform(a, ref _transform);
			b = Vec3.Transform(b, ref _transform);
			c = Vec3.Transform(c, ref _transform);
			d = Vec3.Transform(d, ref _transform);
			e = Vec3.Transform(e, ref _transform);
			f = Vec3.Transform(f, ref _transform);
			g = Vec3.Transform(g, ref _transform);
			h = Vec3.Transform(h, ref _transform);

			List<TriangleFrac> tris = new List<TriangleFrac>();

			tris.Add(new TriangleFrac(a, c, b));
			tris.Add(new TriangleFrac(a, d, c));

			tris.Add(new TriangleFrac(a, b, f));
			tris.Add(new TriangleFrac(a, f, e));

			tris.Add(new TriangleFrac(b, c, g));
			tris.Add(new TriangleFrac(b, g, f));

			tris.Add(new TriangleFrac(c, d, h));
			tris.Add(new TriangleFrac(c, h, g));

			tris.Add(new TriangleFrac(d, a, e));
			tris.Add(new TriangleFrac(d, e, h));

			tris.Add(new TriangleFrac(e, f, g));
			tris.Add(new TriangleFrac(e, g, h));

			return tris;
		}
	}
}
