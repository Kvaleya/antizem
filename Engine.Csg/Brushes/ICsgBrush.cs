﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Csg.Brushes
{
	public interface ICsgBrush
	{
		List<TriangleFrac> Triangulate();
	}
}
