﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Csg.Brushes
{
	public class UnionBrush : ICsgBrush
	{
		public ICsgBrush A, B;

		public UnionBrush(ICsgBrush a, ICsgBrush b)
		{
			A = a;
			B = b;
		}

		public List<TriangleFrac> Triangulate()
		{
			List<TriangleFrac> trisA = A.Triangulate();
			List<TriangleFrac> trisB = B.Triangulate();

			return CsgOps.Combine(trisA, trisB, true, true, false, false);
		}
	}
}
