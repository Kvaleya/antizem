﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Csg
{
	public struct Vec3 : IEquatable<Vec3>
	{
		public Scalar X, Y, Z;

		public Vec3(Vector3 v)
		{
			X = new Scalar(v.X);
			Y = new Scalar(v.Y);
			Z = new Scalar(v.Z);
		}

		public Vec3(Scalar x, Scalar y, Scalar z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public static Scalar Dot(Vec3 a, Vec3 b)
		{
			return Dot(ref a, ref b.X, ref b.Y, ref b.Z);
		}

		public static Scalar Dot(ref Vec3 a, ref Vec3 b)
		{
			return Dot(ref a, ref b.X, ref b.Y, ref b.Z);
		}

		public static Scalar Dot(ref Vec3 v, ref Scalar x, ref Scalar y, ref Scalar z)
		{
			return v.X * x + v.Y * y + v.Z * z;
		}

		public static Vec3 Cross(Vec3 a, Vec3 b)
		{
			return Cross(ref a, ref b.X, ref b.Y, ref b.Z);
		}

		public static Vec3 Cross(ref Vec3 a, ref Vec3 b)
		{
			return Cross(ref a, ref b.X, ref b.Y, ref b.Z);
		}

		public static Vec3 Cross(ref Vec3 v, ref Scalar x, ref Scalar y, ref Scalar z)
		{
			return new Vec3(v.Y * z - v.Z * y, v.Z * x - v.X * z, v.X * y - v.Y * x);
		}

		public static Vec3 Transform(Vec3 v, Mat4x3 m)
		{
			return Transform(ref v, ref m);
		}

		public static Vec3 Transform(Vec3 v, ref Mat4x3 m)
		{
			return Transform(ref v, ref m);
		}

		public static Vec3 Transform(ref Vec3 v, ref Mat4x3 m)
		{
			return new Vec3(
				v.X * m[0, 0] + v.Y * m[1, 0] + v.Z * m[2, 0] + m[3, 0],
				v.X * m[0, 1] + v.Y * m[1, 1] + v.Z * m[2, 1] + m[3, 1],
				v.X * m[0, 2] + v.Y * m[1, 2] + v.Z * m[2, 2] + m[3, 2]
				);
		}

		public static Vec3 operator +(Vec3 a, Vec3 b)
		{
			return new Vec3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
		}

		public static Vec3 operator -(Vec3 a, Vec3 b)
		{
			return new Vec3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
		}

		public static Vec3 operator *(Vec3 a, Vec3 b)
		{
			return new Vec3(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
		}

		public static Vec3 operator *(Vec3 a, Scalar b)
		{
			return new Vec3(a.X * b, a.Y * b, a.Z * b);
		}

		public static Vec3 operator *(Scalar a, Vec3 b)
		{
			return new Vec3(a * b.X, a * b.Y, a * b.Z);
		}

		public static Vec3 operator /(Vec3 a, Vec3 b)
		{
			return new Vec3(a.X / b.X, a.Y / b.Y, a.Z / b.Z);
		}

		public static Vec3 operator /(Vec3 a, Scalar b)
		{
			return new Vec3(a.X / b, a.Y / b, a.Z / b);
		}

		public static Vec3 operator /(Scalar a, Vec3 b)
		{
			return new Vec3(a / b.X, a / b.Y, a / b.Z);
		}

		public override string ToString()
		{
			return "(" + X.ToDouble().ToString(CultureInfo.InvariantCulture) + "; " + Y.ToDouble().ToString(CultureInfo.InvariantCulture) + "; " + Z.ToDouble().ToString(CultureInfo.InvariantCulture) + ")";
		}

		public bool Equals(Vec3 other)
		{
			return X.Equals(other.X) && Y.Equals(other.Y) && Z.Equals(other.Z);
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			return obj is Vec3 && Equals((Vec3)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = X.GetHashCode();
				hashCode = (hashCode * 397) ^ Y.GetHashCode();
				hashCode = (hashCode * 397) ^ Z.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(Vec3 left, Vec3 right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Vec3 left, Vec3 right)
		{
			return !left.Equals(right);
		}
	}
}
