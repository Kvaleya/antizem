﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Csg
{
	public struct Scalar : IEquatable<Scalar>
	{
		// Denominator is always positive
		//readonly BigInteger Numerator, Denominator;

		static readonly double _tolerance = 1e-10;

		double _value;
		bool _valid;

		public bool IsValid
		{
			get { return _valid; }
		}


		public bool IsPositive
		{
			get { return _value > 0; }
		}

		public bool IsNegative
		{
			get { return _value < 0; }
		}

		public bool IsZero
		{
			get { return _value < _tolerance && _value > -_tolerance; }
		}

		public bool IsNotPositive
		{
			get { return _value <= 0; }
		}

		public bool IsNotNegative
		{
			get { return _value >= 0; }
		}

		public bool IsLessThanOne
		{
			get { return _value < 1; }
		}

		public bool IsLessOrEqualThanOne
		{
			get { return _value <= 1; }
		}

		public Scalar(float value)
		{
			if(float.IsPositiveInfinity(value))
			{
				_valid = false;
				_value = 1;
				return;
			}
			if(float.IsNegativeInfinity(value))
			{
				_valid = false;
				_value = -1;
				return;
			}
			if(float.IsNaN(value))
			{
				_valid = false;
				_value = 0;
				return;
			}

			_value = (double)value;
			_valid = true;
		}

		public Scalar(double value)
		{
			_value = value;
			_valid = true;
			if(double.IsNaN(value) || double.IsInfinity(value))
				_valid = false;
		}

		public static Scalar operator +(Scalar a, Scalar b)
		{
			return new Scalar(a._value + b._value);
		}

		public static Scalar operator -(Scalar a, Scalar b)
		{
			return new Scalar(a._value - b._value);
		}

		public static Scalar operator *(Scalar a, Scalar b)
		{
			return new Scalar(a._value * b._value);
		}

		public static Scalar operator /(Scalar a, Scalar b)
		{
			if(b._value == 0d)
			{
				if(a.IsPositive)
					return new Scalar(float.PositiveInfinity);
				if(a.IsNegative)
					return new Scalar(float.NegativeInfinity);
				return new Scalar(float.NaN);
			}

			return new Scalar(a._value / b._value);
		}

		public static bool operator <(Scalar a, Scalar b)
		{
			return a._value < b._value;
		}

		public static bool operator ==(Scalar a, Scalar b)
		{
			return a._value == b._value;
		}

		public static bool operator !=(Scalar a, Scalar b)
		{
			return a._value != b._value;
		}

		public static bool operator >(Scalar a, Scalar b)
		{
			return a._value > b._value;
		}

		public static bool operator <=(Scalar a, Scalar b)
		{
			return a._value <= b._value;
		}

		public static bool operator >=(Scalar a, Scalar b)
		{
			return a._value >= b._value;
		}

		public static Scalar operator -(Scalar a)
		{
			return new Scalar(-a._value);
		}

		public static implicit operator Scalar(float f)
		{
			return new Scalar(f);
		}

		public static implicit operator Scalar(double f)
		{
			return new Scalar(f);
		}

		public static implicit operator Scalar(int i)
		{
			return new Scalar((double)i);
		}
		/*
		public static Scalar Sqrt(Scalar scalar, int iterations)
		{
			BigInteger move = 1;
			Scalar result = scalar;
			for (int i = 0; i < iterations; i++)
			{
				move = move << 1;

				if (result * result == scalar)
					return result;

				if (result * result < scalar)
				{
					result += scalar * new Scalar(1, move);
				}
				else
				{
					result -= scalar * new Scalar(1, move);
				}
			}
			return result;
		}

		public static BigInteger GreatestCommonDivisor(BigInteger u, BigInteger w)
		{
			BigInteger r = 0;
			while (w != 0)
			{
				r = u % w;
				u = w;
				w = r;
			}
			return u;
		}
		*/

		public double ToDouble()
		{
			if(!_valid)
			{
				if(IsPositive)
					return double.PositiveInfinity;
				if(IsNegative)
					return double.NegativeInfinity;
				return double.NaN;
			}
			return (double)_value;
		}

		public override string ToString()
		{
			return this.ToDouble().ToString(CultureInfo.InvariantCulture);
		}

		public bool Equals(Scalar other)
		{
			return _value == other._value;
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			return obj is Scalar && Equals((Scalar)obj);
		}

		public override int GetHashCode()
		{
			return _value.GetHashCode();
		}

		static unsafe int SingleToInt32Bits(float value)
		{
			return *(int*)(&value);
		}
		static unsafe float Int32BitsToSingle(int value)
		{
			return *(float*)(&value);
		}
	}
}
