﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using FreeImageAPI;

namespace TextureConverter
{
	static class ImageLoader
	{
		public static DirectBitmap LoadImage(string file)
		{
			string ext = Path.GetExtension(file).ToLowerInvariant();
			var format = GetFormat(ext);
			//FIBITMAP dib = FreeImage.Load(format, file, FREE_IMAGE_LOAD_FLAGS.DEFAULT);
			try
			{
				var bm = FreeImage.LoadBitmap(file, FREE_IMAGE_LOAD_FLAGS.DEFAULT, ref format);
				return new DirectBitmap(bm);
			}
			catch(Exception e)
			{
				Console.WriteLine(e.ToString() + "\n" + e.Message + "\n");
				if(e.InnerException != null)
					Console.WriteLine(e.InnerException.ToString() + "\n" + e.InnerException.Message);
				return null;
			}
		}

		static FREE_IMAGE_FORMAT GetFormat(string ext)
		{
			switch(ext)
			{
				case ".jpg":
					return FREE_IMAGE_FORMAT.FIF_JPEG;
				case ".png":
					return FREE_IMAGE_FORMAT.FIF_PNG;
				case ".bmp":
					return FREE_IMAGE_FORMAT.FIF_BMP;
				case ".tff":
					return FREE_IMAGE_FORMAT.FIF_TIFF;
				case ".tiff":
					return FREE_IMAGE_FORMAT.FIF_TIFF;
				case ".dds":
					return FREE_IMAGE_FORMAT.FIF_DDS;
				case ".gif":
					return FREE_IMAGE_FORMAT.FIF_GIF;
				case ".tga":
					return FREE_IMAGE_FORMAT.FIF_TARGA;
				default:
				{
					return FREE_IMAGE_FORMAT.FIF_UNKNOWN;
				}
			}
		}
	}
}
