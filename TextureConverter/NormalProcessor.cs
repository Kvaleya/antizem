﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Engine.Common;
using OpenTK;

namespace TextureConverter
{
	class NormalProcessor
	{
		//private static System.Drawing.Bitmap BitmapFromSource(BitmapSource bitmapsource)
		//{
		//	System.Drawing.Bitmap bitmap;
		//	using(MemoryStream outStream = new MemoryStream())
		//	{
		//		BitmapEncoder enc = new BmpBitmapEncoder();
		//		enc.Frames.Add(BitmapFrame.Create(bitmapsource));
		//		enc.Save(outStream);
		//		bitmap = new System.Drawing.Bitmap(outStream);
		//	}
		//	return bitmap;
		//}

		public static DirectBitmap LoadBitmap(string file)
		{
			DirectBitmap dbm = null;

			if(!File.Exists(file))
				return null;

			//try
			{
				//dbm = new DirectBitmap(Image.FromFile(file));
				dbm = ImageLoader.LoadImage(file);
			}
			//catch(OutOfMemoryException e)
			//{
			//	try
			//	{
			//		if(Path.GetExtension(file) == ".tga")
			//		{
			//			using(var fs = new System.IO.FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
			//			using(var reader = new System.IO.BinaryReader(fs))
			//			{
			//				var tga = new TgaLib.TgaImage(reader);
			//				System.Windows.Media.Imaging.BitmapSource source = tga.GetBitmap();
			//				dbm = new DirectBitmap(BitmapFromSource(source));
			//			}
			//		}
			//		else
			//		{
			//			Console.WriteLine("Error loading file, unknown format: " + file);
			//		}
			//	}
			//	catch(Exception ex)
			//	{
			//		Console.WriteLine(ex.ToString());
			//		return null;
			//	}
			//}

			return dbm;
		}

		public static DirectBitmap Process(string fileNormal, string fileRoughness, string fileMetallic, string fileSubsurface, int maxParallelism = -1)
		{
			DirectBitmap texNormal = null, texRoughness, texMetallic, texSubsurface;

			int w = 1, h = 1;

			texNormal = LoadBitmap(fileNormal);
			texRoughness = LoadBitmap(fileRoughness);
			texMetallic = LoadBitmap(fileMetallic);
			texSubsurface = LoadBitmap(fileSubsurface);

			if(texNormal != null)
			{
				w = Math.Max(w, texNormal.Width);
				h = Math.Max(h, texNormal.Height);
			}
			else
			{
				texNormal = DirectBitmap.FromSingleValue(new Vector4(0.5f, 0.5f, 1.0f, 0.0f));
			}

			if(texRoughness != null)
			{
				w = Math.Max(w, texRoughness.Width);
				h = Math.Max(h, texRoughness.Height);
			}
			else
			{
				texRoughness = DirectBitmap.FromSingleValue(new Vector4(0));
			}

			if(texMetallic != null)
			{
				w = Math.Max(w, texMetallic.Width);
				h = Math.Max(h, texMetallic.Height);
			}
			else
			{
				texMetallic = DirectBitmap.FromSingleValue(new Vector4(0));
			}

			if(texSubsurface != null)
			{
				w = Math.Max(w, texSubsurface.Width);
				h = Math.Max(h, texSubsurface.Height);
			}
			else
			{
				texSubsurface = DirectBitmap.FromSingleValue(new Vector4(0));
			}

			DirectBitmap result = new DirectBitmap(w, h);

			//for(int y = 0; y < h; y++)
			Parallel.For(0, h, new ParallelOptions()
			{
				MaxDegreeOfParallelism = maxParallelism,
			}, y =>
			{
				for(int x = 0; x < w; x++)
				{
					Vector2d tc = new Vector2d((0.5 + x) / w, (0.5 + y) / h);

					var normal = texNormal.Sample(tc);
					var roughness = texRoughness.Sample(tc).X;
					var metallic = texMetallic.Sample(tc).X;
					var subsurface = texSubsurface.Sample(tc).X;

					Vector4 final = Vector4.Zero;
					final.Xy = normal.Xy;
					final.Z = roughness;
					final.W = metallic * 0.5f + 0.5f - subsurface * 0.5f;

					result.ImageStore(x, y, final);
				}
			});

			return result;
		}
	}
}
