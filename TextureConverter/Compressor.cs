﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextureConverter
{
	class Compressor
	{
		public byte[] CompressBC4(byte[] sourceTex, int w, int h, int maxParallelism = -1)
		{
			int numBlocksX = (w + 3) / 4;
			int numBlocksY = (h + 3) / 4;

			byte[] blocks = new byte[numBlocksX * numBlocksY * 8];

			Parallel.For(0, numBlocksY, new ParallelOptions()
			{
				MaxDegreeOfParallelism = maxParallelism,
			}, blockY =>
			{
				for(int blockX = 0; blockX < numBlocksX; blockX++)
				{
					byte[] blockSrc = new byte[16];

					for(int ly = 0; ly < 4; ly++)
					{
						for(int lx = 0; lx < 4; lx++)
						{
							int x = Math.Min(blockX * 4 + lx, w - 1);
							int y = Math.Min(blockY * 4 + ly, h - 1);

							byte texel = sourceTex[y * w + x];
							blockSrc[ly * 4 + lx] = texel;
						}
					}

					var compressed = CompressBlockBC4(blockSrc);

					int index = (blockY * numBlocksX + blockX) * 8;

					for(int i = 0; i < 8; i++)
					{
						blocks[index + i] = (byte)((compressed >> (i * 8)) & 0xFF);
					}
				}
			});

			return blocks;
		}

		ulong CompressBlockBC4(byte[] tex)
		{
			ulong vmin = 255, vmax = 0, vmine = 255, vmaxe = 0;

			for(int i = 0; i < 16; i++)
			{
				byte texel = tex[i];

				vmin = Math.Min(vmin, texel);
				vmax = Math.Max(vmax, texel);

				if(texel != 0 && texel != 255)
				{
					vmine = Math.Min(vmine, texel);
					vmaxe = Math.Max(vmaxe, texel);
				}
			}

			ulong blockA = 0;
			ulong blockB = 0;
			int shift;

			// Block compress using 8 indices
			blockA = blockA | vmax | (vmin << 8);
			shift = 16;
			for(int i = 0; i < 16; i++)
			{
				float mix = (tex[i] - vmin) / (float)(vmax - vmin);
				mix = mix * 7;

				ulong index = (7 - (ulong) mix) + 1;

				if(index == 1)
					index = 0;
				if(index == 8)
					index = 1;

				blockA = blockA | (index << shift);
				shift += 3;
			}

			// Block compress using 6 indices and special indices for 0 and 255
			blockB = blockB | vmine | (vmaxe << 8);
			shift = 16;
			for(int i = 0; i < 16; i++)
			{
				ulong index = 0;

				if(tex[i] == 0 || tex[i] == 255)
				{
					if(tex[i] == 0)
						index = 6;
					else
						index = 7;
				}
				else
				{
					float mix = (tex[i] - vmine) / (float)(vmaxe - vmine);
					mix = mix * 5;

					index = (5 - (ulong)mix) + 1;

					if(index == 1)
						index = 0;
					if(index == 6)
						index = 1;
				}

				blockB = blockB | (index << shift);
				shift += 3;
			}

			// Use the more accurate one
			float ea = GetError(tex, BlockDecompressBC4(blockA));
			float eb = GetError(tex, BlockDecompressBC4(blockB));

			return (ea < eb) ? blockA : blockB;
		}

		byte[] BlockDecompressBC4(ulong block)
		{
			float[] red = new float[8];
			red[0] = ((block >> 0) & 0xFF) / 255f;
			red[1] = ((block >> 8) & 0xFF) / 255f;

			if(red[0] > red[1])
			{
				// 6 interpolated color values
				red[2] = (6 * red[0] + 1 * red[1]) / 7.0f; // bit code 010
				red[3] = (5 * red[0] + 2 * red[1]) / 7.0f; // bit code 011
				red[4] = (4 * red[0] + 3 * red[1]) / 7.0f; // bit code 100
				red[5] = (3 * red[0] + 4 * red[1]) / 7.0f; // bit code 101
				red[6] = (2 * red[0] + 5 * red[1]) / 7.0f; // bit code 110
				red[7] = (1 * red[0] + 6 * red[1]) / 7.0f; // bit code 111
			}
			else
			{
				// 4 interpolated color values
				red[2] = (4 * red[0] + 1 * red[1]) / 5.0f; // bit code 010
				red[3] = (3 * red[0] + 2 * red[1]) / 5.0f; // bit code 011
				red[4] = (2 * red[0] + 3 * red[1]) / 5.0f; // bit code 100
				red[5] = (1 * red[0] + 4 * red[1]) / 5.0f; // bit code 101
				red[6] = 0.0f;                     // bit code 110
				red[7] = 1.0f;                     // bit code 111
			}

			int shift = 16;

			byte[] textureData = new byte[16];

			for(int i = 0; i < 16; i++)
			{
				int index = (int)((block >> shift) & 0x7);
				textureData[i] = (byte)(red[index] * 255f);
				shift += 3;
			}

			return textureData;
		}

		float GetError(byte[] original, byte[] compressed)
		{
			float e = 0;

			for(int i = 0; i < 16; i++)
			{
				float o = original[i] / 255f;
				float c = compressed[i] / 255f;
				float d = o - c;
				e += d * d;
			}

			return e;
		}
	}
}
