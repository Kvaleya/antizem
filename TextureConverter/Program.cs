﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;

namespace TextureConverter
{
	class Program
	{
		const string ArgTexBasecolor = "-cm";
		const string ArgTexNormal = "-nm";
		const string ArgTexRoughness = "-rm";
		const string ArgTexMetallic = "-mm";
		const string ArgTexSubsurface = "-ssm";
		const string ArgTexAlpha = "-am";
		const string ArgTexBaseColorAlpha = "-cam";
		const string ArgTexOutput = "-output";
		const string ArgParallelism = "-par";
		
		const string TexFormatBc1 = "BC1_UNORM";
		const string TexFormatBc4 = "BC4_UNORM";
		const string TexFormatBc5 = "BC5_UNORM";
		const string TexFormatBc7 = "BC7_UNORM";

		const string OutputDirectory = "TexconvOutput";
		const string TempFilename = "kvengine_texconverter_temporary_file";

		static void Main(string[] args)
		{
			ParseArgs(args);

			if(args.Length > 0)
				return;

			Console.WriteLine("Note: alpha will not work with .tga source images");
			Console.WriteLine("Example:");
			Console.WriteLine("-cm colormap.jpg -nm normalmap.jpg -rm roughness.jpg -mm metallic.jpg -ssm subsurface.jpg");

			while(true)
			{
				var line = Console.ReadLine().Split(' ');
				ParseArgs(line);
			}
		}

		static void ParseArgs(string[] args)
		{
			string fileBaseColor = null;
			//string outputFile = null;
			string fileNormal = null;
			string fileRoughness = null;
			string fileMetallic = null;
			string fileSubsurface = null;

			string fileAlpha = null;

			int maxParallelism = -1;

			bool baseColorAlpha = false;

			for(int i = 0; i < args.Length; i++)
			{
				string arg = args[i];

				if(string.IsNullOrEmpty(arg))
					continue;

				switch(arg)
				{
					case ArgParallelism:
						{
							i++;
							if(i < args.Length)
							{
								int par;
								if(int.TryParse(args[i], out par))
								{
									if(par == 0)
									{
										par = Environment.ProcessorCount - 1;
									}
									if(par < -1)
									{
										par = -1;
									}
									maxParallelism = par;
								}
							}

							break;
						}
					case ArgTexBasecolor:
						{
							i++;
							if(i < args.Length)
							{
								fileBaseColor = args[i];
							}
							break;
						}
					case ArgTexBaseColorAlpha:
						{
							i++;
							if(i < args.Length)
							{
								baseColorAlpha = true;
								fileBaseColor = args[i];
							}
							break;
						}
					case ArgTexNormal:
						{
							i++;
							if(i < args.Length)
							{
								fileNormal = args[i];
							}
							break;
						}
					case ArgTexRoughness:
						{
							i++;
							if(i < args.Length)
							{
								fileRoughness = args[i];
							}
							break;
						}
					case ArgTexMetallic:
						{
							i++;
							if(i < args.Length)
							{
								fileMetallic = args[i];
							}
							break;
						}
					case ArgTexSubsurface:
						{
							i++;
							if(i < args.Length)
							{
								fileSubsurface = args[i];
							}
							break;
						}
					case ArgTexAlpha:
						{
							i++;
							if(i < args.Length)
							{
								fileAlpha = args[i];
							}
							break;
						}
					//case ArgTexOutput:
					//	{
					//		i++;
					//		if(i < args.Length)
					//		{
					//			outputFile = args[i];
					//		}
					//		break;
					//	}
					default:
						{
							Console.WriteLine("Unknown argument: " + arg);
							break;
						}
				}
			}

			bool hasBasecolor = !string.IsNullOrEmpty(fileBaseColor);
			bool hasNormal = !string.IsNullOrEmpty(fileNormal);
			bool hasMetallic = !string.IsNullOrEmpty(fileMetallic);
			bool hasRoughness = !string.IsNullOrEmpty(fileRoughness);
			bool hasSubsurface = !string.IsNullOrEmpty(fileSubsurface);
			bool hasAlpha = !string.IsNullOrEmpty(fileAlpha);

			string tempDirPath = Path.Combine(Environment.CurrentDirectory, OutputDirectory);
			Directory.CreateDirectory(tempDirPath);

			if(hasBasecolor)
			{
				try
				{
					if(baseColorAlpha)
					{
						var filename = Path.GetFileNameWithoutExtension(fileBaseColor);
						var tex = NormalProcessor.LoadBitmap(fileBaseColor);

						AlphaTestTexGen gen = new AlphaTestTexGen(maxParallelism);

						if(tex == null)
						{
							Console.WriteLine("Error loading alpha file: " + fileAlpha);
							return;
						}

						gen.CorrectBaseColor(tex);

						string tmpFile = Path.Combine(tempDirPath,
							Path.GetFileNameWithoutExtension(fileBaseColor) + "_basecolor.png");

						if(File.Exists(tmpFile))
							File.Delete(tmpFile);

						tex.Bitmap.Save(tmpFile, ImageFormat.Png);

						CreateTexBc1(tmpFile);

						var src = Path.Combine(Environment.CurrentDirectory, Path.GetFileNameWithoutExtension(tmpFile)) +
						          ".DDS";
						var dstBasecolor = Path.Combine(tempDirPath, filename + ".DDS");

						MoveFile(src, dstBasecolor);
					}
					else
					{
						var filename = Path.GetFileNameWithoutExtension(fileBaseColor);

						string tmpFile = Path.Combine(tempDirPath,
							Path.GetFileNameWithoutExtension(fileBaseColor) + "_basecolor" + Path.GetExtension(fileBaseColor));

						if(File.Exists(tmpFile))
							File.Delete(tmpFile);
						File.Copy(Path.Combine(Environment.CurrentDirectory, fileBaseColor), tmpFile);

						CreateTexBc1(tmpFile);

						var src = Path.Combine(Environment.CurrentDirectory, Path.GetFileNameWithoutExtension(tmpFile)) +
								  ".DDS";
						var dst = Path.Combine(tempDirPath, filename + ".DDS");

						MoveFile(src, dst);
					}
				}
				catch(Exception e)
				{
					Console.WriteLine("Error converting file: " + fileBaseColor);
					Console.WriteLine(e.ToString());
				}
			}

			if(hasAlpha)
			{
				var filename = Path.GetFileNameWithoutExtension(fileAlpha);
				var tex = NormalProcessor.LoadBitmap(fileAlpha);

				var dst = Path.Combine(tempDirPath, filename + TextureContainer.Extension);

				AlphaTestTexGen gen = new AlphaTestTexGen(maxParallelism);

				if(tex == null)
				{
					Console.WriteLine("Error loading alpha file: " + fileAlpha);
					return;
				}

				var mips = gen.GenSdfAlphaTexture(tex);

				TextureContainer.SaveToFile(dst, mips, BlockCompression.BC4);
			}

			if(hasNormal || hasRoughness || hasMetallic || hasSubsurface)
			{
				var result = NormalProcessor.Process(fileNormal, fileRoughness, fileMetallic, fileSubsurface, maxParallelism);

				result.Bitmap.Save(Path.Combine(tempDirPath, TempFilename) + ".png", ImageFormat.Png);

				CreateTexBc7(Path.Combine(OutputDirectory, TempFilename + ".png"));

				MoveFile(Path.Combine(Environment.CurrentDirectory, TempFilename) + ".DDS", Path.Combine(tempDirPath, Path.GetFileNameWithoutExtension(fileNormal) + "_nrm.DDS"));
			}

			Console.WriteLine("Done!");
		}

		static void MoveFile(string src, string dst)
		{
			if(File.Exists(src) && File.Exists(dst))
			{
				File.Delete(dst);
			}

			File.Move(src, dst);
		}

		static void CreateTexBc1(string filename)
		{
			RunTexConv("-srgb -f " + TexFormatBc1 + " " + '"' + filename + '"');
		}

		static void CreateTexBc7(string filename)
		{
			RunTexConv("-srgb -sepalpha -f " + TexFormatBc7 + " " + '"' + filename + '"');
		}

		static void RunTexConv(string args)
		{
			Process process = new Process();
			process.StartInfo.FileName = "texconv.exe";
			process.StartInfo.Arguments = "-y -o " + (Path.Combine(Environment.CurrentDirectory)) + " " + args;
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardOutput = true;
			process.OutputDataReceived += (sender, eventArgs) => Console.WriteLine("texconv.exe: {0}", eventArgs.Data);
			process.Start();
			process.BeginOutputReadLine();
			process.WaitForExit(int.MaxValue);
		}
	}
}
