﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TextureConverter
{
	class AlphaTestTexGen
	{
		const float SdfDistanceScale = 1f / 32;

		int _maxParallelism;

		public AlphaTestTexGen(int maxParallelism = -1)
		{
			_maxParallelism = maxParallelism;
		}

		// Based on:
		// Guerrilla Games - GDC 2018 – Between Tech and Art: The Vegetation of Horizon Zero Dawn

		/// <summary>
		/// Will modify the tex DirectBitmap!
		/// </summary>
		/// <param name="tex"></param>
		/// <returns></returns>
		public void CorrectBaseColor(DirectBitmap tex)
		{
			List<DirectBitmap> dbmips = new List<DirectBitmap>();

			dbmips.Add(tex);

			while(dbmips[dbmips.Count - 1].Width > 1 || dbmips[dbmips.Count - 1].Height > 1)
			{
				var prev = dbmips[dbmips.Count - 1];

				var db = new DirectBitmap(Math.Max(prev.Width / 2, 1), Math.Max(prev.Height / 2, 1));

				Parallel.For(0, db.Height, new ParallelOptions()
				{
					MaxDegreeOfParallelism = _maxParallelism,
				}, y =>
				{
					for(int x = 0; x < db.Width; x++)
					{
						var s00 = prev.TexelFetch(x * 2 + 0, y * 2 + 0);
						var s10 = prev.TexelFetch(x * 2 + 1, y * 2 + 0);
						var s01 = prev.TexelFetch(x * 2 + 0, y * 2 + 1);
						var s11 = prev.TexelFetch(x * 2 + 1, y * 2 + 1);

						Vector4 s = Vector4.Zero;

						if(s00.W > 0.999f)
							s += s00;
						if(s10.W > 0.999f)
							s += s10;
						if(s01.W > 0.999f)
							s += s01;
						if(s11.W > 0.999f)
							s += s11;

						if(s.W != 0)
							s = s / s.W;

						db.ImageStore(x, y, s);
					}
				});

				dbmips.Add(db);
			}

			for(int i = dbmips.Count - 2; i >= 0; i--)
			{
				var src = dbmips[i + 1];
				var db = dbmips[i];

				Parallel.For(0, db.Height, new ParallelOptions()
				{
					MaxDegreeOfParallelism = _maxParallelism,
				}, y =>
				{
					for(int x = 0; x < db.Width; x++)
					{
						var here = db.TexelFetch(x, y);

						if(here.W < 1)
						{
							here = src.Sample(new Vector2d((x + 0.5) / db.Width, (y + 0.5) / db.Height));
							db.ImageStore(x, y, here);
						}
					}
				});
			}
		}

		public TextureContainer.Mip[] GenSdfAlphaTexture(DirectBitmap tex)
		{
			TextureContainer.Mip[] texmips;
			float[] sdf;
			GenSdfAlphaTexture(tex, out texmips, out sdf);
			return texmips;
		}

		void  GenSdfAlphaTexture(DirectBitmap tex, out TextureContainer.Mip[] texmips, out float[] sdf)
		{
			const int HistogramSamplingRes = 4096;

			var coverageData = GenCoverageMap(tex);

			int coverage = GetCoverage(coverageData);

			int targetCoverage = (int)(coverage * (double)(HistogramSamplingRes * HistogramSamplingRes) / tex.Width / tex.Height);

			sdf = GenSdf(coverageData, tex.Width, tex.Height);

			Compressor compressor = new Compressor();

			List<TextureContainer.Mip> mips = new List<TextureContainer.Mip>();

			int w = tex.Width;
			int h = tex.Height;
			var mip = sdf;

			//Save(mip, w, h, 0);

			mips.Add(new TextureContainer.Mip()
			{
				WidthTexels = w,
				HeightTexels = h,
				Data = compressor.CompressBC4(FloatTexToByte(mip), w, h, _maxParallelism),
			});

			while(w != 1 || h != 1)
			{
				int prevW = w;
				int prevH = h;

				w = Math.Max(w / 2, 1);
				h = Math.Max(h / 2, 1);

				var mipNext = GenMip(mip, prevW, prevH, w, h);
				Scale(mipNext, FindScaleValue(GetHistogram(mipNext, w, h, HistogramSamplingRes , HistogramSamplingRes), targetCoverage));

				mip = mipNext;

				//Save(mip, w, h, mips.Count);

				mips.Add(new TextureContainer.Mip()
				{
					WidthTexels = w,
					HeightTexels = h,
					Data = compressor.CompressBC4(FloatTexToByte(mip), w, h, _maxParallelism)
				});
			}

			texmips = mips.ToArray();
		}

		void Save(float[] mip, int w, int h, int m)
		{
			var bytes = FloatTexToByte(mip);
			DirectBitmap dbm = new DirectBitmap(w, h);

			for(int i = 0; i < bytes.Length; i++)
			{
				dbm.Bits[i * 4] = bytes[i];
				dbm.Bits[i * 4 + 1] = bytes[i];
				dbm.Bits[i * 4 + 2] = bytes[i];
				dbm.Bits[i * 4 + 3] = 255;
			}

			dbm.ToBitmap().Save("sdfOut" + m + ".png");
		}

		int GetCoverage(byte[] coverage)
		{
			int c = 0;

			for(int i = 0; i < coverage.Length; i++)
			{
				if(coverage[i] > 127)
					c++;
			}

			return c;
		}

		byte[] GenCoverageMap(DirectBitmap tex)
		{
			byte[] coverage = new byte[tex.Width * tex.Height];

			for(int i = 0; i < coverage.Length; i++)
			{
				coverage[i] = (byte)((tex.Bits[i * 4 + 3] < 128) ? 255 : 0);
			}

			return coverage;
		}

		float FindScaleValue(int[] histogram, int coverage)
		{
			int bin = 0;
			int count = 0;

			while(count < coverage && bin < histogram.Length)
			{
				count += histogram[bin];
				bin++;
			}

			return 0.5f / ((bin + 0.5f) / histogram.Length);
		}

		// Computes the histogram of values 0..1 of the texture as if it were bilinearly upsampled to sampleW*sampleH resolution
		int[] GetHistogram(float[] tex, int texW, int texH, int sampleW, int sampleH)
		{
			const int NumBins = 256;

			int[] histogram = new int[NumBins];

			for(int y = 0; y < sampleH; y++)
			{
				for(int x = 0; x < sampleW; x++)
				{
					Vector2d tc = new Vector2d((x + 0.5) / sampleW, (y + 0.5) / sampleH);
					float v = Sample(tex, texW, texH, tc);
					int bin = (int)Math.Min(v * histogram.Length, histogram.Length - 1);
					histogram[bin]++;
				}
			}

			return histogram;
		}

		byte[] FloatTexToByte(float[] tex)
		{
			byte[] data = new byte[tex.Length];

			for(int i = 0; i < tex.Length; i++)
			{
				data[i] = (byte) (tex[i] * 255);
			}

			return data;
		}

		void Scale(float[] tex, float scale)
		{
			for(int i = 0; i < tex.Length; i++)
			{
				tex[i] = Math.Min(Math.Max(tex[i] * scale, 0), 1);
			}
		}

		float[] GenMip(float[] tex, int texW, int texH, int mipW, int mipH)
		{
			float[] mip = new float[mipW * mipH];

			for(int y = 0; y < mipH; y++)
			{
				for(int x = 0; x < mipW; x++)
				{
					float a = tex[(y * 2 + 0) * texW + (x * 2 + 0)];
					float b = a, c = a, d = a;

					int lx, ly;

					lx = (x * 2 + 1);
					ly = (y * 2 + 0);
					if(lx < texW && ly < texH)
						b = tex[ly * texW + lx];

					lx = (x * 2 + 0);
					ly = (y * 2 + 1);
					if(lx < texW && ly < texH)
						c = tex[ly * texW + lx];

					lx = (x * 2 + 1);
					ly = (y * 2 + 1);
					if(lx < texW && ly < texH)
						d = tex[ly * texW + lx];

					mip[y * mipW + x] = (a + b + c + d) * 0.25f;
				}
			}

			return mip;
		}

		float Sample(float[] tex, int w, int h, Vector2d texcoord)
		{
			Vector2d texel = Vector2d.Clamp(texcoord * new Vector2d(w, h) - new Vector2d(0.5f), Vector2d.Zero, new Vector2d(w - 1, h - 1));
			int x = (int)texel.X;
			int y = (int)texel.Y;

			Vector2 fract = new Vector2((float)(texel.X - x), (float)(texel.Y - y));

			Func<int, int, float> texelFetch = (u, v) =>
			{
				u = Math.Min(Math.Max(u, 0), w - 1);
				v = Math.Min(Math.Max(v, 0), h - 1);
				return tex[v * w + u];
			};

			Func<float, float, float, float> lerp = (a, b, mix) =>
			{
				return a * (1 - mix) + b * mix;
			};

			return lerp(lerp(texelFetch(x, y), texelFetch(x + 1, y), fract.X),
				lerp(texelFetch(x, y + 1), texelFetch(x + 1, y + 1), fract.X), fract.Y);
		}

		float[] GenSdf(byte[] coverage, int w, int h)
		{
			const int SearchSize = 128;

			float[] sdf = new float[w * h];

			Parallel.For(0, h, new ParallelOptions()
			{
				MaxDegreeOfParallelism = _maxParallelism,
			}, y =>
			{
				for(int x = 0; x < w; x++)
				{
					float dist0 = SearchSize * 1.5f;
					float dist1 = SearchSize * 1.5f;

					for(int sy = -SearchSize; sy <= SearchSize; sy++)
					{
						for(int sx = -SearchSize; sx <= SearchSize; sx++)
						{
							int lx = x + sx;
							int ly = y + sy;

							if(lx < 0 || lx >= w || ly < 0 || ly >= h)
								continue;

							float dist = (float)Math.Sqrt(sx * sx + sy * sy);

							if(coverage[ly * w + lx] <= 127)
							{
								dist0 = Math.Min(dist0, dist);
							}
							else
							{
								dist1 = Math.Min(dist1, dist);
							}
						}
					}

					float finalD = dist1;

					if(coverage[y * w + x] > 127)
					{
						finalD = -dist0;
					}

					finalD *= SdfDistanceScale;

					finalD = Math.Min(Math.Max(finalD * 0.5f + 0.5f, 0), 1);

					sdf[y * w + x] = finalD;
				}
			});

			return sdf;
		}
	}
}
