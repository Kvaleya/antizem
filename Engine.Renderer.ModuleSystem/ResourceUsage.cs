﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Renderer.ModuleSystem
{
	public enum ResourceRWUsage
	{
		In,
		Out,
		InOut,
	}

	[AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
	public class ResourceUsage : Attribute
	{
		internal readonly ResourceRWUsage Usage;

		public ResourceUsage(ResourceRWUsage usage)
		{
			Usage = usage;
		}
	}
}
