﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Renderer.ModuleSystem
{
	public abstract class RenderModule : IDisposable
	{
		internal bool HasSideEffects;

		public RenderModule(bool hasSideEffects)
		{
			HasSideEffects = hasSideEffects;
		}

		public abstract void Execute();

		public abstract void Dispose();
	}
}
