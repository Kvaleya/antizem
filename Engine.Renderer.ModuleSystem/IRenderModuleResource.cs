﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Renderer.ModuleSystem
{
	public interface IRenderModuleResource : IDisposable
	{
		void Create(int displayWidth, int displayHeight, int renderWidth, int renderHeight);
	}
}
