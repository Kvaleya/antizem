﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common
{
	public static class Utils
	{
		public static ParallelLoopResult ParallelForRange(int fromInclusive, int toExclusive, int threads, Action<int, int> body)
		{
			if(threads < 1)
				threads = Environment.ProcessorCount;

			int range = toExclusive - fromInclusive;
			int stride = range / threads;
			if(range <= 0) threads = 0;
			return Parallel.For(0, threads, i =>
			{
				int start = i * stride;
				int end = (i == threads - 1) ? toExclusive : start + stride;
				body(start, end);
			});
		}

		public static class Meshes
		{
			const char MeshPathNameDivisor = '|';

			public static string MeshFileAndNameCombine(string fileName, string meshName)
			{
				return fileName + MeshPathNameDivisor + meshName;
			}

			public static void NameToPathAndMeshName(string name, out string path, out string meshName)
			{
				path = null;
				meshName = null;

				if(string.IsNullOrEmpty(name))
					return;

				var split = name.Split(MeshPathNameDivisor);

				path = split[0];
				if(split.Length > 1)
					meshName = split[1];
			}
		}

		public static class Pack
		{
			public static ulong PackVec4(Vector4 vec, Vector4 min, Vector4 max)
			{
				ulong u = 0;
				u += (ulong)PackUnorm(vec.W, min.W, max.W) << 48;
				u += (ulong)PackUnorm(vec.Z, min.Z, max.Z) << 32;
				u += (ulong)PackUnorm(vec.Y, min.Y, max.Y) << 16;
				u += (ulong)PackUnorm(vec.X, min.X, max.X) << 0;
				return u;
			}

			public static ulong PackVec3(Vector3 vec, Vector3 min, Vector3 max, ushort extra)
			{
				ulong u = 0;
				u += (ulong)extra << 48;
				u += (ulong)PackUnorm(vec.Z, min.Z, max.Z) << 32;
				u += (ulong)PackUnorm(vec.Y, min.Y, max.Y) << 16;
				u += (ulong)PackUnorm(vec.X, min.X, max.X) << 0;
				return u;
			}

			public static ulong PackVec3(Vector3 vec, Vector3 min, Vector3 max)
			{
				ulong u = 0;
				u += (ulong)PackUnorm(vec.Z, min.Z, max.Z) << 32;
				u += (ulong)PackUnorm(vec.Y, min.Y, max.Y) << 16;
				u += (ulong)PackUnorm(vec.X, min.X, max.X) << 0;
				return u;
			}

			public static uint PackVec2(Vector2 vec, Vector2 min, Vector2 max)
			{
				uint u = 0;
				u += (uint)PackUnorm(vec.Y, min.Y, max.Y) << 16;
				u += (uint)PackUnorm(vec.X, min.X, max.X) << 0;
				return u;
			}

			public static ushort PackUnorm(float v, float min, float max)
			{
				return (ushort)((System.Math.Min(System.Math.Max(v, min), max) - min) / (max - min) * 65535);
			}

			public static ushort PackUnorm(float v)
			{
				return (ushort)(v * 65535);
			}

			public static uint PackNormal(Vector3 n, uint a = 0)
			{
				n.X = System.Math.Min(System.Math.Max(n.X, -1.0f), 1.0f);
				n.Y = System.Math.Min(System.Math.Max(n.Y, -1.0f), 1.0f);
				n.Z = System.Math.Min(System.Math.Max(n.Z, -1.0f), 1.0f);
				//n = n * 0.5f + new Vector3(0.5f);
				//n = new Vector3(1f) - n;
				uint u = 0x00000000;
				uint x = (uint)(System.Math.Abs(n.X) * 0x000001FF) & 0x000001FF;
				uint y = (uint)(System.Math.Abs(n.Y) * 0x000001FF) & 0x000001FF;
				uint z = (uint)(System.Math.Abs(n.Z) * 0x000001FF) & 0x000001FF;
				if(n.X < 0)
					x = (0x000003FF - x);
				if(n.Y < 0)
					y = (0x000003FF - y);
				if(n.Z < 0)
					z = (0x000003FF - z);
				u += (x << 20);
				u += (y << 10);
				u += (z << 00);
				u += ((a & 0x00000003) << 30);
				return u;
			}

			public static Vector3 UnpackNormal(uint u)
			{
				u = u & 0x3FFFFFFF;
				uint z = (u & 0x000003FF);
				uint y = (u & 0x000FFC00) >> 10;
				uint x = (u & 0x3FF00000) >> 20;
				Vector3 n = new Vector3(
					(x & 0x000001FF) / 511f,
					(y & 0x000001FF) / 511f,
					(z & 0x000001FF) / 511f
					);
				if((x & 0x00000200) != 0)
					n.X = -1 + n.X;
				if((y & 0x00000200) != 0)
					n.Y = -1 + n.Y;
				if((z & 0x00000200) != 0)
					n.Z = -1 + n.Z;
				return n;
			}
		}

		public static class Math
		{
			// Snaps pos to a distance from plane equal to integer multiple of alignDistance
			public static Vector3d GridAlign(Vector3d pos, Vector3d planeNormal, double alignDistance)
			{
				double dist = Vector3d.Dot(pos, planeNormal);
				return pos + planeNormal * (System.Math.Floor(dist / alignDistance) * alignDistance - dist);
			}

			public static Vector2 Abs(Vector2 v)
			{
				return new Vector2(System.Math.Abs(v.X), System.Math.Abs(v.Y));
			}

			public static Vector3 Abs(Vector3 v)
			{
				return new Vector3(System.Math.Abs(v.X), System.Math.Abs(v.Y), System.Math.Abs(v.Z));
			}

			public static Vector4 Abs(Vector4 v)
			{
				return new Vector4(System.Math.Abs(v.X), System.Math.Abs(v.Y), System.Math.Abs(v.Z), System.Math.Abs(v.W));
			}

			public static void GetPlanes(Matrix4 projcam, out Vector4 plane0, out Vector4 plane1, out Vector4 plane2, out Vector4 plane3)
			{
				const float w = 1.0f;
				var inverted = projcam.Inverted();
				Vector3[] corners = new Vector3[8];
				for(int i = 0; i < 8; i++)
				{
					Vector3 pos = Vector3.One;

					if((i & 1) == 0)
					{
						pos.X = -pos.X;
					}
					if((i & 2) > 0)
					{
						pos.Y = -pos.Y;
					}
					if((i & 4) == 0)
					{
						pos.Z = -pos.Z;
					}

					Vector4 t = Vector4.Transform(new Vector4(pos, w), inverted);
					corners[i] = t.Xyz / t.W;
				}

				Func<Vector3, Vector3, Vector3, Vector4> getPlane = (a, b, c) =>
				{
					Vector3 normal = Vector3.Cross(b - a, c - a).Normalized();
					return new Vector4(normal, -Vector3.Dot(a, normal));
				};

				plane0 = getPlane(corners[0], corners[4], corners[2]);
				plane1 = getPlane(corners[5], corners[1], corners[7]);
				plane2 = getPlane(corners[2], corners[6], corners[3]);
				plane3 = getPlane(corners[1], corners[5], corners[0]);
			}

			static Matrix4 GetWorldMatrix(Vector3 position, Quaternion rotation, Vector4 scale)
			{
				var wm = QuaternionToMatrix(rotation.Normalized());
				wm.Row0 *= scale;
				wm.Row1 *= scale;
				wm.Row2 *= scale;
				wm.Row3 = new Vector4(position, 1);

				return wm;
			}

			public static Matrix4 GetWorldMatrix(Vector3 position, Quaternion rotation, Vector3 scale)
			{
				return GetWorldMatrix(position, rotation, new Vector4(scale, 1.0f));
			}

			public static Matrix4 GetWorldMatrix(Vector3 position, Quaternion rotation, float scale)
			{
				return GetWorldMatrix(position, rotation, new Vector4(scale, scale, scale, 1.0f));
			}

			public static Matrix4 QuaternionToMatrix(Quaternion q)
			{
				Vector3 p = new Vector3(q.X * q.X, q.Y * q.Y, q.Z * q.Z);
				Matrix4 m = Matrix4.Identity;
				//m.Row0 = new Vector4(1f - 2 * p.Y - 2 * p.Z, 2 * q.X * q.Y - 2 * q.Z * q.W, 2 * q.X * q.Z + 2 * q.Y * q.W, 0.0f);
				//m.Row1 = new Vector4(2 * q.X * q.Y + 2 * q.Z * q.W, 1f - 2 * p.X - 2 * p.Z, 2 * q.Y * q.Z - 2 * q.X * q.W, 0.0f);
				//m.Row2 = new Vector4(2f * q.X * q.Z - 2 * q.Y * q.W, 2 * q.Y * q.Z + 2 * q.X * q.W, 1 - 2 * p.X - 2 * p.Y, 0.0f);
				//m.Transpose();

				m.Row0 = new Vector4(1f - 2 * p.Y - 2 * p.Z, 2 * q.X * q.Y + 2 * q.Z * q.W, 2f * q.X * q.Z - 2 * q.Y * q.W, 0.0f);
				m.Row1 = new Vector4(2 * q.X * q.Y - 2 * q.Z * q.W, 1f - 2 * p.X - 2 * p.Z, 2 * q.Y * q.Z + 2 * q.X * q.W, 0.0f);
				m.Row2 = new Vector4(2 * q.X * q.Z + 2 * q.Y * q.W, 2 * q.Y * q.Z - 2 * q.X * q.W, 1 - 2 * p.X - 2 * p.Y, 0.0f);

				return m;
			}

			public static Matrix4 GetProjectionPerspective(int x, int y, float fov, float near, float far)
			{
				float ar = x / (float)y;
				float fovn = fov * (float)System.Math.PI / 180; // transform fov from degrees to radians
				fovn = (float)(2.0f * System.Math.Atan(System.Math.Tan(fovn * 0.5f) / ar)); // transform from horizontal fov to vertical fov
				float tan = (float)System.Math.Tan(fovn / 2.0f); // tangent of half vertical fov

				Matrix4 proj = Matrix4.Identity;
				proj.Row0 = new Vector4(1f / (ar * tan), 0, 0, 0);
				proj.Row1 = new Vector4(0, 1f / tan, 0, 0);
				proj.Row2 = new Vector4(0, 0, (near + far) / (near - far), -1f);
				proj.Row3 = new Vector4(0, 0, (2f * near * far) / (near - far), 0);

				return proj;
			}

			public static Vector4 BoundingSphereTransformed(Vector4 sphere, Matrix4 m)
			{
				var scale = m.ExtractScale();
				float r = sphere.W * System.Math.Max(System.Math.Abs(scale.X), System.Math.Max(System.Math.Abs(scale.Y), System.Math.Abs(scale.Z)));
				return new Vector4(Vector4.Transform(new Vector4(sphere.Xyz, 1), m).Xyz, r);
			}

			public static float MaxComponent(Vector4 v)
			{
				return System.Math.Max(System.Math.Max(v.X, v.Y), System.Math.Max(v.Z, v.W));
			}

			public static float MinComponent(Vector4 v)
			{
				return System.Math.Min(System.Math.Min(v.X, v.Y), System.Math.Min(v.Z, v.W));
			}

			public static float Clamp(float value, float min = 0f, float max = 1f)
			{
				return System.Math.Min(System.Math.Max(value, min), max);
			}

			public static float Lerp(float a, float b, float mix)
			{
				return a * (1.0f - mix) + b * mix;
			}

			public static float LerpClamped(float a, float b, float mix)
			{
				return Lerp(a, b, Clamp(mix));
			}
		}

		public static unsafe class Unsafe
		{
			// https://stackoverflow.com/questions/20981551/c-sharp-marshal-copy-intptr-to-16-bit-managed-unsigned-integer-array
			public static unsafe void Copy(int[] source, IntPtr ptrDest, int start, uint elements)
			{
				fixed (int* ptrSource = &source[start])
				{
					CisMemcpy(ptrDest, (IntPtr)ptrSource, elements * 8);
				}
			}

			public static int SingleToInt32Bits(float value)
			{
				return *(int*)(&value);
			}
			public static float Int32BitsToSingle(int value)
			{
				return *(float*)(&value);
			}

			/*
			[DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory", SetLastError = false)]
			public static extern void CopyMemory(IntPtr destination, IntPtr source, uint length);

			[DllImport("msvcrt.dll", SetLastError = false)]
			public static extern IntPtr memcpy(IntPtr dest, IntPtr src, int count);
			*/

			public static void CisMemcpy(IntPtr destination, IntPtr source, long count)
			{
				// srcPtr and destPtr are IntPtr's pointing to valid memory locations
				// size is the number of long (normally 4 bytes) to copy
				long* src = (long*)source;
				long* dest = (long*)destination;
				for(long i = 0; i < count / sizeof(long); i++)
				{
					dest[i] = src[i];
				}

				byte* bsrc = (byte*)source;
				byte* bdest = (byte*)destination;
				for(long i = (count / sizeof(long)) * sizeof(long); i < count; i++)
				{
					bdest[i] = bsrc[i];
				}
			}
		}
	}
}
