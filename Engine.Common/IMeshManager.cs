﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common
{
	public interface IMeshManager
	{
		IMesh GetMesh(string name);
		long BytesUsed { get; }
		void Update();
	}
}
