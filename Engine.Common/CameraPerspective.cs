﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common;
using Engine.Common.Rendering;
using OpenTK;

namespace Engine.Common
{
	public class CameraPerspective : ICamera
	{
		public Vector3d Position { get; private set; }
		public Quaternion Rotation { get; private set; }
		public float Fov { get; private set; } // TODO: horizontal or vertical fov?

		public CameraPerspective(Vector3d pos, Quaternion rot, float fov)
		{
			Position = pos;
			Rotation = rot;
			Fov = fov;
		}

		public void Move(Vector3d translation, Quaternion newRotation)
		{
			Position = Position + translation;
			Rotation = newRotation;
		}

		public CameraPerspective Clone()
		{
			return new CameraPerspective(Position, Rotation, Fov);
		}

		public Matrix4 ToCameraMatrix(Vector3 position)
		{
			Matrix4 m = Matrix4.Identity;
			m.Row3 = new Vector4(-position, 1);
			m = m * Utils.Math.QuaternionToMatrix(Rotation.Normalized());

			return m;
		}

		public Matrix4 ToProjectionCamera(int w, int h, float near = 1f, float far = 128f)
		{
			var camera = ToCameraMatrix(Vector3.Zero);
			var proj = Utils.Math.GetProjectionPerspective(w, h, Fov, near, far);
			return camera * proj;
		}
		
		//public static Matrix4 Difference(CameraPerspective origin, CameraPerspective destination)
		//{
		//	var pos = destination.Position.Translated(origin.Position.Negated());
		//	Quaternion rot = origin.Rotation.Inverted() * destination.Rotation;
		//	return Utils.Math.GetWorldMatrix(pos.GetVector(), rot, Vector3.One);
		//}
	}
}
