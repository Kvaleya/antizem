﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common
{
	/*
	public class Transform
	{
		private readonly int _positionBaseX, _positionBaseY, _positionBaseZ;
		private readonly Vector3 _positionOffset;
		public readonly Quaternion Rotation;
		public readonly Vector3 Scale;

		public Transform(Vector3 position, Quaternion rotation, Vector3 scale)
		{
			GetAccuratePosition(position, out _positionOffset, out _positionBaseX, out _positionBaseY, out _positionBaseZ);
			Rotation = rotation;
			Scale = scale;
		}

		public Transform(Vector3 positionOffset, Quaternion rotation, Vector3 scale, int x, int y, int z)
		{
			_positionOffset = positionOffset;
			_positionBaseX = x;
			_positionBaseY = y;
			_positionBaseZ = z;
			Rotation = rotation;
			Scale = scale;
		}

		public Matrix4 GetMatrix(int originX, int originY, int originZ)
		{
			Vector3 pos = _positionOffset;
			pos.X += _positionBaseX - originX;
			pos.Y += _positionBaseY - originY;
			pos.Z += _positionBaseZ - originZ;

			return Utils.Math.GetWorldMatrix(pos, Rotation, Scale);
		}

		public Transform Transformed(Vector3 translate, Quaternion rotate, Vector3 scale)
		{
			int x, y, z;
			Vector3 offset;
			GetAccuratePosition(_positionOffset + translate, out offset, out x, out y, out z);
			x += _positionBaseX;
			y += _positionBaseY;
			z += _positionBaseZ;
			return new Transform(offset, rotate * Rotation, scale * Scale, x, y, z);
		}

		public Transform CameraMove(Vector3 translation, Quaternion newRotation, Vector3 newScale)
		{
			int x, y, z;
			Vector3 offset;
			GetAccuratePosition(_positionOffset + translation, out offset, out x, out y, out z);
			x += _positionBaseX;
			y += _positionBaseY;
			z += _positionBaseZ;
			return new Transform(offset, newRotation, newScale, x, y, z);
		}

		static void GetAccuratePosition(Vector3 pos, out Vector3 offset, out int x, out int y, out int z)
		{
			x = (int) Math.Floor(pos.X);
			y = (int) Math.Floor(pos.Y);
			z = (int) Math.Floor(pos.Z);

			offset = pos;

			offset.X -= x;
			offset.Y -= y;
			offset.Z -= z;
		}

		public int DistanceSquaredInteger(int x, int y, int z)
		{
			x -= _positionBaseX;
			y -= _positionBaseY;
			z -= _positionBaseZ;

			return x * x + y * y + z * z;
		}
	}
	*/
}
