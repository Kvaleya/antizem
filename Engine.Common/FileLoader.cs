﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GameFramework;

namespace Engine.Common
{
	/// <summary>
	/// TODO: Possibly useless (update: yes, very much useless)
	/// </summary>
	public class FileLoader
	{
		IFileManager _fileManager;
		Thread _loaderThread;
		BlockingCollection<LoadRequest> _loadRequests = new BlockingCollection<LoadRequest>(new ConcurrentQueue<LoadRequest>());

		public FileLoader(IFileManager fileManager)
		{
			_fileManager = fileManager;
			_loaderThread = new Thread((() =>
			{
				StartLoaderThread(this);
			}));
			_loaderThread.Name = "Engine.Common.FileLoader";
		}

		public void Start()
		{
			_loaderThread.Start();
		}

		public MemoryStream GetStream(string fileName)
		{
			LoadRequest r = new LoadRequest()
			{
				FileName = fileName,
				WaitEvent = new ManualResetEvent(false),
				Stream = null,
			};

			_loadRequests.Add(r);

			r.WaitEvent.WaitOne();

			return r.Stream;
		}

		static void StartLoaderThread(FileLoader loader)
		{
			while(true)
			{
				foreach(LoadRequest request in loader._loadRequests.GetConsumingEnumerable())
				{
					var stream = loader._fileManager.GetStream(request.FileName);

					if(stream == null)
					{
						request.Stream = null;
						request.WaitEvent.Set();
						continue;
					}

					byte[] bytes = new byte[(int)stream.Length];
					stream.Read(bytes, 0, bytes.Length);

					stream.Dispose();

					request.Stream = new MemoryStream(bytes);
					request.WaitEvent.Set();
				}
			}
		}

		class LoadRequest
		{
			public ManualResetEvent WaitEvent;
			public string FileName;
			public MemoryStream Stream;
		}
	}
}
