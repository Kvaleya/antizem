﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenTK.Platform.Windows;

namespace Engine.Common
{
	public interface IResource
	{
		float TopDistance { get; }
		void SetDistance(Object caller, float d);
		void RemoveDistance(Object caller);
	}

	// A smart reference to a resource such as a mesh, a sound or a texture.
	// Underlying resource may not exist.
	// Scene objects using the resource can input their distance from camera into the resource,
	// which will be used to determine resource importance and handle loading and disposing.
	// Corresponding resource manager should handle resource loading, caching and disposing.
	public class Resource<T> : IResource
		where T : class, IDisposable
	{
		T _item = null;
		Heap<float, Object> _distances = new Heap<float, Object>(-1); // Heap of all scene object distances, keeps track of the minimal distance
		List<Action<Resource<T>>> _onLoad = new List<Action<Resource<T>>>();

		// TODO: Essencial resource support
		public string Name { get; private set; }

		public T Item { get { return _item; } }

		public float TopDistance { get { return _distances.Top.Key; } }

		public void DisposeItem()
		{
			_item?.Dispose();
			_item = null;
		}

		public Resource(string name, T item)
		{
			Name = name;
			_distances.Add(float.PositiveInfinity, this);
			_item = item;
		}

		public void AddOnLoadAction(Action<Resource<T>> action)
		{
			lock(_onLoad)
			{
				// If item is already loaded, execute immediately
				if(_item != null)
				{
					action.Invoke(this);
				}
				else
				{
					_onLoad.Add(action);
				}
			}
		}

		public void Update(T newResource)
		{
			Interlocked.Exchange(ref _item, newResource);

			if(newResource != null)
			{
				lock(_onLoad)
				{
					foreach(var action in _onLoad)
					{
						action.Invoke(this);
					}
					_onLoad.Clear();
				}
			}
		}

		// This should be called from scene objects that use this resource when their LOD is updated
		// Parameter "d" is the distance of the scene object from camera
		// When a scene object that uses this resource is destroyed, RemoveDistance should be called
		public void SetDistance(Object caller, float d)
		{
			if(_distances.Contains(caller))
			{
				_distances.Update(d, caller);
			}
			else
			{
				_distances.Add(d, caller);
			}
		}

		public void RemoveDistance(Object caller)
		{
			_distances.Remove(caller);
		}
	}
}
