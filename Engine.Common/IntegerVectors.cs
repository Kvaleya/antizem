﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common
{
	public struct Vec2i : IEquatable<Vec2i>
	{
		public int X, Y;

		public Vec2i(int x, int y)
		{
			X = x;
			Y = y;
		}

		public Vec2i(int xyz)
			: this(xyz, xyz)
		{

		}

		public Vec2i Abs()
		{
			return new Vec2i(Math.Abs(X), Math.Abs(Y));
		}

		public Vector2 ToVector2()
		{
			return new Vector2(X, Y);
		}

		public static int Dot(Vec2i a, Vec2i b)
		{
			return a.X * b.X + a.Y * b.Y;
		}

		public static Vec2i Clamp(Vec2i value, Vec2i min, Vec2i max)
		{
			return new Vec2i(
				Math.Min(Math.Max(value.X, min.X), max.X),
				Math.Min(Math.Max(value.Y, min.Y), max.Y)
				);
		}

		public int LengthSquared()
		{
			return X * X + Y * Y;
		}

		public int this[int i]
		{
			get
			{
				if(i == 0)
					return X;
				if(i == 1)
					return Y;
				throw new IndexOutOfRangeException();
			}
			set
			{
				if(i < 0 || i >= 2)
					throw new IndexOutOfRangeException();
				if(i == 0)
					X = value;
				if(i == 1)
					Y = value;
			}
		}

		public static Vec2i operator -(Vec2i a)
		{
			return new Vec2i(
				-a.X,
				-a.Y
			);
		}

		public static Vec2i operator +(Vec2i a, Vec2i b)
		{
			return new Vec2i(
				a.X + b.X,
				a.Y + b.Y
			);
		}

		public static Vec2i operator +(Vec2i a, int b)
		{
			return new Vec2i(
				a.X + b,
				a.Y + b
			);
		}

		public static Vec2i operator -(Vec2i a, Vec2i b)
		{
			return new Vec2i(
				a.X - b.X,
				a.Y - b.Y
			);
		}

		public static Vec2i operator -(Vec2i a, int b)
		{
			return new Vec2i(
				a.X - b,
				a.Y - b
			);
		}

		public static Vec2i operator *(Vec2i a, Vec2i b)
		{
			return new Vec2i(
				a.X * b.X,
				a.Y * b.Y
			);
		}

		public static Vec2i operator *(Vec2i a, int b)
		{
			return new Vec2i(
				a.X * b,
				a.Y * b
			);
		}

		public static Vec2i operator /(Vec2i a, Vec2i b)
		{
			return new Vec2i(
				a.X / b.X,
				a.Y / b.Y
			);
		}

		public static Vec2i operator /(Vec2i a, int b)
		{
			return new Vec2i(
				a.X / b,
				a.Y / b
			);
		}

		public override string ToString()
		{
			return "(" + X + ", " + Y + ")";
		}

		public bool Equals(Vec2i other)
		{
			return X == other.X && Y == other.Y;
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			return obj is Vec2i && Equals((Vec2i)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (X * 397) ^ Y;
			}
		}

		public static bool operator ==(Vec2i left, Vec2i right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Vec2i left, Vec2i right)
		{
			return !left.Equals(right);
		}
	}

	public struct Vec3i : IEquatable<Vec3i>
	{
		public int X, Y, Z;

		public Vec2i Xy { get { return new Vec2i(X, Y); } }

		public Vec3i(int x, int y, int z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public Vec3i(int xyz)
			: this(xyz, xyz, xyz)
		{

		}

		public Vec3i Abs()
		{
			return new Vec3i(Math.Abs(X), Math.Abs(Y), Math.Abs(Z));
		}

		public Vector3 ToVector3()
		{
			return new Vector3(X, Y, Z);
		}

		public static int Dot(Vec3i a, Vec3i b)
		{
			return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
		}

		public static Vec3i Clamp(Vec3i value, Vec3i min, Vec3i max)
		{
			return new Vec3i(
				Math.Min(Math.Max(value.X, min.X), max.X),
				Math.Min(Math.Max(value.Y, min.Y), max.Y),
				Math.Min(Math.Max(value.Z, min.Z), max.Z)
				);
		}

		public int LengthSquared()
		{
			return X * X + Y * Y + Z * Z;
		}

		public int this[int i]
		{
			get
			{
				if(i == 0)
					return X;
				if(i == 1)
					return Y;
				if(i == 2)
					return Z;
				throw new IndexOutOfRangeException();
			}
			set
			{
				if(i < 0 || i >= 3)
					throw new IndexOutOfRangeException();
				if(i == 0)
					X = value;
				if(i == 1)
					Y = value;
				if(i == 2)
					Z = value;
			}
		}

		public static Vec3i operator -(Vec3i a)
		{
			return new Vec3i(
				-a.X,
				-a.Y,
				-a.Z
			);
		}

		public static Vec3i operator +(Vec3i a, Vec3i b)
		{
			return new Vec3i(
				a.X + b.X,
				a.Y + b.Y,
				a.Z + b.Z
			);
		}

		public static Vec3i operator +(Vec3i a, int b)
		{
			return new Vec3i(
				a.X + b,
				a.Y + b,
				a.Z + b
			);
		}

		public static Vec3i operator -(Vec3i a, Vec3i b)
		{
			return new Vec3i(
				a.X - b.X,
				a.Y - b.Y,
				a.Z - b.Z
			);
		}

		public static Vec3i operator -(Vec3i a, int b)
		{
			return new Vec3i(
				a.X - b,
				a.Y - b,
				a.Z - b
			);
		}

		public static Vec3i operator *(Vec3i a, Vec3i b)
		{
			return new Vec3i(
				a.X * b.X,
				a.Y * b.Y,
				a.Z * b.Z
			);
		}

		public static Vec3i operator *(Vec3i a, int b)
		{
			return new Vec3i(
				a.X * b,
				a.Y * b,
				a.Z * b
			);
		}

		public static Vec3i operator /(Vec3i a, Vec3i b)
		{
			return new Vec3i(
				a.X / b.X,
				a.Y / b.Y,
				a.Z / b.Z
			);
		}

		public static Vec3i operator /(Vec3i a, int b)
		{
			return new Vec3i(
				a.X / b,
				a.Y / b,
				a.Z / b
			);
		}

		public override string ToString()
		{
			return "(" + X + ", " + Y + ", " + Z + ")";
		}

		public bool Equals(Vec3i other)
		{
			return X == other.X && Y == other.Y && Z == other.Z;
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			return obj is Vec3i && Equals((Vec3i)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = X;
				hashCode = (hashCode * 397) ^ Y;
				hashCode = (hashCode * 397) ^ Z;
				return hashCode;
			}
		}

		public static bool operator ==(Vec3i left, Vec3i right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Vec3i left, Vec3i right)
		{
			return !left.Equals(right);
		}
	}

	public struct Vec4i : IEquatable<Vec4i>
	{
		public int X, Y, Z, W;

		public Vec2i Xy { get { return new Vec2i(X, Y); } }
		public Vec3i Xyz { get { return new Vec3i(X, Y, Z); } }

		public Vec4i(int x, int y, int z, int w)
		{
			X = x;
			Y = y;
			Z = z;
			W = w;
		}

		public Vec4i(int xyz)
			: this(xyz, xyz, xyz, xyz)
		{

		}

		public Vec4i Abs()
		{
			return new Vec4i(Math.Abs(X), Math.Abs(Y), Math.Abs(Z), Math.Abs(W));
		}

		public Vector4 ToVector4()
		{
			return new Vector4(X, Y, Z, W);
		}

		public static int Dot(Vec4i a, Vec4i b)
		{
			return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;
		}

		public static Vec4i Clamp(Vec4i value, Vec4i min, Vec4i max)
		{
			return new Vec4i(
				Math.Min(Math.Max(value.X, min.X), max.X),
				Math.Min(Math.Max(value.Y, min.Y), max.Y),
				Math.Min(Math.Max(value.Z, min.Z), max.Z),
				Math.Min(Math.Max(value.W, min.W), max.W)
				);
		}

		public int LengthSquared()
		{
			return X * X + Y * Y + Z * Z + W * W;
		}

		public int this[int i]
		{
			get
			{
				if(i == 0)
					return X;
				if(i == 1)
					return Y;
				if(i == 2)
					return Z;
				if(i == 3)
					return W;
				throw new IndexOutOfRangeException();
			}
			set
			{
				if(i < 0 || i >= 4)
					throw new IndexOutOfRangeException();
				if(i == 0)
					X = value;
				if(i == 1)
					Y = value;
				if(i == 2)
					Z = value;
				if(i == 3)
					W = value;
			}
		}

		public static Vec4i operator -(Vec4i a)
		{
			return new Vec4i(
				-a.X,
				-a.Y,
				-a.Z,
				-a.W
			);
		}

		public static Vec4i operator +(Vec4i a, Vec4i b)
		{
			return new Vec4i(
				a.X + b.X,
				a.Y + b.Y,
				a.Z + b.Z,
				a.W + b.W
			);
		}

		public static Vec4i operator +(Vec4i a, int b)
		{
			return new Vec4i(
				a.X + b,
				a.Y + b,
				a.Z + b,
				a.W + b
			);
		}

		public static Vec4i operator -(Vec4i a, Vec4i b)
		{
			return new Vec4i(
				a.X - b.X,
				a.Y - b.Y,
				a.Z - b.Z,
				a.W - b.W
			);
		}

		public static Vec4i operator -(Vec4i a, int b)
		{
			return new Vec4i(
				a.X - b,
				a.Y - b,
				a.Z - b,
				a.W - b
			);
		}

		public static Vec4i operator *(Vec4i a, Vec4i b)
		{
			return new Vec4i(
				a.X * b.X,
				a.Y * b.Y,
				a.Z * b.Z,
				a.W * b.W
			);
		}

		public static Vec4i operator *(Vec4i a, int b)
		{
			return new Vec4i(
				a.X * b,
				a.Y * b,
				a.Z * b,
				a.W * b
			);
		}

		public static Vec4i operator /(Vec4i a, Vec4i b)
		{
			return new Vec4i(
				a.X / b.X,
				a.Y / b.Y,
				a.Z / b.Z,
				a.W / b.W
			);
		}

		public static Vec4i operator /(Vec4i a, int b)
		{
			return new Vec4i(
				a.X / b,
				a.Y / b,
				a.Z / b,
				a.W / b
			);
		}

		public override string ToString()
		{
			return "(" + X + ", " + Y + ", " + Z + ", " + W + ")";
		}

		public bool Equals(Vec4i other)
		{
			return X == other.X && Y == other.Y && Z == other.Z && W == other.W;
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			return obj is Vec4i && Equals((Vec4i)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = X;
				hashCode = (hashCode * 397) ^ Y;
				hashCode = (hashCode * 397) ^ Z;
				hashCode = (hashCode * 397) ^ W;
				return hashCode;
			}
		}

		public static bool operator ==(Vec4i left, Vec4i right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Vec4i left, Vec4i right)
		{
			return !left.Equals(right);
		}
	}
}
