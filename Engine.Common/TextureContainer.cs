﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;

namespace Engine.Common
{
	public class TextureContainer : IBcSurface
	{
		const int Magick = 44444444;
		const int FormatVersion = 1;

		public const string Extension = ".kvtex";

		public int Width { get; private set; }
		public int Height { get; private set; }
		public int NumMips { get; private set; }

		public string Filename { get; private set; }

		public BlockCompression Compression { get; private set; }

		public int BlockSize { get { return DirectDrawSurface.GetBlockSize(Compression); } }

		public int GetMipSize(int mip)
		{
			int w = Width;
			int h = Height;

			if(mip >= NumMips)
				throw new ArgumentOutOfRangeException("No such mip  (" + mip + ") exists in this texture (" + NumMips + " mips)");

			for(int i = 0; i < mip; i++)
			{
				w = Math.Max(w / 2, 1);
				h = Math.Max(h / 2, 1);
			}

			int bx = (w + 3) / 4;
			int by = (h + 3) / 4;

			return bx * by * BlockSize;
		}

		public byte[] GetMipData(IFileManager fileManager, int mip)
		{
			if(mip >= NumMips)
				throw new ArgumentOutOfRangeException("No such mip  (" + mip + ") exists in this texture (" + NumMips + " mips)");

			var stream = fileManager.GetStream(Filename);

			if(stream == null)
				return null;

			try
			{
				// Change offset when adding new stuff to the header
				long offset = 24;

				for(int i = 0; i < mip; i++)
				{
					offset += GetMipSize(i);
				}

				using(BinaryReader br = new BinaryReader(stream))
				{
					br.BaseStream.Seek(offset, SeekOrigin.Begin);
					int size = GetMipSize(mip);
					var bytes = br.ReadBytes(size);
					return bytes;
				}
			}
			finally
			{
				stream.Dispose();
			}
		}

		public static TextureContainer LoadFromFile(string filename, IFileManager fileManager)
		{
			var stream = fileManager.GetStream(filename);

			if(stream == null)
				return null;

			TextureContainer tc;

			try
			{
				tc = LoadFromFile(stream);
				tc.Filename = filename;
			}
			finally
			{
				stream.Dispose();
			}

			return tc;
		}

		static TextureContainer LoadFromFile(Stream s)
		{
			TextureContainer tc = new TextureContainer();

			using(BinaryReader br = new BinaryReader(s))
			{
				if(br.ReadInt32() != Magick)
					return null;
				var version = br.ReadInt32();
				tc.Width = br.ReadInt32();
				tc.Height = br.ReadInt32();
				tc.NumMips = br.ReadInt32();
				tc.Compression = (BlockCompression)br.ReadInt32();
				return tc;
			}
		}

		public static void SaveToFile(string filename, Mip[] mips, BlockCompression compression)
		{
			using(BinaryWriter bw = new BinaryWriter(new FileStream(filename, FileMode.Create)))
			{
				// Change getmipdata offset when adding new stuff to the header
				bw.Write(Magick);
				bw.Write(FormatVersion);
				bw.Write(mips[0].WidthTexels);
				bw.Write(mips[0].HeightTexels);
				bw.Write(mips.Length);
				bw.Write((int)compression);
				for(int i = 0; i < mips.Length; i++)
				{
					bw.Write(mips[i].Data);
				}
			}
		}

		public class Mip
		{
			public int WidthTexels;
			public int HeightTexels;

			public byte[] Data;
		}
	}
}
