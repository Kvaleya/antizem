﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common
{
	public interface IMesh
	{
		string FileName { get; }
		string MeshName { get; }

		Vector4 BoundingSphere { get; }

		int MeshID { get; }

		string BatchKey { get; }

		bool IsLoaded { get; }

		// Resource loading
		void SetDistance(Object caller, float d);
		void RemoveDistance(Object caller);
	}
}
