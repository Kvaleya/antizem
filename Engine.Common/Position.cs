﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common
{
	/*
	public struct Position
	{
		public readonly int BaseX, BaseY, BaseZ;
		public readonly Vector3 Offset;

		public Position(Vector3 position)
		{
			GetAccuratePosition(position, out Offset, out BaseX, out BaseY, out BaseZ);
		}

		public Position(Vector3d position)
		{
			GetAccuratePosition(position, out Offset, out BaseX, out BaseY, out BaseZ);
		}

		public Position(int x, int y, int z)
		{
			Offset = Vector3.Zero;
			BaseX = x;
			BaseY = y;
			BaseZ = z;
		}

		public Position(Vector3 offset, int x, int y, int z)
		{
			Offset = offset;
			BaseX = x;
			BaseY = y;
			BaseZ = z;
		}

		public Position Translated(Vector3 translation)
		{
			int x, y, z;
			Vector3 offset;

			GetAccuratePosition(Offset + translation, out offset, out x, out y, out z);
			x += BaseX;
			y += BaseY;
			z += BaseZ;

			return new Position(offset, x, y, z);
		}

		public Position Translated(Position translation)
		{
			int x, y, z;
			Vector3 offset;

			GetAccuratePosition(Offset + translation.Offset, out offset, out x, out y, out z);
			x += BaseX;
			y += BaseY;
			z += BaseZ;

			x += translation.BaseX;
			y += translation.BaseY;
			z += translation.BaseZ;

			return new Position(offset, x, y, z);
		}

		public Position Negated()
		{
			//Vector3 offset;
			//int x, y, z;
			//GetAccuratePosition(Vector3.One - Offset, out offset, out x, out y, out z);
			//return new Position(offset, x - BaseX - 1, y - BaseY - 1, z - BaseZ - 1);

			return new Position(Vector3.One - Offset, -BaseX - 1, -BaseY - 1, -BaseZ - 1);
		}

		public Vector3 GetVector(int originX = 0, int originY = 0, int originZ = 0)
		{
			Vector3 pos = Offset;
			pos.X += BaseX - originX;
			pos.Y += BaseY - originY;
			pos.Z += BaseZ - originZ;
			return pos;
		}

		public int DistanceSquaredInteger(int x, int y, int z)
		{
			x -= BaseX;
			y -= BaseY;
			z -= BaseZ;

			return x * x + y * y + z * z;
		}

		static void GetAccuratePosition(Vector3 pos, out Vector3 offset, out int x, out int y, out int z)
		{
			GetAccuratePosition(new Vector3d(pos.X, pos.Y, pos.Z), out offset, out x, out y, out z);
		}

		static void GetAccuratePosition(Vector3d pos, out Vector3 offset, out int x, out int y, out int z)
		{
			x = (int)Math.Floor(pos.X);
			y = (int)Math.Floor(pos.Y);
			z = (int)Math.Floor(pos.Z);

			pos.X -= x;
			pos.Y -= y;
			pos.Z -= z;

			offset = new Vector3((float)pos.X, (float)pos.Y, (float)pos.Z);
		}

		string NumberToString(int i, float f)
		{
			string sf = f.ToString("0.00000", CultureInfo.InvariantCulture).Split('.')[1];
			return i.ToString() + '.' + sf;
		}

		public override string ToString()
		{
			return NumberToString(BaseX, Offset.X) + " " + NumberToString(BaseY, Offset.Y) + " " + NumberToString(BaseZ, Offset.Z);
		}
	}
	*/
}
