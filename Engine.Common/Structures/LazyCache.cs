﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common
{
	public class LazyCache<TKey, TValue>
	{
		ConcurrentDictionary<TKey, Lazy<TValue>> _items;
		Func<TKey, TValue> _factory;

		public LazyCache(Func<TKey, TValue> valueFactory)
		{
			_items = new ConcurrentDictionary<TKey, Lazy<TValue>>();
			_factory = valueFactory;
		}

		public TValue GetItem(TKey key)
		{
			Lazy<TValue> lazy = _items.GetOrAdd(key, new Lazy<TValue>(() => _factory(key)));
			return lazy.Value;
		}

		public TValue AddOrGet(TKey key, Func<TValue> factory)
		{
			return _items.GetOrAdd(key, key1 => new Lazy<TValue>(factory)).Value;
		}
	}
}
