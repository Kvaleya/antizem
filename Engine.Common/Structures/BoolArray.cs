﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common.Structures
{
	public class BoolArray
	{
		readonly int _length;

		uint[] _data;

		public bool this[int index]
		{
			get
			{
				if(index < 0 || index >= _length)
					throw new IndexOutOfRangeException();
				int arrayIndex = index / 32;
				int uintIndex = index % 32;
				uint mask = 1u << uintIndex;
				return (_data[arrayIndex] & mask) > 0;
			}
			set
			{
				if(index < 0 || index >= _length)
					throw new IndexOutOfRangeException();
				int arrayIndex = index / 32;
				int uintIndex = index % 32;
				uint mask = 1u << uintIndex;

				if(value)
				{
					_data[arrayIndex] = _data[arrayIndex] | mask;
				}
				else
				{
					_data[arrayIndex] = _data[arrayIndex] & (~mask);
				}
			}
		}

		public BoolArray(int length, bool initialValue = false)
		{
			_length = length;
			_data = new uint[(length + 31) / 32];
			Clear(initialValue);
		}

		public void Clear(bool value)
		{
			uint d = 0;

			if(value)
				d = ~d;

			for(int i = 0; i < _data.Length; i++)
			{
				_data[i] = d;
			}
		}
	}
}
