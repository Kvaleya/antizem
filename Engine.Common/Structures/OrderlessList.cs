﻿using System;
using System.Collections.Generic;
using System.Deployment.Internal;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common
{
	interface IOrderlessList
	{
		void RemoveAt(IntWrapper index);
	}

	public class IntWrapper
	{
		public int Value;
		IOrderlessList _list;

		internal IntWrapper(IOrderlessList list, int value)
		{
			_list = list;
			Value = value;
		}

		public void RemoveFromList()
		{
			_list.RemoveAt(this);
		}
	}

	public class OrderlessList<T> : IOrderlessList
	{
		List<T> _list;
		List<IntWrapper> _indices;
		Stack<IntWrapper> _freeIndices;

		public List<T> List { get { return _list; } } 

		public OrderlessList()
		{
			_list = new List<T>();
			_indices = new List<IntWrapper>();
			_freeIndices = new Stack<IntWrapper>();
		}

		public IntWrapper Add(T item)
		{
			IntWrapper index;
			if(_freeIndices.Count > 0)
			{
				index = _freeIndices.Pop();
				_list[index.Value] = item;
				_indices[index.Value] = index;
			}
			else
			{
				index = new IntWrapper(this, _list.Count);
				_list.Add(item);
				_indices.Add(index);
			}
			return index;
		}

		public void RemoveAt(IntWrapper index)
		{
			_freeIndices.Push(index);
		}

		public void Defrag()
		{
			while(_freeIndices.Count > 0)
			{
				var index = _freeIndices.Pop();

				if(index.Value == _list.Count - 1)
				{
					_list.RemoveAt(_list.Count - 1);
					_indices.RemoveAt(_indices.Count - 1);
					return;
				}

				var tempItem = _list[_list.Count - 1];
				var tempIndex = _indices[_indices.Count - 1];
				_list.RemoveAt(_list.Count - 1);
				_indices.RemoveAt(_list.Count - 1);
				_list[index.Value] = tempItem;
				_indices[index.Value] = tempIndex;
				tempIndex.Value = index.Value;
			}
		}

		public void Clear()
		{
			foreach(IntWrapper index in _indices)
			{
				index.Value = -1;
			}
			_indices.Clear();
			_list.Clear();
		}
	}
}
