﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Engine.Common
{
	/// <summary>
	/// A generic heap collection - keeps track of the greatest item.</summary>
	/// <typeparam name="T">
	/// Item type.</typeparam>
	public class Heap<K, V>
		where K : IComparable<K>
	{
		// The heap array
		readonly List<KeyValuePair<K, V>> _data = new List<KeyValuePair<K, V>>();

		// Maps each item to its index in the heap array (for fast removal)
		readonly Dictionary<V, int> _index = new Dictionary<V, int>();

		readonly int ComparatorNegate = 1;

		public List<KeyValuePair<K, V>> Data { get { return _data; } }

		// -1 for minheap, 1 for maxheap
		public Heap(int comp)
		{
			ComparatorNegate = comp;
		}
		
		/// <summary>
		/// Gets the current greatest item in the heap, in sense defined by CompareTo() method of the item's class.</summary>
		public KeyValuePair<K, V> Top
		{
			get
			{
				if(_data.Count == 0)
				{
					throw new InvalidOperationException("The heap is empty.");
				}

				return _data[0];
			}
		}

		void HeapInsert(K key, V value)
		{
			_data.Add(new KeyValuePair<K, V>(key, value));

			int index = _data.Count - 1;

			_index[value] = index;
			BubbleUp(index);
		}

		// Removes item at the given index (0 for the top item)
		void HeapRemove(int index)
		{
			int lastIndex = _data.Count - 1;

			if(index == lastIndex)
			{
				// Simple enough if deleteing the last item
				_index.Remove(_data[index].Value);
				_data.RemoveAt(index);
			}
			else
			{
				// We swap the item with the last...
				Swap(index, lastIndex);

				// ...which is the one that gets removed
				_index.Remove(_data[lastIndex].Value);
				_data.RemoveAt(lastIndex);

				BubbleUp(index);
				BubbleDown(index);
			}
		}

		void BubbleUp(int index)
		{
			int childIndex = index;
			int parentIndex = ParentIndex(childIndex);

			while(childIndex > 0 && IsGreaterThan(childIndex, parentIndex))
			{
				Swap(childIndex, parentIndex);
				childIndex = parentIndex;
				parentIndex = ParentIndex(childIndex);
			}
		}

		void BubbleDown(int index)
		{
			int parentIndex = index;

			while(HasChild(parentIndex))
			{
				int maximalChildIndex = MaximalChildIndex(parentIndex);

				if(IsGreaterThan(maximalChildIndex, parentIndex))
				{
					Swap(maximalChildIndex, parentIndex);
				}
				else
				{
					return;
				}
			}
		}

		void Swap(int indexA, int indexB)
		{
			if(indexA == indexB)
			{
				return;
			}

			var itemA = _data[indexA];
			var itemB = _data[indexB];

			_data[indexA] = itemB;
			_data[indexB] = itemA;

			_index[itemA.Value] = indexB;
			_index[itemB.Value] = indexA;
		}

		// Returns index of the maximal child of the parent.
		// There must be at least one child!
		int MaximalChildIndex(int parentIndex)
		{
			Debug.Assert(HasChild(parentIndex));

			int child1Index = ChildIndex(parentIndex);
			int child2Index = child1Index + 1;

			if(child2Index < _data.Count)
			{
				// Parent has both children, we take the greater one
				return Maximum(child1Index, child2Index);
			}
			else
			{
				// Only one child to choose from here
				return child1Index;
			}
		}

		int Maximum(int indexA, int indexB)
		{
			return IsGreaterThan(indexA, indexB) ? indexA : indexB;
		}

		bool IsGreaterThan(int indexA, int indexB)
		{
			return (_data[indexA].Key.CompareTo(_data[indexB].Key) * ComparatorNegate) > 0;
		}

		bool HasChild(int parentIndex)
		{
			return ChildIndex(parentIndex) < _data.Count;
		}

		static int ParentIndex(int childIndex)
		{
			return (childIndex - 1) / 2;
		}

		static int ChildIndex(int parentIndex)
		{
			return (parentIndex * 2) + 1;
		}

		public void Update(K newKey, V value)
		{
			int i = _index[value];
			_data[i] = new KeyValuePair<K, V>(newKey, value);
			BubbleUp(i);
			BubbleDown(i);
		}

		public void Add(K key, V value)
		{
			HeapInsert(key, value);
		}

		public void Clear()
		{
			_data.Clear();
			_index.Clear();
		}

		public bool Contains(V value)
		{
			return _index.ContainsKey(value);
		}

		public bool Remove(V value)
		{
			if(_index.ContainsKey(value))
			{
				HeapRemove(_index[value]);
				return true;
			}
			else
			{
				return false;
			}
		}

		public void RemoveTop()
		{
			if(Count < 1)
				return;
			HeapRemove(0);
		}

		public int Count
		{
			get { return _data.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}
	}

}
