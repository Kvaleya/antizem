﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common
{
	public class HashedOrderlessList<T>
	{
		public List<T> List { get { return _list; } }
		List<T> _list = new List<T>();
		Dictionary<T, int> _indices = new Dictionary<T, int>();

		public void Add(T item)
		{
			int index = _list.Count;
			_list.Add(item);
			_indices.Add(item, index);
		}

		public void Remove(T item)
		{
			int index = _indices[item];
			_indices.Remove(item);
			T temp = _list[_list.Count - 1];
			_list.RemoveAt(_list.Count - 1);

			if(index == _list.Count)
				return; // Removing the last element - do not sawp

			_list[index] = temp;
			_indices[temp] = index;
		}

		public bool Contains(T item)
		{
			return _indices.ContainsKey(item);
		}

		public void Clear()
		{
			_list.Clear();
			_indices.Clear();
		}
	}
}
