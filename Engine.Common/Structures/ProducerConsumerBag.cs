﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Engine.Common
{
	public class ProducerConsumerBag<T>
	{
		ConcurrentBag<T> _front = new ConcurrentBag<T>();
		ConcurrentBag<T> _back = new ConcurrentBag<T>();

		public ProducerConsumerBag()
		{
			
		}

		public void Add(T item)
		{
			_front.Add(item);
		}

		public void TryCollectAndClear(ICollection<T> collection)
		{
			if(_front.IsEmpty)
				return;

			_back = Interlocked.Exchange(ref _front, _back);

			while(!_back.IsEmpty)
			{
				T item;
				if(_back.TryTake(out item))
					collection.Add(item);
			}
		}
	}
}
