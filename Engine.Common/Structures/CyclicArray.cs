﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common.Structures
{
	public class CyclicArray<T>
	{
		T[] _array;
		int _shift = 0;

		public int Shift
		{
			get { return _shift; }
			set { _shift = value % _array.Length; }
		}

		public int Length { get { return _array.Length; } }

		public T this[int index]
		{
			get { return _array[GetIndex(index)]; }
			set { _array[GetIndex(index)] = value; }
		}

		public CyclicArray(int length)
		{
			_array = new T[length];
		}

		public CyclicArray(int length, Func<int, T> init)
		{
			_array = new T[length];

			for(int i = 0; i < length; i++)
			{
				_array[i] = init(i);
			}
		}

		int GetIndex(int i)
		{
			return Mod(i + _shift, _array.Length);
		}

		static int Mod(int x, int m)
		{
			int r = x % m;
			return r < 0 ? r + m : r;
		}
	}
}
