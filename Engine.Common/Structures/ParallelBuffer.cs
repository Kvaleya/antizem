﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace Engine.Common
{
	public class ParallelBuffer<T>
	{
		int _count;
		T[] _array;
		bool _allowOverflow;

		public int Count { get { return _count; } }
		public T[] Array { get { return _array; } }

		public T this[int index]
		{
			get { return _array[index]; }
		}

		public ParallelBuffer(int capacity, bool allowOverflow = false)
		{
			_array = new T[capacity];
			_allowOverflow = allowOverflow;
			_count = 0;
		}

		// Discard new items on overflow
		public void Add(T item)
		{
			int index = Interlocked.Increment(ref _count) - 1;
			if(index < _array.Length)
				_array[index] = item;
			else
			{
				if(!_allowOverflow)
					throw new OverflowException("Item array is full!");
			}
		}

		public void AddRange(T[] items)
		{
			int index = Interlocked.Add(ref _count, items.Length) - items.Length;

			for(int i = 0; i < items.Length; i++)
			{
				if(index >= _array.Length)
				{
					if(!_allowOverflow)
						throw new OverflowException("Item array is full!");
					break;
				}

				_array[index] = items[i];

				index++;
			}
		}

		public List<T> ToList()
		{
			return _array.ToList().GetRange(0, _count);
		}

		public void Reset()
		{
			Interlocked.Exchange(ref _count, 0);
		}
	}
}
