﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common.Structures
{
	public class ListSegment<T> : IReadOnlyList<T>
	{
		public int Count { get { return _length; } }

		int _first;
		int _length;
		IList<T> _base;

		public T this[int index]
		{
			get
			{
				if(index < 0 || index >= _length)
					throw new IndexOutOfRangeException();
				return _base[_first + index];
			}
			set
			{
				if(index < 0 || index >= _length)
					throw new IndexOutOfRangeException();
				_base[_first + index] = value;
			}
		}

		public ListSegment(IList<T> baseList, int first, int length)
		{
			_base = baseList;
			_first = first;
			_length = length;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<T> GetEnumerator()
		{
			for(int i = 0; i < _length; i++)
			{
				yield return this[i];
			}
		}
	}
}
