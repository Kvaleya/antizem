﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common.Rendering
{
	public class MeshInstanceData : IEquatable<MeshInstanceData>
	{
		// vec3: rotation xyz
		// ushort: material id
		// ushort: flags, rotation w sign (1 == negative)
		// vec3: translation xyz
		// int: meshId

		public readonly Matrix4x3 Transform;

		public readonly IMesh Mesh;
		public readonly int MaterialId;
		public readonly int Flags;

		public MeshInstanceData(Matrix4 transform, IMesh mesh, int materialID, int flags)
		{
			Transform = new Matrix4x3(transform.Row0.Xyz, transform.Row1.Xyz, transform.Row2.Xyz, transform.Row3.Xyz);
			Mesh = mesh;
			MaterialId = materialID;
			Flags = flags;
		}

		//public MeshInstanceData(Matrix4 transform, int offsetX, int offsetY, int offsetZ, IMesh mesh, int materialID, int extra2)
		//{
		//	Transform = new Matrix4x3(transform.Row0.Xyz, transform.Row1.Xyz, transform.Row2.Xyz, transform.Row3.Xyz);
		//	Transform.Row3.X += offsetX;
		//	Transform.Row3.Y += offsetY;
		//	Transform.Row3.Z += offsetZ;
		//	Mesh = mesh;
		//	MaterialId = materialID;
		//	Extra02 = extra2;
		//}

		public Matrix4 Transform4x4
		{
			get
			{
				Matrix4 m = Matrix4.Identity;
				m.Column0 = Transform.Column0;
				m.Column1 = Transform.Column1;
				m.Column2 = Transform.Column2;
				return m;
			}
		}

		public void MarshalTo(IntPtr ptr, int special)
		{
			float[] matrix = new float[16];
			int i = 0;
			for(int c = 0; c < 3; c++)
			{
				for(int r = 0; r < 4; r++)
				{
					matrix[i++] = Transform[r, c];
				}
			}

			matrix[i++] = Utils.Unsafe.Int32BitsToSingle(Mesh.MeshID);
			matrix[i++] = Utils.Unsafe.Int32BitsToSingle(MaterialId);
			matrix[i++] = Utils.Unsafe.Int32BitsToSingle(Flags);
			matrix[i++] = Utils.Unsafe.Int32BitsToSingle(special);

			Marshal.Copy(matrix, 0, ptr, matrix.Length);
		}

		public bool Equals(MeshInstanceData other)
		{
			if(ReferenceEquals(null, other)) return false;
			if(ReferenceEquals(this, other)) return true;
			return Transform.Equals(other.Transform) && Mesh == other.Mesh && MaterialId == other.MaterialId && Flags == other.Flags;
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			if(ReferenceEquals(this, obj)) return true;
			if(obj.GetType() != this.GetType()) return false;
			return Equals((MeshInstanceData)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = Transform.GetHashCode();
				hashCode = (hashCode * 397) ^ Mesh.GetHashCode();
				hashCode = (hashCode * 397) ^ MaterialId;
				hashCode = (hashCode * 397) ^ Flags;
				return hashCode;
			}
		}

		public static bool operator ==(MeshInstanceData left, MeshInstanceData right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(MeshInstanceData left, MeshInstanceData right)
		{
			return !Equals(left, right);
		}
	}
}
