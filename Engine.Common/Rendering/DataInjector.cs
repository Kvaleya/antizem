﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Engine.Common.Rendering
{
	// TODO: move these elsewhere?

	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
	public class InjectProperty : Attribute
	{
	}

	public class DataInjector
	{
		ConcurrentBag<object> _dataPrepare = new ConcurrentBag<object>();
		Dictionary<Type, object> _dataInject = new Dictionary<Type, object>();
		List<object> _modules = new List<object>();

		Action _injectAction; 
		//ManualResetEvent _sync = new ManualResetEvent(true);

		/// <summary>
		/// Not thread safe, but should not be ever called by multiple threads anyway.
		/// </summary>
		/// <param name="module"></param>
		public void AddModule(object module)
		{
			_modules.Add(module);
		}

		/// <summary>
		/// Thread safe
		/// </summary>
		/// <param name="data"></param>
		public void AddData(object data)
		{
			_dataPrepare.Add(data);
		}

		/// <summary>
		/// Only call this after all systems have already added their data - not thread safe!
		/// </summary>
		public void Swap()
		{
			_dataInject.Clear();
			var bag = _dataPrepare.ToArray();
			_dataPrepare = new ConcurrentBag<object>();

			foreach(object o in bag)
			{
				_dataInject.Add(o.GetType(), o);
			}
		}

		public void Inject()
		{
			_injectAction?.Invoke();
		}

		public void CompileInjector()
		{
			if(_modules.Count == 0)
				return;

			List<Expression> injectExpressions = new List<Expression>();

			MethodInfo getDataMethod = typeof(DataInjector).GetMethod("GetData");

			foreach(var module in _modules)
			{
				var type = module.GetType();
				var properties = type.GetProperties();

				foreach(var property in properties)
				{
					var attribute = property.GetCustomAttribute<InjectProperty>();
					if(attribute != null)
					{
						var setMethod = property.GetSetMethod();
						var propertyType = property.PropertyType;

						var expression = Expression.Call(Expression.Constant(module), setMethod,
							Expression.Convert(Expression.Call(Expression.Constant(this), getDataMethod, Expression.Constant(propertyType)),
								propertyType));
						injectExpressions.Add(expression);

						//Action a = () =>
						//{
						//	property.SetValue(module, GetData(propertyType));
						//};
					}
				}
			}

			if(injectExpressions.Count == 0)
				return;

			BlockExpression block = Expression.Block(injectExpressions.ToArray());

			_injectAction = Expression.Lambda<Action>(block).Compile();
		}

		public object GetData(Type type)
		{
			if(_dataInject.ContainsKey(type))
				return _dataInject[type];
			return null;
		}
	}
}
