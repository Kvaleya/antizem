﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common.Rendering
{
	public enum TimerQueryKind
	{
		GFX,
		OpenGL,
		CPU,
	}

	public interface ITimerQuery
	{
		string Name { get; }
		double DisplayEllapsed { get; }
		TimerQueryKind Kind { get; }
	}
}
