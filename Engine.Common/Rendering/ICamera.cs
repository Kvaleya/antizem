﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common.Rendering
{
	public interface ICamera
	{
		Vector3d Position { get; }
	}
}
