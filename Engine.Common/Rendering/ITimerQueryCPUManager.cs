﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common.Rendering
{
	public interface ITimerQueryCPUManager
	{
		void AddQueryCPU(ITimerQuery query);
	}
}
