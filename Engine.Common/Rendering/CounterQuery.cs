﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common.Rendering
{
	public class CounterQuery : ITimerQuery
	{
		public string Name { get; private set; }

		long _number;

		public double DisplayEllapsed
		{
			get { return _number; }
		}

		public TimerQueryKind Kind { get; set; }

		public CounterQuery(ITimerQueryCPUManager manager, string name)
		{
			Name = name;
			Kind=  TimerQueryKind.CPU;
			manager.AddQueryCPU(this);
		}

		public void Add(long number)
		{
			_number += number;
		}
	}
}
