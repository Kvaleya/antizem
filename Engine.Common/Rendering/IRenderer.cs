﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;

namespace Engine.Common.Rendering
{
	public interface IRenderer
	{
		void Inicialize(DataInjector dataInjector, object meshBinaryResourceManager, object materialManager, out IMeshManager meshManager);

		FrameOutput RenderFrame();

		void Resize(int displayWidth, int displayHeight, int renderWidth, int renderHeight);
	}
}
