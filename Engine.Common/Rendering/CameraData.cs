﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common.Rendering
{
	public class CameraData
	{
		public readonly CameraPerspective CameraCurrent;
		public readonly CameraPerspective CameraHistory;
		//public Vector3 Ray00, Ray01, Ray11, Ray10;

		public CameraData(CameraPerspective current, CameraPerspective history)
		{
			CameraCurrent = current.Clone();
			CameraHistory = history.Clone();
		}
	}
}
