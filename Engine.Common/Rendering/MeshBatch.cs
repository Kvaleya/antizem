﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Common.Structures;

namespace Engine.Common.Rendering
{
	/// <summary>
	/// Readonly collection of mesh instances of meshes that use the same batch key
	/// </summary>
	public class MeshBatch
	{
		public readonly string BatchKey;
		public readonly IReadOnlyList<MeshInstanceData> Instances;

		public MeshBatch(string batchKey, IReadOnlyList<MeshInstanceData> instances)
		{
			BatchKey = batchKey;
			Instances = instances;
		}

		/// <summary>
		/// Returns list of MeshBatches generated from input instances. Meshes are batched primarily by format, secondarly by mesh itself
		/// </summary>
		/// <param name="instances">Instances to batch</param>
		/// <returns></returns>
		public static List<MeshBatch> GetBatchedInstances(List<MeshInstanceData> instances)
		{
			// TODO: support shadow and alpha-tested shadow batching
			Dictionary<IMesh, List<MeshInstanceData>> instancesByMesh = new Dictionary<IMesh, List<MeshInstanceData>>();
			Dictionary<string, List<MeshInstanceData>> instancesByBatch = new Dictionary<string, List<MeshInstanceData>>();
			HashSet<IMesh> meshes = new HashSet<IMesh>();

			foreach(MeshInstanceData instance in instances)
			{
				if(!instancesByMesh.ContainsKey(instance.Mesh))
				{
					instancesByMesh.Add(instance.Mesh, new List<MeshInstanceData>());
				}
				instancesByMesh[instance.Mesh].Add(instance);

				if(!meshes.Contains(instance.Mesh))
					meshes.Add(instance.Mesh);
			}

			foreach(var mesh in meshes)
			{
				if(!instancesByBatch.ContainsKey(mesh.BatchKey))
				{
					instancesByBatch.Add(mesh.BatchKey, new List<MeshInstanceData>());
				}
				instancesByBatch[mesh.BatchKey].AddRange(instancesByMesh[mesh]);
			}

			List<MeshBatch> batches = new List<MeshBatch>();

			foreach(var pair in instancesByBatch)
			{
				batches.Add(new MeshBatch(pair.Key, pair.Value));
			}

			return batches;
		}
	}
}
