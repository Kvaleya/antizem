﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Platform.Windows;

namespace Engine.Common.Rendering
{
	public class TimerQueryCPU : ITimerQuery
	{
		public string Name { get; private set; }
		public double DisplayEllapsed { get; private set; }
		public TimerQueryKind Kind { get; set; }
		Stopwatch _stopwatch;

		public TimerQueryCPU(ITimerQueryCPUManager manager, string name)
		{
			Kind = TimerQueryKind.CPU;
			Name = name;
			_stopwatch = new Stopwatch();
			manager.AddQueryCPU(this);
		}

		public void StartQuery()
		{
			_stopwatch.Start();
		}

		public void EndQuery()
		{
			_stopwatch.Stop();
			DisplayEllapsed = _stopwatch.ElapsedTicks / (double)Stopwatch.Frequency;
		}
	}
}
