﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common.Rendering
{
	public class TestSceneData
	{
		public CameraData Camera;
		//public List<MeshInstanceData> AllInstances;
		public List<MeshBatch> Batches;
		public List<MeshBatch> BatchesAlphaTest;
		public bool VisualizeCulling = false;
		public bool VisualizeVirtualTexture = false;
		public bool ComputeCulling = false;
		public bool EnableTonemap = true;
		public bool EnableUI = true;
		public bool EnableAA = true;
		public Vector3 SunDir;
		public Vector3 SunLight;
		public Vector3 SunLight2km;
		public Vector3 SunLight4km;
		public Vector3 SunLight8km;
		public Vector3 SunLight16km;
		public Vector3 SunLight32km;
		public Vector3 SunLight64km;
		public Vector3 SunLight128km;
		public float Exposure;
		public double Ellapsed;
		public double TimeDelta;
	}
}
