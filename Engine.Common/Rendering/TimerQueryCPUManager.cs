﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common.Rendering
{
	public class TimerQueryCPUManager : ITimerQueryCPUManager
	{
		ParallelBuffer<ITimerQuery> _queries = new ParallelBuffer<ITimerQuery>(10000);

		public void AddQueryCPU(ITimerQuery query)
		{
			_queries.Add(query);
		}

		public List<ITimerQuery> GetQueries()
		{
			return _queries.Array.ToList().GetRange(0, _queries.Count);
		}
	}
}
