﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common.Rendering
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct VertexUI
	{
		public const int MaxVertices = 1 << 17;
		public const int Size = 16;

		public ushort X, Y, Z, W;
		public ushort TexCoordX, TexCoordY;
		public uint Color;

		public VertexUI(ushort x, ushort y, ushort z, ushort w, ushort texCoordX, ushort texCoordY, uint color)
		{
			X = x;
			Y = y;
			Z = z;
			W = w;
			TexCoordX = texCoordX;
			TexCoordY = texCoordY;
			Color = color;
		}

		public VertexUI(Vector4 position, Vector2 texCoord, Vector4 color)
		{
			X = PackToUnormLargerRange(position.X);
			Y = PackToUnormLargerRange(position.Y);
			Z = PackToUnormLargerRange(position.Z);
			W = PackToUnormLargerRange(position.W);
			TexCoordX = PackToUnorm(texCoord.X);
			TexCoordY = PackToUnorm(texCoord.Y);
			Color = PackColorToUint(color);
		}

		/// <summary>
		/// Pack values from -0.5 to 1.5
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		static ushort PackToUnormLargerRange(float value)
		{
			value *= 0.5f;
			value += 0.25f;
			return PackToUnorm(value);
		}

		static ushort PackToUnorm(float value)
		{
			return (ushort)(Math.Min(Math.Max(value, 0.0f), 1.0f) * 65536);
		}

		static byte FloatColorToByte(float value)
		{
			return (byte)(value * 255.0);
		}

		public static uint PackColorToUint(Vector4 color)
		{
			return PackColorToUint(FloatColorToByte(color.W), FloatColorToByte(color.Z), FloatColorToByte(color.Y),
				FloatColorToByte(color.X));
		}

		public static uint PackColorToUint(int r, int g, int b, int a)
		{
			uint result = 0x0;
			result = result | (((uint)r & 0xFF) << 24);
			result = result | (((uint)g & 0xFF) << 16);
			result = result | (((uint)b & 0xFF) << 8);
			result = result | (((uint)a & 0xFF) << 0);
			return result;
		}
	}
}
