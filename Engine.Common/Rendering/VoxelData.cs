﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common.Rendering
{
	public class VoxelData
	{
		public Vec3i CubeIncludeCenter;
		public int CubeIncludeSize;
		public Vec3i CubeExcludeCenter;
		public int CubeExcludeSize;
		public List<MeshBatch> Batches;
		public int ClipmapLevel = 0;
		public int DebugClipmapLevel = 0;
	}
}
