﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Common.Rendering
{
	public class UIFrameData
	{
		public VertexUI[] Vertices;
		public int VertexCount;
	}
}
