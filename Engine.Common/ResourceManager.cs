﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Engine.Common
{
	public abstract class ResourceManager<T>
		where T : class, IDisposable
	{
		const float LoadedDistanceScale = 0.9f;

		// Only create a resource instance for each name once
		protected LazyCache<string, Resource<T>> _createdResources;
		// Max heap, top resource is the most distant
		protected Heap<float, Resource<T>> _loadedResources = new Heap<float, Resource<T>>(1);
		// Min heap, top resource is the nearest
		protected Heap<float, Resource<T>> _unloadedResources = new Heap<float, Resource<T>>(-1);
		// Resources that have finished loading and can be marked as not being loaded
		private List<Resource<T>> _resourcesLoadFinished = new List<Resource<T>>();
		// Replaces IsBeingLoaded property of resources
		private Dictionary<Resource<T>, bool> _isBeingLoaded = new Dictionary<Resource<T>, bool>();
		// Memory used by each resource
		private Dictionary<Resource<T>, long> _resourceSizes = new Dictionary<Resource<T>, long>();

		Object _updateSync = new Object();

		public long MemoryBytesBudget { get; private set; }
		public long MemoryBytesUsed { get; private set; }

		protected ResourceManager(long memoryBudgetBytes)
		{
			MemoryBytesBudget = memoryBudgetBytes;
			MemoryBytesUsed = 0;

			_createdResources = new LazyCache<string, Resource<T>>(name =>
			{
				// Inicialize resource
				var resource = new Resource<T>(name, null);
				
				lock(_updateSync)
				{
					_isBeingLoaded.Add(resource, false);
					_unloadedResources.Add(resource.TopDistance, resource);
					_resourceSizes.Add(resource, 0);
				}
				
				GetResourceSize(resource);

				return resource;
			});
		}

		// Main method for getting a resource
		public Resource<T> GetResource(string name)
		{
			return _createdResources.GetItem(name);
		}

		// Loads and unloads resources, should be called reasonably often (every few frames)
		public virtual void Update()
		{
			lock(_updateSync)
			{
				// Update heaps with new distances
				foreach(var resource in _loadedResources.Data.ToArray())
				{
					float current = resource.Key;
					float updated = resource.Value.TopDistance;

					if(!current.Equals(updated))
					{
						_loadedResources.Update(updated, resource.Value);
					}
				}
				foreach(var resource in _unloadedResources.Data.ToArray())
				{
					float current = resource.Key;
					float updated = resource.Value.TopDistance;

					if(!current.Equals(updated))
					{
						_unloadedResources.Update(updated, resource.Value);
					}
				}

				HashSet<Resource<T>> toLoad = new HashSet<Resource<T>>();

				// Unload all resources whose distance is infinite (thus no scene object is using them)
				while(_loadedResources.Count > 0 && float.IsPositiveInfinity(_loadedResources.Top.Key))
				{
					DestoryTopResource();
				}

				// Try to load all resources based on distance
				while(_unloadedResources.Count > 0 &&
				      (_loadedResources.Count == 0 || _unloadedResources.Top.Key <= _loadedResources.Top.Key * LoadedDistanceScale))
				{
					var resource = _unloadedResources.Top;
					MemoryBytesUsed += _resourceSizes[resource.Value];
					_unloadedResources.RemoveTop();
					_loadedResources.Add(resource.Key, resource.Value);
					toLoad.Add(resource.Value);
				}
				// Then try to fill up the memory budget
				while(_unloadedResources.Count > 0 &&
				      (_loadedResources.Count == 0 || MemoryBytesUsed < MemoryBytesBudget) && !float.IsPositiveInfinity(_unloadedResources.Top.Key))
				{
					var resource = _unloadedResources.Top;

					if(MemoryBytesUsed + _resourceSizes[resource.Value] > MemoryBytesBudget)
						break;

					MemoryBytesUsed += _resourceSizes[resource.Value];
					_unloadedResources.RemoveTop();
					_loadedResources.Add(resource.Key, resource.Value);
					toLoad.Add(resource.Value);
				}

				// Finally delete the most distant resources until the memory budget is met
				while(_loadedResources.Count > 0 && MemoryBytesUsed > MemoryBytesBudget)
				{
					if(toLoad.Contains(_loadedResources.Top.Value))
						toLoad.Remove(_loadedResources.Top.Value);
					DestoryTopResource();
				}

				// Mark all loaded resources as not being loaded, discard newly loaded resource if no longer needed
				lock(_resourcesLoadFinished)
				{
					foreach(var resource in _resourcesLoadFinished)
					{
						_isBeingLoaded[resource] = false;
						if(!_loadedResources.Contains(resource))
						{
							resource.DisposeItem();
						}
					}
					_resourcesLoadFinished.Clear();
				}

				// Load all resources that are now in the loaded heap
				foreach(var resource in toLoad)
				{
					if(!_isBeingLoaded[resource])
					{
						_isBeingLoaded[resource] = true;
						LoadResource(resource);
						//Console.WriteLine("Loading resource " + resource.Name);
					}
				}
			}
		}

		// Unloads the top resource from the loaded heap
		// Removes it from the loaded heap, calls DestroyResource and adds it to the unloaded heap
		void DestoryTopResource()
		{
			var resource = _loadedResources.Top;
			DestroyResource(resource.Value);
			_loadedResources.RemoveTop();
			_unloadedResources.Add(resource.Key, resource.Value);
			MemoryBytesUsed -= _resourceSizes[resource.Value];
		}

		// Called when a resource is being unloaded
		protected virtual void DestroyResource(Resource<T> resource)
		{
			resource.DisposeItem();
		}

		protected void OnResourceSizeKnown(Resource<T> resource, long size)
		{
			lock (_updateSync)
			{
				long difference = size - _resourceSizes[resource];
				_resourceSizes[resource] = size;

				if(_loadedResources.Contains(resource))
				{
					MemoryBytesUsed += difference;
				}
			}
		}

		// Loads the resource, implementation should not take a long time to return (usually just dispatch a task that does the heavy work)
		// Do not forget to call OnResourceLoadFinished
		protected abstract void LoadResource(Resource<T> resource);

		// Gets the size of the resource in memory without actually loading it (ideally). Used in loading logic. Should not block the main thread
		// Should call OnResourceSizeKnown once the size is known
		protected abstract void GetResourceSize(Resource<T> resource);

		// The resource will be marked as not being loaded during the next heap update
		protected void OnResourceLoadFinished(Resource<T> resource)
		{
			lock(_updateSync)
			{
				_resourcesLoadFinished.Add(resource);
			}
		}
	}
}
