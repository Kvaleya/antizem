﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Engine.Common
{
	public class WorkerThread
	{
		Thread _thread;
		ConcurrentQueue<Action> _tasks;

		AutoResetEvent _sync = new AutoResetEvent(false);

		public WorkerThread(string name)
		{
			_tasks = new ConcurrentQueue<Action>();
			_thread = new Thread(Loop);
			_thread.Name = name;
			_thread.Start();
		}

		public void Enqueue(Action a)
		{
			_tasks.Enqueue(a);
			_sync.Set();
		}

		void Loop()
		{
			while(true)
			{
				Action a;

				if(_tasks.TryDequeue(out a))
				{
					a.Invoke();
				}
				else
				{
					_sync.WaitOne();
				}
			}
		}
	}
}
