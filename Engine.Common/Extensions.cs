﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Engine.Common
{
	public static class Extensions
	{
		public static Vector3d Double(this Vector3 v)
		{
			return new Vector3d(v.X, v.Y, v.Z);
		}
	}
}
