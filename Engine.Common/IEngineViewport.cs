﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using GameFramework;

namespace Engine.Common
{
	public interface IEngineViewport : IViewport
	{
		bool IsFocused { get; }

		FrameOutput FrameOutput { get; }

		int DisplayWidth { get; }
		int DisplayHeight { get; }

		int ResShift { get; set; }

		void StartMainLoop(ManualResetEvent onFramePrepared, ManualResetEvent onFrameDone, IRenderer renderer, bool debugFrameLimit);

		bool MouseLocked { get; set; }

		void SwitchFullscreenWindowed();
	}
}
