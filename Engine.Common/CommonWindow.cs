﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Engine.Common.Rendering;
using OpenTK;
using OpenTK.Graphics;

namespace Engine.Common
{
	public class CommonWindow : GameWindow, IEngineViewport
	{
		IRenderer _renderer;
		ManualResetEvent _onFramePrepared, _onFrameDone;

		public FrameOutput FrameOutput { get; private set; }

		// Simple FPS calc
		Stopwatch _sw = new Stopwatch();
		int _frames = 0;

		string _title;

		int _windowedResX = 1280;
		int _windowedResY = 720;
		int _resShift = 0;
		bool _switchFullScreenWindowed = false;
		bool _resize = false;
		Object _sync = new Object();

		public int ResShift { get { return _resShift; }
			set
			{
				_resShift = value;
				if(_resShift > 1)
				{
					_resShift = 1;
				}
				if(_resShift < -6)
				{
					_resShift = -6;
				}
				lock(_sync)
				{
					_resize = true;
				}
			}
		}

		public bool IsFocused { get { return this.Focused; } }

		public bool MouseLocked { get; set; } = false;

		public int DisplayWidth { get { return this.Width; } }
		public int DisplayHeight { get { return this.Height; } }

		public CommonWindow(int width, int height, bool debug = false)
			: base(width, height, GraphicsMode.Default, "Test", GameWindowFlags.Default, DisplayDevice.Default, -1, -1,
				  (debug ? (GraphicsContextFlags.Debug | GraphicsContextFlags.ForwardCompatible) : GraphicsContextFlags.Default))
		{
			_sw.Start();
			this.VSync = VSyncMode.Off;
			_title = this.Title;
			this.CursorVisible = true;
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			bool cursorVisible = !MouseLocked;

			if(this.CursorVisible != cursorVisible)
			{
				this.CursorVisible = cursorVisible;
			}

			// resize
			if(_resize)
			{
				lock(_sync)
				{
					_resize = false;
				}
				OnResize(null);
			}

			// fullscreen/window switch
			if(_switchFullScreenWindowed)
			{
				lock(_sync)
				{
					if(this.WindowState == WindowState.Fullscreen)
					{
						SwitchToWindowed();
					}
					else
					{
						SwitchToFullscreen();
					}
					_switchFullScreenWindowed = false;
				}
			}

			_onFramePrepared.WaitOne();
			_onFramePrepared.Reset();

			FrameOutput = _renderer.RenderFrame();

			_onFrameDone.Set();
			SwapBuffers();
			_frames++;
			if(_sw.ElapsedMilliseconds >= 1000)
			{
				this.Title = _title + " FPS: " + _frames;
				_frames = 0;
				_sw.Restart();
			}
		}

		int MultiplyByPowerOfTwo(int number, int power)
		{
			if(power > 0)
			{
				return number << power;
			}
			if(power < 0)
			{
				return number >> (-power);
			}
			return number;
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			_renderer?.Resize(Width, Height, MultiplyByPowerOfTwo(Width, _resShift), MultiplyByPowerOfTwo(Height, _resShift)); // TODO: proper render resolution handling
		}

		public Tuple<int, int> TopLeftPoint
		{
			get
			{
				Point point = this.PointToScreen(new System.Drawing.Point(0, 0));
				return new Tuple<int, int>(point.X, point.Y);
			}
		}

		public void StartMainLoop(ManualResetEvent onFramePrepared, ManualResetEvent onFrameDone, IRenderer renderer, bool debugFrameLimit)
		{
			_onFramePrepared = onFramePrepared;
			_onFrameDone = onFrameDone;
			_renderer = renderer;
			if(debugFrameLimit)
			{
				const int fps = 15;
				VSync = VSyncMode.On;
				Run(fps, fps);
			}
			else
			{
				Run();
			}
		}

		public void SwitchFullscreenWindowed()
		{
			lock(_sync)
			{
				_switchFullScreenWindowed = true;
			}
		}

		public void SwitchToFullscreen()
		{
			Rectangle resolution = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
			SwitchToFullscreen(resolution.Width, resolution.Height);
		}

		public void SwitchToFullscreen(int w, int h)
		{
			_windowedResX = Width;
			_windowedResY = Height;

			//this.WindowBorder = WindowBorder.Hidden;

			Width = w;
			Height = h;

			// Go fullscreen
			this.WindowState = WindowState.Fullscreen;
		}

		public void SwitchToWindowed()
		{
			this.WindowBorder = WindowBorder.Resizable;

			// Go windowed
			this.WindowState = WindowState.Normal;

			Width = _windowedResX;
			Height = _windowedResY;
		}
	}
}
