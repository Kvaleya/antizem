﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;

namespace Engine.Common
{
	public class DirectDrawSurface : IBcSurface
	{
		BlockCompression _compression;
		string _filename;
		int[] _mipDataStarts;
		int[] _mipDataLengths;
		int _width;
		int _height;

		public int Width { get { return _width; } }
		public int Height { get { return _height; } }
		public int NumMips { get { return _mipDataLengths.Length; } }
		public BlockCompression Compression { get { return _compression; } }
		public int BlockSize => GetBlockSize(_compression);

		public string Filename { get { return _filename; } }

		internal DirectDrawSurface(BlockCompression compression, string filename, int[] mipDataStarts, int[] mipDataLengths, int width, int height)
		{
			_compression = compression;
			_filename = filename;
			_mipDataStarts = mipDataStarts;
			_mipDataLengths = mipDataLengths;
			_width = width;
			_height = height;
		}

		public byte[] GetMipData(IFileManager fileManager, int mip)
		{
			if(mip >= _mipDataStarts.Length)
				throw new IndexOutOfRangeException("Requested mip does not exist!");
			using(var stream = fileManager.GetStream(_filename))
			{
				if(stream == null)
					return null;
				byte[] data = new byte[_mipDataLengths[mip]];
				stream.Seek(_mipDataStarts[mip], SeekOrigin.Begin);
				stream.Read(data, 0, data.Length);
				return data;
			}
		}

		public int GetMipSize(int mip)
		{
			return _mipDataLengths[mip];
		}

		public static int GetBlockSize(BlockCompression compression)
		{
			switch(compression)
			{
				case BlockCompression.BC1:
					{
						return 8;
					}
				case BlockCompression.BC3:
					{
						return 16;
					}
				case BlockCompression.BC4:
					{
						return 8;
					}
				case BlockCompression.BC5:
					{
						return 16;
					}
				case BlockCompression.BC6H:
					{
						return 16;
					}
				case BlockCompression.BC7:
					{
						return 16;
					}
				default:
				{
					return 16;
				}
			}
		}
	}

	public enum BlockCompression
	{
		Unknown = 0,
		BC1 = 1,
		BC3 = 2,
		BC4 = 3,
		BC5 = 4,
		BC6H = 5,
		BC7 = 6,
	}

	enum PixelFormatFourCC
	{
		DXT1,
		DXT2,
		DXT3,
		DXT4,
		DXT5,
		DX10,
	}

	public class BcSurfaceLoader
	{
		const int Magic = 0x20534444;

		public static IBcSurface Load(string filename, IFileManager fileManager)
		{
			string ext = Path.GetExtension(filename);
			if(ext == TextureContainer.Extension)
			{
				return TextureContainer.LoadFromFile(filename, fileManager);
			}
			else
			{
				return LoadDds(filename, fileManager);
			}
		}

		public static DirectDrawSurface LoadDds(string filename, IFileManager fileManager)
		{
			using(var stream = fileManager.GetStream(filename))
			{
				if(stream == null)
					return null;

				using(var br = new BinaryReader(stream))
				{
					return LoadDds(br, filename);
				}
			}
		}

		static DirectDrawSurface LoadDds(BinaryReader br, string filename)
		{
			int magic = br.ReadInt32();
			if(magic != Magic)
			{
				throw new Exception("Wrong file format!");
			}
			DDSHeader header = new DDSHeader(br);
			DDSHeaderDXT10 headerDXT10 = new DDSHeaderDXT10();
			bool hasDXT10 = header.PixelFormat.HasDX10Header;
			if(hasDXT10)
			{
				headerDXT10 = new DDSHeaderDXT10(br);
			}

			int numMips = 1;
			if(header.Flags.HasFlag(DDSHeaderFlags.DDSD_MIPMAPCOUNT))
			{
				numMips = header.MipMapCount;
			}

			BlockCompression compression = BlockCompression.Unknown;

			if(hasDXT10)
			{
				var f = headerDXT10.DXGIFormat;

				if(f == DXGI_FORMAT.DXGI_FORMAT_BC7_TYPELESS || f == DXGI_FORMAT.DXGI_FORMAT_BC7_UNORM || f == DXGI_FORMAT.DXGI_FORMAT_BC7_UNORM_SRGB)
					compression = BlockCompression.BC7;
				if(f == DXGI_FORMAT.DXGI_FORMAT_BC4_SNORM || f == DXGI_FORMAT.DXGI_FORMAT_BC4_TYPELESS || f == DXGI_FORMAT.DXGI_FORMAT_BC4_UNORM)
					compression = BlockCompression.BC4;
				if(f == DXGI_FORMAT.DXGI_FORMAT_BC5_SNORM || f == DXGI_FORMAT.DXGI_FORMAT_BC5_UNORM || f == DXGI_FORMAT.DXGI_FORMAT_BC5_TYPELESS)
					compression = BlockCompression.BC5;
				if(f == DXGI_FORMAT.DXGI_FORMAT_BC6H_SF16 || f == DXGI_FORMAT.DXGI_FORMAT_BC6H_UF16 || f == DXGI_FORMAT.DXGI_FORMAT_BC6H_TYPELESS)
					compression = BlockCompression.BC6H;
			}
			else
			{
				switch(header.PixelFormat.FourCC)
				{
					case PixelFormatFourCC.DXT1:
					{
						compression = BlockCompression.BC1;
						break;
					}
					case PixelFormatFourCC.DXT5:
					{
						compression = BlockCompression.BC3;
						break;
					}
				}
			}

			int bytesPerBlock = DirectDrawSurface.GetBlockSize(compression);

			List<int> mipSizes = new List<int>();
			List<int> mipStarts = new List<int>();

			int mipWidth = header.Width;
			int mipHeight = header.Height;
			int mipStart = (hasDXT10 ? 148 : 128);

			for(int i = 0; i < numMips; i++)
			{
				int blocksX = (mipWidth + 3) / 4;
				int blocksY = (mipHeight + 3) / 4;
				int size = blocksX * blocksY * bytesPerBlock;

				mipStarts.Add(mipStart);
				mipSizes.Add(size);

				mipStart += size;
				mipWidth = Math.Max(1, mipWidth / 2);
				mipHeight = Math.Max(1, mipHeight / 2);
			}

			return new DirectDrawSurface(compression, filename, mipStarts.ToArray(), mipSizes.ToArray(), header.Width, header.Height);
		}
	}

	struct DDSHeader
	{
		public int Size;
		public DDSHeaderFlags Flags;
		public int Height;
		public int Width;
		public int PitchOrLinearSize;
		public int Depth;
		public int MipMapCount;
		public DDSPixelFormat PixelFormat;
		public DDSCaps Caps;
		public DDSCaps2 Caps2;
		public int Caps3;
		public int Caps4;

		public DDSHeader(BinaryReader br)
		{
			Size = br.ReadInt32();
			Flags = (DDSHeaderFlags)br.ReadInt32();
			Height = br.ReadInt32();
			Width = br.ReadInt32();
			PitchOrLinearSize = br.ReadInt32();
			Depth = br.ReadInt32();
			MipMapCount = br.ReadInt32();
			br.ReadBytes(11 * 4); // Reserved bytes
			PixelFormat = new DDSPixelFormat(br);
			Caps = (DDSCaps)br.ReadInt32();
			Caps2 = (DDSCaps2)br.ReadInt32();
			Caps3 = br.ReadInt32();
			Caps4 = br.ReadInt32();
			br.ReadInt32(); // Reserved2
		}
	}

	struct DDSPixelFormat
	{
		public int Size;
		public DDSPixelFormatFlags Flags;
		public PixelFormatFourCC FourCC;
		public int RGBBitCount;
		public int RBitMask;
		public int GBitMask;
		public int BBitMask;
		public int ABitMask;

		public bool HasDX10Header
		{
			get { return FourCC == PixelFormatFourCC.DX10; }
		}

		public DDSPixelFormat(BinaryReader br)
		{
			Size = br.ReadInt32();
			Flags = (DDSPixelFormatFlags)br.ReadInt32();

			int fourCC = br.ReadInt32();

			FourCC = PixelFormatFourCC.DXT1;

			if(fourCC == Convert4("DXT1"))
				FourCC = PixelFormatFourCC.DXT1;
			if(fourCC == Convert4("DXT2"))
				FourCC = PixelFormatFourCC.DXT2;
			if(fourCC == Convert4("DXT3"))
				FourCC = PixelFormatFourCC.DXT3;
			if(fourCC == Convert4("DXT4"))
				FourCC = PixelFormatFourCC.DXT4;
			if(fourCC == Convert4("DXT5"))
				FourCC = PixelFormatFourCC.DXT5;
			if(fourCC == Convert4("DX10"))
				FourCC = PixelFormatFourCC.DX10;

			RGBBitCount = br.ReadInt32();
			RBitMask = br.ReadInt32();
			GBitMask = br.ReadInt32();
			BBitMask = br.ReadInt32();
			ABitMask = br.ReadInt32();
		}

		// https://stackoverflow.com/questions/3858908/convert-a-4-char-string-into-int32
		static int Convert4(string key)
		{
			return (key[3] << 24) + (key[2] << 16) + (key[1] << 8) + key[0];
		}
	};

	struct DDSHeaderDXT10
	{
		public DXGI_FORMAT DXGIFormat;
		public D3D10_RESOURCE_DIMENSION ResourceDimension;
		public DDS_RESOURCE_MISC MiscFlag;
		public int ArraySize;
		public DDS_RESOURCE_MISC2 MiscFlags2;

		public DDSHeaderDXT10(BinaryReader br)
		{
			DXGIFormat = (DXGI_FORMAT)br.ReadInt32();
			ResourceDimension = (D3D10_RESOURCE_DIMENSION)br.ReadInt32();
			MiscFlag = (DDS_RESOURCE_MISC)br.ReadInt32();
			ArraySize = br.ReadInt32();
			MiscFlags2 = (DDS_RESOURCE_MISC2)br.ReadInt32();
		}
	}

	[Flags]
	enum DDSHeaderFlags
	{
		DDSD_CAPS = 0x1,
		DDSD_HEIGHT = 0x2,
		DDSD_WIDTH = 0x4,
		DDSD_PITCH = 0x8,
		DDSD_PIXELFORMAT = 0x1000,
		DDSD_MIPMAPCOUNT = 0x20000,
		DDSD_LINEARSIZE = 0x80000,
		DDSD_DEPTH = 0x800000,
	}

	[Flags]
	enum DDSPixelFormatFlags
	{
		DDPF_ALPHAPIXELS = 0x1,
		DDPF_ALPHA = 0x2,
		DDPF_FOURCC = 0x4,
		DDPF_RGB = 0x40,
		DDPF_YUV = 0x200,
		DDPF_LUMINANCE = 0x20000,
	}

	[Flags]
	enum DDSCaps
	{
		DDSCAPS_COMPLEX = 0x8,
		DDSCAPS_MIPMAP = 0x400000,
		DDSCAPS_TEXTURE = 0x1000,
	}

	[Flags]
	enum DDSCaps2
	{
		DDSCAPS2_CUBEMAP = 0x200,
		DDSCAPS2_CUBEMAP_POSITIVEX = 0x400,
		DDSCAPS2_CUBEMAP_NEGATIVEX = 0x800,
		DDSCAPS2_CUBEMAP_POSITIVEY = 0x1000,
		DDSCAPS2_CUBEMAP_NEGATIVEY = 0x2000,
		DDSCAPS2_CUBEMAP_POSITIVEZ = 0x4000,
		DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x8000,
		DDSCAPS2_VOLUME = 0x200000,
	}

	enum D3D10_RESOURCE_DIMENSION
	{
		D3D10_RESOURCE_DIMENSION_UNKNOWN = 0,
		D3D10_RESOURCE_DIMENSION_BUFFER = 1,
		D3D10_RESOURCE_DIMENSION_TEXTURE1D = 2,
		D3D10_RESOURCE_DIMENSION_TEXTURE2D = 3,
		D3D10_RESOURCE_DIMENSION_TEXTURE3D = 4,
	}

	[Flags]
	enum DDS_RESOURCE_MISC
	{
		DDS_RESOURCE_MISC_TEXTURECUBE = 0x4,
	}

	[Flags]
	enum DDS_RESOURCE_MISC2
	{
		DDS_ALPHA_MODE_UNKNOWN = 0x0,
		DDS_ALPHA_MODE_STRAIGHT = 0x1,
		DDS_ALPHA_MODE_PREMULTIPLIED = 0x2,
		DDS_ALPHA_MODE_OPAQUE = 0x3,
		DDS_ALPHA_MODE_CUSTOM = 0x4,
	}

	enum DXGI_FORMAT
	{
		DXGI_FORMAT_UNKNOWN,
		DXGI_FORMAT_R32G32B32A32_TYPELESS,
		DXGI_FORMAT_R32G32B32A32_FLOAT,
		DXGI_FORMAT_R32G32B32A32_UINT,
		DXGI_FORMAT_R32G32B32A32_SINT,
		DXGI_FORMAT_R32G32B32_TYPELESS,
		DXGI_FORMAT_R32G32B32_FLOAT,
		DXGI_FORMAT_R32G32B32_UINT,
		DXGI_FORMAT_R32G32B32_SINT,
		DXGI_FORMAT_R16G16B16A16_TYPELESS,
		DXGI_FORMAT_R16G16B16A16_FLOAT,
		DXGI_FORMAT_R16G16B16A16_UNORM,
		DXGI_FORMAT_R16G16B16A16_UINT,
		DXGI_FORMAT_R16G16B16A16_SNORM,
		DXGI_FORMAT_R16G16B16A16_SINT,
		DXGI_FORMAT_R32G32_TYPELESS,
		DXGI_FORMAT_R32G32_FLOAT,
		DXGI_FORMAT_R32G32_UINT,
		DXGI_FORMAT_R32G32_SINT,
		DXGI_FORMAT_R32G8X24_TYPELESS,
		DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
		DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,
		DXGI_FORMAT_X32_TYPELESS_G8X24_UINT,
		DXGI_FORMAT_R10G10B10A2_TYPELESS,
		DXGI_FORMAT_R10G10B10A2_UNORM,
		DXGI_FORMAT_R10G10B10A2_UINT,
		DXGI_FORMAT_R11G11B10_FLOAT,
		DXGI_FORMAT_R8G8B8A8_TYPELESS,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,
		DXGI_FORMAT_R8G8B8A8_UINT,
		DXGI_FORMAT_R8G8B8A8_SNORM,
		DXGI_FORMAT_R8G8B8A8_SINT,
		DXGI_FORMAT_R16G16_TYPELESS,
		DXGI_FORMAT_R16G16_FLOAT,
		DXGI_FORMAT_R16G16_UNORM,
		DXGI_FORMAT_R16G16_UINT,
		DXGI_FORMAT_R16G16_SNORM,
		DXGI_FORMAT_R16G16_SINT,
		DXGI_FORMAT_R32_TYPELESS,
		DXGI_FORMAT_D32_FLOAT,
		DXGI_FORMAT_R32_FLOAT,
		DXGI_FORMAT_R32_UINT,
		DXGI_FORMAT_R32_SINT,
		DXGI_FORMAT_R24G8_TYPELESS,
		DXGI_FORMAT_D24_UNORM_S8_UINT,
		DXGI_FORMAT_R24_UNORM_X8_TYPELESS,
		DXGI_FORMAT_X24_TYPELESS_G8_UINT,
		DXGI_FORMAT_R8G8_TYPELESS,
		DXGI_FORMAT_R8G8_UNORM,
		DXGI_FORMAT_R8G8_UINT,
		DXGI_FORMAT_R8G8_SNORM,
		DXGI_FORMAT_R8G8_SINT,
		DXGI_FORMAT_R16_TYPELESS,
		DXGI_FORMAT_R16_FLOAT,
		DXGI_FORMAT_D16_UNORM,
		DXGI_FORMAT_R16_UNORM,
		DXGI_FORMAT_R16_UINT,
		DXGI_FORMAT_R16_SNORM,
		DXGI_FORMAT_R16_SINT,
		DXGI_FORMAT_R8_TYPELESS,
		DXGI_FORMAT_R8_UNORM,
		DXGI_FORMAT_R8_UINT,
		DXGI_FORMAT_R8_SNORM,
		DXGI_FORMAT_R8_SINT,
		DXGI_FORMAT_A8_UNORM,
		DXGI_FORMAT_R1_UNORM,
		DXGI_FORMAT_R9G9B9E5_SHAREDEXP,
		DXGI_FORMAT_R8G8_B8G8_UNORM,
		DXGI_FORMAT_G8R8_G8B8_UNORM,
		DXGI_FORMAT_BC1_TYPELESS,
		DXGI_FORMAT_BC1_UNORM,
		DXGI_FORMAT_BC1_UNORM_SRGB,
		DXGI_FORMAT_BC2_TYPELESS,
		DXGI_FORMAT_BC2_UNORM,
		DXGI_FORMAT_BC2_UNORM_SRGB,
		DXGI_FORMAT_BC3_TYPELESS,
		DXGI_FORMAT_BC3_UNORM,
		DXGI_FORMAT_BC3_UNORM_SRGB,
		DXGI_FORMAT_BC4_TYPELESS,
		DXGI_FORMAT_BC4_UNORM,
		DXGI_FORMAT_BC4_SNORM,
		DXGI_FORMAT_BC5_TYPELESS,
		DXGI_FORMAT_BC5_UNORM,
		DXGI_FORMAT_BC5_SNORM,
		DXGI_FORMAT_B5G6R5_UNORM,
		DXGI_FORMAT_B5G5R5A1_UNORM,
		DXGI_FORMAT_B8G8R8A8_UNORM,
		DXGI_FORMAT_B8G8R8X8_UNORM,
		DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM,
		DXGI_FORMAT_B8G8R8A8_TYPELESS,
		DXGI_FORMAT_B8G8R8A8_UNORM_SRGB,
		DXGI_FORMAT_B8G8R8X8_TYPELESS,
		DXGI_FORMAT_B8G8R8X8_UNORM_SRGB,
		DXGI_FORMAT_BC6H_TYPELESS,
		DXGI_FORMAT_BC6H_UF16,
		DXGI_FORMAT_BC6H_SF16,
		DXGI_FORMAT_BC7_TYPELESS,
		DXGI_FORMAT_BC7_UNORM,
		DXGI_FORMAT_BC7_UNORM_SRGB,
		DXGI_FORMAT_AYUV,
		DXGI_FORMAT_Y410,
		DXGI_FORMAT_Y416,
		DXGI_FORMAT_NV12,
		DXGI_FORMAT_P010,
		DXGI_FORMAT_P016,
		DXGI_FORMAT_420_OPAQUE,
		DXGI_FORMAT_YUY2,
		DXGI_FORMAT_Y210,
		DXGI_FORMAT_Y216,
		DXGI_FORMAT_NV11,
		DXGI_FORMAT_AI44,
		DXGI_FORMAT_IA44,
		DXGI_FORMAT_P8,
		DXGI_FORMAT_A8P8,
		DXGI_FORMAT_B4G4R4A4_UNORM,
		DXGI_FORMAT_P208,
		DXGI_FORMAT_V208,
		DXGI_FORMAT_V408,
		DXGI_FORMAT_FORCE_UINT
	}
}
