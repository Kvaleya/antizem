﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;

namespace Engine.Common
{
	public interface IBcSurface
	{
		int Width { get; }
		int Height { get; }
		int NumMips { get; }
		BlockCompression Compression { get; }
		int BlockSize { get; }
		string Filename { get; }

		int GetMipSize(int mip);
		byte[] GetMipData(IFileManager fileManager, int mip);
	}
}
