![Screenshot from Antizem](demo_clouds.jpg "Volumetric clouds and fog")

# Antizem

## Current features

- AZDO rendering: the entire scene is rendered using a small number of multi draw calls
	- GPU accelerated culling
		- per-triangle culling
		- occlusion, backface, frustum and screen size culling
		- can draw many millions of triangles efficiently
	- virtual texturing
- GTAO
	- uses a [fast and edge preserving filtering method](https://bartwronski.com/2019/09/22/local-linear-models-guided-filter/)
	- combines it with temporal accumulation
- Volumetric clouds
	- WIP
	- clouds shadows
	- volumetric fog
- Atmospheric multiple scattering
	- precomputed into several LUTs at startup
- Runtime shader reloading
	- enables wysiwyg editing of any shader
- Scene management
	- resource streaming
	- LOD
- Deferred rendering
	- PBR

## Planned features

- Whatever I want to explore :)
