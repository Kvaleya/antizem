﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;
using Glob;
using Glob.States;
using Glob.Textures;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TestApp
{
	class ShaderRenderer : ITestRenderer
	{
		string _shaderFile;

		GraphicsPipeline _pipelineShader;

		GlobContext _context;
		DeviceState _deviceState;

		IFileManager _fileManager;
		TextOutput _textOutput;
		TestWindow _window;

		GraphicsPipeline _psoShaderRender;

		Stopwatch _sw;

		public ShaderRenderer(GlobContext context, DeviceState deviceState, TestWindow window, IFileManager fileManager, TextOutput textOutput, string shaderFile)
		{
			_window = window;
			_textOutput = textOutput;
			_fileManager = fileManager;
			_context = context;
			_deviceState = deviceState;
			_shaderFile = shaderFile;
		}

		public void OnLoad()
		{
			GL.Enable(EnableCap.DepthTest);

			_psoShaderRender = new GraphicsPipeline(_context, _context.GetShader("fullscreenSimple.vert"), _context.GetShader(_shaderFile), null, new RasterizerState(), new DepthState());
			OnResize();

			_deviceState.Invalidate();

			_sw = new Stopwatch();
			_sw.Start();
		}

		public void OnResize()
		{
			_deviceState.Invalidate();
		}

		public void OnRenderFrame()
		{
			_context.Update();
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			float radius = 4.0f;
			float t = _sw.ElapsedMilliseconds / 1000f;

			float x = (float)Math.Cos(t) * radius;
			float y = (float)Math.Sin(t) * radius;

			Vector3 pos = new Vector3(x, y, 0);

			Vector3 light = new Vector3(1f, 0.5f, 2f).Normalized();

			Quaternion rot = Quaternion.FromAxisAngle(Vector3.UnitZ, -t + (float)Math.PI * 0.5f);

			Matrix4 camera = Glob.Utils.GetCameraMatrix(pos, Quaternion.FromAxisAngle(Vector3.UnitX, (float)Math.PI * 0.5f) * rot);
			Matrix4 proj = Glob.Utils.GetProjectionPerspective(_window.Width, _window.Height, 90, 0.125f, 256f);

			_deviceState.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			_deviceState.BindPipeline(_psoShaderRender);

			Vector4 ray00, ray10, ray11, ray01;
			Glob.Utils.GetCameraCornerRays(proj, camera, out ray00, out ray10, out ray11, out ray01);

			_psoShaderRender.ShaderFragment.SetUniformF("lightDir", Vector3.Transform(light, rot));
			_psoShaderRender.ShaderFragment.SetUniformF("cameraPosition", pos);
			_psoShaderRender.ShaderFragment.SetUniformF("ray00", ray00);
			_psoShaderRender.ShaderFragment.SetUniformF("ray01", ray01);
			_psoShaderRender.ShaderFragment.SetUniformF("ray11", ray11);
			_psoShaderRender.ShaderFragment.SetUniformF("ray10", ray10);
			_psoShaderRender.ShaderFragment.SetUniformF("resolution", new Vector2(_window.Width, _window.Height));
			_psoShaderRender.ShaderFragment.SetUniformF("resolution_rcp", new Vector2(1f / _window.Width, 1f / _window.Height));
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
