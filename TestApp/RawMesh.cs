﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;

namespace TestApp
{
	// Used to store geometry of a mesh and perform operations on it (like generating normals)
    class RawMesh
    {
	    public string Filename;
        string _name, _group, _material;
        public List<Vertex> Vertices;

        public string FullName
        {
            get { return _name + " " + _group + " " + _material; }
        }
        public string Name
        {
            get { return _name; }
        }
        public string Group
        {
            get { return _group; }
        }
        public string Material
        {
            get { return _material; }
        }  

        public RawMesh(string name, string group, string material, string filename)
        {
	        Filename = filename;
            _name = name;
            _group = group;
            _material = material;
            Vertices = new List<Vertex>();
		}

        public void AddVertex(Vertex v)
        {
            Vertices.Add(v);
        }

        public RawMesh Clone()
        {
            RawMesh r = new RawMesh(_name, _group, _material, Filename);
            foreach(var v in Vertices)
            {
                r.AddVertex(v);
            }
            return r;
        }

        public void NormalizeNormals()
        {
            for(int i = 0; i < Vertices.Count; i++)
            {
                var v = Vertices[i];
                Vertices[i] = new Vertex(v.Position, v.Normal.Normalized(), v.TexCoord, v.Tangent);
            }
        }

		public void GenerateTangents()
	    {
			Vertex v0, v1, v2;

			for (int i = 2; i < Vertices.Count; i += 3)
			{
				v0 = Vertices[i - 2];
				v1 = Vertices[i - 1];
				v2 = Vertices[i - 0];

				Vector3 edge1 = v1.Position - v0.Position;
				Vector3 edge2 = v2.Position - v0.Position;

				float deltaU1 = v1.TexCoord.X - v0.TexCoord.X;
				float deltaV1 = v1.TexCoord.Y - v0.TexCoord.Y;
				float deltaU2 = v2.TexCoord.X - v0.TexCoord.X;
				float deltaV2 = v2.TexCoord.Y - v0.TexCoord.Y;

				float f = 1.0f / (deltaU1 * deltaV2 - deltaU2 * deltaV1);

				Vector3 tangent;

				tangent.X = f * (deltaV2 * edge1.X - deltaV1 * edge2.X);
				tangent.Y = f * (deltaV2 * edge1.Y - deltaV1 * edge2.Y);
				tangent.Z = f * (deltaV2 * edge1.Z - deltaV1 * edge2.Z);

				if (float.IsNaN(tangent.X) || float.IsNaN(tangent.Y) || float.IsNaN(tangent.Z))
				{
					tangent = new Vector3(1, 0, 0);
				}

				v0.Tangent = tangent;
				v1.Tangent = tangent;
				v2.Tangent = tangent;

				Vertices[i - 2] = v0;
				Vertices[i - 1] = v1;
				Vertices[i - 0] = v2;
			}
		}
    }
}
