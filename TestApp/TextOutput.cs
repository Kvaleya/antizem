﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;
using Glob;

namespace TestApp
{
	class TextOutput : GameFramework.TextOutput, ITextOutputGlob
	{
		public TextOutput(string logFile, int oldLogBackupCount)
			: base(logFile, oldLogBackupCount)
		{
		}

		public void Print(OutputTypeGlob type, string message)
		{
			OutputType ot = OutputType.Notify;

			if(type == OutputTypeGlob.LogOnly)
				ot = OutputType.LogOnly;
			if(type == OutputTypeGlob.Debug)
				ot = OutputType.Debug;
			if(type == OutputTypeGlob.Notify)
				ot = OutputType.Notify;
			if(type == OutputTypeGlob.Warning)
				ot = OutputType.Warning;
			if(type == OutputTypeGlob.PerformanceWarning)
				ot = OutputType.PerformanceWarning;
			if(type == OutputTypeGlob.Error)
				ot = OutputType.Error;

			Print(message, ot);
		}
	}
}
