﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;

namespace TestApp
{
	static class MeshLoader
	{
		public static void LoadMesh(string meshFile, IFileManager fileManager, TextOutput output, out float[] positions, out List<int> indices)
		{
			bool loadObj = true;

			string filePositions = meshFile + ".positions";
			string fileIndices = meshFile + ".indices";

			if(fileManager.FileExists(filePositions) && fileManager.FileExists(fileIndices))
				loadObj = false;

			if(loadObj)
			{
				// Load mesh
				string report;
				var stream = new StreamReader(fileManager.GetStream(meshFile));
				var meshes = ObjLoader.LoadMeshes(stream, meshFile, out report);
				stream.Dispose();

				output.Print(report, OutputType.Debug);

				List<Vertex> vertices = new List<Vertex>();
				indices = new List<int>();
				foreach(var mesh in meshes)
				{
					List<Vertex> localVerts;
					List<int> localIndices;
					ObjLoader.GenerateIndexedMesh(mesh, out localVerts, out localIndices);

					for(int i = 0; i < localIndices.Count; i++)
					{
						localIndices[i] += vertices.Count;
					}

					vertices.AddRange(localVerts);
					indices.AddRange(localIndices);
				}

				positions = new float[vertices.Count * 4];
				for(int i = 0; i < vertices.Count; i++)
				{
					positions[i * 4 + 0] = vertices[i].Position.X;
					positions[i * 4 + 1] = vertices[i].Position.Y;
					positions[i * 4 + 2] = vertices[i].Position.Z;
					positions[i * 4 + 3] = 0.0f;
				}

				// Raw mesh write

				using(BinaryWriter bw = new BinaryWriter(File.Open(meshFile + ".positions", FileMode.Create)))
				{
					for(int i = 0; i < positions.Length; i++)
					{
						bw.Write(positions[i]);
					}
				}
				using(BinaryWriter bw = new BinaryWriter(File.Open(meshFile + ".indices", FileMode.Create)))
				{
					for(int i = 0; i < indices.Count; i++)
					{
						bw.Write(indices[i]);
					}
				}
			}
			else
			{
				var streamPositions = fileManager.GetStream(meshFile + ".positions");
				var streamIndices = fileManager.GetStream(meshFile + ".indices");

				using(BinaryReader br = new BinaryReader(streamPositions))
				{
					positions = new float[(int)(br.BaseStream.Length / 4)];
					for(int i = 0; i < positions.Length; i++)
					{
						positions[i] = br.ReadSingle();
					}
				}
				indices = new List<int>();

				using(BinaryReader br = new BinaryReader(streamIndices))
				{
					int count = (int)(br.BaseStream.Length / 4);
					for(int i = 0; i < count; i++)
					{
						indices.Add(br.ReadInt32());
					}
				}
			}
		}
	}
}
