﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using Glob.States;
using OpenTK.Graphics.OpenGL;

namespace TestApp
{
	class HelloWorldRenderer : ITestRenderer
	{
		GlobContext _context;
		DeviceState _deviceState;

		GraphicsPipeline _graphicsPipeline;

		public HelloWorldRenderer(GlobContext context, DeviceState deviceState)
		{
			_context = context;
			_deviceState = deviceState;
		}

		public void OnLoad()
		{
			_deviceState.Invalidate();
			_graphicsPipeline = new GraphicsPipeline(_context, _context.GetShader("testVertex.vert"), _context.GetShader("testFragment.frag"), null, new RasterizerState(CullfaceState.None), new DepthState());
		}

		public void OnResize()
		{

		}

		public void OnRenderFrame()
		{
			_context.Update();

			GL.ClearColor(0f, 1f, 0f, 0f);
			GL.Clear(ClearBufferMask.ColorBufferBit);

			_deviceState.BindPipeline(_graphicsPipeline);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
