﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glob;
using Glob.Shaders;
using Glob.States;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace TestApp
{
	class TestWindow : GameWindow
	{
		ITestRenderer _renderer;

		public TestWindow(int w, int h)
			: base(w, h, GraphicsMode.Default, String.Empty, GameWindowFlags.Default, DisplayDevice.Default, -1, -1, GraphicsContextFlags.Debug | GraphicsContextFlags.ForwardCompatible)
		{
			VSync = VSyncMode.Off;
		}

		public void Run(ITestRenderer renderer)
		{
			_renderer = renderer;
			renderer.OnLoad();
			Run();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);
			
			_renderer.OnRenderFrame();

			SwapBuffers();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			GL.Viewport(0, 0, this.Width, this.Height);
			_renderer.OnResize();
		}
	}
}
