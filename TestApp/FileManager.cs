﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;

namespace TestApp
{
	class FileManager : FileManagerZip, Glob.IFileManagerGlob
	{
		string _shaderSourcePath;
		string _shaderResolvedPath;
		string _shaderCompilePath;

		public FileManager(GameFramework.TextOutput textOutput, string baseRelativePath, string shaderSourcePath, string shaderResolvedPath, string shaderCompiledPath)
			: base(textOutput, baseRelativePath)
		{
			_shaderSourcePath = shaderSourcePath;
			_shaderResolvedPath = shaderResolvedPath;
			_shaderCompilePath = shaderCompiledPath;
		}

		public string GetPathShaderSource(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderSourcePath;
			return Path.Combine(_shaderSourcePath, file);
		}

		public string GetPathShaderCacheResolved(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderResolvedPath;
			return Path.Combine(_shaderResolvedPath, file);
		}

		public string GetPathShaderCacheCompiled(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderCompilePath;
			return Path.Combine(_shaderCompilePath, file);
		}
	}
}
