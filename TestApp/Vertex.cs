﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace TestApp
{
	class Vertex
	{
		public Vector3 Position;
		public Vector3 Normal;
		public Vector2 TexCoord;
		public Vector3 Tangent;

		public Vertex(Vector3 position, Vector3 normal, Vector2 texCoord, Vector3 tangent)
		{
			Position = position;
			Normal = normal;
			TexCoord = texCoord;
			Tangent = tangent;
		}

		public bool Equals(Vertex other)
		{
			return other.Position.Equals(Position) && other.Normal.Equals(Normal) && other.TexCoord.Equals(TexCoord) && other.Tangent.Equals(Tangent);
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			if(obj.GetType() != typeof(Vertex)) return false;
			return Equals((Vertex)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int result = Position.GetHashCode();
				result = (result * 397) ^ Normal.GetHashCode();
				result = (result * 397) ^ TexCoord.GetHashCode();
				result = (result * 397) ^ Tangent.GetHashCode();
				return result;
			}
		}

		public static bool operator ==(Vertex left, Vertex right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Vertex left, Vertex right)
		{
			return !left.Equals(right);
		}
	}
}
