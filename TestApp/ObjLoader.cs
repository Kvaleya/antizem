﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using OpenTK;
using System.Globalization;
using System.Text.RegularExpressions;

namespace TestApp
{
	static class ObjLoader
	{
		public static List<RawMesh> LoadMeshes(StreamReader sr, string filename, out string report)
		{
			int failedPositions = 0, failedTexcoords = 0, failedNormals = 0, failedPolygons = 0, errorIndices = 0;

			string objectName = "defaultObject";
			string groupName = "defaultGroup";
			string materialName = "defaultMaterial";

			RawMesh currentMesh = new RawMesh(objectName, groupName, materialName, filename);

			float defaultvalue = 0f;

			List<Vector3> positions = new List<Vector3>()
			{
				new Vector3(defaultvalue)
			};
			List<Vector3> normals = new List<Vector3>()
			{
				new Vector3(defaultvalue)
			};
			List<Vector2> texcoords = new List<Vector2>()
			{
				new Vector2(defaultvalue)
			};

			List<RawMesh> meshes = new List<RawMesh>()
			{
				currentMesh
			};

			// Data loading
			while (!sr.EndOfStream)
			{
				string line = sr.ReadLine().Trim();
				line = Regex.Replace(line, @"\s+", " ");

				var split = line.Split(' ');

				if (split.Length < 2)
					continue;

				switch(split[0])
				{
					case "v":
						{
							// Vertex position
							float x, y, z;

							if(split.Length < 4)
							{
								failedPositions++;
								break;
							}

							if(float.TryParse(split[1], NumberStyles.Float, CultureInfo.InvariantCulture, out x) &&
								float.TryParse(split[2], NumberStyles.Float, CultureInfo.InvariantCulture, out y) &&
								float.TryParse(split[3], NumberStyles.Float, CultureInfo.InvariantCulture, out z))
							{
								positions.Add(new Vector3(x, y, z));
							}
							else
							{
								positions.Add(positions[0]);
								failedPositions++;
							}

							break;
						}
					case "vt":
						{
							// Texcoord
							float x, y;

							if (split.Length < 3)
							{
								failedTexcoords++;
								break;
							}

							if (float.TryParse(split[1], NumberStyles.Float, CultureInfo.InvariantCulture, out x) &&
								float.TryParse(split[2], NumberStyles.Float, CultureInfo.InvariantCulture, out y))
							{
								texcoords.Add(new Vector2(x, y));
							}
							else
							{
								texcoords.Add(texcoords[0]);
								failedTexcoords++;
							}

							break;
						}
					case "vn":
						{
							// Normal
							float x, y, z;

							if (split.Length < 4)
							{
								failedNormals++;
								break;
							}

							if (float.TryParse(split[1], NumberStyles.Float, CultureInfo.InvariantCulture, out x) &&
								float.TryParse(split[2], NumberStyles.Float, CultureInfo.InvariantCulture, out y) &&
								float.TryParse(split[3], NumberStyles.Float, CultureInfo.InvariantCulture, out z))
							{
								normals.Add(new Vector3(x, y, z));
							}
							else
							{
								normals.Add(normals[0]);
								failedNormals++;
							}

							break;
						}
					case "f":
						{
							// Polygon

							string[] vertextext;

							if(split.Length < 4)
							{
								failedPolygons++;
								break;
							}

							List<Vertex> polygon = new List<Vertex>();

							bool failed = false;

							for(int i = 1; i < split.Length; i++)
							{
								vertextext = split[i].Split('/');
								int p = 0, t = 0, n = 0;
								
								if(vertextext.Length < 1)
								{
									failed = true;
									break;
								}

								if(!int.TryParse(vertextext[0], out p))
								{
									failed = true;
									break;
								}

								if(vertextext.Length > 1)
								{
									if(!string.IsNullOrEmpty(vertextext[1]))
										t = int.Parse(vertextext[1]);
								}
								if (vertextext.Length > 2)
								{
									if (!string.IsNullOrEmpty(vertextext[2]))
										n = int.Parse(vertextext[2]);
								}

								// Check for wrong indices
								if(p >= positions.Count)
								{
									errorIndices++;
									failed = true;
									break;
								}
								if (t >= texcoords.Count)
								{
									errorIndices++;
									t = 0;
								}
								if (n >= normals.Count)
								{
									errorIndices++;
									n = 0;
								}
								if (p < 0)
									p = positions.Count + p;
								if (t < 0)
									t = texcoords.Count + t;
								if (n < 0)
									n = normals.Count + n;

								// Assemble the polygon
								if (polygon.Count > 2)
								{
									polygon.Add(polygon[0]);
									polygon.Add(polygon[polygon.Count - 2]);
								}
								polygon.Add(new Vertex(positions[p], normals[n], texcoords[t], new Vector3(0)));
							}

							if(failed)
							{
								failedPolygons++;
							}
							else
							{
								currentMesh.Vertices.AddRange(polygon);
							}

							break;
						}
					case "o":
						{
							objectName = split[1];
							currentMesh = new RawMesh(objectName, groupName, materialName, filename);
							meshes.Add(currentMesh);
							break;
						}
					case "g":
						{
							groupName = split[1];
							currentMesh = new RawMesh(objectName, groupName, materialName, filename);
							meshes.Add(currentMesh);
							break;
						}
					case "usemtl":
						{
							materialName = split[1];
							currentMesh = new RawMesh(objectName, groupName, materialName, filename);
							meshes.Add(currentMesh);
							break;
						}
				}
			}

			// Remove empty meshes or meshes with less than 3 vertices
			List<RawMesh> meshesNew = new List<RawMesh>();
			foreach(var mesh in meshes)
			{
				if (mesh.Vertices.Count > 2)
					meshesNew.Add(mesh);
			}
			meshes = meshesNew;

			// Generate errors

			report = "";

			if(failedPositions > 0)
			{
				report += "Failed positions: " + failedPositions.ToString() + "; ";
			}
			if (failedNormals > 0)
			{
				report += "Failed normals: " + failedNormals.ToString() + "; ";
			}
			if (failedPolygons > 0)
			{
				report += "Failed polygons: " + failedPolygons.ToString() + "; ";
			}
			if (failedTexcoords > 0)
			{
				report += "Failed texcoords: " + failedTexcoords.ToString() + "; ";
			}
			if (errorIndices > 0)
			{
				report += "Failed indices: " + errorIndices.ToString() + "; ";
			}

			return meshes;
		}

		public static void GenerateIndexedMesh(RawMesh mesh, out List<Vertex> vertices, out List<int> indices)
		{
			Dictionary<Vertex, int> vertexIndices = new Dictionary<Vertex, int>();
			vertices = new List<Vertex>();
			indices = new List<int>();

			int lastIndex = 0;

			foreach(Vertex v in mesh.Vertices)
			{
				int index = lastIndex;

				if(vertexIndices.ContainsKey(v))
				{
					index = vertexIndices[v];
				}
				else
				{
					vertexIndices.Add(v, lastIndex);
					lastIndex++;
					vertices.Add(v);
				}

				indices.Add(index);
			}
		}
	}
}
