﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Meshes;
using GameFramework;
using Glob;
using Glob.States;
using Glob.Textures;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TestApp
{
	class CullingRenderer : ITestRenderer
	{
		GlobContext _context;
		DeviceState _deviceState;

		IFileManager _fileManager;
		TextOutput _textOutput;
		TestWindow _window;

		string _meshFile;
		float _meshScale;

		VertexArrayObject _vao;

		int _vbo;
		int _ebo;
		int _bufferIndicesCulled;
		int _bufferDrawArgsCulled;
		int _bufferDrawArgsCulledDefaultState;

		int _indirectBufferSize;

		int _meshVertexCount;
		int _meshIndexCount;

		GraphicsPipeline _psoRenderMeshBasic;
		GraphicsPipeline _psoDisplayDepth;
		ComputePipeline _psoCullTriangles;
		ComputePipeline _psoDepthMipGenInit;
		ComputePipeline _psoDepthMipGen;
		ComputePipeline _psoDepthMipClear;

		VertexBufferSource _meshVertexBuffer;

		Texture2D _texSceneDepth;
		Texture2D _texDepthMipped;
		FrameBuffer _fboScene;

		MeshBinary _mesh;
		VertexStream _streamPositions;
		VertexStream _streamIndices;

		Stopwatch _sw;

		public CullingRenderer(GlobContext context, DeviceState deviceState, TestWindow window, IFileManager fileManager, TextOutput textOutput, string meshFile, float meshScale = 0.5f)
		{
			_window = window;
			_textOutput = textOutput;
			_fileManager = fileManager;
			_context = context;
			_deviceState = deviceState;
			_meshFile = meshFile;
			_meshScale = meshScale;
		}

		public void OnLoad()
		{
			GL.Enable(EnableCap.DepthTest);
			/*
			float[] positions;
			List<int> indices;

			_vao = new VertexArrayObject();
			
			MeshLoader.LoadMesh(_meshFile, _fileManager, _textOutput, out positions, out indices);
			_meshVertexCount = positions.Length / 4;
			_meshIndexCount = indices.Count;

			_vbo = Glob.Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(positions.Length * 4), positions,
				BufferStorageFlags.None, "MeshVertexBuffer");
			_ebo = Glob.Utils.CreateBuffer(BufferTarget.ElementArrayBuffer, new IntPtr(indices.Count * 4), indices.ToArray(),
				BufferStorageFlags.None, "MeshIndexBuffer");
			_bufferIndicesCulled = Glob.Utils.CreateBuffer(BufferTarget.ElementArrayBuffer, new IntPtr(indices.Count * 4), indices.ToArray(),
				BufferStorageFlags.None, "MeshIndexBufferCulled");
			*/

			_meshVertexCount = 0;
			_meshIndexCount = 0;

			using(var stream = _fileManager.GetStream(_meshFile))
			{
				string report;
				_mesh = MeshBinary.Load(stream, null);
				/*
				var group = Engine.Meshes.ObjLoader.LoadMeshes(new StreamReader(stream), _meshFile, out report);

				_mesh = MeshBinary.FromMeshData(group.Meshes[0], true, (float)Math.PI * 0.25f);
				*/
			}

			_meshVertexCount += _mesh.Description.VertexCount;
			_meshIndexCount += _mesh.Description.IndexCount;

			_textOutput.Print("Mesh vertices: " + _meshVertexCount, OutputType.Notify);
			_textOutput.Print("Mesh indices: " + _meshIndexCount, OutputType.Notify);

			_streamPositions = _mesh.GetStream(VertexStreamUsage.Positions);
			_streamIndices = _mesh.GetStream(VertexStreamUsage.Indices);

			_vbo = Glob.Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(_meshVertexCount * 8), _streamPositions.Data,
				BufferStorageFlags.None, "MeshVertexBuffer");
			_ebo = Glob.Utils.CreateBuffer(BufferTarget.ElementArrayBuffer, new IntPtr(_meshIndexCount * 2), _streamIndices.Data,
				BufferStorageFlags.None, "MeshIndexBuffer");
			_bufferIndicesCulled = Glob.Utils.CreateBuffer(BufferTarget.ElementArrayBuffer, new IntPtr(_meshIndexCount * 2), IntPtr.Zero, 
				BufferStorageFlags.None, "MeshIndexBufferCulled");

			_vao = new VertexArrayObject();

			int[] drawArgsDefault = new int[]
			{
				0,
				1,
				0,
				0,
				0,
			};

			_indirectBufferSize = drawArgsDefault.Length * 4;

			_bufferDrawArgsCulled = Glob.Utils.CreateBuffer(BufferTarget.DrawIndirectBuffer, new IntPtr(_indirectBufferSize), drawArgsDefault,
				BufferStorageFlags.None, "DrawArgsCulled");
			_bufferDrawArgsCulledDefaultState = Glob.Utils.CreateBuffer(BufferTarget.DrawIndirectBuffer, new IntPtr(_indirectBufferSize), drawArgsDefault,
				BufferStorageFlags.None, "DrawArgsCulledDefaultState");

			_meshVertexBuffer = new VertexBufferSource(0, new VertexBufferBinding(_vbo, 0, 8, 0));

			_psoRenderMeshBasic = new GraphicsPipeline(_context, _context.GetShader("simpleMesh.vert"), null,
				new VertexBufferFormat(new VertexAttribDescription(0, 0, 3, 0, VertexAttribType.UnsignedShort, VertexAttribClass.Float,
					true)), new RasterizerState(CullfaceState.Back), new DepthState(DepthFunction.Less, true));
			_psoDisplayDepth = new GraphicsPipeline(_context, _context.GetShader("fullscreenSimple.vert"), _context.GetShader("depthDisplay.frag"), null, new RasterizerState(), new DepthState());
			_psoCullTriangles = new ComputePipeline(_context, _context.GetShader("cullTriangles.comp"));
			_psoDepthMipGenInit = new ComputePipeline(_context, _context.GetShader("depthMipGen.comp", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("INIT", "")
			}));
			_psoDepthMipGen = new ComputePipeline(_context, _context.GetShader("depthMipGen.comp"));

			_psoDepthMipClear = new ComputePipeline(_context, _context.GetShader("clear.comp"));

			OnResize();

			_deviceState.Invalidate();

			_sw = new Stopwatch();
			_sw.Start();
		}

		public void OnResize()
		{
			if(_fboScene != null)
				_fboScene.Dispose();
			if(_texSceneDepth != null)
				_texSceneDepth.Dispose();

			_texSceneDepth = new Texture2D(_deviceState, "SceneDepth", SizedInternalFormatGlob.DEPTH_COMPONENT32F, _window.Width, _window.Height, 1);
			_texDepthMipped = new Texture2D(_deviceState, "SceneDepthMips", SizedInternalFormatGlob.R32F, _window.Width / 2, _window.Height / 2, 0);

			_fboScene = new FrameBuffer();
			_deviceState.BindFrameBuffer(_fboScene, FramebufferTarget.Framebuffer);
			_fboScene.Attach(FramebufferAttachment.DepthAttachment, _texSceneDepth);

			_deviceState.Invalidate();
		}

		int _frames = 0;
		const int BenchMillis = 5000;
		bool _compute = true;
		bool _depthMipValid = false;

		public void OnRenderFrame()
		{
			_context.Update();

			if(_sw.ElapsedMilliseconds > BenchMillis)
			{
				//_echoed = true;
				if(_compute)
				{
					_textOutput.Print("Compute:", OutputType.Notify);
				}
				else
				{
					_textOutput.Print("Raster:", OutputType.Notify);
				}
				double fps = ((double)_frames / _sw.ElapsedMilliseconds * 1000);

				_textOutput.Print("Avg fps: " + fps + " Avg millis/frame: " + (1000d / fps), OutputType.Notify);

				_frames = 0;
				_sw.Restart();
				_compute = !_compute;
				_depthMipValid = false;
			}

			_frames++;

			Matrix4 world = Glob.Utils.GetWorldMatrix(new Vector3(0, 0, -1), Quaternion.FromAxisAngle(Vector3.UnitX, _sw.ElapsedMilliseconds * 0.0003f) * Quaternion.FromAxisAngle(Vector3.UnitY, _sw.ElapsedMilliseconds * 0.0005f), new Vector3(_meshScale));
			Matrix4 proj = Glob.Utils.GetProjectionPerspective(_window.Width, _window.Height, 90, 0.125f, 256f);
			Matrix4 camera = Glob.Utils.GetCameraMatrix(new Vector3(0, 0, 4), Quaternion.Identity);

			Matrix4 projcam = world * camera * proj;

			_vao.Bind(_deviceState);

			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			if(_compute)
			{
				// Clear depth mip
				if(!_depthMipValid)
				{
					_deviceState.BindPipeline(_psoDepthMipClear);
					for(int i = 0; i < _texDepthMipped.Levels; i++)
					{
						GL.BindImageTexture(0, _texDepthMipped.Handle, i, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
						_deviceState.DispatchComputeThreads(_texDepthMipped.Width >> i, _texDepthMipped.Height >> i);
					}
					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);
				}

				// Cull triangles
				//if(_sw.ElapsedMilliseconds < 1000)
				{
					// Reset indirect dispatch buffer
					GL.BindBuffer(BufferTarget.CopyReadBuffer, _bufferDrawArgsCulledDefaultState);
					GL.BindBuffer(BufferTarget.CopyWriteBuffer, _bufferDrawArgsCulled);
					GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, IntPtr.Zero, IntPtr.Zero, new IntPtr(_indirectBufferSize));
					GL.BindBuffer(BufferTarget.CopyReadBuffer, 0);
					GL.BindBuffer(BufferTarget.CopyWriteBuffer, 0);

					// Clear index buffer
					ushort zero = 0;
					GL.BindBuffer(BufferTarget.ShaderStorageBuffer, _bufferIndicesCulled);
					GL.ClearBufferData(BufferTarget.ShaderStorageBuffer, PixelInternalFormat.R16ui, PixelFormat.Red, PixelType.UnsignedShort, ref zero);
					GL.BindBuffer(BufferTarget.ShaderStorageBuffer, 0);

					_deviceState.BindPipeline(_psoCullTriangles);
					_deviceState.BufferBindingManager.BindBufferBase(0, BufferRangeTarget.ShaderStorageBuffer, _vbo);
					_deviceState.BufferBindingManager.BindBufferBase(1, BufferRangeTarget.ShaderStorageBuffer, _ebo);
					_deviceState.BufferBindingManager.BindBufferBase(2, BufferRangeTarget.ShaderStorageBuffer, _bufferIndicesCulled);
					_deviceState.BufferBindingManager.BindBufferBase(3, BufferRangeTarget.ShaderStorageBuffer, _bufferDrawArgsCulled);
					_deviceState.TextureUnit.BindTexture(_texDepthMipped, 0);
					_psoCullTriangles.ShaderCompute.SetUniformF("posOffset", _streamPositions.ValueOffset.Xyz);
					_psoCullTriangles.ShaderCompute.SetUniformF("posScale", _streamPositions.ValueScale.Xyz);
					_psoCullTriangles.ShaderCompute.SetUniformI("maxDepthLevel", _texDepthMipped.Levels - 1);
					_psoCullTriangles.ShaderCompute.SetUniformI("elementCount", _meshIndexCount);
					_psoCullTriangles.ShaderCompute.SetUniformI("firstElement", 0);
					_psoCullTriangles.ShaderCompute.SetUniformF("projection", projcam);
					_psoCullTriangles.ShaderCompute.SetUniformF("resolution", new Vector2(_window.Width, _window.Height));
					_deviceState.DispatchComputeThreads(_meshIndexCount / 3);

					GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);
				}
				
				// Render surviving triangles
				using(_deviceState.BindFrameBufferPushViewport(_fboScene, "Mesh rendering"))
				{
					_deviceState.BindPipeline(_psoRenderMeshBasic);
					_deviceState.BindVertexBufferSource(_meshVertexBuffer);
					_psoRenderMeshBasic.ShaderVertex.SetUniformF("projection", projcam);
					_psoRenderMeshBasic.ShaderVertex.SetUniformF("posOffset", _streamPositions.ValueOffset.Xyz);
					_psoRenderMeshBasic.ShaderVertex.SetUniformF("posScale", _streamPositions.ValueScale.Xyz);

					GL.Clear(ClearBufferMask.DepthBufferBit);

					GL.BindBuffer(BufferTarget.ElementArrayBuffer, _bufferIndicesCulled);
					GL.BindBuffer(BufferTarget.DrawIndirectBuffer, _bufferDrawArgsCulled);

					GL.MemoryBarrier(MemoryBarrierFlags.ElementArrayBarrierBit);

					GL.DrawElementsIndirect(PrimitiveType.Triangles, DrawElementsType.UnsignedShort, IntPtr.Zero);

					GL.BindBuffer(BufferTarget.DrawIndirectBuffer, 0);
					GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
				}

				// Generate depth mip
				{
					_deviceState.BindPipeline(_psoDepthMipGenInit);
					_psoDepthMipGenInit.ShaderCompute.SetUniformF("texsize_rcp", new Vector2(1.0f / _texSceneDepth.Width, 1.0f / _texSceneDepth.Height));
					_deviceState.TextureUnit.BindTexture(_texSceneDepth, 0);
					GL.BindImageTexture(0, _texDepthMipped.Handle, 0, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(1, _texDepthMipped.Handle, 1, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(2, _texDepthMipped.Handle, 2, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(3, _texDepthMipped.Handle, 3, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(4, _texDepthMipped.Handle, 4, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					_deviceState.DispatchComputeThreads((_texSceneDepth.Width >> 1), (_texSceneDepth.Height >> 1));
					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);
					
					_deviceState.BindPipeline(_psoDepthMipGen);
					_psoDepthMipGen.ShaderCompute.SetUniformF("texsize_rcp", new Vector2(1.0f / (_texSceneDepth.Width >> 5), 1.0f / (_texSceneDepth.Height >> 5)));
					GL.BindImageTexture(5, _texDepthMipped.Handle, 4, false, 0, TextureAccess.ReadOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(0, _texDepthMipped.Handle, 5, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(1, _texDepthMipped.Handle, 6, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(2, _texDepthMipped.Handle, 7, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(3, _texDepthMipped.Handle, 8, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					GL.BindImageTexture(4, _texDepthMipped.Handle, 9, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
					_deviceState.DispatchComputeThreads((_texSceneDepth.Width >> 6), (_texSceneDepth.Height >> 6));
					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

					_depthMipValid = true;
				}
			}
			else
			{
				using(_deviceState.BindFrameBufferPushViewport(_fboScene, "Mesh rendering"))
				{
					_deviceState.BindPipeline(_psoRenderMeshBasic);
					_deviceState.BindVertexBufferSource(_meshVertexBuffer);
					_psoRenderMeshBasic.ShaderVertex.SetUniformF("projection", projcam);
					_psoRenderMeshBasic.ShaderVertex.SetUniformF("posOffset", _streamPositions.ValueOffset.Xyz);
					_psoRenderMeshBasic.ShaderVertex.SetUniformF("posScale", _streamPositions.ValueScale.Xyz);

					GL.Clear(ClearBufferMask.DepthBufferBit);

					GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);

					GL.DrawElements(PrimitiveType.Triangles, _meshIndexCount, DrawElementsType.UnsignedShort, IntPtr.Zero);
				}
			}

			_deviceState.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			_deviceState.BindPipeline(_psoDisplayDepth);
			_deviceState.TextureUnit.BindTexture(_texSceneDepth, 0);
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
