﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;
using Glob;
using Glob.States;
using Glob.Textures;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using IFileManager = GameFramework.IFileManager;

namespace TestApp
{
	class MeshRenderer : ITestRenderer
	{
		Stopwatch _sw;

		VertexArrayObject _vao;
		int _vbo;
		int _ebo;
		int _bufferTransformedCache;

		const int IndirectBufferSize = 16;
		int _bufferTileTriangles;
		int _bufferIndirectDispatch;
		int _bufferIndirectDispatchDefaultState;

		const int TilePixelSizePow = 4;
		const int TilePixelSize = 1 << TilePixelSizePow;
		const int TrianglesPerTilePow = TilePixelSizePow * 2;
		const int TrianglesPerTile = 1 << TrianglesPerTilePow;
		const int TrianglesTotalWorstCase = 1 << 23; // 8M, 32mb
		int _trianglesLooseStart;

		int _tilesX;
		int _tilesY;
		int _tilesTotal;

		GraphicsPipeline _depthRenderPipeline;
		GraphicsPipeline _depthDisplayPipeline;
		ComputePipeline _rasterizeCompute;
		ComputePipeline _rasterizeComputeClear;
		ComputePipeline _rasterizeComputeClearBuffers;
		ComputePipeline _rasterizeComputeRasterCoarse;
		ComputePipeline _rasterizeComputeRasterFineTiled;
		ComputePipeline _rasterizeComputeRasterFineLoose;

		ComputePipeline _rasterizeComputeTransform;
		ComputePipeline _rasterizeComputeRaster;

		int _numIndices;
		int _numVerts;
		FrameBuffer _fbo;
		Texture2D _sceneDepth;
		Texture2D _sceneDepthComputeInt;
		Texture2D _sceneDepthComputeFloat;

		GlobContext _context;
		DeviceState _deviceState;

		IFileManager _fileManager;
		TextOutput _textOutput;
		TestWindow _window;

		string _meshFile;

		public MeshRenderer(GlobContext context, DeviceState deviceState, TestWindow window, IFileManager fileManager, TextOutput textOutput, string meshFile)
		{
			_window = window;
			_textOutput = textOutput;
			_fileManager = fileManager;
			_context = context;
			_deviceState = deviceState;
			_meshFile = meshFile;
		}

		public void OnLoad()
		{
			GL.Enable(EnableCap.DepthTest);

			List<int> indices;
			float[] positions;

			MeshLoader.LoadMesh(_meshFile, _fileManager, _textOutput, out positions, out indices);

			_numVerts = positions.Length / 4;
			_numIndices = indices.Count;

			_textOutput.Print("Mesh vertices: " + _numVerts, OutputType.Notify);
			_textOutput.Print("Mesh indices: " + _numIndices, OutputType.Notify);

			_vbo = Glob.Utils.CreateBuffer(BufferTarget.ArrayBuffer, new IntPtr(positions.Length * 4), positions,
				BufferStorageFlags.None, "MeshPositions");
			_bufferTransformedCache = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(positions.Length * 4), IntPtr.Zero,
				BufferStorageFlags.None, "VertexTransformedCache");
			_ebo = Glob.Utils.CreateBuffer(BufferTarget.ElementArrayBuffer, new IntPtr(indices.Count * 4), indices.ToArray(),
				BufferStorageFlags.None, "MeshIndices");

			_vao = new VertexArrayObject();

			_vao.Bind(_deviceState);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);

			GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
			GL.EnableVertexAttribArray(0); // Pos
			GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 16, 0);

			_deviceState.BindVao(0);

			int numTilesX = (_window.Width + TilePixelSize - 1) / TilePixelSize;
			int numTilesY = (_window.Height + TilePixelSize - 1) / TilePixelSize;

			int numTilesTotal = numTilesX * numTilesY;

			_tilesX = numTilesX;
			_tilesY = numTilesY;
			_tilesTotal = numTilesTotal;

			_trianglesLooseStart = numTilesTotal * TrianglesPerTile;

			_bufferTileTriangles = Glob.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer,
				new IntPtr(TrianglesTotalWorstCase * 4), IntPtr.Zero, BufferStorageFlags.None, "RasterTileTriangles");
			_bufferIndirectDispatch = Glob.Utils.CreateBuffer(BufferTarget.DispatchIndirectBuffer, new IntPtr(IndirectBufferSize), IntPtr.Zero,
				BufferStorageFlags.None, "RasterizeIndirectDispatchBuffer");

			int[] indirectState = new int[]
			{
				0,
				1,
				1,
				0,
			};

			_bufferIndirectDispatchDefaultState = Glob.Utils.CreateBuffer(BufferTarget.CopyReadBuffer, new IntPtr(IndirectBufferSize),
				indirectState, BufferStorageFlags.None, "RasterizeIndirectDispatchBuffer_DefaultState");

			_sceneDepth = new Texture2D(_deviceState, "SceneDepth", SizedInternalFormatGlob.DEPTH_COMPONENT32F, _window.Width, _window.Height, 1);
			_sceneDepthComputeInt = new Texture2D(_deviceState, "SceneDepthComputeFloat", SizedInternalFormatGlob.R32UI, _sceneDepth.Width, _sceneDepth.Height, 1);
			_sceneDepthComputeFloat = new Texture2D(_deviceState, "SceneDepthComputeInt", SizedInternalFormatGlob.R32F, _sceneDepthComputeInt, 0, 1, 0);

			_fbo = new FrameBuffer();
			_deviceState.BindFrameBuffer(_fbo, FramebufferTarget.Framebuffer);
			_fbo.Attach(FramebufferAttachment.DepthAttachment, _sceneDepth);

			var tileSizeMacro = new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("TILE_TRIANGLE_COUNT", TrianglesPerTile.ToString()),
				new Tuple<string, string>("TILE_PIXEL_SIZE", TilePixelSize.ToString()),
				new Tuple<string, string>("TILE_PIXEL_SIZE_POW", TilePixelSizePow.ToString()),
			};

			_rasterizeCompute = new ComputePipeline(_context, _context.GetShader("rasterize.comp"));
			//_rasterizeCompute.ShaderCompute.ShaderStorageBlockBinding("bufferPositions", 0);
			//_rasterizeCompute.ShaderCompute.ShaderStorageBlockBinding("bufferIndices", 1);

			_rasterizeComputeTransform = new ComputePipeline(_context, _context.GetShader("rast_vertex_transform.comp"));
			_rasterizeComputeRaster = new ComputePipeline(_context, _context.GetShader("rast_fine_vertcached.comp"));

			_rasterizeComputeClear = new ComputePipeline(_context, _context.GetShader("clear.comp"));
			_rasterizeComputeClear.ShaderCompute.SetUniformI("tex", 0);

			_rasterizeComputeClearBuffers = new ComputePipeline(_context, _context.GetShader("rast_clearBuffers.comp", tileSizeMacro));
			_rasterizeComputeRasterCoarse = new ComputePipeline(_context, _context.GetShader("rast_coarse.comp", tileSizeMacro));
			_rasterizeComputeRasterFineTiled = new ComputePipeline(_context, _context.GetShader("rast_fine_tiled.comp", tileSizeMacro));
			_rasterizeComputeRasterFineLoose = new ComputePipeline(_context, _context.GetShader("rast_fine_loose.comp", tileSizeMacro));

			_depthRenderPipeline = new GraphicsPipeline(_context, _context.GetShader("simpleMesh.vert"), null, null, new RasterizerState(), new DepthState());

			_depthDisplayPipeline = new GraphicsPipeline(_context, _context.GetShader("fullscreenSimple.vert"), _context.GetShader("depthDisplay.frag"), null, new RasterizerState(), new DepthState());
			//_depthDisplayPipeline.ShaderFragment.SetUniformI("texDepth", 0);

			_deviceState.Invalidate();

			_sw = new Stopwatch();
			_sw.Start();
		}

		public void OnResize()
		{

		}

		int _frames = 0;
		const int BenchMillis = 5000;
		bool _compute = true;
		const bool _tiledRaster = false;
		
		public void OnRenderFrame()
		{
			_context.Update();

			if(_sw.ElapsedMilliseconds > BenchMillis)
			{
				//_echoed = true;
				if(_compute)
				{
					_textOutput.Print("Compute:", OutputType.Notify);
				}
				else
				{
					_textOutput.Print("Raster:", OutputType.Notify);
				}
				double fps = ((double)_frames / _sw.ElapsedMilliseconds * 1000);

				_textOutput.Print("Avg fps: " + fps + " Avg millis/frame: " + (1000d / fps), OutputType.Notify);

				_frames = 0;
				_sw.Restart();
				_compute = !_compute;
			}

			_frames++;

			Matrix4 world = Glob.Utils.GetWorldMatrix(Vector3.Zero, Quaternion.FromAxisAngle(Vector3.UnitX, _sw.ElapsedMilliseconds * 0.0003f) * Quaternion.FromAxisAngle(Vector3.UnitY, _sw.ElapsedMilliseconds * 0.002f), new Vector3(0.5f));
			Matrix4 proj = Glob.Utils.GetProjectionPerspective(_window.Width, _window.Height, 90, 1, 128);
			Matrix4 camera = Glob.Utils.GetCameraMatrix(new Vector3(0, 0, 4), Quaternion.Identity);

			Matrix4 projcam = world * camera * proj;

			GL.ClearColor(0f, 1f, 0f, 0f);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			if(_compute)
			{
				if(_tiledRaster)
				{
					using(_deviceState.DebugMessageManager.PushGroupMarker("ComputeRasterizerTiled"))
					{
						// Reset indirect dispatch buffer
						GL.BindBuffer(BufferTarget.CopyReadBuffer, _bufferIndirectDispatchDefaultState);
						GL.BindBuffer(BufferTarget.CopyWriteBuffer, _bufferIndirectDispatch);
						GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, IntPtr.Zero, IntPtr.Zero, new IntPtr(IndirectBufferSize));
						GL.BindBuffer(BufferTarget.CopyReadBuffer, 0);
						GL.BindBuffer(BufferTarget.CopyWriteBuffer, 0);

						// Clear depth buffer
						_deviceState.BindPipeline(_rasterizeComputeClear);
						GL.BindImageTexture(0, _sceneDepthComputeFloat.Handle, 0, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
						_deviceState.DispatchComputeThreads(_sceneDepthComputeFloat.Width, _sceneDepthComputeFloat.Height, 1);

						// Clear triangle buffer
						_deviceState.BindPipeline(_rasterizeComputeClearBuffers);
						_deviceState.BufferBindingManager.BindBufferBase(0, BufferRangeTarget.ShaderStorageBuffer, _bufferTileTriangles);
						_rasterizeComputeClearBuffers.ShaderCompute.SetUniformI("stridePow", TrianglesPerTilePow);
						_deviceState.DispatchComputeThreads(TrianglesTotalWorstCase >> TrianglesPerTilePow);

						GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.ShaderStorageBarrierBit);

						// Bind buffers
						_deviceState.BufferBindingManager.BindBufferBase(0, BufferRangeTarget.ShaderStorageBuffer, _vbo);
						_deviceState.BufferBindingManager.BindBufferBase(1, BufferRangeTarget.ShaderStorageBuffer, _ebo);
						_deviceState.BufferBindingManager.BindBufferBase(2, BufferRangeTarget.ShaderStorageBuffer, _bufferTransformedCache);
						_deviceState.BufferBindingManager.BindBufferBase(3, BufferRangeTarget.ShaderStorageBuffer, _bufferIndirectDispatch);
						_deviceState.BufferBindingManager.BindBufferBase(4, BufferRangeTarget.ShaderStorageBuffer, _bufferTileTriangles);
						
						// Transform vertices
						_deviceState.BindPipeline(_rasterizeComputeTransform);
						_rasterizeComputeTransform.ShaderCompute.SetUniformF("projection", projcam);
						_deviceState.DispatchComputeThreads(_numVerts);
						GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

						// Coarse rasterizer
						int coarseX = (_sceneDepthComputeInt.Width + TilePixelSize - 1) / TilePixelSize;
						int coarseY = (_sceneDepthComputeInt.Height + TilePixelSize - 1) / TilePixelSize;

						_deviceState.BindPipeline(_rasterizeComputeRasterCoarse);
						_rasterizeComputeRasterCoarse.ShaderCompute.SetUniformI("numTilesX", _tilesX);
						_rasterizeComputeRasterCoarse.ShaderCompute.SetUniformI("numTilesTotal", _tilesTotal);
						_rasterizeComputeRasterCoarse.ShaderCompute.SetUniformI("looseTrianglesStart", _trianglesLooseStart);
						_rasterizeComputeRasterCoarse.ShaderCompute.SetUniformI("firstElement", 0);
						_rasterizeComputeRasterCoarse.ShaderCompute.SetUniformI("elementCount", _numIndices);
						_rasterizeComputeRasterCoarse.ShaderCompute.SetUniformF("resolution", new Vector2(coarseX, coarseY));
						_rasterizeComputeRasterCoarse.ShaderCompute.SetUniformF("resolution_rcp", new Vector2(1f / coarseX, 1f / coarseY));
						_deviceState.DispatchComputeThreads(_numIndices / 3);

						GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

						// Fine rasterized tiled
						_deviceState.BindPipeline(_rasterizeComputeRasterFineTiled);
						GL.BindImageTexture(0, _sceneDepthComputeInt.Handle, 0, false, 0, TextureAccess.ReadWrite, SizedInternalFormat.R32ui);
						_rasterizeComputeRasterFineTiled.ShaderCompute.SetUniformI("numTilesX", _tilesX);
						_rasterizeComputeRasterFineTiled.ShaderCompute.SetUniformI("numTilesTotal", _tilesTotal);
						_rasterizeComputeRasterFineTiled.ShaderCompute.SetUniformI("looseTrianglesStart", _trianglesLooseStart);
						_rasterizeComputeRasterFineTiled.ShaderCompute.SetUniformF("resolution", new Vector2(_sceneDepthComputeInt.Width, _sceneDepthComputeInt.Height));
						_rasterizeComputeRasterFineTiled.ShaderCompute.SetUniformF("resolution_rcp", new Vector2(1f / _sceneDepthComputeInt.Width, 1f / _sceneDepthComputeInt.Height));
						_deviceState.DispatchComputeGroups((_window.Width + TilePixelSize - 1) / TilePixelSize, (_window.Height + TilePixelSize - 1) / TilePixelSize);

						GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

						// Fine rasterizer loose
						_deviceState.BindPipeline(_rasterizeComputeRasterFineLoose);
						_rasterizeComputeRasterFineLoose.ShaderCompute.SetUniformI("numTilesX", _tilesX);
						_rasterizeComputeRasterFineLoose.ShaderCompute.SetUniformI("numTilesTotal", _tilesTotal);
						_rasterizeComputeRasterFineLoose.ShaderCompute.SetUniformI("looseTrianglesStart", _trianglesLooseStart);
						_rasterizeComputeRasterFineLoose.ShaderCompute.SetUniformF("resolution", new Vector2(_sceneDepthComputeInt.Width, _sceneDepthComputeInt.Height));
						_rasterizeComputeRasterFineLoose.ShaderCompute.SetUniformF("resolution_rcp", new Vector2(1f / _sceneDepthComputeInt.Width, 1f / _sceneDepthComputeInt.Height));
						GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, _bufferIndirectDispatch);
						GL.DispatchComputeIndirect(IntPtr.Zero);
						GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, 0);

						GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);
					}
				}
				else
				{
					using(_deviceState.DebugMessageManager.PushGroupMarker("ComputeRasterizer"))
					{
						_deviceState.BindPipeline(_rasterizeComputeClear);
						GL.BindImageTexture(0, _sceneDepthComputeFloat.Handle, 0, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.R32f);
						_deviceState.DispatchComputeThreads(_sceneDepthComputeFloat.Width, _sceneDepthComputeFloat.Height, 1);
						GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

						//_deviceState.BindPipeline(_rasterizeCompute);
						//GL.BindImageTexture(0, _sceneDepthComputeInt.Handle, 0, false, 0, TextureAccess.ReadWrite, SizedInternalFormat.R32ui);
						//_deviceState.BufferBindingManager.BindBufferBase(0, BufferRangeTarget.ShaderStorageBuffer, _vbo);
						//_deviceState.BufferBindingManager.BindBufferBase(1, BufferRangeTarget.ShaderStorageBuffer, _ebo);
						//_rasterizeCompute.ShaderCompute.SetUniformF("resolution", new Vector2(_sceneDepthComputeInt.Width, _sceneDepthComputeInt.Height));
						//_rasterizeCompute.ShaderCompute.SetUniformF("resolution_rcp", new Vector2(1f / _sceneDepthComputeInt.Width, 1f / _sceneDepthComputeInt.Height));
						//_rasterizeCompute.ShaderCompute.SetUniformI("firstElement", 0);
						//_rasterizeCompute.ShaderCompute.SetUniformI("elementCount", _numIndices);
						//_rasterizeCompute.ShaderCompute.SetUniformF("projection", projcam);
						//_rasterizeCompute.DispatchComputeThreads(_numIndices / 3);
						//GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);

						_deviceState.BindPipeline(_rasterizeComputeTransform);
						_deviceState.BufferBindingManager.BindBufferBase(0, BufferRangeTarget.ShaderStorageBuffer, _vbo);
						_deviceState.BufferBindingManager.BindBufferBase(1, BufferRangeTarget.ShaderStorageBuffer, _ebo);
						_deviceState.BufferBindingManager.BindBufferBase(2, BufferRangeTarget.ShaderStorageBuffer, _bufferTransformedCache);
						_rasterizeComputeTransform.ShaderCompute.SetUniformF("projection", projcam);
						_deviceState.DispatchComputeThreads(_numVerts);
						GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

						_deviceState.BindPipeline(_rasterizeComputeRaster);
						GL.BindImageTexture(0, _sceneDepthComputeInt.Handle, 0, false, 0, TextureAccess.ReadWrite, SizedInternalFormat.R32ui);
						_rasterizeComputeRaster.ShaderCompute.SetUniformF("resolution", new Vector2(_sceneDepthComputeInt.Width, _sceneDepthComputeInt.Height));
						_rasterizeComputeRaster.ShaderCompute.SetUniformF("resolution_rcp", new Vector2(1f / _sceneDepthComputeInt.Width, 1f / _sceneDepthComputeInt.Height));
						_rasterizeComputeRaster.ShaderCompute.SetUniformI("firstElement", 0);
						_rasterizeComputeRaster.ShaderCompute.SetUniformI("elementCount", _numIndices);
						_deviceState.DispatchComputeThreads(_numIndices / 3);
						GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit);
					}
				}
			}
			else
			{
				using(_deviceState.BindFrameBufferPushViewport(_fbo, "Depth rendering"))
				{
					GL.Clear(ClearBufferMask.DepthBufferBit);

					GL.Enable(EnableCap.CullFace);
					GL.CullFace(CullFaceMode.Back);
					GL.DepthFunc(DepthFunction.Less);

					_deviceState.BindPipeline(_depthRenderPipeline);
					_depthRenderPipeline.ShaderVertex.SetUniformF("projection", projcam);

					_vao.Bind(_deviceState);
					GL.DrawElements(PrimitiveType.Triangles, _numIndices, DrawElementsType.UnsignedInt, IntPtr.Zero);
				}
			}

			_deviceState.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			_deviceState.BindVao(0);

			_deviceState.BindPipeline(_depthDisplayPipeline);
			if(_compute)
			{
				_deviceState.TextureUnit.BindTexture(_sceneDepthComputeFloat, 0);
			}
			else
			{
				_deviceState.TextureUnit.BindTexture(_sceneDepth, 0);
			}
			
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
