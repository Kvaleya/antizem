﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;
using Glob;
using Glob.States;
using IFileManager = GameFramework.IFileManager;

namespace TestApp
{
	class Program
	{
		static void Main(string[] args)
		{
			TextOutput output = new TextOutput("log", 8);
			output.PrintEvent += GameFramework.TextOutput.OnPrintConsole;

			output.Print(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), OutputType.Debug);
			FileManager fileManager = new FileManager(output, "", "src/Shaders", "src/Shaders/Resolved", "src/Shaders/Compiled");

			TestWindow w = new TestWindow(1280, 720);

			GlobContext context = new GlobContext(output, fileManager);
			DebugMessageManager.SetupDebugCallback(output);

			ITestRenderer renderer;
			//renderer = new HelloWorldRenderer(context, new DeviceState());
			//fileManager.MountArchive("hairball.zip");
			//fileManager.MountArchive("sanMiguel.zip");
			//renderer = new MeshRenderer(context, new DeviceState(), w, fileManager, output, "hairball.obj");
			renderer = new CullingRenderer(context, new DeviceState(), w, fileManager, output, "teapot.zip", 0.5f);
			//renderer = new ShaderRenderer(context, new DeviceState(), w, fileManager, output, "fog_test.frag");
			//renderer = new MeshRenderer(context, new DeviceState(), fileManager, output, "hairball.obj");

			try
			{
				w.Run(renderer);
			}
			catch(Exception e)
			{
				output.PrintException(OutputType.Error, "Fatal exception", e);
				return;
			}
		}

		static void PrintFile(TextOutput output, IFileManager fileManager, string file)
		{
			Stream s = fileManager.GetStream(file);

			if(s == null)
				return;

			using(StreamReader sr = new StreamReader(s))
			{
				output.Print(sr.ReadToEnd(), OutputType.Debug);
			}
		}
	}
}
